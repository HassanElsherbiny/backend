﻿Group: ExternalAuth methods
FriendlyName: Facebook authentication
SystemName: ExternalAuth.Facebook
Version: 1.42
SupportedVersions: 4.00
Author: zajelnode team
DisplayOrder: 5
FileName: Zajel.Plugin.ExternalAuth.Facebook.dll
Description: This plugin enables Facebook authentication (login using Facebook account)