﻿using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Plugin.ExternalAuth.Facebook.Models
{
    public class ConfigurationModel : BaseZajelModel
    {
        public string ActiveStoreScopeConfiguration { get; set; }

        [ZajelResourceDisplayName("Plugins.ExternalAuth.Facebook.ClientKeyIdentifier")]
        public string ClientId { get; set; }
        public bool ClientId_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Plugins.ExternalAuth.Facebook.ClientSecret")]
        public string ClientSecret { get; set; }
        public bool ClientSecret_OverrideForStore { get; set; }
    }
}