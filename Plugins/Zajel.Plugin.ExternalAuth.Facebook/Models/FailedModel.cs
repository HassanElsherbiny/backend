﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Plugin.ExternalAuth.Facebook.Models
{
    public class FailedModel : BaseZajelModel
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
    }
}
