﻿namespace Zajel.Framework.UI
{
    public enum NotifyType
    {
        Success,
        Error,
        Warning
    }
}
