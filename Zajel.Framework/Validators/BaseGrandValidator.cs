using System.Linq;
//using System.Linq.Dynamic;
using FluentValidation;
using Zajel.Core.Infrastructure;
using Zajel.Data;
using Zajel.Services.Localization;

namespace Zajel.Framework.Validators
{
    public abstract class BaseZajelValidator<T> : AbstractValidator<T> where T : class
    {
        protected BaseZajelValidator()
        {
            PostInitialize();
        }

        /// <summary>
        /// Developers can override this method in custom partial classes
        /// in order to add some custom initialization code to constructors
        /// </summary>
        protected virtual void PostInitialize()
        {

        }


    }
}