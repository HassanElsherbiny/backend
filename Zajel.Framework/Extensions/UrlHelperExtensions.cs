﻿using Microsoft.AspNetCore.Mvc;

namespace Zajel.Framework.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string LogOn(this IUrlHelper urlHelper, string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
                return urlHelper.Action("Login", "User", new { ReturnUrl = returnUrl });
            return urlHelper.Action("Login", "User");
        }

        public static string LogOff(this IUrlHelper urlHelper, string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
                return urlHelper.Action("Logout", "User", new { ReturnUrl = returnUrl });
            return urlHelper.Action("Logout", "User");
        }
    }
}