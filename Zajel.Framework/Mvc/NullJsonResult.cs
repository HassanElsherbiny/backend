﻿using Microsoft.AspNetCore.Mvc;

namespace Zajel.Framework.Mvc
{
    public class NullJsonResult : JsonResult
    {
        public NullJsonResult() : base(null)
        {
        }
    }
}
