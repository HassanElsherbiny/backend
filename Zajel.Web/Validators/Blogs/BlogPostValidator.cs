﻿using FluentValidation;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using Zajel.Web.Models.Blogs;

namespace Zajel.Web.Validators.Blogs
{
    public class BlogPostValidator : BaseZajelValidator<BlogPostModel>
    {
        public BlogPostValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.AddNewComment.CommentText).NotEmpty().WithMessage(localizationService.GetResource("Blog.Comments.CommentText.Required")).When(x => x.AddNewComment != null);
        }}
}