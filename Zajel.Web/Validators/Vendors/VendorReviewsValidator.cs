﻿using FluentValidation;
using Zajel.Framework.Validators;
using Zajel.Services.Localization;
using Zajel.Web.Models.Vendors;

namespace Zajel.Web.Validators.Vendors
{
    public class VendorReviewsValidator : BaseZajelValidator<VendorReviewsModel>
    {
        public VendorReviewsValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.AddVendorReview.Title).NotEmpty().WithMessage(localizationService.GetResource("Reviews.Fields.Title.Required")).When(x => x.AddVendorReview != null);
            RuleFor(x => x.AddVendorReview.Title).Length(1, 200).WithMessage(string.Format(localizationService.GetResource("Reviews.Fields.Title.MaxLengthValidation"), 200)).When(x => x.AddVendorReview != null && !string.IsNullOrEmpty(x.AddVendorReview.Title));
            RuleFor(x => x.AddVendorReview.ReviewText).NotEmpty().WithMessage(localizationService.GetResource("Reviews.Fields.ReviewText.Required")).When(x => x.AddVendorReview != null);
        }
    }
}
