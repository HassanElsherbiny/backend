﻿using FluentValidation;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using Zajel.Web.Models.Vendors;

namespace Zajel.Web.Validators.Vendors
{
    public class VendorInfoValidator : BaseZajelValidator<VendorInfoModel>
    {
        public VendorInfoValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Account.VendorInfo.Name.Required"));

            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.VendorInfo.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
        }
    }
}