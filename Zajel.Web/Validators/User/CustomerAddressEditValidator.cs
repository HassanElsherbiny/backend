﻿using Zajel.Core.Domain.Common;
using Zajel.Framework.Validators;
using Zajel.Services.Directory;
using Zajel.Services.Localization;
using Zajel.Web.Models.User;
using Zajel.Web.Validators.Common;

namespace Zajel.Web.Validators.User
{
    public class UserAddressEditValidator : BaseZajelValidator<UserAddressEditModel>
    {
        public UserAddressEditValidator(ILocalizationService localizationService,
            IStateProvinceService stateProvinceService,
            AddressSettings addressSettings)
        {
            RuleFor(x => x.Address).SetValidator(new AddressValidator(localizationService, stateProvinceService, addressSettings));
        }
    }
}
