﻿using FluentValidation;
using Zajel.Core.Domain.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using Zajel.Web.Models.User;

namespace Zajel.Web.Validators.User
{
    public class LoginValidator : BaseZajelValidator<LoginModel>
    {
        public LoginValidator(ILocalizationService localizationService, UserSettings userSettings)
        {
            if (!userSettings.UsernamesEnabled)
            {
                //login by email
                RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.Login.Fields.Email.Required"));
                RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
            }
        }
    }
}