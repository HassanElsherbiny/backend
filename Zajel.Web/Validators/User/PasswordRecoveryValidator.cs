﻿using FluentValidation;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using Zajel.Web.Models.User;

namespace Zajel.Web.Validators.User
{
    public class PasswordRecoveryValidator : BaseZajelValidator<PasswordRecoveryModel>
    {
        public PasswordRecoveryValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localizationService.GetResource("Account.PasswordRecovery.Email.Required"));
            RuleFor(x => x.Email).EmailAddress().WithMessage(localizationService.GetResource("Common.WrongEmail"));
        }}
}