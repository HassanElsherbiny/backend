﻿using FluentValidation;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using Zajel.Web.Models.Boards;

namespace Zajel.Web.Validators.Boards
{
    public class EditForumTopicValidator : BaseZajelValidator<EditForumTopicModel>
    {
        public EditForumTopicValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Subject).NotEmpty().WithMessage(localizationService.GetResource("Forum.TopicSubjectCannotBeEmpty"));
            RuleFor(x => x.Text).NotEmpty().WithMessage(localizationService.GetResource("Forum.TextCannotBeEmpty"));
        }
    }
}