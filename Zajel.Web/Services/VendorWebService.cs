﻿using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Localization;
using Zajel.Core.Domain.Vendors;
using Zajel.Core.Infrastructure;
using Zajel.Framework.Security.Captcha;
using Zajel.Services.Users;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Messages;
using Zajel.Services.Security;
using Zajel.Services.Seo;
using Zajel.Services.Vendors;
using Zajel.Web.Models.Vendors;
using System;

namespace Zajel.Web.Services
{
    public partial class VendorWebService : IVendorWebService
    {
        private readonly IPermissionService _permissionService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IVendorService _vendorService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly VendorSettings _vendorSettings;
        private readonly UserSettings _userSettings;
        private readonly CaptchaSettings _captchaSettings;
        private readonly LocalizationSettings _localizationSettings;

        public VendorWebService(
            IPermissionService permissionService,
            IWorkContext workContext,
            IStoreContext storeContext,
            IVendorService vendorService,
            ICacheManager cacheManager,
            IDateTimeHelper dateTimeHelper,
            IWorkflowMessageService workflowMessageService,
            VendorSettings vendorSettings,
            UserSettings userSettings,
            CaptchaSettings captchaSettings,
            LocalizationSettings localizationSettings)
        {
            this._permissionService = permissionService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._vendorService = vendorService;
            this._cacheManager = cacheManager;
            this._vendorService = vendorService;
            this._dateTimeHelper = dateTimeHelper;
            this._workflowMessageService = workflowMessageService;

            this._vendorSettings = vendorSettings;
            this._userSettings = userSettings;
            this._captchaSettings = captchaSettings;
            this._localizationSettings = localizationSettings;
        }

        public virtual VendorReviewOverviewModel PrepareVendorReviewOverviewModel(Vendor vendor)
        {
            VendorReviewOverviewModel VendorReview = new VendorReviewOverviewModel()
            {
                RatingSum = vendor.ApprovedRatingSum,
                TotalReviews = vendor.ApprovedTotalReviews
            };
            
            if (VendorReview != null)
            {
                VendorReview.VendorId = vendor.Id;
                VendorReview.AllowUserReviews = vendor.AllowUserReviews;
            }
            return VendorReview;
        }

        public virtual void PrepareVendorReviewsModel(VendorReviewsModel model, Vendor vendor)
        {
            if (vendor == null)
                throw new ArgumentNullException("vendor");

            if (model == null)
                throw new ArgumentNullException("model");

            model.VendorId = vendor.Id;
            model.VendorName = vendor.GetLocalized(x => x.Name);
            model.VendorSeName = vendor.GetSeName();

            var vendorReviews = _vendorService.GetAllVendorReviews("", true, null, null, "", vendor.Id);
            foreach (var pr in vendorReviews)
            {
                var user = EngineContext.Current.Resolve<IUserService>().GetUserById(pr.UserId);
                model.Items.Add(new VendorReviewModel
                {
                    Id = pr.Id,
                    UserId = pr.UserId,
                    UserName = user.FormatUserName(),
                    AllowViewingProfiles = _userSettings.AllowViewingProfiles && user != null && !user.IsGuest(),
                    Title = pr.Title,
                    ReviewText = pr.ReviewText,
                    Rating = pr.Rating,
                    Helpfulness = new VendorReviewHelpfulnessModel
                    {
                        VendorId = vendor.Id,
                        VendorReviewId = pr.Id,
                        HelpfulYesTotal = pr.HelpfulYesTotal,
                        HelpfulNoTotal = pr.HelpfulNoTotal,
                    },
                    WrittenOnStr = _dateTimeHelper.ConvertToUserTime(pr.CreatedOnUtc, DateTimeKind.Utc).ToString("g"),
                });
            }

            model.AddVendorReview.CanCurrentUserLeaveReview = _vendorSettings.AllowAnonymousUsersToReviewVendor || !_workContext.CurrentUser.IsGuest();
            model.AddVendorReview.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnVendorReviewPage;
        }

        public virtual VendorReview InsertVendorReview(Vendor vendor, VendorReviewsModel model)
        {
            //save review
            int rating = model.AddVendorReview.Rating;
            if (rating < 1 || rating > 5)
                rating = _vendorSettings.DefaultVendorRatingValue;
            bool isApproved = !_vendorSettings.VendorReviewsMustBeApproved;

            var vendorReview = new VendorReview
            {
                VendorId = vendor.Id,
                UserId = _workContext.CurrentUser.Id,
                Title = model.AddVendorReview.Title,
                ReviewText = model.AddVendorReview.ReviewText,
                Rating = rating,
                HelpfulYesTotal = 0,
                HelpfulNoTotal = 0,
                IsApproved = isApproved,
                CreatedOnUtc = DateTime.UtcNow,
            };
            _vendorService.InsertVendorReview(vendorReview);

            if (!_workContext.CurrentUser.IsHasVendorReview)
            {
                _workContext.CurrentUser.IsHasVendorReview = true;
                EngineContext.Current.Resolve<IUserService>().UpdateHasVendorReview(_workContext.CurrentUser.Id);
            }

            //update vendor totals
            _vendorService.UpdateVendorReviewTotals(vendor);

            //notify store owner
            if (_vendorSettings.NotifyVendorAboutNewVendorReviews)
                _workflowMessageService.SendVendorReviewNotificationMessage(vendorReview, _localizationSettings.DefaultAdminLanguageId);

            return vendorReview;
        }
    }
}