﻿using Zajel.Core.Domain.Blogs;
using Zajel.Web.Models.Blogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial interface IBlogWebService
    {
        void PrepareBlogPostModel(BlogPostModel model, BlogPost blogPost, bool prepareComments);
        BlogCommentModel PrepareBlogPostCommentModel(BlogComment blogComment);
        BlogPostListModel PrepareBlogPostListModel(BlogPagingFilteringModel command);
        BlogPostTagListModel PrepareBlogPostTagListModel();
        List<BlogPostYearModel> PrepareBlogPostYearModel();
        BlogComment InsertBlogComment(BlogPostModel model, BlogPost blogPost);
    }
}