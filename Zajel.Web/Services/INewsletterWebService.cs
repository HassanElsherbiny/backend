﻿using Zajel.Core.Domain.Messages;
using Zajel.Web.Models.Newsletter;


namespace Zajel.Web.Services
{
    public partial interface INewsletterWebService
    {
        NewsletterCategoryModel PrepareNewsletterCategory(string id);
        NewsletterBoxModel PrepareNewsletterBox();
        SubscribeNewsletterResultModel SubscribeNewsletter(string email, bool subscribe);
        SubscriptionActivationModel PrepareSubscriptionActivation(NewsLetterSubscription subscription, bool active);
    }
}