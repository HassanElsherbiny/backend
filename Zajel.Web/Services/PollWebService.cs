﻿using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Domain.Polls;
using Zajel.Core.Infrastructure;
using Zajel.Services.Users;
using Zajel.Services.Localization;
using Zajel.Services.Polls;
using Zajel.Services.Security;
using Zajel.Web.Infrastructure.Cache;
using Zajel.Web.Models.Polls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial class PollWebService: IPollWebService
    {
        private readonly IPollService _pollService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ICacheManager _cacheManager;
        private readonly IAclService _aclService;

        public PollWebService(IPollService pollService, IWorkContext workContext, IStoreContext storeContext, ICacheManager cacheManager, IAclService aclService)
        {
            this._pollService = pollService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._cacheManager = cacheManager;
            this._aclService = aclService;
        }

        public virtual PollModel PreparePoll(Poll poll, bool setAlreadyVotedProperty)
        {
            var model = new PollModel
            {
                Id = poll.Id,
                AlreadyVoted = setAlreadyVotedProperty && _pollService.AlreadyVoted(poll.Id, _workContext.CurrentUser.Id),
                Name = poll.GetLocalized(x => x.Name)
            };
            var answers = poll.PollAnswers.OrderBy(x => x.DisplayOrder);
            foreach (var answer in answers)
                model.TotalVotes += answer.NumberOfVotes;
            foreach (var pa in answers)
            {
                model.Answers.Add(new PollAnswerModel
                {
                    Id = pa.Id,
                    PollId = poll.Id,
                    Name = pa.GetLocalized(x => x.Name),
                    NumberOfVotes = pa.NumberOfVotes,
                    PercentOfTotalVotes = model.TotalVotes > 0 ? ((Convert.ToDouble(pa.NumberOfVotes) / Convert.ToDouble(model.TotalVotes)) * Convert.ToDouble(100)) : 0,
                });
            }

            return model;
        }
        public virtual PollModel PreparePollBySystemName(string systemKeyword)
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.POLL_BY_SYSTEMNAME__MODEL_KEY, systemKeyword, _storeContext.CurrentStore.Id);
            var cachedModel = _cacheManager.Get(cacheKey, () =>
            {
                Poll poll = _pollService.GetPollBySystemKeyword(systemKeyword, _storeContext.CurrentStore.Id);
                //ACL (access control list)
                if (!_aclService.Authorize(poll))
                    return new PollModel { Id = "" };

                if (poll == null ||
                    !poll.Published ||
                    (poll.StartDateUtc.HasValue && poll.StartDateUtc.Value > DateTime.UtcNow) ||
                    (poll.EndDateUtc.HasValue && poll.EndDateUtc.Value < DateTime.UtcNow))
                    //we do not cache nulls. that's why let's return an empty record (ID = 0)
                    return new PollModel { Id = "" };

                return PreparePoll(poll, false);
            });
            if (cachedModel == null || cachedModel.Id == "")
                return null;

            //"AlreadyVoted" property of "PollModel" object depends on the current user. Let's update it.
            //But first we need to clone the cached model (the updated one should not be cached)
            var model = (PollModel)cachedModel.Clone();
            model.AlreadyVoted = _pollService.AlreadyVoted(model.Id, _workContext.CurrentUser.Id);
            return model;
        }
        public virtual List<PollModel> PrepareHomePagePoll()
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.HOMEPAGE_POLLS_MODEL_KEY, _workContext.WorkingLanguage.Id);
            var cachedModel = _cacheManager.Get(cacheKey, () =>
                _pollService.GetPolls(_storeContext.CurrentStore.Id, true)
                .Select(x => PreparePoll(x, false))
                .ToList());
            //"AlreadyVoted" property of "PollModel" object depends on the current user. Let's update it.
            //But first we need to clone the cached model (the updated one should not be cached)
            var model = new List<PollModel>();
            foreach (var p in cachedModel)
            {
                var pollModel = (PollModel)p.Clone();
                pollModel.AlreadyVoted = _pollService.AlreadyVoted(pollModel.Id, _workContext.CurrentUser.Id);
                model.Add(pollModel);
            }

            return cachedModel;

        }

        public virtual void PollVoting(Poll poll, PollAnswer pollAnswer)
        {
            pollAnswer.PollVotingRecords.Add(new PollVotingRecord
            {
                PollId = poll.Id,
                PollAnswerId = pollAnswer.Id,
                UserId = _workContext.CurrentUser.Id,
                CreatedOnUtc = DateTime.UtcNow
            });
            //update totals
            pollAnswer.NumberOfVotes = pollAnswer.PollVotingRecords.Count;
            _pollService.UpdatePoll(poll);

            if (!_workContext.CurrentUser.IsHasPoolVoting)
            {
                _workContext.CurrentUser.IsHasPoolVoting = true;
                EngineContext.Current.Resolve<IUserService>().UpdateHasPoolVoting(_workContext.CurrentUser.Id);
            }
        }

        public virtual PollModel PreparePollModel(Poll poll, bool setAlreadyVotedProperty)
        {
            var model = new PollModel
            {
                Id = poll.Id,
                AlreadyVoted = setAlreadyVotedProperty && _pollService.AlreadyVoted(poll.Id, _workContext.CurrentUser.Id),
                Name = poll.GetLocalized(x => x.Name)
            };
            var answers = poll.PollAnswers.OrderBy(x => x.DisplayOrder);
            foreach (var answer in answers)
                model.TotalVotes += answer.NumberOfVotes;
            foreach (var pa in answers)
            {
                model.Answers.Add(new PollAnswerModel
                {
                    Id = pa.Id,
                    PollId = poll.Id,
                    Name = pa.GetLocalized(x => x.Name),
                    NumberOfVotes = pa.NumberOfVotes,
                    PercentOfTotalVotes = model.TotalVotes > 0 ? ((Convert.ToDouble(pa.NumberOfVotes) / Convert.ToDouble(model.TotalVotes)) * Convert.ToDouble(100)) : 0,
                });
            }
            return model;
        }

    }
}