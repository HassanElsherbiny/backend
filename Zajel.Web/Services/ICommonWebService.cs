﻿using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Vendors;
using Zajel.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;

namespace Zajel.Web.Services
{
    public partial interface ICommonWebService
    {
        LogoModel PrepareLogo();
        LanguageSelectorModel PrepareLanguageSelector();
        void SetLanguage(string langid);
        CurrencySelectorModel PrepareCurrencySelector();
        void SetCurrency(string userCurrency);
        TaxTypeSelectorModel PrepareTaxTypeSelector();
        void SetTaxType(int userTaxType);
        int GetUnreadPrivateMessages();
        HeaderLinksModel PrepareHeaderLinks(User user);
        AdminHeaderLinksModel PrepareAdminHeaderLinks(User user);
        FooterModel PrepareFooter();
        ContactUsModel PrepareContactUs();
        ContactUsModel SendContactUs(ContactUsModel model);
        ContactVendorModel PrepareContactVendor(Vendor vendor);
        ContactVendorModel SendContactVendor(ContactVendorModel model, Vendor vendor);
        SitemapModel PrepareSitemap();
        string SitemapXml(int? id, IUrlHelper url);
        StoreThemeSelectorModel PrepareStoreThemeSelector();
        FaviconModel PrepareFavicon();
        string PrepareRobotsTextFile();

    }
}