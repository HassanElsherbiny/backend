﻿using Zajel.Core.Domain.News;
using Zajel.Web.Models.News;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial interface INewsWebService
    {
        void PrepareNewsItemModel(NewsItemModel model, NewsItem newsItem, bool prepareComments);
        HomePageNewsItemsModel PrepareHomePageNewsItems();
        NewsItemListModel PrepareNewsItemList(NewsPagingFilteringModel command);

        void InsertNewsComment(NewsItem newsItem, NewsItemModel model);

    }
}