﻿using Zajel.Core.Domain.Common;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Orders;
using Zajel.Services.Payments;
using Zajel.Web.Models.Checkout;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial interface ICheckoutWebService
    {
        bool IsPaymentWorkflowRequired(IList<ShoppingCartItem> cart, bool? useRewardPoints = null);
        CheckoutBillingAddressModel PrepareBillingAddress(
            IList<ShoppingCartItem> cart, string selectedCountryId = null,
            bool prePopulateNewAddressWithUserFields = false, string overrideAttributesXml = "");
        CheckoutShippingAddressModel PrepareShippingAddress(string selectedCountryId = null,
            bool prePopulateNewAddressWithUserFields = false, string overrideAttributesXml = "");
        CheckoutShippingMethodModel PrepareShippingMethod(IList<ShoppingCartItem> cart, Address shippingAddress);
        CheckoutPaymentMethodModel PreparePaymentMethod(IList<ShoppingCartItem> cart, string filterByCountryId);
        CheckoutPaymentInfoModel PreparePaymentInfo(IPaymentMethod paymentMethod);
        CheckoutConfirmModel PrepareConfirmOrder(IList<ShoppingCartItem> cart);
        bool IsMinimumOrderPlacementIntervalValid(User user);
    }
}