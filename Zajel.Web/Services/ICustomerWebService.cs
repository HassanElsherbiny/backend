﻿using Zajel.Core.Domain.Users;
using Zajel.Web.Models.User;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Zajel.Web.Services
{
    public partial interface IUserWebService
    {
        void TryAssociateAccountWithExternalAccount(User user);

        IList<UserAttributeModel> PrepareCustomAttributes(User user,
            string overrideAttributesXml = "");
        UserInfoModel PrepareInfoModel(UserInfoModel model, User user,
            bool excludeProperties, string overrideCustomUserAttributesXml = "");
        RegisterModel PrepareRegisterModel(RegisterModel model, bool excludeProperties,
            string overrideCustomUserAttributesXml = "");
        string ParseCustomAttributes(IFormCollection form);

        LoginModel PrepareLogin(bool? checkoutAsGuest);
        PasswordRecoveryModel PreparePasswordRecovery();
        PasswordRecoveryConfirmModel PreparePasswordRecoveryConfirmModel(User user, string token);
        void PasswordRecoverySend(PasswordRecoveryModel model, User user);
        UserNavigationModel PrepareNavigation(int selectedTabId = 0);
        UserAddressListModel PrepareAddressList(User user);
        UserDownloadableProductsModel PrepareDownloadableProducts(string userId);
        UserAgreementModel PrepareUserAgreement(Guid orderItemId);
        UserAvatarModel PrepareAvatar(User user);

    }
}