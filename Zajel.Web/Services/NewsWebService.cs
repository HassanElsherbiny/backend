﻿using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Localization;
using Zajel.Core.Domain.Media;
using Zajel.Core.Domain.News;
using Zajel.Core.Infrastructure;
using Zajel.Services.Common;
using Zajel.Services.Users;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Media;
using Zajel.Services.Messages;
using Zajel.Services.News;
using Zajel.Services.Seo;
using Zajel.Framework.Security.Captcha;
using Zajel.Web.Infrastructure.Cache;
using Zajel.Web.Models.News;
using System;
using System.Linq;
using Zajel.Web.Models.Media;

namespace Zajel.Web.Services
{
    public partial class NewsWebService: INewsWebService
    {

        private readonly INewsService _newsService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ILocalizationService _localizationService;
        private readonly IWebHelper _webHelper;

        private readonly CaptchaSettings _captchaSettings;
        private readonly UserSettings _userSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly NewsSettings _newsSettings;
        private readonly LocalizationSettings _localizationSettings;

        public NewsWebService(INewsService newsService, IWorkContext workContext, IStoreContext storeContext,
            IPictureService pictureService, IDateTimeHelper dateTimeHelper, ICacheManager cacheManager,
            IWorkflowMessageService workflowMessageService, ILocalizationService localizationService, IWebHelper webHelper,
            CaptchaSettings captchaSettings, NewsSettings newsSettings,
            UserSettings userSettings, MediaSettings mediaSettings, LocalizationSettings localizationSettings)
        {
            this._newsService = newsService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._dateTimeHelper = dateTimeHelper;
            this._cacheManager = cacheManager;
            this._workflowMessageService = workflowMessageService;
            this._localizationService = localizationService;
            this._webHelper = webHelper;

            this._captchaSettings = captchaSettings;
            this._newsSettings = newsSettings;
            this._userSettings = userSettings;
            this._mediaSettings = mediaSettings;
            this._localizationSettings = localizationSettings;
        }

        public virtual void PrepareNewsItemModel(NewsItemModel model, NewsItem newsItem, bool prepareComments)
        {
            if (newsItem == null)
                throw new ArgumentNullException("newsItem");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = newsItem.Id;
            model.MetaTitle = newsItem.GetLocalized(x => x.MetaTitle);
            model.MetaDescription = newsItem.GetLocalized(x => x.MetaDescription);
            model.MetaKeywords = newsItem.GetLocalized(x => x.MetaKeywords);
            model.SeName = newsItem.GetSeName();
            model.Title = newsItem.GetLocalized(x => x.Title);
            model.Short = newsItem.GetLocalized(x => x.Short);
            model.Full = newsItem.GetLocalized(x => x.Full);
            model.AllowComments = newsItem.AllowComments;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(newsItem.StartDateUtc ?? newsItem.CreatedOnUtc, DateTimeKind.Utc);
            model.NumberOfComments = newsItem.CommentCount;
            model.AddNewComment.DisplayCaptcha = _captchaSettings.Enabled && _captchaSettings.ShowOnNewsCommentPage;
            if (prepareComments)
            {
                var newsComments = newsItem.NewsComments.OrderBy(pr => pr.CreatedOnUtc);
                foreach (var nc in newsComments)
                {
                    var user = EngineContext.Current.Resolve<IUserService>().GetUserById(nc.UserId);
                    var commentModel = new NewsCommentModel
                    {
                        Id = nc.Id,
                        UserId = nc.UserId,
                        UserName = user.FormatUserName(),
                        CommentTitle = nc.CommentTitle,
                        CommentText = nc.CommentText,
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(nc.CreatedOnUtc, DateTimeKind.Utc),
                        AllowViewingProfiles = _userSettings.AllowViewingProfiles && user != null && !user.IsGuest(),
                    };
                    if (_userSettings.AllowUsersToUploadAvatars)
                    {
                        commentModel.UserAvatarUrl = _pictureService.GetPictureUrl(
                            user.GetAttribute<string>(SystemUserAttributeNames.AvatarPictureId),
                            _mediaSettings.AvatarPictureSize,
                            _userSettings.DefaultAvatarEnabled,
                            defaultPictureType: PictureType.Avatar);
                    }
                    model.Comments.Add(commentModel);
                }
            }
            //prepare picture model
            if (!string.IsNullOrEmpty(newsItem.PictureId))
            {
                int pictureSize = prepareComments ? _mediaSettings.NewsThumbPictureSize : _mediaSettings.NewsListThumbPictureSize;
                var categoryPictureCacheKey = string.Format(ModelCacheEventConsumer.NEWS_PICTURE_MODEL_KEY, newsItem.Id, pictureSize, true, _workContext.WorkingLanguage.Id, _webHelper.IsCurrentConnectionSecured(), _storeContext.CurrentStore.Id);
                model.PictureModel = _cacheManager.Get(categoryPictureCacheKey, () =>
                {
                    var picture = _pictureService.GetPictureById(newsItem.PictureId);
                    var pictureModel = new PictureModel
                    {
                        FullSizeImageUrl = _pictureService.GetPictureUrl(picture),
                        ImageUrl = _pictureService.GetPictureUrl(picture, pictureSize),
                        Title = string.Format(_localizationService.GetResource("Media.News.ImageLinkTitleFormat"), newsItem.Title),
                        AlternateText = string.Format(_localizationService.GetResource("Media.News.ImageAlternateTextFormat"), newsItem.Title)
                    };
                    return pictureModel;
                });
            }

        }
        public virtual HomePageNewsItemsModel PrepareHomePageNewsItems()
        {
            var cacheKey = string.Format(ModelCacheEventConsumer.HOMEPAGE_NEWSMODEL_KEY, _workContext.WorkingLanguage.Id, _storeContext.CurrentStore.Id);
            var cachedModel = _cacheManager.Get(cacheKey, () =>
            {
                var newsItems = _newsService.GetAllNews(_storeContext.CurrentStore.Id, 0, _newsSettings.MainPageNewsCount);
                return new HomePageNewsItemsModel
                {
                    WorkingLanguageId = _workContext.WorkingLanguage.Id,
                    NewsItems = newsItems
                        .Select(x =>
                        {
                            var newsModel = new NewsItemModel();
                            PrepareNewsItemModel(newsModel, x, false);
                            return newsModel;
                        })
                        .ToList()
                };
            });

            //"Comments" property of "NewsItemModel" object depends on the current user.
            //Furthermore, we just don't need it for home page news. So let's reset it.
            //But first we need to clone the cached model (the updated one should not be cached)
            var model = (HomePageNewsItemsModel)cachedModel.Clone();
            foreach (var newsItemModel in model.NewsItems)
                newsItemModel.Comments.Clear();

            return model;
        }
        public virtual NewsItemListModel PrepareNewsItemList(NewsPagingFilteringModel command)
        {
            var model = new NewsItemListModel();
            model.WorkingLanguageId = _workContext.WorkingLanguage.Id;

            if (command.PageSize <= 0) command.PageSize = _newsSettings.NewsArchivePageSize;
            if (command.PageNumber <= 0) command.PageNumber = 1;

            var newsItems = _newsService.GetAllNews(_storeContext.CurrentStore.Id,
                command.PageNumber - 1, command.PageSize);
            model.PagingFilteringContext.LoadPagedList(newsItems);

            model.NewsItems = newsItems
                .Select(x =>
                {
                    var newsModel = new NewsItemModel();
                    PrepareNewsItemModel(newsModel, x, false);
                    return newsModel;
                })
                .ToList();

            return model;
        }
        public virtual void InsertNewsComment(NewsItem newsItem, NewsItemModel model)
        {
            var comment = new NewsComment
            {
                NewsItemId = newsItem.Id,
                UserId = _workContext.CurrentUser.Id,
                CommentTitle = model.AddNewComment.CommentTitle,
                CommentText = model.AddNewComment.CommentText,
                CreatedOnUtc = DateTime.UtcNow,
            };
            newsItem.NewsComments.Add(comment);
            //update totals
            newsItem.CommentCount = newsItem.NewsComments.Count;
            _newsService.UpdateNews(newsItem);
            EngineContext.Current.Resolve<IUserService>().UpdateNewsItem(_workContext.CurrentUser);

            //notify a store owner;
            if (_newsSettings.NotifyAboutNewNewsComments)
                _workflowMessageService.SendNewsCommentNotificationMessage(comment, _localizationSettings.DefaultAdminLanguageId);

        }

    }
}