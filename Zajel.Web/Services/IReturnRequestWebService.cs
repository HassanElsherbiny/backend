﻿using Zajel.Core.Domain.Orders;
using Zajel.Web.Models.Order;

namespace Zajel.Web.Services
{
    public partial interface IReturnRequestWebService
    {
        SubmitReturnRequestModel PrepareReturnRequest(SubmitReturnRequestModel model, Order order);
        UserReturnRequestsModel PrepareUserReturnRequests();
    }
}