﻿using Zajel.Core;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Media;
using Zajel.Core.Infrastructure;
using Zajel.Services.Common;
using Zajel.Services.Users;
using Zajel.Services.Directory;
using Zajel.Services.Forums;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Media;
using Zajel.Services.Seo;
using Zajel.Web.Models.Boards;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Framework;
using Zajel.Web.Models.Common;
using Zajel.Core.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Zajel.Web.Services
{
    public partial class BoardsWebService: IBoardsWebService
    {
        private readonly IForumService _forumService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly ICountryService _countryService;
        private readonly IWebHelper _webHelper;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly ForumSettings _forumSettings;
        private readonly UserSettings _userSettings;
        private readonly MediaSettings _mediaSettings;
        private readonly IDateTimeHelper _dateTimeHelper;

        public BoardsWebService(IForumService forumService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICountryService countryService,
            IWebHelper webHelper,
            IWorkContext workContext,
            IStoreContext storeContext,
            ForumSettings forumSettings,
            UserSettings userSettings,
            MediaSettings mediaSettings,
            IDateTimeHelper dateTimeHelper)
        {
            this._forumService = forumService;
            this._localizationService = localizationService;
            this._pictureService = pictureService;
            this._countryService = countryService;
            this._webHelper = webHelper;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._forumSettings = forumSettings;
            this._userSettings = userSettings;
            this._mediaSettings = mediaSettings;
            this._dateTimeHelper = dateTimeHelper;
        }

        public virtual BoardsIndexModel PrepareBoardsIndex()
        {
            var forumGroups = _forumService.GetAllForumGroups();

            var model = new BoardsIndexModel();
            foreach (var forumGroup in forumGroups)
            {

                var forumGroupModel = PrepareForumGroup(forumGroup);
                model.ForumGroups.Add(forumGroupModel);
            }
            return model;
        }

        public virtual ActiveDiscussionsModel PrepareActiveDiscussions()
        {
            var topics = _forumService.GetActiveTopics("", 0, _forumSettings.HomePageActiveDiscussionsTopicCount);
            if (!topics.Any())
                return null;
            var model = new ActiveDiscussionsModel();
            foreach (var topic in topics)
            {
                var topicModel = PrepareForumTopicRow(topic);
                model.ForumTopics.Add(topicModel);
            }
            model.ViewAllLinkEnabled = true;
            model.ActiveDiscussionsFeedEnabled = _forumSettings.ActiveDiscussionsFeedEnabled;
            model.PostsPageSize = _forumSettings.PostsPageSize;
            model.AllowPostVoting = _forumSettings.AllowPostVoting;

            return model;
        }
        public virtual ActiveDiscussionsModel PrepareActiveDiscussions(string forumId = "", int pageNumber = 1)
        {
            var model = new ActiveDiscussionsModel();

            int pageSize = _forumSettings.ActiveDiscussionsPageSize > 0 ? _forumSettings.ActiveDiscussionsPageSize : 50;

            var topics = _forumService.GetActiveTopics(forumId, (pageNumber - 1), pageSize);
            model.TopicPageSize = topics.PageSize;
            model.TopicTotalRecords = topics.TotalCount;
            model.TopicPageIndex = topics.PageIndex;
            foreach (var topic in topics)
            {
                var topicModel = PrepareForumTopicRow(topic);
                model.ForumTopics.Add(topicModel);
            }
            model.ViewAllLinkEnabled = false;
            model.ActiveDiscussionsFeedEnabled = _forumSettings.ActiveDiscussionsFeedEnabled;
            model.PostsPageSize = _forumSettings.PostsPageSize;
            model.AllowPostVoting = _forumSettings.AllowPostVoting;
            return model;
        }

        public virtual ForumPageModel PrepareForumPage(Forum forum, int pageNumber)
        {
            var model = new ForumPageModel();
            model.Id = forum.Id;
            model.Name = forum.Name;
            model.SeName = forum.GetSeName();
            model.Description = forum.Description;

            int pageSize = _forumSettings.TopicsPageSize > 0 ? _forumSettings.TopicsPageSize : 10;

            //subscription                
            if (_forumService.IsUserAllowedToSubscribe(_workContext.CurrentUser))
            {
                model.WatchForumText = _localizationService.GetResource("Forum.WatchForum");

                var forumSubscription = _forumService.GetAllSubscriptions(_workContext.CurrentUser.Id, forum.Id, "", 0, 1).FirstOrDefault();
                if (forumSubscription != null)
                {
                    model.WatchForumText = _localizationService.GetResource("Forum.UnwatchForum");
                }
            }

            var topics = _forumService.GetAllTopics(forum.Id, "", string.Empty,
                ForumSearchType.All, 0, (pageNumber - 1), pageSize);
            model.TopicPageSize = topics.PageSize;
            model.TopicTotalRecords = topics.TotalCount;
            model.TopicPageIndex = topics.PageIndex;
            foreach (var topic in topics)
            {
                var topicModel = PrepareForumTopicRow(topic);
                model.ForumTopics.Add(topicModel);
            }
            model.IsUserAllowedToSubscribe = _forumService.IsUserAllowedToSubscribe(_workContext.CurrentUser);
            model.ForumFeedsEnabled = _forumSettings.ForumFeedsEnabled;
            model.PostsPageSize = _forumSettings.PostsPageSize;
            model.AllowPostVoting = _forumSettings.AllowPostVoting;
            return model;
        }

        public virtual ForumTopicRowModel PrepareForumTopicRow(ForumTopic topic)
        {
            var user = EngineContext.Current.Resolve<IUserService>().GetUserById(topic.UserId);
            var topicModel = new ForumTopicRowModel
            {
                Id = topic.Id,
                Subject = topic.Subject,
                SeName = topic.GetSeName(),
                LastPostId = topic.LastPostId,
                NumPosts = topic.NumPosts,
                Views = topic.Views,
                NumReplies = topic.NumReplies,
                ForumTopicType = topic.ForumTopicType,
                UserId = topic.UserId,
                AllowViewingProfiles = _userSettings.AllowViewingProfiles,
                UserName = user.FormatUserName(),
                IsUserGuest = user.IsGuest()
            };

            var forumPosts = _forumService.GetAllPosts(topic.Id, "", string.Empty, 1, _forumSettings.PostsPageSize);
            topicModel.TotalPostPages = forumPosts.TotalPages;

            var firstPost = topic.GetFirstPost(_forumService);
            topicModel.Votes = firstPost != null ? firstPost.VoteCount : 0;

            return topicModel;

        }
        public virtual ForumRowModel PrepareForumRow(Forum forum)
        {
            var forumModel = new ForumRowModel
            {
                Id = forum.Id,
                Name = forum.Name,
                SeName = forum.GetSeName(),
                Description = forum.Description,
                NumTopics = forum.NumTopics,
                NumPosts = forum.NumPosts,
                LastPostId = forum.LastPostId,
            };
            return forumModel;
        }
        public virtual ForumGroupModel PrepareForumGroup(ForumGroup forumGroup)
        {
            var forumGroupModel = new ForumGroupModel
            {
                Id = forumGroup.Id,
                Name = forumGroup.Name,
                SeName = forumGroup.GetSeName(),
            };
            var forums = _forumService.GetAllForumsByGroupId(forumGroup.Id);
            foreach (var forum in forums)
            {
                var forumModel = PrepareForumRow(forum);
                forumGroupModel.Forums.Add(forumModel);
            }
            return forumGroupModel;
        }
        public virtual IEnumerable<SelectListItem> ForumTopicTypesList()
        {
            var list = new List<SelectListItem>();

            list.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Forum.Normal"),
                Value = ((int)ForumTopicType.Normal).ToString()
            });

            list.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Forum.Sticky"),
                Value = ((int)ForumTopicType.Sticky).ToString()
            });

            list.Add(new SelectListItem
            {
                Text = _localizationService.GetResource("Forum.Announcement"),
                Value = ((int)ForumTopicType.Announcement).ToString()
            });

            return list;

        }
        public virtual IEnumerable<SelectListItem> ForumGroupsForumsList()
        {
            var forumsList = new List<SelectListItem>();
            var separator = "--";
            var forumGroups = _forumService.GetAllForumGroups();

            foreach (var fg in forumGroups)
            {
                // Add the forum group with Value of 0 so it won't be used as a target forum
                forumsList.Add(new SelectListItem { Text = fg.Name, Value = "" });

                var forums = _forumService.GetAllForumsByGroupId(fg.Id);
                foreach (var f in forums)
                {
                    forumsList.Add(new SelectListItem { Text = string.Format("{0}{1}", separator, f.Name), Value = f.Id.ToString() });
                }
            }
            return forumsList;
        }
        public virtual ForumTopicPageModel PrepareForumTopicPage(ForumTopic forumTopic, int pageNumber)
        {
            var posts = _forumService.GetAllPosts(forumTopic.Id, "", string.Empty,
                pageNumber - 1, _forumSettings.PostsPageSize);

            //prepare model
            var model = new ForumTopicPageModel();
            model.Id = forumTopic.Id;
            model.Subject = forumTopic.Subject;
            model.SeName = forumTopic.GetSeName();
            var currentuser = _workContext.CurrentUser;
            model.IsUserAllowedToEditTopic = _forumService.IsUserAllowedToEditTopic(currentuser, forumTopic);
            model.IsUserAllowedToDeleteTopic = _forumService.IsUserAllowedToDeleteTopic(currentuser, forumTopic);
            model.IsUserAllowedToMoveTopic = _forumService.IsUserAllowedToMoveTopic(currentuser, forumTopic);
            model.IsUserAllowedToSubscribe = _forumService.IsUserAllowedToSubscribe(currentuser);

            if (model.IsUserAllowedToSubscribe)
            {
                model.WatchTopicText = _localizationService.GetResource("Forum.WatchTopic");

                var forumTopicSubscription = _forumService.GetAllSubscriptions(currentuser.Id,
                    "", forumTopic.Id, 0, 1).FirstOrDefault();
                if (forumTopicSubscription != null)
                {
                    model.WatchTopicText = _localizationService.GetResource("Forum.UnwatchTopic");
                }
            }
            model.PostsPageIndex = posts.PageIndex;
            model.PostsPageSize = posts.PageSize;
            model.PostsTotalRecords = posts.TotalCount;
            foreach (var post in posts)
            {
                var user = EngineContext.Current.Resolve<IUserService>().GetUserById(post.UserId);
                var forumPostModel = new ForumPostModel
                {
                    Id = post.Id,
                    ForumTopicId = post.TopicId,
                    ForumTopicSeName = forumTopic.GetSeName(),
                    FormattedText = post.FormatPostText(),
                    IsCurrentUserAllowedToEditPost = _forumService.IsUserAllowedToEditPost(currentuser, post),
                    IsCurrentUserAllowedToDeletePost = _forumService.IsUserAllowedToDeletePost(currentuser, post),
                    UserId = post.UserId,
                    AllowViewingProfiles = _userSettings.AllowViewingProfiles,
                    UserName = user.FormatUserName(),
                    IsUserForumModerator = user.IsForumModerator(),
                    IsUserGuest = user.IsGuest(),
                    ShowUsersPostCount = _forumSettings.ShowUsersPostCount,
                    ForumPostCount = user.GetAttribute<int>(SystemUserAttributeNames.ForumPostCount),
                    ShowUsersJoinDate = _userSettings.ShowUsersJoinDate,
                    UserJoinDate = user.CreatedOnUtc,
                    AllowPrivateMessages = _forumSettings.AllowPrivateMessages,
                    SignaturesEnabled = _forumSettings.SignaturesEnabled,
                    FormattedSignature = user.GetAttribute<string>(SystemUserAttributeNames.Signature).FormatForumSignatureText(),
                };
                //created on string
                if (_forumSettings.RelativeDateTimeFormattingEnabled)
                    forumPostModel.PostCreatedOnStr = post.CreatedOnUtc.ToString("f");
                else
                    forumPostModel.PostCreatedOnStr = _dateTimeHelper.ConvertToUserTime(post.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
                //avatar
                if (_userSettings.AllowUsersToUploadAvatars)
                {
                    forumPostModel.UserAvatarUrl = _pictureService.GetPictureUrl(
                        user.GetAttribute<string>(SystemUserAttributeNames.AvatarPictureId),
                        _mediaSettings.AvatarPictureSize,
                        _userSettings.DefaultAvatarEnabled,
                        defaultPictureType: PictureType.Avatar);
                }
                //location
                forumPostModel.ShowUsersLocation = _userSettings.ShowUsersLocation;
                if (_userSettings.ShowUsersLocation)
                {
                    var countryId = user.GetAttribute<string>(SystemUserAttributeNames.CountryId);
                    var country = _countryService.GetCountryById(countryId);
                    forumPostModel.UserLocation = country != null ? country.GetLocalized(x => x.Name) : string.Empty;
                }

                if (_forumSettings.AllowPostVoting)
                {
                    forumPostModel.AllowPostVoting = true;
                    forumPostModel.VoteCount = post.VoteCount;
                    var postVote = _forumService.GetPostVote(post.Id, _workContext.CurrentUser.Id);
                    if (postVote != null)
                        forumPostModel.VoteIsUp = postVote.IsUp;
                }
                // page number is needed for creating post link in _ForumPost partial view
                forumPostModel.CurrentTopicPage = pageNumber;
                model.ForumPostModels.Add(forumPostModel);
            }

            return model;
        }
        public virtual TopicMoveModel PrepareTopicMove(ForumTopic forumTopic)
        {
            var model = new TopicMoveModel();
            model.ForumList = ForumGroupsForumsList();
            model.Id = forumTopic.Id;
            model.TopicSeName = forumTopic.GetSeName();
            model.ForumSelected = forumTopic.ForumId;
            return model;
        }

        public virtual EditForumTopicModel PrepareEditForumTopic(Forum forum)
        {
            var model = new EditForumTopicModel();
            model.Id = "";
            model.IsEdit = false;
            model.ForumId = forum.Id;
            model.ForumName = forum.Name;
            model.ForumSeName = forum.GetSeName();
            model.ForumEditor = _forumSettings.ForumEditor;
            model.IsUserAllowedToSetTopicPriority = _forumService.IsUserAllowedToSetTopicPriority(_workContext.CurrentUser);
            model.TopicPriorities = ForumTopicTypesList();
            model.IsUserAllowedToSubscribe = _forumService.IsUserAllowedToSubscribe(_workContext.CurrentUser);
            model.Subscribed = false;
            return model;
        }
        public virtual EditForumPostModel PrepareEditForumPost(Forum forum, ForumTopic forumTopic, string quote)
        {
            var model = new EditForumPostModel
            {
                Id = "",
                ForumTopicId = forumTopic.Id,
                IsEdit = false,
                ForumEditor = _forumSettings.ForumEditor,
                ForumName = forum.Name,
                ForumTopicSubject = forumTopic.Subject,
                ForumTopicSeName = forumTopic.GetSeName(),
                IsUserAllowedToSubscribe = _forumService.IsUserAllowedToSubscribe(_workContext.CurrentUser),
                Subscribed = false,
            };

            //subscription            
            if (model.IsUserAllowedToSubscribe)
            {
                var forumSubscription = _forumService.GetAllSubscriptions(_workContext.CurrentUser.Id,
                    "", forumTopic.Id, 0, 1).FirstOrDefault();
                model.Subscribed = forumSubscription != null;
            }

            // Insert the quoted text
            string text = string.Empty;
            if (!String.IsNullOrEmpty(quote))
            {
                var quotePost = _forumService.GetPostById(quote);
                if (quotePost != null && quotePost.TopicId == forumTopic.Id)
                {
                    var quotePostText = quotePost.Text;
                    var user = EngineContext.Current.Resolve<IUserService>().GetUserById(quotePost.UserId);
                    switch (_forumSettings.ForumEditor)
                    {
                        case EditorType.SimpleTextBox:
                            text = String.Format("{0}:\n{1}\n", user.FormatUserName(), quotePostText);
                            break;
                        case EditorType.BBCodeEditor:
                            text = String.Format("[quote={0}]{1}[/quote]", user.FormatUserName(), BBCodeHelper.RemoveQuotes(quotePostText));
                            break;
                    }
                    model.Text = text;
                }
            }
            return model;
        }

        public virtual LastPostModel PrepareLastPost(ForumPost post, bool showTopic)
        {
            var model = new LastPostModel();
            if (post != null)
            {
                var forumTopic = _forumService.GetTopicById(post.TopicId);
                if (forumTopic != null)
                {
                    var user = EngineContext.Current.Resolve<IUserService>().GetUserById(post.UserId);
                    model.Id = post.Id;
                    model.ForumTopicId = post.TopicId;
                    model.ForumTopicSeName = forumTopic.GetSeName();
                    model.ForumTopicSubject = forumTopic.StripTopicSubject();
                    model.UserId = post.UserId;
                    model.AllowViewingProfiles = _userSettings.AllowViewingProfiles;
                    model.UserName = user.FormatUserName();
                    model.IsUserGuest = user.IsGuest();
                    //created on string
                    if (_forumSettings.RelativeDateTimeFormattingEnabled)
                        model.PostCreatedOnStr = post.CreatedOnUtc.ToString("f");
                    else
                        model.PostCreatedOnStr = _dateTimeHelper.ConvertToUserTime(post.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
                }
            }
            model.ShowTopic = showTopic;
            return model;
        }

        public virtual ForumBreadcrumbModel PrepareForumBreadcrumb(string forumGroupId, string forumId, string forumTopicId)
        {
            var model = new ForumBreadcrumbModel();

            ForumTopic forumTopic = null;
            if (!String.IsNullOrEmpty(forumTopicId))
            {
                forumTopic = _forumService.GetTopicById(forumTopicId);
                if (forumTopic != null)
                {
                    model.ForumTopicId = forumTopic.Id;
                    model.ForumTopicSubject = forumTopic.Subject;
                    model.ForumTopicSeName = forumTopic.GetSeName();
                }
            }

            Forum forum = _forumService.GetForumById(forumTopic != null ? forumTopic.ForumId : (!String.IsNullOrEmpty(forumId) ? forumId : ""));
            if (forum != null)
            {
                model.ForumId = forum.Id;
                model.ForumName = forum.Name;
                model.ForumSeName = forum.GetSeName();
            }

            var forumGroup = _forumService.GetForumGroupById(forum != null ? forum.ForumGroupId : (!String.IsNullOrEmpty(forumGroupId) ? forumGroupId : ""));
            if (forumGroup != null)
            {
                model.ForumGroupId = forumGroup.Id;
                model.ForumGroupName = forumGroup.Name;
                model.ForumGroupSeName = forumGroup.GetSeName();
            }

            return model;
        }

        public virtual UserForumSubscriptionsModel PrepareUserForumSubscriptions(int pageIndex)
        {
            var pageSize = _forumSettings.ForumSubscriptionsPageSize;
            var list = _forumService.GetAllSubscriptions(_workContext.CurrentUser.Id, "", "", pageIndex, pageSize);
            var model = new UserForumSubscriptionsModel();
            foreach (var forumSubscription in list)
            {
                var forumTopicId = forumSubscription.TopicId;
                var forumId = forumSubscription.ForumId;
                bool topicSubscription = false;
                var title = string.Empty;
                var slug = string.Empty;

                if (!String.IsNullOrEmpty(forumTopicId))
                {
                    topicSubscription = true;
                    var forumTopic = _forumService.GetTopicById(forumTopicId);
                    if (forumTopic != null)
                    {
                        title = forumTopic.Subject;
                        slug = forumTopic.GetSeName();
                    }
                }
                else
                {
                    var forum = _forumService.GetForumById(forumId);
                    if (forum != null)
                    {
                        title = forum.Name;
                        slug = forum.GetSeName();
                    }
                }

                model.ForumSubscriptions.Add(new UserForumSubscriptionsModel.ForumSubscriptionModel
                {
                    Id = forumSubscription.Id,
                    ForumTopicId = forumTopicId,
                    ForumId = forumSubscription.ForumId,
                    TopicSubscription = topicSubscription,
                    Title = title,
                    Slug = slug,
                });
            }

            model.PagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "UserForumSubscriptionsPaged",
                UseRouteLinks = true,
                RouteValues = new ForumSubscriptionsRouteValues { pageNumber = pageIndex }
            };

            return model;
        }

        public virtual SearchModel PrepareSearch(string searchterms, bool? adv, string forumId,
            string within, string limitDays, int pageNumber = 1)
        {
            int pageSize = 10;

            var model = new SearchModel();

            // Create the values for the "Limit results to previous" select list
            var limitList = new List<SelectListItem>
                            {
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.AllResults"),
                                    Value = "0"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.1day"),
                                    Value = "1"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.7days"),
                                    Value = "7"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.2weeks"),
                                    Value = "14"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.1month"),
                                    Value = "30"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.3months"),
                                    Value = "92"
                                },
                                new SelectListItem
                                {
                                    Text= _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.6months"),
                                    Value = "183"
                                },
                                new SelectListItem
                                {
                                    Text = _localizationService.GetResource("Forum.Search.LimitResultsToPrevious.1year"),
                                    Value = "365"
                                }
                            };
            model.LimitList = limitList;

            // Create the values for the "Search in forum" select list
            var forumsSelectList = new List<SelectListItem>();
            forumsSelectList.Add(
                new SelectListItem
                {
                    Text = _localizationService.GetResource("Forum.Search.SearchInForum.All"),
                    Value = "",
                    Selected = true,
                });
            var separator = "--";
            var forumGroups = _forumService.GetAllForumGroups();
            foreach (var fg in forumGroups)
            {
                // Add the forum group with value as '-' so it can't be used as a target forum id
                forumsSelectList.Add(new SelectListItem { Text = fg.Name, Value = "-" });

                var forums = _forumService.GetAllForumsByGroupId(fg.Id);
                foreach (var f in forums)
                {
                    forumsSelectList.Add(
                        new SelectListItem
                        {
                            Text = string.Format("{0}{1}", separator, f.Name),
                            Value = f.Id.ToString()
                        });
                }
            }
            model.ForumList = forumsSelectList;

            // Create the values for "Search within" select list            
            var withinList = new List<SelectListItem>
                                {
                                    new SelectListItem
                                    {
                                        Value = ((int)ForumSearchType.All).ToString(),
                                        Text = _localizationService.GetResource("Forum.Search.SearchWithin.All")
                                    },
                                    new SelectListItem
                                    {
                                        Value = ((int)ForumSearchType.TopicTitlesOnly).ToString(),
                                        Text = _localizationService.GetResource("Forum.Search.SearchWithin.TopicTitlesOnly")
                                    },
                                    new SelectListItem
                                    {
                                        Value = ((int)ForumSearchType.PostTextOnly).ToString(),
                                        Text = _localizationService.GetResource("Forum.Search.SearchWithin.PostTextOnly")
                                    }
                                };
            model.WithinList = withinList;

            string forumIdSelected = forumId;
            model.ForumIdSelected = forumIdSelected;

            int withinSelected;
            int.TryParse(within, out withinSelected);
            model.WithinSelected = withinSelected;

            int limitDaysSelected;
            int.TryParse(limitDays, out limitDaysSelected);
            model.LimitDaysSelected = limitDaysSelected;

            int searchTermMinimumLength = _forumSettings.ForumSearchTermMinimumLength;

            model.ShowAdvancedSearch = adv.GetValueOrDefault();
            model.SearchResultsVisible = false;
            model.NoResultsVisisble = false;
            model.PostsPageSize = _forumSettings.PostsPageSize;
            model.AllowPostVoting = _forumSettings.AllowPostVoting;

            try
            {
                if (!String.IsNullOrWhiteSpace(searchterms))
                {
                    searchterms = searchterms.Trim();
                    model.SearchTerms = searchterms;

                    if (searchterms.Length < searchTermMinimumLength)
                    {
                        throw new ZajelException(string.Format(_localizationService.GetResource("Forum.SearchTermMinimumLengthIsNCharacters"),
                            searchTermMinimumLength));
                    }

                    ForumSearchType searchWithin = 0;
                    int limitResultsToPrevious = 0;
                    if (adv.GetValueOrDefault())
                    {
                        searchWithin = (ForumSearchType)withinSelected;
                        limitResultsToPrevious = limitDaysSelected;
                    }

                    if (_forumSettings.SearchResultsPageSize > 0)
                    {
                        pageSize = _forumSettings.SearchResultsPageSize;
                    }

                    var topics = _forumService.GetAllTopics(forumIdSelected, "", searchterms, searchWithin,
                        limitResultsToPrevious, pageNumber - 1, pageSize);
                    model.TopicPageSize = topics.PageSize;
                    model.TopicTotalRecords = topics.TotalCount;
                    model.TopicPageIndex = topics.PageIndex;
                    foreach (var topic in topics)
                    {
                        var topicModel = PrepareForumTopicRow(topic);
                        model.ForumTopics.Add(topicModel);
                    }

                    model.SearchResultsVisible = (topics.Any());
                    model.NoResultsVisisble = !(model.SearchResultsVisible);

                    return model;
                }
                model.SearchResultsVisible = false;
            }
            catch (Exception ex)
            {
                model.Error = ex.Message;
            }

            //some exception raised
            model.TopicPageSize = pageSize;
            model.TopicTotalRecords = 0;
            model.TopicPageIndex = pageNumber - 1;

            return model;
        }
    }
}