﻿using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Shipping;
using Zajel.Web.Models.Order;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Zajel.Web.Services
{
    public partial interface IOrderWebService
    {
        UserOrderListModel PrepareUserOrderList();
        OrderDetailsModel PrepareOrderDetails(Order order);
        ShipmentDetailsModel PrepareShipmentDetails(Shipment shipment);

        UserRewardPointsModel PrepareUserRewardPoints(User user);

    }
}