﻿using Zajel.Core.Domain.Vendors;
using Zajel.Web.Models.Vendors;

namespace Zajel.Web.Services
{
    public partial interface IVendorWebService
    {
        VendorReviewOverviewModel PrepareVendorReviewOverviewModel(Vendor vendor);

        void PrepareVendorReviewsModel(VendorReviewsModel model, Vendor vendor);

        VendorReview InsertVendorReview(Vendor vendor, VendorReviewsModel model);
    }
}