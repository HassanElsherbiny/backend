﻿using Zajel.Web.Models.User;
using System.Collections.Generic;

namespace Zajel.Web.Services
{
    public partial interface IExternalAuthenticationWebService
    {
        List<ExternalAuthenticationMethodModel> PrepereExternalAuthenticationMethodModel();
    }
}