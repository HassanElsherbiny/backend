﻿using Zajel.Core.Domain.Common;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Directory;
using Zajel.Services.Common;
using Zajel.Web.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Zajel.Web.Services
{
    public partial interface IAddressWebService
    {

        void PrepareModel(AddressModel model,
            Address address, bool excludeProperties,
            Func<IList<Country>> loadCountries = null,
            bool prePopulateWithUserFields = false,
            User user = null,
            string overrideAttributesXml = "");

        void PrepareCustomAddressAttributes(AddressModel model,
            Address address, string overrideAttributesXml = "");

        string ParseCustomAddressAttributes(IFormCollection form);

        AddressSettings AddressSettings();
        IList<string> GetAttributeWarnings(string attributesXml);
    }
}