﻿using Zajel.Core;
using Zajel.Services.Authentication.External;
using Zajel.Web.Models.User;
using Microsoft.AspNetCore.Routing;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial class ExternalAuthenticationWebService: IExternalAuthenticationWebService
    {
        private readonly IExternalAuthenticationService _externalAuthenticationService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;

        public ExternalAuthenticationWebService(IExternalAuthenticationService externalAuthenticationService,
            IStoreContext storeContext, IWorkContext workContext)
        {
            this._externalAuthenticationService = externalAuthenticationService;
            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public virtual List<ExternalAuthenticationMethodModel> PrepereExternalAuthenticationMethodModel()
        {
            var models = _externalAuthenticationService
                .LoadActiveExternalAuthenticationMethods(_workContext.CurrentUser, _storeContext.CurrentStore.Id)
                .Select(authenticationMethod =>
                {
                    authenticationMethod.GetPublicViewComponent(out string viewComponentName);

                    return new ExternalAuthenticationMethodModel
                    {
                        ViewComponentName = viewComponentName
                    };
                }).ToList();

            return models;
        }
    }
}