﻿using Zajel.Core.Domain.Polls;
using Zajel.Web.Models.Polls;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Services
{
    public partial interface IPollWebService
    {
        PollModel PreparePoll(Poll poll, bool setAlreadyVotedProperty);
        PollModel PreparePollBySystemName(string systemKeyword);
        List<PollModel> PrepareHomePagePoll();

        void PollVoting(Poll poll, PollAnswer pollAnswer);
        PollModel PreparePollModel(Poll poll, bool setAlreadyVotedProperty);
    }
}