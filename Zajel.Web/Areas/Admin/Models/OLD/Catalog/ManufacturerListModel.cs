﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.Collections.Generic;

namespace Zajel.Web.Areas.Admin.Models.Catalog
{
    public partial class ManufacturerListModel : BaseZajelModel
    {
        public ManufacturerListModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchManufacturerName")]
        
        public string SearchManufacturerName { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchStore")]
        public string SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}