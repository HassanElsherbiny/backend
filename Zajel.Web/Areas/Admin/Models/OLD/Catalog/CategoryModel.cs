﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Web.Areas.Admin.Models.Discounts;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Web.Areas.Admin.Validators.Catalog;
using Zajel.Framework.Localization;
using System;
using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
namespace Zajel.Web.Areas.Admin.Models.Catalog
{
    [Validator(typeof(CategoryValidator))]
    public partial class CategoryModel : BaseZajelEntityModel, ILocalizedModel<CategoryLocalizedModel>
    {
        public CategoryModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
            Locales = new List<CategoryLocalizedModel>();
            AvailableCategoryTemplates = new List<SelectListItem>();
            AvailableCategories = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Description")]
        
        public string Description { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.CategoryTemplate")]
        public string CategoryTemplateId { get; set; }
        public IList<SelectListItem> AvailableCategoryTemplates { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.SeName")]
        
        public string SeName { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Parent")]
        public string ParentCategoryId { get; set; }

        [UIHint("Picture")]
        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Picture")]
        public string PictureId { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.PageSize")]
        public int PageSize { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.AllowUsersToSelectPageSize")]
        public bool AllowUsersToSelectPageSize { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.PriceRanges")]
        
        public string PriceRanges { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.ShowOnHomePage")]
        public bool ShowOnHomePage { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.IncludeInTopMenu")]
        public bool IncludeInTopMenu { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Published")]
        public bool Published { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Deleted")]
        public bool Deleted { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        
        public IList<CategoryLocalizedModel> Locales { get; set; }

        public string Breadcrumb { get; set; }

        //ACL
        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.SubjectToAcl")]
        public bool SubjectToAcl { get; set; }
        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.AclUserRoles")]
        public List<UserRoleModel> AvailableUserRoles { get; set; }
        public string[] SelectedUserRoleIds { get; set; }

        //Store mapping
        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.AvailableStores")]
        public List<StoreModel> AvailableStores { get; set; }
        public string[] SelectedStoreIds { get; set; }


        public IList<SelectListItem> AvailableCategories { get; set; }


        //discounts
        public List<DiscountModel> AvailableDiscounts { get; set; }
        public string[] SelectedDiscountIds { get; set; }


        #region Nested classes

        public partial class CategoryProductModel : BaseZajelEntityModel
        {
            public string CategoryId { get; set; }

            public string ProductId { get; set; }

            [ZajelResourceDisplayName("Admin.Catalog.Categories.Products.Fields.Product")]
            public string ProductName { get; set; }

            [ZajelResourceDisplayName("Admin.Catalog.Categories.Products.Fields.IsFeaturedProduct")]
            public bool IsFeaturedProduct { get; set; }

            [ZajelResourceDisplayName("Admin.Catalog.Categories.Products.Fields.DisplayOrder")]
            public int DisplayOrder { get; set; }
        }

        public partial class AddCategoryProductModel : BaseZajelModel
        {
            public AddCategoryProductModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableManufacturers = new List<SelectListItem>();
                AvailableStores = new List<SelectListItem>();
                AvailableVendors = new List<SelectListItem>();
                AvailableProductTypes = new List<SelectListItem>();
            }

            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
            
            public string SearchProductName { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
            public string SearchCategoryId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
            public string SearchManufacturerId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
            public string SearchStoreId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchVendor")]
            public string SearchVendorId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
            public int SearchProductTypeId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableManufacturers { get; set; }
            public IList<SelectListItem> AvailableStores { get; set; }
            public IList<SelectListItem> AvailableVendors { get; set; }
            public IList<SelectListItem> AvailableProductTypes { get; set; }

            public string CategoryId { get; set; }

            public string[] SelectedProductIds { get; set; }
        }

        public partial class ActivityLogModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Catalog.Categories.ActivityLogType")]
            public string ActivityLogTypeName { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Categories.ActivityLog.Comment")]
            public string Comment { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Categories.ActivityLog.CreatedOn")]
            public DateTime CreatedOn { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Categories.ActivityLog.User")]
            public string UserId { get; set; }
            public string UserEmail { get; set; }
        }


        #endregion
    }

    public partial class CategoryLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.Description")]
        
        public string Description {get;set;}

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.Fields.SeName")]
        
        public string SeName { get; set; }
    }

}