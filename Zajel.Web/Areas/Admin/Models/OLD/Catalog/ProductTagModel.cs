﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Catalog;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Catalog
{
    [Validator(typeof(ProductTagValidator))]
    public partial class ProductTagModel : BaseZajelEntityModel, ILocalizedModel<ProductTagLocalizedModel>
    {
        public ProductTagModel()
        {
            Locales = new List<ProductTagLocalizedModel>();
        }
        [ZajelResourceDisplayName("Admin.Catalog.ProductTags.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.ProductTags.Fields.ProductCount")]
        public int ProductCount { get; set; }

        public IList<ProductTagLocalizedModel> Locales { get; set; }
    }

    public partial class ProductTagLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.ProductTags.Fields.Name")]
        
        public string Name { get; set; }
    }
}