﻿
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.Collections.Generic;

using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Areas.Admin.Models.Catalog
{
    public partial class CategoryListModel : BaseZajelModel
    {
        public CategoryListModel()
        {
            AvailableStores = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
        
        public string SearchCategoryName { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.Categories.List.SearchStore")]
        public string SearchStoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }
    }
}