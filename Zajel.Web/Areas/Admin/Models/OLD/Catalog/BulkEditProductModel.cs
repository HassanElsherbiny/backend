﻿
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;

using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
namespace Zajel.Web.Areas.Admin.Models.Catalog
{
    public partial class BulkEditProductModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.SKU")]
        
        public string Sku { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.Price")]
        public decimal Price { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.OldPrice")]
        public decimal OldPrice { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.ManageInventoryMethod")]
        public string ManageInventoryMethod { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.StockQuantity")]
        public int StockQuantity { get; set; }

        [ZajelResourceDisplayName("Admin.Catalog.BulkEdit.Fields.Published")]
        public bool Published { get; set; }
    }
}