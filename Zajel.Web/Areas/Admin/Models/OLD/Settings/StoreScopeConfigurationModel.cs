﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Settings
{
    public partial class StoreScopeConfigurationModel : BaseZajelModel
    {
        public StoreScopeConfigurationModel()
        {
            Stores = new List<StoreModel>();
        }

        public string StoreId { get; set; }
        public IList<StoreModel> Stores { get; set; }
    }
}