﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Zajel.Web.Areas.Admin.Models.Settings
{
    public partial class UserUserSettingsModel : BaseZajelModel
    {
        public UserUserSettingsModel()
        {
            UserSettings = new UserSettingsModel();
            AddressSettings = new AddressSettingsModel();
            DateTimeSettings = new DateTimeSettingsModel();
            ExternalAuthenticationSettings = new ExternalAuthenticationSettingsModel();
        }
        public UserSettingsModel UserSettings { get; set; }
        public AddressSettingsModel AddressSettings { get; set; }
        public DateTimeSettingsModel DateTimeSettings { get; set; }
        public ExternalAuthenticationSettingsModel ExternalAuthenticationSettings { get; set; }

        #region Nested classes

        public partial class UserSettingsModel : BaseZajelModel
        {
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.UsernamesEnabled")]
            public bool UsernamesEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AllowUsersToChangeUsernames")]
            public bool AllowUsersToChangeUsernames { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CheckUsernameAvailabilityEnabled")]
            public bool CheckUsernameAvailabilityEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.UserRegistrationType")]
            public int UserRegistrationType { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AllowUsersToUploadAvatars")]
            public bool AllowUsersToUploadAvatars { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DefaultAvatarEnabled")]
            public bool DefaultAvatarEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.ShowUsersLocation")]
            public bool ShowUsersLocation { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.ShowUsersJoinDate")]
            public bool ShowUsersJoinDate { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AllowViewingProfiles")]
            public bool AllowViewingProfiles { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.NotifyNewUserRegistration")]
            public bool NotifyNewUserRegistration { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.HideDownloadableProductsTab")]
            public bool HideDownloadableProductsTab { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.HideBackInStockSubscriptionsTab")]
            public bool HideBackInStockSubscriptionsTab { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.UserNameFormat")]
            public int UserNameFormat { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.PasswordMinLength")]
            public int PasswordMinLength { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.UnduplicatedPasswordsNumber")]
            public int UnduplicatedPasswordsNumber { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.PasswordRecoveryLinkDaysValid")]
            public int PasswordRecoveryLinkDaysValid { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.PasswordLifetime")]
            public int PasswordLifetime { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DefaultPasswordFormat")]
            public int DefaultPasswordFormat { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.FailedPasswordAllowedAttempts")]
            public int FailedPasswordAllowedAttempts { get; set; }
  
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.FailedPasswordLockoutMinutes")]
            public int FailedPasswordLockoutMinutes { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.NewsletterEnabled")]
            public bool NewsletterEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.NewsletterTickedByDefault")]
            public bool NewsletterTickedByDefault { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.HideNewsletterBlock")]
            public bool HideNewsletterBlock { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.NewsletterBlockAllowToUnsubscribe")]
            public bool NewsletterBlockAllowToUnsubscribe { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.RegistrationFreeShipping")]
            public bool RegistrationFreeShipping { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StoreLastVisitedPage")]
            public bool StoreLastVisitedPage { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.GenderEnabled")]
            public bool GenderEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DateOfBirthEnabled")]
            public bool DateOfBirthEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DateOfBirthRequired")]
            public bool DateOfBirthRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DateOfBirthMinimumAge")]
            [UIHint("Int32Nullable")]
            public int? DateOfBirthMinimumAge { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CompanyEnabled")]
            public bool CompanyEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CompanyRequired")]
            public bool CompanyRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StreetAddressEnabled")]
            public bool StreetAddressEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StreetAddressRequired")]
            public bool StreetAddressRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StreetAddress2Enabled")]
            public bool StreetAddress2Enabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StreetAddress2Required")]
            public bool StreetAddress2Required { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.ZipPostalCodeEnabled")]
            public bool ZipPostalCodeEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.ZipPostalCodeRequired")]
            public bool ZipPostalCodeRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CityEnabled")]
            public bool CityEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CityRequired")]
            public bool CityRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CountryEnabled")]
            public bool CountryEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.CountryRequired")]
            public bool CountryRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StateProvinceEnabled")]
            public bool StateProvinceEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.StateProvinceRequired")]
            public bool StateProvinceRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.PhoneEnabled")]
            public bool PhoneEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.PhoneRequired")]
            public bool PhoneRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.FaxEnabled")]
            public bool FaxEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.FaxRequired")]
            public bool FaxRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AcceptPrivacyPolicyEnabled")]
            public bool AcceptPrivacyPolicyEnabled { get; set; }
        }

        public partial class AddressSettingsModel : BaseZajelModel
        {
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.CompanyEnabled")]
            public bool CompanyEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.CompanyRequired")]
            public bool CompanyRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.StreetAddressEnabled")]
            public bool StreetAddressEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.StreetAddressRequired")]
            public bool StreetAddressRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.StreetAddress2Enabled")]
            public bool StreetAddress2Enabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.StreetAddress2Required")]
            public bool StreetAddress2Required { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.ZipPostalCodeEnabled")]
            public bool ZipPostalCodeEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.ZipPostalCodeRequired")]
            public bool ZipPostalCodeRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.CityEnabled")]
            public bool CityEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.CityRequired")]
            public bool CityRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.CountryEnabled")]
            public bool CountryEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.StateProvinceEnabled")]
            public bool StateProvinceEnabled { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.PhoneEnabled")]
            public bool PhoneEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.PhoneRequired")]
            public bool PhoneRequired { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.FaxEnabled")]
            public bool FaxEnabled { get; set; }
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AddressFormFields.FaxRequired")]
            public bool FaxRequired { get; set; }
        }

        public partial class DateTimeSettingsModel : BaseZajelModel
        {
            public DateTimeSettingsModel()
            {
                AvailableTimeZones = new List<SelectListItem>();
            }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.AllowUsersToSetTimeZone")]
            public bool AllowUsersToSetTimeZone { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DefaultStoreTimeZone")]
            public string DefaultStoreTimeZoneId { get; set; }

            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.DefaultStoreTimeZone")]
            public IList<SelectListItem> AvailableTimeZones { get; set; }
        }

        public partial class ExternalAuthenticationSettingsModel : BaseZajelModel
        {
            [ZajelResourceDisplayName("Admin.Configuration.Settings.UserUser.ExternalAuthenticationAutoRegisterEnabled")]
            public bool AutoRegisterEnabled { get; set; }
        }
        #endregion
    }
}