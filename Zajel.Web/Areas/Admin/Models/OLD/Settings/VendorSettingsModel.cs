﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Settings
{
    public partial class VendorSettingsModel : BaseZajelModel
    {
        public string ActiveStoreScopeConfiguration { get; set; }


        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.VendorsBlockItemsToDisplay")]
        public int VendorsBlockItemsToDisplay { get; set; }
        public bool VendorsBlockItemsToDisplay_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.ShowVendorOnProductDetailsPage")]
        public bool ShowVendorOnProductDetailsPage { get; set; }
        public bool ShowVendorOnProductDetailsPage_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowUsersToContactVendors")]
        public bool AllowUsersToContactVendors { get; set; }
        public bool AllowUsersToContactVendors_OverrideForStore { get; set; }
        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowUsersToApplyForVendorAccount")]
        public bool AllowUsersToApplyForVendorAccount { get; set; }
        public bool AllowUsersToApplyForVendorAccount_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowSearchByVendor")]
        public bool AllowSearchByVendor { get; set; }
        public bool AllowSearchByVendor_OverrideForStore { get; set; }


        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowVendorsToEditInfo")]
        public bool AllowVendorsToEditInfo { get; set; }
        public bool AllowVendorsToEditInfo_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.NotifyStoreOwnerAboutVendorInformationChange")]
        public bool NotifyStoreOwnerAboutVendorInformationChange { get; set; }
        public bool NotifyStoreOwnerAboutVendorInformationChange_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.TermsOfServiceEnabled")]
        public bool TermsOfServiceEnabled { get; set; }
        public bool TermsOfServiceEnabled_OverrideForStore { get; set; }

        //review vendor
        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.VendorReviewsMustBeApproved")]
        public bool VendorReviewsMustBeApproved { get; set; }
        public bool VendorReviewsMustBeApproved_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.AllowAnonymousUsersToReviewVendor")]
        public bool AllowAnonymousUsersToReviewVendor { get; set; }
        public bool AllowAnonymousUsersToReviewVendor_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.VendorReviewPossibleOnlyAfterPurchasing")]
        public bool VendorReviewPossibleOnlyAfterPurchasing { get; set; }
        public bool VendorReviewPossibleOnlyAfterPurchasing_OverrideForStore { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Settings.Vendor.NotifyVendorAboutNewVendorReviews")]
        public bool NotifyVendorAboutNewVendorReviews { get; set; }
        public bool NotifyVendorAboutNewVendorReviews_OverrideForStore { get; set; }

    }
}