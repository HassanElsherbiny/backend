﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Web.Areas.Admin.Validators.News;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Framework.Localization;

namespace Zajel.Web.Areas.Admin.Models.News
{
    [Validator(typeof(NewsItemValidator))]
    public partial class NewsItemModel : BaseZajelEntityModel, ILocalizedModel<NewsLocalizedModel>
    {
        public NewsItemModel()
        {
            this.AvailableStores = new List<StoreModel>();
            AvailableUserRoles = new List<UserRoleModel>();
            Locales = new List<NewsLocalizedModel>();
        }

        //Store mapping
        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.AvailableStores")]
        public List<StoreModel> AvailableStores { get; set; }
        public string[] SelectedStoreIds { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Title")]
        public string Title { get; set; }

        [UIHint("Picture")]
        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Picture")]
        public string PictureId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Short")]
        public string Short { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Full")]
        public string Full { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.AllowComments")]
        public bool AllowComments { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.StartDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDate { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.EndDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDate { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaKeywords")]
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaDescription")]
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaTitle")]
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.SeName")]
        public string SeName { get; set; }

        public IList<NewsLocalizedModel> Locales { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Published")]
        public bool Published { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Comments")]
        public int Comments { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        //ACL
        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.SubjectToAcl")]
        public bool SubjectToAcl { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.AclUserRoles")]
        public List<UserRoleModel> AvailableUserRoles { get; set; }
        public string[] SelectedUserRoleIds { get; set; }

    }

    public partial class NewsLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Title")]
        
        public string Title { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Short")]
        
        public string Short { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.Full")]
        
        public string Full { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.News.NewsItems.Fields.SeName")]
        
        public string SeName { get; set; }

    }

}