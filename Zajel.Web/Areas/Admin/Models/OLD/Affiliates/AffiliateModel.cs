﻿using System;

using Zajel.Web.Areas.Admin.Models.Common;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;

using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
namespace Zajel.Web.Areas.Admin.Models.Affiliates
{
    public partial class AffiliateModel : BaseZajelEntityModel
    {
        public AffiliateModel()
        {
            Address = new AddressModel();
        }

        [ZajelResourceDisplayName("Admin.Affiliates.Fields.ID")]
        public override string Id { get; set; }

        [ZajelResourceDisplayName("Admin.Affiliates.Fields.URL")]
        public string Url { get; set; }


        [ZajelResourceDisplayName("Admin.Affiliates.Fields.AdminComment")]
        
        public string AdminComment { get; set; }

        [ZajelResourceDisplayName("Admin.Affiliates.Fields.FriendlyUrlName")]
        
        public string FriendlyUrlName { get; set; }
        
        [ZajelResourceDisplayName("Admin.Affiliates.Fields.Active")]
        public bool Active { get; set; }

        public AddressModel Address { get; set; }

        #region Nested classes
        
        public partial class AffiliatedOrderModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Affiliates.Orders.Order")]
            public override string Id { get; set; }

            [ZajelResourceDisplayName("Admin.Affiliates.Orders.OrderStatus")]
            public string OrderStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Affiliates.Orders.PaymentStatus")]
            public string PaymentStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Affiliates.Orders.ShippingStatus")]
            public string ShippingStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Affiliates.Orders.OrderTotal")]
            public string OrderTotal { get; set; }

            [ZajelResourceDisplayName("Admin.Affiliates.Orders.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }

        public partial class AffiliatedUserModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Affiliates.Users.Name")]
            public string Name { get; set; }
        }

        #endregion
    }
}