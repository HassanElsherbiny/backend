﻿using System;

using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;

using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
namespace Zajel.Web.Areas.Admin.Models.Blogs
{
    public partial class BlogCommentModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.BlogPost")]
        public string BlogPostId { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.BlogPost")]
        
        public string BlogPostTitle { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.User")]
        public string UserId { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.User")]
        public string UserInfo { get; set; }

        
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.Comment")]
        public string Comment { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.Comments.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

    }
}