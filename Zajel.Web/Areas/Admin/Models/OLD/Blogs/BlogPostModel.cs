﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Web.Areas.Admin.Validators.Blogs;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Areas.Admin.Models.Blogs
{
    [Validator(typeof(BlogPostValidator))]
    public partial class BlogPostModel : BaseZajelEntityModel, ILocalizedModel<BlogLocalizedModel>
    {
        public BlogPostModel()
        {
            this.AvailableStores = new List<StoreModel>();
            Locales = new List<BlogLocalizedModel>();
        }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Title")]
        public string Title { get; set; }

        [UIHint("Picture")]
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Picture")]
        public string PictureId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.BodyOverview")]
        
        public string BodyOverview { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.AllowComments")]
        public bool AllowComments { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Tags")]
        
        public string Tags { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Comments")]
        public int Comments { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.StartDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDate { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.EndDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDate { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.SeName")]
        
        public string SeName { get; set; }

        public IList<BlogLocalizedModel> Locales { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        //Store mapping
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.AvailableStores")]
        public List<StoreModel> AvailableStores { get; set; }
        public string[] SelectedStoreIds { get; set; }


    }

    public partial class BlogLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Title")]
        public string Title { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.BodyOverview")]
        
        public string BodyOverview { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Blog.BlogPosts.Fields.SeName")]
        
        public string SeName { get; set; }

    }
}