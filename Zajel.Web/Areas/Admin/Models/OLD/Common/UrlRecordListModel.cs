﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Common
{
    public partial class UrlRecordListModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.System.SeNames.Name")]
        
        public string SeName { get; set; }
    }
}