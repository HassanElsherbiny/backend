﻿using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Common;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Areas.Admin.Models.Common
{
    [Validator(typeof(AddressAttributeValidator))]
    public partial class AddressAttributeModel : BaseZajelEntityModel, ILocalizedModel<AddressAttributeLocalizedModel>
    {
        public AddressAttributeModel()
        {
            Locales = new List<AddressAttributeLocalizedModel>();
        }

        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.IsRequired")]
        public bool IsRequired { get; set; }

        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.AttributeControlType")]
        public int AttributeControlTypeId { get; set; }
        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.AttributeControlType")]
        
        public string AttributeControlTypeName { get; set; }

        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }


        public IList<AddressAttributeLocalizedModel> Locales { get; set; }

    }

    public partial class AddressAttributeLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Address.AddressAttributes.Fields.Name")]
        
        public string Name { get; set; }

    }
}