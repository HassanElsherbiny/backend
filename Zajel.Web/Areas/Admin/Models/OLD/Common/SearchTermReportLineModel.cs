using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Common
{
    public partial class SearchTermReportLineModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.SearchTermReport.Keyword")]
        public string Keyword { get; set; }

        [ZajelResourceDisplayName("Admin.SearchTermReport.Count")]
        public int Count { get; set; }
    }
}
