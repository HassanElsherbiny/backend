﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Common
{
    public partial class UrlRecordModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.SeNames.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.System.SeNames.EntityId")]
        public string EntityId { get; set; }

        [ZajelResourceDisplayName("Admin.System.SeNames.EntityName")]
        public string EntityName { get; set; }

        [ZajelResourceDisplayName("Admin.System.SeNames.IsActive")]
        public bool IsActive { get; set; }

        [ZajelResourceDisplayName("Admin.System.SeNames.Language")]
        public string Language { get; set; }

        [ZajelResourceDisplayName("Admin.System.SeNames.Details")]
        public string DetailsUrl { get; set; }
    }
}