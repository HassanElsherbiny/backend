﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Directory;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Directory
{
    [Validator(typeof(MeasureDimensionValidator))]
    public partial class MeasureDimensionModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.Configuration.Measures.Dimensions.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Measures.Dimensions.Fields.SystemKeyword")]
        
        public string SystemKeyword { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Measures.Dimensions.Fields.Ratio")]
        public decimal Ratio { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Measures.Dimensions.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Measures.Dimensions.Fields.IsPrimaryDimension")]
        public bool IsPrimaryDimension { get; set; }
    }
}