﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Polls;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Localization;
using System.Collections.Generic;

namespace Zajel.Web.Areas.Admin.Models.Polls
{
    [Validator(typeof(PollAnswerValidator))]
    public partial class PollAnswerModel : BaseZajelEntityModel, ILocalizedModel<PollAnswerLocalizedModel>
    {
        public PollAnswerModel()
        {
            Locales = new List<PollAnswerLocalizedModel>();
        }

        public string PollId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Answers.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Answers.Fields.NumberOfVotes")]
        public int NumberOfVotes { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Answers.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        public IList<PollAnswerLocalizedModel> Locales { get; set; }


    }

    public partial class PollAnswerLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Answers.Fields.Name")]
        
        public string Name { get; set; }
    }


}