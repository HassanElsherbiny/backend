﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.ComponentModel.DataAnnotations;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Polls;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Localization;
using Zajel.Web.Areas.Admin.Models.Stores;
using System.Collections.Generic;
using Zajel.Web.Areas.Admin.Models.Users;

namespace Zajel.Web.Areas.Admin.Models.Polls
{
    [Validator(typeof(PollValidator))]
    public partial class PollModel : BaseZajelEntityModel, ILocalizedModel<PollLocalizedModel>
    {

        public PollModel()
        {
            AvailableStores = new List<StoreModel>();
            Locales = new List<PollLocalizedModel>();
            AvailableUserRoles = new List<UserRoleModel>();
        }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.SystemKeyword")]
        
        public string SystemKeyword { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.Published")]
        public bool Published { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.ShowOnHomePage")]
        public bool ShowOnHomePage { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.AllowGuestsToVote")]
        public bool AllowGuestsToVote { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.StartDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? StartDate { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.EndDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? EndDate { get; set; }

        //Store mapping
        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.AvailableStores")]
        public IList<StoreModel> AvailableStores { get; set; }
        public string[] SelectedStoreIds { get; set; }
        public IList<PollLocalizedModel> Locales { get; set; }


        //ACL
        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.SubjectToAcl")]
        public bool SubjectToAcl { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.AclUserRoles")]
        public List<UserRoleModel> AvailableUserRoles { get; set; }
        public string[] SelectedUserRoleIds { get; set; }

    }

    public partial class PollLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.Polls.Fields.Name")]
        public string Name { get; set; }

    }

}