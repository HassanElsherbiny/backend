﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Security
{
    public partial class PermissionRecordModel : BaseZajelModel
    {
        public string Name { get; set; }
        public string SystemName { get; set; }
    }
}