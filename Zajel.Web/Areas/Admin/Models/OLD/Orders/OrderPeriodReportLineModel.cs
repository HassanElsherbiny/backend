﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Zajel.Web.Areas.Admin.Models.Orders
{
    public partial class OrderPeriodReportLineModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.SalesReport.Period.Name")]
        public string Period { get; set; }

        [ZajelResourceDisplayName("Admin.SalesReport.Period.Count")]
        public int Count { get; set; }

        [ZajelResourceDisplayName("Admin.SalesReport.Period.Amount")]
        public decimal Amount { get; set; }

    }
}