﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;

using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Orders
{
    public partial class GiftCardModel: BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.GiftCards.Fields.GiftCardType")]
        public int GiftCardTypeId { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.Order")]
        public string PurchasedWithOrderId { get; set; }
        public int PurchasedWithOrderNumber { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.Amount")]
        public decimal Amount { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.Amount")]
        public string AmountStr { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.RemainingAmount")]
        public string RemainingAmountStr { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.IsGiftCardActivated")]
        public bool IsGiftCardActivated { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.GiftCardCouponCode")]
        
        public string GiftCardCouponCode { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.RecipientName")]
        
        public string RecipientName { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.RecipientEmail")]
        
        public string RecipientEmail { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.SenderName")]
        
        public string SenderName { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.SenderEmail")]
        
        public string SenderEmail { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.Message")]
        
        public string Message { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.IsRecipientNotified")]
        public bool IsRecipientNotified { get; set; }

        [ZajelResourceDisplayName("Admin.GiftCards.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        public string PrimaryStoreCurrencyCode { get; set; }

        #region Nested classes

        public partial class GiftCardUsageHistoryModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.GiftCards.History.UsedValue")]
            public string UsedValue { get; set; }

            [ZajelResourceDisplayName("Admin.GiftCards.History.Order")]
            public string OrderId { get; set; }
            public int OrderNumber { get; set; }

            [ZajelResourceDisplayName("Admin.GiftCards.History.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}