using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Orders
{
    public partial class OrderIncompleteReportLineModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.SalesReport.Incomplete.Item")]
        public string Item { get; set; }

        [ZajelResourceDisplayName("Admin.SalesReport.Incomplete.Total")]
        public string Total { get; set; }

        [ZajelResourceDisplayName("Admin.SalesReport.Incomplete.Count")]
        public int Count { get; set; }

        [ZajelResourceDisplayName("Admin.SalesReport.Incomplete.View")]
        public string ViewLink { get; set; }
    }
}
