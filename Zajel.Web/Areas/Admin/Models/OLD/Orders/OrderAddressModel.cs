﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Web.Areas.Admin.Models.Common;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Orders
{
    public partial class OrderAddressModel : BaseZajelModel
    {
        public string OrderId { get; set; }
        public AddressModel Address { get; set; }
    }
}