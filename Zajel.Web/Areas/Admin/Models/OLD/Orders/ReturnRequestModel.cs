﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;

using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Orders
{
    public partial class ReturnRequestModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.ID")]
        public override string Id { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.ID")]
        public int ReturnNumber { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.Order")]
        public string OrderId { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.User")]
        public string UserId { get; set; }
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.User")]
        public string UserInfo { get; set; }

        public string ProductId { get; set; }
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.Product")]
        public string ProductName { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.Quantity")]
        public int Quantity { get; set; }

        
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.ReasonForReturn")]
        public string ReasonForReturn { get; set; }

        
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.RequestedAction")]
        public string RequestedAction { get; set; }

        
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.UserComments")]
        public string UserComments { get; set; }

        
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.StaffNotes")]
        public string StaffNotes { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.Status")]
        public int ReturnRequestStatusId { get; set; }
        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.Status")]
        public string ReturnRequestStatusStr { get; set; }

        [ZajelResourceDisplayName("Admin.ReturnRequests.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}