﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Stores;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Stores
{
    [Validator(typeof(StoreValidator))]
    public partial class StoreModel : BaseZajelEntityModel, ILocalizedModel<StoreLocalizedModel>
    {
        public StoreModel()
        {
            Locales = new List<StoreLocalizedModel>();
            AvailableLanguages = new List<SelectListItem>();
            AvailableWarehouses = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.Url")]
        
        public string Url { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.SslEnabled")]
        public virtual bool SslEnabled { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.SecureUrl")]
        
        public virtual string SecureUrl { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.Hosts")]
        
        public string Hosts { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyName")]
        
        public string CompanyName { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyAddress")]
        
        public string CompanyAddress { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyPhoneNumber")]
        
        public string CompanyPhoneNumber { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.CompanyVat")]
        
        public string CompanyVat { get; set; }


        public IList<StoreLocalizedModel> Locales { get; set; }
        //default language
        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.DefaultLanguage")]
        
        public string DefaultLanguageId { get; set; }
        public IList<SelectListItem> AvailableLanguages { get; set; }

        //default warehouse
        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.DefaultWarehouse")]
        
        public string DefaultWarehouseId { get; set; }
        public IList<SelectListItem> AvailableWarehouses { get; set; }

    }

    public partial class StoreLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Stores.Fields.Name")]
        
        public string Name { get; set; }
    }
}