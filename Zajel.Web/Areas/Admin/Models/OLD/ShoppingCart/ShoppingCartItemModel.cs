﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartItemModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.CurrentCarts.Store")]
        public string Store { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.Product")]
        public string ProductId { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.Product")]
        public string ProductName { get; set; }
        public string AttributeInfo { get; set; }

        [ZajelResourceDisplayName("Admin.CurrentCarts.UnitPrice")]
        public string UnitPrice { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.Quantity")]
        public int Quantity { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.Total")]
        public string Total { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.UpdatedOn")]
        public DateTime UpdatedOn { get; set; }
    }
}