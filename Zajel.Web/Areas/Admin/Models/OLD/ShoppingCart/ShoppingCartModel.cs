﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.ShoppingCart
{
    public partial class ShoppingCartModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.CurrentCarts.User")]
        public string UserId { get; set; }
        [ZajelResourceDisplayName("Admin.CurrentCarts.User")]
        public string UserEmail { get; set; }

        [ZajelResourceDisplayName("Admin.CurrentCarts.TotalItems")]
        public int TotalItems { get; set; }
    }
}