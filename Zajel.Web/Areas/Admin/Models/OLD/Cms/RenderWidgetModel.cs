﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Areas.Admin.Models.Cms
{
    public partial class RenderWidgetModel : BaseZajelModel
    {
        public string WidgetViewComponentName { get; set; }
        public object WidgetViewComponentArguments { get; set; }
    }
}