﻿using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Areas.Admin.Models.Tax
{
    public partial class TaxProviderModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.Configuration.Tax.Providers.Fields.FriendlyName")]
        
        public string FriendlyName { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Tax.Providers.Fields.SystemName")]
        
        public string SystemName { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Tax.Providers.Fields.IsPrimaryTaxProvider")]
        public bool IsPrimaryTaxProvider { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Tax.Providers.Fields.Configure")]
        public string ConfigurationUrl { get; set; }
    }
}