﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(QueuedEmailValidator))]
    public partial class QueuedEmailModel: BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.Id")]
        public override string Id { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.Priority")]
        public string PriorityName { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.From")]
        
        public string From { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.FromName")]
        
        public string FromName { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.To")]
        
        public string To { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.ToName")]
        
        public string ToName { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.ReplyTo")]
        
        public string ReplyTo { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.ReplyToName")]
        
        public string ReplyToName { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.CC")]
        
        public string CC { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.Bcc")]
        
        public string Bcc { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.Subject")]
        
        public string Subject { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.AttachmentFilePath")]
        
        public string AttachmentFilePath { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.AttachedDownload")]
        [UIHint("Download")]
        public string AttachedDownloadId { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }
        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.SendImmediately")]
        public bool SendImmediately { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.DontSendBeforeDate")]
        [UIHint("DateTimeNullable")]
        public DateTime? DontSendBeforeDate { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.SentTries")]
        public int SentTries { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.SentOn")]
        public DateTime? SentOn { get; set; }

        [ZajelResourceDisplayName("Admin.System.QueuedEmails.Fields.EmailAccountName")]
        
        public string EmailAccountName { get; set; }
    }
}