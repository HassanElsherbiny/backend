﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;



namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(NewsletterCategoryValidator))]
    public partial class NewsletterCategoryModel: BaseZajelEntityModel, ILocalizedModel<NewsletterCategoryLocalizedModel>
    {
        public NewsletterCategoryModel()
        {
            Locales = new List<NewsletterCategoryLocalizedModel>();
        }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.Description")]
        
        public string Description { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.Selected")]
        public bool Selected { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.AvailableStores")]
        public List<StoreModel> AvailableStores { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        public string[] SelectedStoreIds { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }
        public IList<NewsletterCategoryLocalizedModel> Locales { get; set; }
    }

    public partial class NewsletterCategoryLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.NewsletterCategory.Fields.Description")]
        
        public string Description { get; set; }

    }
}