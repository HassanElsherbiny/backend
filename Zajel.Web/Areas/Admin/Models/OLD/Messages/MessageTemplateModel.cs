﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Stores;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(MessageTemplateValidator))]
    public partial class MessageTemplateModel : BaseZajelEntityModel, ILocalizedModel<MessageTemplateLocalizedModel>
    {
        public MessageTemplateModel()
        {
            Locales = new List<MessageTemplateLocalizedModel>();
            AvailableEmailAccounts = new List<EmailAccountModel>();
            AvailableStores = new List<StoreModel>();
        }


        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.AllowedTokens")]
        public string AllowedTokens { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.BccEmailAddresses")]
        
        public string BccEmailAddresses { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Subject")]
        
        public string Subject { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.IsActive")]
        
        public bool IsActive { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.SendImmediately")]
        public bool SendImmediately { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.DelayBeforeSend")]
        [UIHint("Int32Nullable")]
        public int? DelayBeforeSend { get; set; }
        public int DelayPeriodId { get; set; }

        public bool HasAttachedDownload { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.AttachedDownload")]
        [UIHint("Download")]
        public string AttachedDownloadId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.EmailAccount")]
        public string EmailAccountId { get; set; }
        public IList<EmailAccountModel> AvailableEmailAccounts { get; set; }

        //Store mapping
        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.LimitedToStores")]
        public bool LimitedToStores { get; set; }
        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.AvailableStores")]
        public List<StoreModel> AvailableStores { get; set; }
        public string[] SelectedStoreIds { get; set; }
        //comma-separated list of stores used on the list page
        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.LimitedToStores")]
        public string ListOfStores { get; set; }



        public IList<MessageTemplateLocalizedModel> Locales { get; set; }
    }

    public partial class MessageTemplateLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.BccEmailAddresses")]
        
        public string BccEmailAddresses { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Subject")]
        
        public string Subject { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.ContentManagement.MessageTemplates.Fields.EmailAccount")]
        public string EmailAccountId { get; set; }
    }
}