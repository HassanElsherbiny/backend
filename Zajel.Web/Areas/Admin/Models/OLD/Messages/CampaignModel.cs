﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(CampaignValidator))]
    public partial class CampaignModel : BaseZajelEntityModel
    {
        public CampaignModel()
        {
            this.AvailableStores = new List<SelectListItem>();
            this.AvailableUserTags = new List<SelectListItem>();
            this.UserTags = new List<string>();
            this.NewsletterCategories = new List<string>();
            this.AvailableUserRoles = new List<SelectListItem>();
            this.UserRoles = new List<string>();
            this.AvailableEmailAccounts = new List<EmailAccountModel>();
        }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.Subject")]
        
        public string Subject { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.Body")]
        
        public string Body { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.Store")]
        public string StoreId { get; set; }
        public IList<SelectListItem> AvailableStores { get; set; }


        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserCreatedDateFrom")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserCreatedDateFrom { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserCreatedDateTo")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserCreatedDateTo { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserLastActivityDateFrom")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserLastActivityDateFrom { get; set; }
        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserLastActivityDateTo")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserLastActivityDateTo { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserLastPurchaseDateFrom")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserLastPurchaseDateFrom { get; set; }
        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserLastPurchaseDateTo")]
        [UIHint("DateTimeNullable")]
        public DateTime? UserLastPurchaseDateTo { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserHasOrders")]
        public int UserHasOrders { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserHasShoppingCart")]
        public int UserHasShoppingCart { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserTags")]
        public IList<SelectListItem> AvailableUserTags { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserTags")]
        [UIHint("MultiSelect")]
        public IList<string> UserTags { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.NewsletterCategory")]
        [UIHint("MultiSelect")]
        public IList<string> NewsletterCategories { get; set; }
        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.NewsletterCategory")]
        public IList<SelectListItem> AvailableNewsletterCategories { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserRoles")]
        public IList<SelectListItem> AvailableUserRoles { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.UserRoles")]
        [UIHint("MultiSelect")]
        public IList<string> UserRoles { get; set; }


        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.AllowedTokens")]
        public string AllowedTokens { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.EmailAccount")]
        public string EmailAccountId { get; set; }
        public IList<EmailAccountModel> AvailableEmailAccounts { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Campaigns.Fields.TestEmail")]
        
        public string TestEmail { get; set; }
    }
}