﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    public partial class ContactFormModel: BaseZajelEntityModel
    {
        public override string Id { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.Store")]
        public string Store { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.Email")]
        
        public string Email { get; set; }
        public string FullName { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.IpAddress")]
        public string IpAddress { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.Subject")]
        
        public string Subject { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.Enquiry")]
        
        public string Enquiry { get; set; }

        [ZajelResourceDisplayName("Admin.System.ContactForm.Fields.EmailAccountName")]
        
        public string EmailAccountName { get; set; }
    }
}