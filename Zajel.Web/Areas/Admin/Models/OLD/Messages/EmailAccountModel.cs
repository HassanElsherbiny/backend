﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(EmailAccountValidator))]
    public partial class EmailAccountModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Email")]
        
        public string Email { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.DisplayName")]
        
        public string DisplayName { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Host")]
        
        public string Host { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Port")]
        public int Port { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Username")]
        
        public string Username { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.Password")]
        
        public string Password { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.EnableSsl")]
        public bool EnableSsl { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.UseDefaultCredentials")]
        public bool UseDefaultCredentials { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.IsDefaultEmailAccount")]
        public bool IsDefaultEmailAccount { get; set; }


        [ZajelResourceDisplayName("Admin.Configuration.EmailAccounts.Fields.SendTestEmailTo")]
        
        public string SendTestEmailTo { get; set; }

    }
}