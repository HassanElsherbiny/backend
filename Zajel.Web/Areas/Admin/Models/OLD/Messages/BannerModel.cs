﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Messages;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using Zajel.Framework.Localization;

namespace Zajel.Web.Areas.Admin.Models.Messages
{
    [Validator(typeof(BannerValidator))]
    public partial class BannerModel : BaseZajelEntityModel, ILocalizedModel<BannerLocalizedModel>
    {
        public BannerModel()
        {
            Locales = new List<BannerLocalizedModel>();
        }

        [ZajelResourceDisplayName("Admin.Promotions.Banners.Fields.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Banners.Fields.Body")]
        
        public string Body { get; set; }
        
        public IList<BannerLocalizedModel> Locales { get; set; }

    }

    public partial class BannerLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Banners.Fields.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Promotions.Banners.Fields.Body")]
        
        public string Body { get; set; }

    }

}