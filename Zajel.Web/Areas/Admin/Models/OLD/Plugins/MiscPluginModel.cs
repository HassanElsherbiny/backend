﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Plugins
{
    public partial class MiscPluginModel : BaseZajelModel
    {
        public string FriendlyName { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Misc.Fields.Configure")]
        public string ConfigurationUrl { get; set; }
    }
}