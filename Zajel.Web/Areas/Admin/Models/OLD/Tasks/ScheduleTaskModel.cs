﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Tasks;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Tasks
{
    [Validator(typeof(ScheduleTaskValidator))]
    public partial class ScheduleTaskModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.ScheduleTaskName")]
        
        public string ScheduleTaskName { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.Type")]
        public string Type { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.Enabled")]
        public bool Enabled { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.StopOnError")]
        public bool StopOnError { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.LastStartUtc")]
        public string LastStartUtc { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.LastEndUtc")]
        public string LastEndUtc { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.LastSuccessUtc")]
        public string LastSuccessUtc { get; set; }

        //Properties below are for FluentScheduler
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.TimeIntervalChoice")]
        public int TimeIntervalChoice { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.TimeInterval")]
        public int TimeInterval { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.MinuteOfHour")]
        public int MinuteOfHour { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.HourOfDay")]
        public int HourOfDay { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.DayOfWeek")]
        public int DayOfWeek { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.MonthOptionChoice")]
        public int MonthOptionChoice { get; set; }
        [ZajelResourceDisplayName("Admin.System.ScheduleTasks.DayOfMonth")]
        public int DayOfMonth { get; set; }
    }
}