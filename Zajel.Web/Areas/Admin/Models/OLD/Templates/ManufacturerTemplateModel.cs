﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Templates;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Templates
{
    [Validator(typeof(ManufacturerTemplateValidator))]
    public partial class ManufacturerTemplateModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.Templates.Manufacturer.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Manufacturer.ViewPath")]
        
        public string ViewPath { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Manufacturer.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}