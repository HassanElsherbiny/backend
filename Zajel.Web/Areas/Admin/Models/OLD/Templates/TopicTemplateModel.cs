﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Templates;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Templates
{
    [Validator(typeof(TopicTemplateValidator))]
    public partial class TopicTemplateModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.Templates.Topic.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Topic.ViewPath")]
        
        public string ViewPath { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Topic.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}