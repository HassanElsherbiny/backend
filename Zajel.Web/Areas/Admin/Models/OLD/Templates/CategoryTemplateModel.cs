﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Templates;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Templates
{
    [Validator(typeof(CategoryTemplateValidator))]
    public partial class CategoryTemplateModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.System.Templates.Category.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Category.ViewPath")]
        
        public string ViewPath { get; set; }

        [ZajelResourceDisplayName("Admin.System.Templates.Category.DisplayOrder")]
        public int DisplayOrder { get; set; }
    }
}