﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Common;
using Zajel.Web.Areas.Admin.Validators.Shipping;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.Collections.Generic;

namespace Zajel.Web.Areas.Admin.Models.Shipping
{
    [Validator(typeof(PickupPointValidator))]
    public partial class PickupPointModel : BaseZajelEntityModel
    {
        public PickupPointModel()
        {
            this.Address = new AddressModel();
            this.AvailableWarehouses = new List<SelectListItem>();
            this.AvailableStores = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.Description")]
        
        public string Description { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.AdminComment")]
        
        public string AdminComment { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.Address")]
        public AddressModel Address { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.Warehouses")]
        public IList<SelectListItem> AvailableWarehouses { get; set; }

        public string WarehouseId { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.Stores")]
        public IList<SelectListItem> AvailableStores { get; set; }
        public string StoreId { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.PickupPoint.Fields.PickupFee")]
        public decimal PickupFee { get; set; }

    }
}