﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Models.Common;
using Zajel.Web.Areas.Admin.Validators.Shipping;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Shipping
{
    [Validator(typeof(WarehouseValidator))]
    public partial class WarehouseModel : BaseZajelEntityModel
    {
        public WarehouseModel()
        {
            this.Address = new AddressModel();
        }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.AdminComment")]
        
        public string AdminComment { get; set; }

        [ZajelResourceDisplayName("Admin.Configuration.Shipping.Warehouses.Fields.Address")]
        public AddressModel Address { get; set; }
    }
}