﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Vendors;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;
using System.ComponentModel.DataAnnotations;
using System;
using Zajel.Web.Areas.Admin.Models.Discounts;

namespace Zajel.Web.Areas.Admin.Models.Vendors
{
    [Validator(typeof(VendorValidator))]
    public partial class VendorModel : BaseZajelEntityModel, ILocalizedModel<VendorLocalizedModel>
    {
        public VendorModel()
        {
            if (PageSize < 1)
            {
                PageSize = 5;
            }
            Locales = new List<VendorLocalizedModel>();
            AssociatedUsers = new List<AssociatedUserInfo>();
        }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Email")]
        
        public string Email { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Description")]
        
        public string Description { get; set; }

        [UIHint("Picture")]
        [ZajelResourceDisplayName("Admin.Vendors.Fields.Picture")]
        public string PictureId { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.AdminComment")]
        
        public string AdminComment { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Active")]
        public bool Active { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.AllowUserReviews")]
        public bool AllowUserReviews { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.SeName")]
        
        public string SeName { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.PageSize")]
        public int PageSize { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.AllowUsersToSelectPageSize")]
        public bool AllowUsersToSelectPageSize { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.PageSizeOptions")]
        public string PageSizeOptions { get; set; }

        public IList<VendorLocalizedModel> Locales { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.AssociatedUserEmails")]
        public IList<AssociatedUserInfo> AssociatedUsers { get; set; }



        //vendor notes
        [ZajelResourceDisplayName("Admin.Vendors.VendorNotes.Fields.Note")]
        
        public string AddVendorNoteMessage { get; set; }

        public List<DiscountModel> AvailableDiscounts { get; set; }
        public string[] SelectedDiscountIds { get; set; }


        #region Nested classes

        public class AssociatedUserInfo : BaseZajelEntityModel
        {
            public string Email { get; set; }
        }


        public partial class VendorNote : BaseZajelEntityModel
        {
            public string VendorId { get; set; }
            [ZajelResourceDisplayName("Admin.Vendors.VendorNotes.Fields.Note")]
            public string Note { get; set; }
            [ZajelResourceDisplayName("Admin.Vendors.VendorNotes.Fields.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }
        #endregion

    }

    public partial class VendorLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.Description")]
        
        public string Description { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaKeywords")]
        
        public string MetaKeywords { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaDescription")]
        
        public string MetaDescription { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.MetaTitle")]
        
        public string MetaTitle { get; set; }

        [ZajelResourceDisplayName("Admin.Vendors.Fields.SeName")]
        
        public string SeName { get; set; }
    }
}