﻿using FluentValidation.Attributes;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Areas.Admin.Validators.Vendors;
using System;

namespace Zajel.Web.Areas.Admin.Models.Vendors
{
    [Validator(typeof(VendorReviewValidator))]
    public partial class VendorReviewModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.Vendor")]
        public string VendorId { get; set; }
        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.Vendor")]
        public string VendorName { get; set; }

        public string Ids
        {
            get
            {
                return Id.ToString() + ":" + VendorId.ToString();
            }
        }
        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.User")]
        public string UserId { get; set; }
        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.User")]
        public string UserInfo { get; set; }

        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.Title")]
        public string Title { get; set; }

        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.ReviewText")]
        public string ReviewText { get; set; }

        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.Rating")]
        public int Rating { get; set; }

        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.IsApproved")]
        public bool IsApproved { get; set; }

        [ZajelResourceDisplayName("Admin.VendorReviews.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }
    }
}
