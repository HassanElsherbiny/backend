﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class BestUsersReportModel : BaseZajelModel
    {
        public BestUsersReportModel()
        {
            AvailableOrderStatuses = new List<SelectListItem>();
            AvailablePaymentStatuses = new List<SelectListItem>();
            AvailableShippingStatuses = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.StartDate")]
        [UIHint("DateNullable")]
        public DateTime? StartDate { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.EndDate")]
        [UIHint("DateNullable")]
        public DateTime? EndDate { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.OrderStatus")]
        public int OrderStatusId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.PaymentStatus")]
        public int PaymentStatusId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.ShippingStatus")]
        public int ShippingStatusId { get; set; }

        public IList<SelectListItem> AvailableOrderStatuses { get; set; }
        public IList<SelectListItem> AvailablePaymentStatuses { get; set; }
        public IList<SelectListItem> AvailableShippingStatuses { get; set; }
    }
}