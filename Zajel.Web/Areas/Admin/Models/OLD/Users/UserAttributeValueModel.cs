﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserAttributeValueValidator))]
    public partial class UserAttributeValueModel : BaseZajelEntityModel, ILocalizedModel<UserAttributeValueLocalizedModel>
    {
        public UserAttributeValueModel()
        {
            Locales = new List<UserAttributeValueLocalizedModel>();
        }

        public string UserAttributeId { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Values.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Values.Fields.IsPreSelected")]
        public bool IsPreSelected { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Values.Fields.DisplayOrder")]
        public int DisplayOrder {get;set;}

        public IList<UserAttributeValueLocalizedModel> Locales { get; set; }

    }

    public partial class UserAttributeValueLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Values.Fields.Name")]
        
        public string Name { get; set; }
    }
}