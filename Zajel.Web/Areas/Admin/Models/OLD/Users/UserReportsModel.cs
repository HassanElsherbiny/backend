﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class UserReportsModel : BaseZajelModel
    {
        public BestUsersReportModel BestUsersByOrderTotal { get; set; }
        public BestUsersReportModel BestUsersByNumberOfOrders { get; set; }
    }
}