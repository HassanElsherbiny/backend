﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class BestUserReportLineModel : BaseZajelModel
    {
        public string UserId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.Fields.User")]
        public string UserName { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.Fields.OrderTotal")]
        public string OrderTotal { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Reports.BestBy.Fields.OrderCount")]
        public decimal OrderCount { get; set; }
    }
}