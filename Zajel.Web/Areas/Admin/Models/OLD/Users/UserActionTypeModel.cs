﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class UserActionTypeModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.User.ActionType.Fields.Name")]
        public string Name { get; set; }
        [ZajelResourceDisplayName("Admin.User.ActionType.Fields.Enabled")]
        public bool Enabled { get; set; }
    }
}