﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Framework;
using Zajel.Framework.Localization;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserAttributeValidator))]
    public partial class UserAttributeModel : BaseZajelEntityModel, ILocalizedModel<UserAttributeLocalizedModel>
    {
        public UserAttributeModel()
        {
            Locales = new List<UserAttributeLocalizedModel>();
        }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.Name")]
        
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.IsRequired")]
        public bool IsRequired { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.AttributeControlType")]
        public int AttributeControlTypeId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.AttributeControlType")]
        
        public string AttributeControlTypeName { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }


        public IList<UserAttributeLocalizedModel> Locales { get; set; }

    }

    public partial class UserAttributeLocalizedModel : ILocalizedModelLocal
    {
        public string LanguageId { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAttributes.Fields.Name")]
        
        public string Name { get; set; }

    }
}