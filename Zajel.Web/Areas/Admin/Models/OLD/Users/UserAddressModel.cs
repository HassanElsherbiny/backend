﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Web.Areas.Admin.Models.Common;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class UserAddressModel : BaseZajelModel
    {
        public string UserId { get; set; }

        public AddressModel Address { get; set; }
    }
}