﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Core.Domain.Catalog;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserValidator))]
    public partial class UserModel : BaseZajelEntityModel
    {
        public UserModel()
        {
            this.AvailableTimeZones = new List<SelectListItem>();
            this.SendEmail = new SendEmailModel() { SendImmediately = true }; 
            this.SendPm = new SendPmModel();
            this.AvailableUserRoles = new List<UserRoleModel>();
            this.AssociatedExternalAuthRecords = new List<AssociatedExternalAuthModel>();
            this.AvailableCountries = new List<SelectListItem>();
            this.AvailableStates = new List<SelectListItem>();
            this.AvailableVendors = new List<SelectListItem>();
            this.UserAttributes = new List<UserAttributeModel>();
            this.AvailableNewsletterSubscriptionStores = new List<StoreModel>();
            this.RewardPointsAvailableStores = new List<SelectListItem>();
        }

        public bool AllowUsersToChangeUsernames { get; set; }
        public bool UsernamesEnabled { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Username")]
        
        public string Username { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Email")]
        
        public string Email { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Password")]
        
        [DataType(DataType.Password)]
        [NoTrim]
        public string Password { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.UserTags")]
        public string UserTags { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Vendor")]
        public string VendorId { get; set; }
        public IList<SelectListItem> AvailableVendors { get; set; }

        //form fields & properties
        public bool GenderEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Gender")]
        public string Gender { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.FirstName")]
        
        public string FirstName { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.LastName")]
        
        public string LastName { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.FullName")]
        public string FullName { get; set; }
        
        public bool DateOfBirthEnabled { get; set; }
        [UIHint("DateNullable")]
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.DateOfBirth")]
        public DateTime? DateOfBirth { get; set; }

        public bool CompanyEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Company")]
        
        public string Company { get; set; }

        public bool StreetAddressEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.StreetAddress")]
        
        public string StreetAddress { get; set; }

        public bool StreetAddress2Enabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.StreetAddress2")]
        
        public string StreetAddress2 { get; set; }

        public bool ZipPostalCodeEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.ZipPostalCode")]
        
        public string ZipPostalCode { get; set; }

        public bool CityEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.City")]
        
        public string City { get; set; }

        public bool CountryEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Country")]
        public string CountryId { get; set; }
        public IList<SelectListItem> AvailableCountries { get; set; }

        public bool StateProvinceEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.StateProvince")]
        public string StateProvinceId { get; set; }
        public IList<SelectListItem> AvailableStates { get; set; }

        public bool PhoneEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Phone")]
        
        public string Phone { get; set; }

        public bool FaxEnabled { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Fax")]
        
        public string Fax { get; set; }

        public List<UserAttributeModel> UserAttributes { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.AdminComment")]
        
        public string AdminComment { get; set; }
        
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.IsTaxExempt")]
        public bool IsTaxExempt { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.FreeShipping")]
        public bool FreeShipping { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Active")]
        public bool Active { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Affiliate")]
        public string AffiliateId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Affiliate")]
        public string AffiliateName { get; set; }

        //time zone
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.TimeZoneId")]
        
        public string TimeZoneId { get; set; }

        public bool AllowUsersToSetTimeZone { get; set; }

        public IList<SelectListItem> AvailableTimeZones { get; set; }

        //EU VAT
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.VatNumber")]
        
        public string VatNumber { get; set; }

        public string VatNumberStatusNote { get; set; }

        public bool DisplayVatNumber { get; set; }

        //registration date
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.CreatedOn")]
        public DateTime CreatedOn { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.LastActivityDate")]
        public DateTime LastActivityDate { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.LastPurchaseDate")]
        public DateTime? LastPurchaseDate { get; set; }

        //IP adderss
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.IPAddress")]
        public string LastIpAddress { get; set; }


        [ZajelResourceDisplayName("Admin.Users.Users.Fields.LastVisitedPage")]
        public string LastVisitedPage { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.Fields.LastUrlReferrer")]
        public string LastUrlReferrer { get; set; }

        //user roles
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.UserRoles")]
        public string UserRoleNames { get; set; }
        public List<UserRoleModel> AvailableUserRoles { get; set; }
        public string[] SelectedUserRoleIds { get; set; }

        //newsletter subscriptions (per store)
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Newsletter")]
        public List<StoreModel> AvailableNewsletterSubscriptionStores { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.Fields.Newsletter")]
        public string[] SelectedNewsletterSubscriptionStoreIds { get; set; }

        //reward points history
        public bool DisplayRewardPointsHistory { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsValue")]
        public int AddRewardPointsValue { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsMessage")]
        
        public string AddRewardPointsMessage { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsStore")]
        public string AddRewardPointsStoreId { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.AddRewardPointsStore")]
        public IList<SelectListItem> RewardPointsAvailableStores { get; set; }

        //send email model
        public SendEmailModel SendEmail { get; set; }
        //send PM model
        public SendPmModel SendPm { get; set; }
        //send the welcome message
        public bool AllowSendingOfWelcomeMessage { get; set; }
        //re-send the activation message
        public bool AllowReSendingOfActivationMessage { get; set; }
        public bool ShowMessageContactForm { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.AssociatedExternalAuth")]
        public IList<AssociatedExternalAuthModel> AssociatedExternalAuthRecords { get; set; }


        #region Nested classes

        public partial class StoreModel : BaseZajelEntityModel
        {
            public string Name { get; set; }
        }

        public partial class AssociatedExternalAuthModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.AssociatedExternalAuth.Fields.Email")]
            public string Email { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.AssociatedExternalAuth.Fields.ExternalIdentifier")]
            public string ExternalIdentifier { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.AssociatedExternalAuth.Fields.AuthMethodName")]
            public string AuthMethodName { get; set; }
        }

        public partial class RewardPointsHistoryModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.Store")]
            public string StoreName { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.Points")]
            public int Points { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.PointsBalance")]
            public int PointsBalance { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.Message")]
            
            public string Message { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.RewardPoints.Fields.Date")]
            public DateTime CreatedOn { get; set; }
        }

        public partial class SendEmailModel : BaseZajelModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.SendEmail.Subject")]
            
            public string Subject { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.SendEmail.Body")]
            
            public string Body { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.SendEmail.SendImmediately")]
            public bool SendImmediately { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.SendEmail.DontSendBeforeDate")]
            [UIHint("DateTimeNullable")]
            public DateTime? DontSendBeforeDate { get; set; }
        }

        public partial class SendPmModel : BaseZajelModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.SendPM.Subject")]
            public string Subject { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.SendPM.Message")]
            public string Message { get; set; }
        }

        public partial class OrderModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.Orders.ID")]
            public override string Id { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.Orders.ID")]
            public int OrderNumber { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.OrderStatus")]
            public string OrderStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.PaymentStatus")]
            public string PaymentStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.ShippingStatus")]
            public string ShippingStatus { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.OrderTotal")]
            public string OrderTotal { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.Store")]
            public string StoreName { get; set; }

            [ZajelResourceDisplayName("Admin.Users.Users.Orders.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }

        public partial class ActivityLogModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.ActivityLog.ActivityLogType")]
            public string ActivityLogTypeName { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.ActivityLog.Comment")]
            public string Comment { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.ActivityLog.CreatedOn")]
            public DateTime CreatedOn { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.ActivityLog.IpAddress")]
            public string IpAddress { get; set; }
        }

        public partial class ProductPriceModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.ProductPrice.ProductName")]
            public string ProductName { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.ProductPrice.Price")]
            public decimal Price { get; set; }
            public string ProductId { get; set; }
        }
        public partial class AddProductModel : BaseZajelModel
        {
            public AddProductModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableManufacturers = new List<SelectListItem>();
                AvailableStores = new List<SelectListItem>();
                AvailableVendors = new List<SelectListItem>();
                AvailableProductTypes = new List<SelectListItem>();
            }

            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]

            public string SearchProductName { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
            public string SearchCategoryId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
            public string SearchManufacturerId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
            public string SearchStoreId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchVendor")]
            public string SearchVendorId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
            public int SearchProductTypeId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableManufacturers { get; set; }
            public IList<SelectListItem> AvailableStores { get; set; }
            public IList<SelectListItem> AvailableVendors { get; set; }
            public IList<SelectListItem> AvailableProductTypes { get; set; }

            public string UserId { get; set; }

            public string[] SelectedProductIds { get; set; }
        }
        public partial class BackInStockSubscriptionModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("Admin.Users.Users.BackInStockSubscriptions.Store")]
            public string StoreName { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.BackInStockSubscriptions.Product")]
            public string ProductId { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.BackInStockSubscriptions.Product")]
            public string ProductName { get; set; }
            [ZajelResourceDisplayName("Admin.Users.Users.BackInStockSubscriptions.CreatedOn")]
            public DateTime CreatedOn { get; set; }
        }

        public partial class UserAttributeModel : BaseZajelEntityModel
        {
            public UserAttributeModel()
            {
                Values = new List<UserAttributeValueModel>();
            }

            public string Name { get; set; }

            public bool IsRequired { get; set; }

            /// <summary>
            /// Default value for textboxes
            /// </summary>
            public string DefaultValue { get; set; }

            public AttributeControlType AttributeControlType { get; set; }

            public IList<UserAttributeValueModel> Values { get; set; }

        }

        public partial class UserAttributeValueModel : BaseZajelEntityModel
        {
            public string Name { get; set; }

            public bool IsPreSelected { get; set; }
        }

        #endregion
    }
}