﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Core.Domain.Users;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;



namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserReminderValidator))]
    public partial class UserReminderModel : BaseZajelEntityModel
    {
        public UserReminderModel()
        {
        }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.StartDate")]
        public DateTime StartDateTimeUtc { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.EndDate")]
        public DateTime EndDateTimeUtc { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.LastUpdateDate")]
        public DateTime LastUpdateDate { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.AllowRenew")]
        public bool AllowRenew { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.RenewedDay")]
        public int RenewedDay { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.Active")]
        public bool Active { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.DisplayOrder")]
        public int DisplayOrder { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.ReminderRule")]
        public int ReminderRuleId { get; set; }
        
        [ZajelResourceDisplayName("Admin.Users.UserReminder.Fields.ConditionId")]
        public int ConditionId { get; set; }
        public int ConditionCount { get; set; }


        public partial class ConditionModel : BaseZajelEntityModel
        {
            public ConditionModel()
            {
                this.ConditionType = new List<SelectListItem>();
            }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Condition.Fields.Name")]
            public string Name { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Condition.Fields.ConditionTypeId")]
            public int ConditionTypeId { get; set; }
            public IList<SelectListItem> ConditionType { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Condition.Fields.ConditionId")]
            public int ConditionId { get; set; }

            public string UserReminderId { get; set; }

            public partial class AddProductToConditionModel
            {
                public AddProductToConditionModel()
                {
                    AvailableCategories = new List<SelectListItem>();
                    AvailableManufacturers = new List<SelectListItem>();
                    AvailableStores = new List<SelectListItem>();
                    AvailableVendors = new List<SelectListItem>();
                    AvailableProductTypes = new List<SelectListItem>();
                }

                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
                
                public string SearchProductName { get; set; }
                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
                public string SearchCategoryId { get; set; }
                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
                public string SearchManufacturerId { get; set; }
                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
                public string SearchStoreId { get; set; }
                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchVendor")]
                public string SearchVendorId { get; set; }
                [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
                public int SearchProductTypeId { get; set; }

                public IList<SelectListItem> AvailableCategories { get; set; }
                public IList<SelectListItem> AvailableManufacturers { get; set; }
                public IList<SelectListItem> AvailableStores { get; set; }
                public IList<SelectListItem> AvailableVendors { get; set; }
                public IList<SelectListItem> AvailableProductTypes { get; set; }

                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }

                public string[] SelectedProductIds { get; set; }
            }
            public partial class AddCategoryConditionModel
            {
                [ZajelResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
                
                public string SearchCategoryName { get; set; }

                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }

                public string[] SelectedCategoryIds { get; set; }
            }
            public partial class AddManufacturerConditionModel
            {
                [ZajelResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchManufacturerName")]
                
                public string SearchManufacturerName { get; set; }

                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }

                public string[] SelectedManufacturerIds { get; set; }
            }
            public partial class AddUserRoleConditionModel
            {
                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }

                public string UserRoleId { get; set; }
                public string Id { get; set; }
            }
            public partial class AddUserTagConditionModel
            {
                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }

                public string UserTagId { get; set; }
                public string Id { get; set; }
            }
            public partial class AddUserRegisterConditionModel
            {
                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }
                public string UserRegisterName { get; set; }
                public string UserRegisterValue { get; set; }
                public string Id { get; set; }
            }
            public partial class AddCustomUserAttributeConditionModel
            {
                public string Id { get; set; }
                public string UserReminderId { get; set; }
                public string ConditionId { get; set; }
                public string UserAttributeName { get; set; }
                public string UserAttributeValue { get; set; }
            }

        }

        [Validator(typeof(UserReminderLevelValidator))]
        public partial class ReminderLevelModel : BaseZajelEntityModel
        {
            public ReminderLevelModel()
            {
                EmailAccounts = new List<SelectListItem>();
            }

            public string UserReminderId { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.SendDay")]
            public int Day { get; set; }
            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.SendHour")]
            public int Hour { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.SendMinutes")]
            public int Minutes { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.Name")]
            public string Name { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.AllowedTokens")]
            public string AllowedTokens { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.Level")]
            public int Level { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.EmailAccountId")]
            public string EmailAccountId { get; set; }
            public IList<SelectListItem> EmailAccounts { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.BccEmailAddresses")]
            public string BccEmailAddresses { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.Subject")]
            public string Subject { get; set; }

            [ZajelResourceDisplayName("Admin.Users.UserReminder.Level.Fields.Body")]
            
            public string Body { get; set; }
        }

    }



}