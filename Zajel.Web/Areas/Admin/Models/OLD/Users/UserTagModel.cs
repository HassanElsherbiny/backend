﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserTagValidator))]
    public partial class UserTagModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Admin.Users.UserTags.Fields.Name")]
        
        public string Name { get; set; }
    }
}