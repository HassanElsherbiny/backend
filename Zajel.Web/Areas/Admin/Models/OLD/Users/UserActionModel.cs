﻿using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Core.Domain.Users;
using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserActionValidator))]
    public partial class UserActionModel : BaseZajelEntityModel
    {
        public UserActionModel()
        {
            this.ActionType = new List<SelectListItem>();
            this.Banners = new List<SelectListItem>();
            this.InteractiveForms = new List<SelectListItem>();
            this.MessageTemplates = new List<SelectListItem>();
            this.UserRoles = new List<SelectListItem>();
            this.UserTags = new List<SelectListItem>();

        }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.Active")]
        public bool Active { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.ActionTypeId")]
        public string ActionTypeId { get; set; }
        public IList<SelectListItem> ActionType { get; set; }


        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.ConditionId")]
        public int ConditionId { get; set; }
        public int ConditionCount { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.ReactionType")]
        public int ReactionTypeId { get; set; }
        public UserReactionTypeEnum ReactionType
        {
            get { return (UserReactionTypeEnum)ReactionTypeId; }
            set { this.ReactionTypeId = (int)value; }
        }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.Banner")]
        public string BannerId { get; set; }
        public IList<SelectListItem> Banners { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.InteractiveForm")]
        public string InteractiveFormId { get; set; }
        public IList<SelectListItem> InteractiveForms { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.MessageTemplate")]
        public string MessageTemplateId { get; set; }
        public IList<SelectListItem> MessageTemplates { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.UserRole")]
        public string UserRoleId { get; set; }
        public IList<SelectListItem> UserRoles { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.UserTag")]
        public string UserTagId { get; set; }
        public IList<SelectListItem> UserTags { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.StartDateTimeUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime StartDateTimeUtc { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserAction.Fields.EndDateTimeUtc")]
        [UIHint("DateTimeNullable")]
        public DateTime EndDateTimeUtc { get; set; }

    }



}