﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using System.Collections.Generic;

using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class UserListModel : BaseZajelModel
    {
        public UserListModel()
        {
            AvailableUserTags = new List<SelectListItem>();
            SearchUserTagIds = new List<string>();
            SearchUserRoleIds = new List<string>();
            AvailableUserRoles = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Users.Users.List.UserRoles")]
        
        public IList<SelectListItem> AvailableUserRoles { get; set; }


        [ZajelResourceDisplayName("Admin.Users.Users.List.UserRoles")]
        [UIHint("MultiSelect")]
        public IList<string> SearchUserRoleIds { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.UserTags")]
        public IList<SelectListItem> AvailableUserTags { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.UserTags")]
        [UIHint("MultiSelect")]
        public IList<string> SearchUserTagIds { get; set; }


        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchEmail")]
        
        public string SearchEmail { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchUsername")]
        
        public string SearchUsername { get; set; }
        public bool UsernamesEnabled { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchFirstName")]
        
        public string SearchFirstName { get; set; }
        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchLastName")]
        
        public string SearchLastName { get; set; }


        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchCompany")]
        
        public string SearchCompany { get; set; }
        public bool CompanyEnabled { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchPhone")]
        
        public string SearchPhone { get; set; }
        public bool PhoneEnabled { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Users.List.SearchZipCode")]
        
        public string SearchZipPostalCode { get; set; }
        public bool ZipPostalCodeEnabled { get; set; }
    }
}