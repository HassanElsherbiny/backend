﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using FluentValidation.Attributes;
using Zajel.Web.Areas.Admin.Validators.Users;
using Zajel.Framework;
using Zajel.Framework.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Zajel.Web.Areas.Admin.Models.Users
{
    [Validator(typeof(UserActionConditionValidator))]
    public partial class UserActionConditionModel : BaseZajelEntityModel
    {
        public UserActionConditionModel()
        {
            this.UserActionConditionType = new List<SelectListItem>();
        }

        [ZajelResourceDisplayName("Admin.Users.UserActionCondition.Fields.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserActionCondition.Fields.UserConditionTypeId")]
        public int UserActionConditionTypeId { get; set; }
        public IList<SelectListItem> UserActionConditionType { get; set; }

        [ZajelResourceDisplayName("Admin.Users.UserActionCondition.Fields.ConditionId")]
        public int ConditionId { get; set; }

        public string UserActionId { get; set; }


        public partial class AddProductToConditionModel 
        {
            public AddProductToConditionModel()
            {
                AvailableCategories = new List<SelectListItem>();
                AvailableManufacturers = new List<SelectListItem>();
                AvailableStores = new List<SelectListItem>();
                AvailableVendors = new List<SelectListItem>();
                AvailableProductTypes = new List<SelectListItem>();
            }

            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductName")]
            
            public string SearchProductName { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchCategory")]
            public string SearchCategoryId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchManufacturer")]
            public string SearchManufacturerId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchStore")]
            public string SearchStoreId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchVendor")]
            public string SearchVendorId { get; set; }
            [ZajelResourceDisplayName("Admin.Catalog.Products.List.SearchProductType")]
            public int SearchProductTypeId { get; set; }

            public IList<SelectListItem> AvailableCategories { get; set; }
            public IList<SelectListItem> AvailableManufacturers { get; set; }
            public IList<SelectListItem> AvailableStores { get; set; }
            public IList<SelectListItem> AvailableVendors { get; set; }
            public IList<SelectListItem> AvailableProductTypes { get; set; }

            public string UserActionId { get; set; }
            public string UserActionConditionId { get; set; }

            public string[] SelectedProductIds { get; set; }
        }

        public partial class AddCategoryConditionModel 
        {
            [ZajelResourceDisplayName("Admin.Catalog.Categories.List.SearchCategoryName")]
            
            public string SearchCategoryName { get; set; }

            public string UserActionId { get; set; }
            public string UserActionConditionId { get; set; }

            public string[] SelectedCategoryIds { get; set; }
        }

        public partial class AddManufacturerConditionModel 
        {
            [ZajelResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchManufacturerName")]
            
            public string SearchManufacturerName { get; set; }

            public string UserActionId { get; set; }
            public string UserActionConditionId { get; set; }

            public string[] SelectedManufacturerIds { get; set; }
        }

        public partial class AddVendorConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }

            public string VendorId { get; set; }
            public string Id { get; set; }
        }

        public partial class AddUserRoleConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }

            public string UserRoleId { get; set; }
            public string Id { get; set; }
        }

        public partial class AddStoreConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }

            public string StoreId { get; set; }
            public string Id { get; set; }
        }

        public partial class AddUserTagConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }

            public string UserTagId { get; set; }
            public string Id { get; set; }
        }

        public partial class AddProductAttributeConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }
            public string ProductAttributeId { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
        }
        public partial class AddUrlConditionModel
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }
        }

        public partial class AddUserRegisterConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }
            public string UserRegisterName { get; set; }
            public string UserRegisterValue { get; set; }
            public string Id { get; set; }
        }

        public partial class AddCustomUserAttributeConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }
            public string UserAttributeName { get; set; }
            public string UserAttributeValue { get; set; }
            public string Id { get; set; }
        }

        public partial class AddProductSpecificationConditionModel
        {
            public string UserActionId { get; set; }
            public string ConditionId { get; set; }
            public string SpecificationId { get; set; }
            public string SpecificationValueId { get; set; }
            public string Name { get; set; }
            public string Id { get; set; }
        }

    }
}