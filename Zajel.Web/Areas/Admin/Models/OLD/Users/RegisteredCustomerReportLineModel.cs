﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Framework;
using Zajel.Framework.Mvc;

namespace Zajel.Web.Areas.Admin.Models.Users
{
    public partial class RegisteredUserReportLineModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Admin.Users.Reports.RegisteredUsers.Fields.Period")]
        public string Period { get; set; }

        [ZajelResourceDisplayName("Admin.Users.Reports.RegisteredUsers.Fields.Users")]
        public int Users { get; set; }
    }
}