﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Shipping;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Shipping
{
    public class PickupPointValidator : BaseZajelValidator<PickupPointModel>
    {
        public PickupPointValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Shipping.PickupPoints.Fields.Name.Required"));
        }
    }
}