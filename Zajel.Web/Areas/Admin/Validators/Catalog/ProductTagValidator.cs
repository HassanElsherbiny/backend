﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Catalog;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Catalog
{
    public class ProductTagValidator : BaseZajelValidator<ProductTagModel>
    {
        public ProductTagValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Catalog.ProductTags.Fields.Name.Required"));
        }
    }
}