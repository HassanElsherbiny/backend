﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Catalog;
using Zajel.Core.Domain.Catalog;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Catalog
{
    public class ProductAttributeValueModelValidator : BaseZajelValidator<ProductModel.ProductAttributeValueModel>
    {
        public ProductAttributeValueModelValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage(localizationService.GetResource("Admin.Catalog.Products.ProductAttributes.Attributes.Values.Fields.Name.Required"));

            RuleFor(x => x.Quantity)
                .GreaterThanOrEqualTo(1)
                .WithMessage(localizationService.GetResource("Admin.Catalog.Products.ProductAttributes.Attributes.Values.Fields.Quantity.GreaterThanOrEqualTo1"))
                .When(x => x.AttributeValueTypeId == (int)AttributeValueType.AssociatedToProduct);
        }
    }
}