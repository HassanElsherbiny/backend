﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Settings;
using Zajel.Core.Domain.Orders;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Settings
{
    public class OrderSettingsValidator : BaseZajelValidator<OrderSettingsModel>
    {
        public OrderSettingsValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.GiftCards_Activated_OrderStatusId).NotEqual((int)OrderStatus.Pending)
                .WithMessage(localizationService.GetResource("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Awarded.Pending"));
            RuleFor(x => x.GiftCards_Deactivated_OrderStatusId).NotEqual((int)OrderStatus.Pending)
                .WithMessage(localizationService.GetResource("Admin.Configuration.Settings.RewardPoints.PointsForPurchases_Canceled.Pending"));
        }
    }
}