﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Directory;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Directory
{
    public class MeasureUnitValidator : BaseZajelValidator<MeasureUnitModel>
    {
        public MeasureUnitValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Configuration.Measures.Units.Fields.Name.Required"));
        }
    }
}