﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserReminderValidator : BaseZajelValidator<UserReminderModel>
    {
        public UserReminderValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserReminder.Fields.Name.Required"));
        }
    }
    public class UserReminderLevelValidator : BaseZajelValidator<UserReminderModel.ReminderLevelModel>
    {
        public UserReminderLevelValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserReminder.Level.Fields.Name.Required"));
            RuleFor(x => x.Subject).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserReminder.Level.Fields.Subject.Required"));
            RuleFor(x => x.Body).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserReminder.Level.Fields.Body.Required"));
            RuleFor(x => x.Hour+x.Day+ x.Minutes).GreaterThan(0).WithMessage(localizationService.GetResource("Admin.Users.UserReminder.Level.Fields.DayHourMin.Required"));
        }
    }
}