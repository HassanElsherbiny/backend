﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserRoleValidator : BaseZajelValidator<UserRoleModel>
    {
        public UserRoleValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserRoles.Fields.Name.Required"));
        }
    }
}