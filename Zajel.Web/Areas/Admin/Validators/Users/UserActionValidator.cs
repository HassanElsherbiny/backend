﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserActionValidator : BaseZajelValidator<UserActionModel>
    {
        public UserActionValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserAction.Fields.Name.Required"));            
        }
    }
}