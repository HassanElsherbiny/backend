﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserAttributeValidator : BaseZajelValidator<UserAttributeModel>
    {
        public UserAttributeValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserAttributes.Fields.Name.Required"));
        }
    }
}