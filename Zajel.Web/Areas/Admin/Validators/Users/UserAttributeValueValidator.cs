﻿using FluentValidation;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserAttributeValueValidator : BaseZajelValidator<UserAttributeValueModel>
    {
        public UserAttributeValueValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.UserAttributes.Values.Fields.Name.Required"));
        }
    }
}