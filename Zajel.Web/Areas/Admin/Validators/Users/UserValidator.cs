﻿using FluentValidation;
using FluentValidation.Results;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Core.Domain.Users;
using Zajel.Services.Directory;
using Zajel.Services.Localization;
using Zajel.Framework.Validators;
using System;
using System.Linq;

namespace Zajel.Web.Areas.Admin.Validators.Users
{
    public class UserValidator : BaseZajelValidator<UserModel>
    {
        public UserValidator(ILocalizationService localizationService,
            IStateProvinceService stateProvinceService,
            UserSettings userSettings)
        {
            //form fields
            if (userSettings.CountryEnabled && userSettings.CountryRequired)
            {
                RuleFor(x => x.CountryId)
                    .NotEqual("")
                    .WithMessage(localizationService.GetResource("Account.Fields.Country.Required"));
            }
            if (userSettings.CountryEnabled &&
                userSettings.StateProvinceEnabled &&
                userSettings.StateProvinceRequired)
            {
                
                RuleFor(x => x).Must((x, context) =>
                {
                    //does selected country have states?
                    var hasStates = stateProvinceService.GetStateProvincesByCountryId(x.CountryId).Any();
                    if (hasStates)
                    {
                        //if yes, then ensure that a state is selected
                        if (string.IsNullOrEmpty(x.StateProvinceId))
                            return true;
                    }
                    return false;
                }).WithMessage(localizationService.GetResource("Account.Fields.StateProvince.Required"));

            }
            if (userSettings.CompanyRequired && userSettings.CompanyEnabled)
                RuleFor(x => x.Company).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.Company.Required"));
            if (userSettings.StreetAddressRequired && userSettings.StreetAddressEnabled) 
                RuleFor(x => x.StreetAddress).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.StreetAddress.Required"));
            if (userSettings.StreetAddress2Required && userSettings.StreetAddress2Enabled)
                RuleFor(x => x.StreetAddress2).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.StreetAddress2.Required"));
            if (userSettings.ZipPostalCodeRequired && userSettings.ZipPostalCodeEnabled)
                RuleFor(x => x.ZipPostalCode).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.ZipPostalCode.Required"));
            if (userSettings.CityRequired && userSettings.CityEnabled)
                RuleFor(x => x.City).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.City.Required"));
            if (userSettings.PhoneRequired && userSettings.PhoneEnabled)
                RuleFor(x => x.Phone).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.Phone.Required"));
            if (userSettings.FaxRequired && userSettings.FaxEnabled) 
                RuleFor(x => x.Fax).NotEmpty().WithMessage(localizationService.GetResource("Admin.Users.Users.Fields.Fax.Required"));
        }
    }
}