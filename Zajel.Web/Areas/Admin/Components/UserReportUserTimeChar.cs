﻿using Zajel.Services.Localization;
using Zajel.Services.Security;
using Microsoft.AspNetCore.Mvc;

namespace Zajel.Web.Areas.Admin.Components
{
    public class UserReportUserTimeCharViewComponent : ViewComponent
    {
        private readonly IPermissionService _permissionService;

        public UserReportUserTimeCharViewComponent(IPermissionService permissionService)
        {
            this._permissionService = permissionService;
        }

        public IViewComponentResult Invoke()//original Action name: ReportUserTimeChar
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return Content("");

            return View();
        }
    }
}