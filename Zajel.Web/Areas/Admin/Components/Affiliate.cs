﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Zajel.Web.Areas.Admin.Models.Affiliates;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Payments;
using Zajel.Core.Domain.Shipping;
using Zajel.Services.Localization;
using Zajel.Services.Security;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Extensions;

namespace Zajel.Web.Areas.Admin.Components
{
    public class AffiliateViewComponent : ViewComponent
    {
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;

        public AffiliateViewComponent(ILocalizationService localizationService, IPermissionService permissionService)
        {
            this._localizationService = localizationService;
            this._permissionService = permissionService;
        }

        public IViewComponentResult Invoke(string affiliateId)//original Action name: AffiliatedOrderList
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageAffiliates))
                return Content("");

            if (String.IsNullOrEmpty(affiliateId))
                throw new Exception("Affliate ID cannot be empty");

            var model = new AffiliatedOrderListModel();
            model.AffliateId = affiliateId;

            //order statuses
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "" });

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "" });

            //shipping statuses
            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "" });

            return View(model);
        }
    }
}