﻿using Zajel.Services.Localization;
using Zajel.Services.Security;
using Microsoft.AspNetCore.Mvc;

namespace Zajel.Web.Areas.Admin.Components
{
    public class UserReportRegisteredUsersViewComponent : ViewComponent
    {
        private readonly IPermissionService _permissionService;

        public UserReportRegisteredUsersViewComponent(IPermissionService permissionService)
        {
            this._permissionService = permissionService;
        }

        public IViewComponentResult Invoke()//original Action name: ReportRegisteredUsers
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return Content("");

            return View();
        }
    }
}
