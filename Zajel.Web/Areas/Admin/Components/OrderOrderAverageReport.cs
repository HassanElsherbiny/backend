﻿using Zajel.Services.Localization;
using Zajel.Services.Security;
using Microsoft.AspNetCore.Mvc;

namespace Zajel.Web.Areas.Admin.Components
{
    public class OrderOrderAverageReportViewComponent : ViewComponent
    {
        private readonly IPermissionService _permissionService;

        public OrderOrderAverageReportViewComponent(IPermissionService permissionService)
        {
            this._permissionService = permissionService;
        }

        public IViewComponentResult Invoke()//original Action name: OrderAverageReport
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            return View();
        }
    }
}
