﻿using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Web.Areas.Admin.Models.Home;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Domain.Common;
using Zajel.Services.Configuration;
using Zajel.Services.Orders;
using Zajel.Services.Users;
using MongoDB.Bson;
using Zajel.Core.Data;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Seo;
using System.Collections.Generic;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class HomeController : BaseAdminController
    {
        #region Fields
        private readonly IStoreContext _storeContext;
        private readonly CommonSettings _commonSettings;
        private readonly GoogleAnalyticsSettings _googleAnalyticsSettings;
        private readonly ISettingService _settingService;
        private readonly IWorkContext _workContext;
        private readonly ICacheManager _cacheManager;
        private readonly IOrderReportService _orderReportService;
        private readonly IUserService _userService;
        private readonly IRepository<Product> _productRepository;
        private readonly IRepository<ReturnRequest> _returnRequestRepository;

        #endregion

        #region Ctor

        public HomeController(IStoreContext storeContext, 
            CommonSettings commonSettings,
            GoogleAnalyticsSettings googleAnalyticsSettings,
            ISettingService settingService,
            IWorkContext workContext,
            ICacheManager cacheManager,
            IOrderReportService orderReportService,
            IUserService userService,
            IRepository<Product> productRepository,
            IRepository<ReturnRequest> returnRequestRepository)
        {
            this._storeContext = storeContext;
            this._commonSettings = commonSettings;
            this._googleAnalyticsSettings = googleAnalyticsSettings;
            this._settingService = settingService;
            this._workContext = workContext;
            this._cacheManager= cacheManager;
            this._orderReportService = orderReportService;
            this._userService = userService;
            this._productRepository = productRepository;
            this._returnRequestRepository = returnRequestRepository;
        }

        #endregion

        #region Utiliti

        private DashboardActivityModel PrepareActivityModel()
        {
            var model = new DashboardActivityModel();
            string vendorId = "";
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            model.OrdersPending = _orderReportService.GetOrderAverageReportLine(os: Core.Domain.Orders.OrderStatus.Pending).CountOrders;
            model.AbandonedCarts = _userService.GetAllUsers(loadOnlyWithShoppingCart: true, pageSize: 1).TotalCount;

            IList<Product> products;
            IList<ProductAttributeCombination> combinations;
            Zajel.Core.Infrastructure.EngineContext.Current.Resolve<Zajel.Services.Catalog.IProductService>()
                .GetLowStockProducts(vendorId, out products, out combinations);

            model.LowStockProducts = products.Count + combinations.Count;

            model.ReturnRequests = (int)_returnRequestRepository.Collection.Count(new BsonDocument());
            model.TodayRegisteredUsers = _userService.GetAllUsers(userRoleIds: new string[] { _userService.GetUserRoleBySystemName(SystemUserRoleNames.Registered).Id }, createdFromUtc: DateTime.UtcNow.Date, pageSize: 1).TotalCount;
            return model;

        }

        #endregion

        #region Methods

        public IActionResult Index()
        {
            var model = new DashboardModel();
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            if (string.IsNullOrEmpty(_googleAnalyticsSettings.gaprivateKey) || 
                string.IsNullOrEmpty(_googleAnalyticsSettings.gaserviceAccountEmail) ||
                string.IsNullOrEmpty(_googleAnalyticsSettings.gaviewID))
                model.HideReportGA = true;

            return View(model);
        }

        public IActionResult Statistics()
        {
            var model = new DashboardModel();
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            return View(model);
        }

        public IActionResult DashboardActivity()
        {
            var model = PrepareActivityModel();
            return PartialView(model);
        }

        #endregion
    }
}
