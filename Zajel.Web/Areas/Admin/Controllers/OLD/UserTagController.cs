﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Core;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Users;
using Zajel.Services.Catalog;
using Zajel.Services.Users;
using Zajel.Services.Localization;
using Zajel.Services.Logging;
using Zajel.Services.Security;
using Zajel.Services.Stores;
using Zajel.Services.Vendors;
using Zajel.Framework.Controllers;
using Zajel.Framework.Kendoui;
using Zajel.Framework.Mvc;
using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Extensions;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class UserTagController : BaseAdminController
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly ILocalizationService _localizationService;
        private readonly IUserActivityService _userActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly IUserTagService _userTagService;
        #endregion

        #region Constructors

        public UserTagController(IUserService userService,
            ILocalizationService localizationService,
            IUserActivityService userActivityService,
            IPermissionService permissionService,
            IProductService productService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IWorkContext workContext,
            IUserTagService userTagService)
        {
            this._userService = userService;
            this._localizationService = localizationService;
            this._userActivityService = userActivityService;
            this._permissionService = permissionService;
            this._productService = productService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._workContext = workContext;
            this._userTagService = userTagService;
        }

        #endregion


        [NonAction]
        protected virtual UserModel PrepareUserModelForList(User user)
        {
            return new UserModel
            {
                Id = user.Id,
                Email = user.IsRegistered() ? user.Email : _localizationService.GetResource("Admin.Users.Guest"),
            };
        }

        #region User Tags

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var usertags = _userTagService.GetAllUserTags();
            var gridModel = new DataSourceResult
            {
                Data = usertags.Select(x => new { Id = x.Id, Name = x.Name, Count = _userTagService.GetUserCount(x.Id) }),
                Total = usertags.Count()
            };
            return Json(gridModel);
        }

        [HttpGet]
        public IActionResult Search(string term)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();
            var usertags = _userTagService.GetUserTagsByName(term).Select(x => x.Name);
            return Json(usertags);
        }



        [HttpPost]
        public IActionResult Users(string userTagId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var users = _userTagService.GetUsersByTag(userTagId, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = users.Select(PrepareUserModelForList),
                Total = users.TotalCount
            };
            return Json(gridModel);
        }

        public IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var model = new UserTagModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Create(UserTagModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var usertag = model.ToEntity();
                usertag.Name = usertag.Name.ToLower();
                _userTagService.InsertUserTag(usertag);

                //activity log
                _userActivityService.InsertActivity("AddNewUserTag", usertag.Id, _localizationService.GetResource("ActivityLog.AddNewUserTag"), usertag.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserTags.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = usertag.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public IActionResult Edit(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var userTag = _userTagService.GetUserTagById(id);
            if (userTag == null)
                //No user role found with the specified id
                return RedirectToAction("List");

            var model = userTag.ToModel();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Edit(UserTagModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var usertag = _userTagService.GetUserTagById(model.Id);
            if (usertag == null)
                //No user role found with the specified id
                return RedirectToAction("List");

            try
            {
                if (ModelState.IsValid)
                {

                    usertag = model.ToEntity(usertag);
                    usertag.Name = usertag.Name.ToLower();

                    _userTagService.UpdateUserTag(usertag);

                    //activity log
                    _userActivityService.InsertActivity("EditUserTage", usertag.Id, _localizationService.GetResource("ActivityLog.EditUserTag"), usertag.Name);

                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserTags.Updated"));
                    return continueEditing ? RedirectToAction("Edit", new { id = usertag.Id }) : RedirectToAction("List");
                }

                //If we got this far, something failed, redisplay form
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = usertag.Id });
            }
        }

        [HttpPost]
        public IActionResult UserDelete(string Id, string userTagId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var usertag = _userTagService.GetUserTagById(userTagId);
            if (usertag == null)
                throw new ArgumentException("No usertag found with the specified id");

            _userTagService.DeleteTagFromUser(userTagId, Id);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var userTag = _userTagService.GetUserTagById(id);
            if (userTag == null)
                //No user role found with the specified id
                return RedirectToAction("List");

            try
            {
                //activity log
                _userActivityService.InsertActivity("DeleteUserTag", userTag.Id, _localizationService.GetResource("ActivityLog.DeleteUserTag"), userTag.Name);

                _userTagService.DeleteUserTag(userTag);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserTags.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = userTag.Id });
            }

        }


        #endregion

        #region Products

        [HttpPost]
        public IActionResult Products(string userTagId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var products = _userTagService.GetUserTagProducts(userTagId);

            var gridModel = new DataSourceResult
            {
                Data = products.Select(x => new UserRoleProductModel
                {
                    Id = x.Id,
                    Name = _productService.GetProductById(x.ProductId)?.Name,
                    DisplayOrder = x.DisplayOrder
                }),
                Total = products.Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ProductDelete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var ctp = _userTagService.GetUserTagProductById(id);
            if (ctp == null)
                throw new ArgumentException("No found the specified id");

            _userTagService.DeleteUserTagProduct(ctp);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult ProductUpdate(UserRoleProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                return AccessDeniedView();

            var ctp = _userTagService.GetUserTagProductById(model.Id);
            if (ctp == null)
                throw new ArgumentException("No user tag product found with the specified id");

            ctp.DisplayOrder = model.DisplayOrder;
            _userTagService.UpdateUserTagProduct(ctp);

            return new NullJsonResult();
        }

        public IActionResult ProductAddPopup(string userTagId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var model = new UserTagProductModel.AddProductModel();
            model.UserTagId = userTagId;
            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });

            return View(model);
        }

        [HttpPost]
        public IActionResult ProductAddPopupList(DataSourceRequest command, UserTagProductModel.AddProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            var searchCategoryIds = new List<string>();
            if (!String.IsNullOrEmpty(model.SearchCategoryId))
                searchCategoryIds.Add(model.SearchCategoryId);

            var products = _productService.SearchProducts(
                categoryIds: searchCategoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
                );
            var gridModel = new DataSourceResult();
            gridModel.Data = products.Select(x => x.ToModel());
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult ProductAddPopup(string btnId, UserTagProductModel.AddProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageProducts))
                return AccessDeniedView();

            if (model.SelectedProductIds != null)
            {
                foreach (string id in model.SelectedProductIds)
                {
                    var product = _productService.GetProductById(id);
                    if (product != null)
                    {
                        var userTagProduct = _userTagService.GetUserTagProduct(model.UserTagId, id);
                        if (userTagProduct == null)
                        {
                            userTagProduct = new UserTagProduct();
                            userTagProduct.UserTagId = model.UserTagId;
                            userTagProduct.ProductId = id;
                            userTagProduct.DisplayOrder = 0;
                            _userTagService.InsertUserTagProduct(userTagProduct);
                        }
                    }
                }
            }

            //a vendor should have access only to his products
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion
    }
}
