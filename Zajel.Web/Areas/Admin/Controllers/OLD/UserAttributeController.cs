﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Mvc.Filters;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Core;
using Zajel.Core.Domain.Users;
using Zajel.Services.Users;
using Zajel.Services.Localization;
using Zajel.Services.Security;
using Zajel.Framework.Kendoui;
using Zajel.Framework.Mvc;
using System.Collections.Generic;
using Zajel.Core.Domain.Localization;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class UserAttributeController : BaseAdminController
    {
        #region Fields

        private readonly IUserAttributeService _userAttributeService;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IPermissionService _permissionService;

        #endregion

        #region Constructors

        public UserAttributeController(IUserAttributeService userAttributeService,
            ILanguageService languageService, 
            ILocalizationService localizationService,
            IWorkContext workContext,
            IPermissionService permissionService)
        {
            this._userAttributeService = userAttributeService;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._permissionService = permissionService;
        }

        #endregion
        
        #region Utilities

        [NonAction]
        protected virtual List<LocalizedProperty> UpdateAttributeLocales(UserAttribute userAttribute, UserAttributeModel model)
        {
            List<LocalizedProperty> localized = new List<LocalizedProperty>();
            foreach (var local in model.Locales)
            {
                if (!(String.IsNullOrEmpty(local.Name)))
                    localized.Add(new LocalizedProperty()
                    {
                        LanguageId = local.LanguageId,
                        LocaleKey = "Name",
                        LocaleValue = local.Name
                    });
            }
            return localized;
        }

        [NonAction]
        protected virtual List<LocalizedProperty> UpdateValueLocales(UserAttributeValue userAttributeValue, UserAttributeValueModel model)
        {
            List<LocalizedProperty> localized = new List<LocalizedProperty>();
            foreach (var local in model.Locales)
            {
                    if (!(String.IsNullOrEmpty(local.Name)))
                        localized.Add(new LocalizedProperty()
                        {
                            LanguageId = local.LanguageId,
                            LocaleKey = "Name",
                            LocaleValue = local.Name
                        });
            }
            return localized;
        }

        #endregion
        
        #region User attributes

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult ListBlock()
        {
            return PartialView("ListBlock");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            //we just redirect a user to the user settings page
            
            //select second tab
            const int userFormFieldIndex = 1;
            SaveSelectedTabIndex(userFormFieldIndex);
            return RedirectToAction("UserUser", "Setting");
        }

        [HttpPost]
        public IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttributes = _userAttributeService.GetAllUserAttributes();
            var gridModel = new DataSourceResult
            {
                Data = userAttributes.Select(x =>
                {
                    var attributeModel = x.ToModel();
                    attributeModel.AttributeControlTypeName = x.AttributeControlType.GetLocalizedEnum(_localizationService, _workContext);
                    return attributeModel;
                }),
                Total = userAttributes.Count()
            };
            return Json(gridModel);
        }
        
        //create
        public IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var model = new UserAttributeModel();
            //locales
            AddLocales(_languageService, model.Locales);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Create(UserAttributeModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var userAttribute = model.ToEntity();
                userAttribute.Locales = UpdateAttributeLocales(userAttribute, model);
                _userAttributeService.InsertUserAttribute(userAttribute);
                
                SuccessNotification(_localizationService.GetResource("Admin.Users.UserAttributes.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = userAttribute.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public IActionResult Edit(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttribute = _userAttributeService.GetUserAttributeById(id);
            if (userAttribute == null)
                //No user attribute found with the specified id
                return RedirectToAction("List");

            var model = userAttribute.ToModel();
            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Name = userAttribute.GetLocalized(x => x.Name, languageId, false, false);
            });
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Edit(UserAttributeModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttribute = _userAttributeService.GetUserAttributeById(model.Id);
            if (userAttribute == null)
                //No user attribute found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                userAttribute = model.ToEntity(userAttribute);
                userAttribute.Locales = UpdateAttributeLocales(userAttribute, model);
                _userAttributeService.UpdateUserAttribute(userAttribute);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserAttributes.Updated"));
                if (continueEditing)
                {
                    //selected tab
                    SaveSelectedTabIndex();

                    return RedirectToAction("Edit", new {id = userAttribute.Id});
                }
                return RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public IActionResult Delete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttribute = _userAttributeService.GetUserAttributeById(id);
            _userAttributeService.DeleteUserAttribute(userAttribute);

            SuccessNotification(_localizationService.GetResource("Admin.Users.UserAttributes.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

        #region User attribute values

        //list
        [HttpPost]
        public IActionResult ValueList(string userAttributeId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var values = _userAttributeService.GetUserAttributeById(userAttributeId).UserAttributeValues;
            var gridModel = new DataSourceResult
            {
                Data = values.Select(x => new UserAttributeValueModel
                {
                    Id = x.Id,
                    UserAttributeId = x.UserAttributeId,
                    Name = x.Name,
                    IsPreSelected = x.IsPreSelected,
                    DisplayOrder = x.DisplayOrder,
                }),
                Total = values.Count()
            };
            return Json(gridModel);
        }

        //create
        public IActionResult ValueCreatePopup(string userAttributeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttribute = _userAttributeService.GetUserAttributeById(userAttributeId);
            if (userAttribute == null)
                //No user attribute found with the specified id
                return RedirectToAction("List");

            var model = new UserAttributeValueModel();
            model.UserAttributeId = userAttributeId;
            //locales
            AddLocales(_languageService, model.Locales);
            return View(model);
        }

        [HttpPost]
        public IActionResult ValueCreatePopup(string btnId, string formId, UserAttributeValueModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var userAttribute = _userAttributeService.GetUserAttributeById(model.UserAttributeId);
            if (userAttribute == null)
                //No user attribute found with the specified id
                return RedirectToAction("List");
            
            if (ModelState.IsValid)
            {
                var cav = new UserAttributeValue
                {
                    UserAttributeId = model.UserAttributeId,
                    Name = model.Name,
                    IsPreSelected = model.IsPreSelected,
                    DisplayOrder = model.DisplayOrder
                };
                cav.Locales = UpdateValueLocales(cav, model);
                _userAttributeService.InsertUserAttributeValue(cav);
                

                ViewBag.RefreshPage = true;
                ViewBag.btnId = btnId;
                ViewBag.formId = formId;
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //edit
        public IActionResult ValueEditPopup(string id, string userAttributeId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();
            var av = _userAttributeService.GetUserAttributeById(userAttributeId);
            var cav = av.UserAttributeValues.FirstOrDefault(x=>x.Id == id);
            if (cav == null)
                //No user attribute value found with the specified id
                return RedirectToAction("List");

            var model = new UserAttributeValueModel
            {
                UserAttributeId = cav.UserAttributeId,
                Name = cav.Name,
                IsPreSelected = cav.IsPreSelected,
                DisplayOrder = cav.DisplayOrder
            };

            //locales
            AddLocales(_languageService, model.Locales, (locale, languageId) =>
            {
                locale.Name = cav.GetLocalized(x => x.Name, languageId, false, false);
            });

            return View(model);
        }

        [HttpPost]
        public IActionResult ValueEditPopup(string btnId, string formId, UserAttributeValueModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var av = _userAttributeService.GetUserAttributeById(model.UserAttributeId);
            var cav = av.UserAttributeValues.FirstOrDefault(x => x.Id == model.Id);
            if (cav == null)
                //No user attribute value found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                cav.Name = model.Name;
                cav.IsPreSelected = model.IsPreSelected;
                cav.DisplayOrder = model.DisplayOrder;
                cav.Locales = UpdateValueLocales(cav, model);
                _userAttributeService.UpdateUserAttributeValue(cav);

                ViewBag.RefreshPage = true;
                ViewBag.btnId = btnId;
                ViewBag.formId = formId;
                return View(model);
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        //delete
        [HttpPost]
        public IActionResult ValueDelete(UserAttributeValueModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageSettings))
                return AccessDeniedView();

            var av = _userAttributeService.GetUserAttributeById(model.UserAttributeId);
            var cav = av.UserAttributeValues.FirstOrDefault(x => x.Id == model.Id);
            if (cav == null)
                throw new ArgumentException("No user attribute value found with the specified id");
            _userAttributeService.DeleteUserAttributeValue(cav);

            return new NullJsonResult();
        }


        #endregion
    }
}
