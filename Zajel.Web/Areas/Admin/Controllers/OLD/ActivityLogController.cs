﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Web.Areas.Admin.Models.Logging;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Logging;
using Zajel.Services.Security;
using Zajel.Framework.Kendoui;
using Zajel.Framework.Mvc;
using Zajel.Services.Users;
using Zajel.Services.Catalog;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class ActivityLogController : BaseAdminController
    {
        #region Fields

        private readonly IUserActivityService _userActivityService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ILocalizationService _localizationService;
        private readonly IPermissionService _permissionService;
        private readonly IUserService _userService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductService _productService;
        #endregion Fields

        #region Constructors

        public ActivityLogController(IUserActivityService userActivityService,
            IDateTimeHelper dateTimeHelper, ILocalizationService localizationService,
            IPermissionService permissionService, IUserService userService,
            ICategoryService categoryService, IManufacturerService manufacturerService,
            IProductService productService)
        {
            this._userActivityService = userActivityService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._permissionService = permissionService;
            this._userService = userService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._productService = productService;
        }

        #endregion

        #region Activity log types

        public IActionResult ListTypes()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            var model = _userActivityService
                .GetAllActivityTypes()
                .Select(x => x.ToModel())
                .ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult SaveTypes(IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();
            
            string formKey = "checkbox_activity_types";
            var checkedActivityTypes = form[formKey].ToString() != null ? form[formKey].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList() : new List<string>();

            var activityTypes = _userActivityService.GetAllActivityTypes();
            foreach (var activityType in activityTypes)
            {
                activityType.Enabled = checkedActivityTypes.Contains(activityType.Id);
                _userActivityService.UpdateActivityType(activityType);
            }
            SuccessNotification(_localizationService.GetResource("Admin.Configuration.ActivityLog.ActivityLogType.Updated"));
            return RedirectToAction("ListTypes");
        }

        #endregion

        #region Activity log

        public IActionResult ListLogs()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            var activityLogSearchModel = new ActivityLogSearchModel();
            activityLogSearchModel.ActivityLogType.Add(new SelectListItem
            {
                Value = "",
                Text = "All"
            });


            foreach (var at in _userActivityService.GetAllActivityTypes())
            {
                activityLogSearchModel.ActivityLogType.Add(new SelectListItem
                {
                    Value = at.Id.ToString(),
                    Text = at.Name
                });
            }
            return View(activityLogSearchModel);
        }

        [HttpPost]
        public IActionResult ListLogs(DataSourceRequest command, ActivityLogSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            DateTime? startDateValue = (model.CreatedOnFrom == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var activityLog = _userActivityService.GetAllActivities(startDateValue, endDateValue, null, model.ActivityLogTypeId, model.IpAddress, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = activityLog.Select(x =>
                {
                    var user = _userService.GetUserById(x.UserId);
                    var m = x.ToModel();
                    m.CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc);
                    m.ActivityLogTypeName = _userActivityService.GetActivityTypeById(m.ActivityLogTypeId)?.Name;
                    m.UserEmail = user != null ? user.Email : "NULL";
                    return m;

                }),
                Total = activityLog.TotalCount
            };
            return Json(gridModel);
        }

        public IActionResult AcivityLogDelete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            var activityLog = _userActivityService.GetActivityById(id);
            if (activityLog == null)
            {
                throw new ArgumentException("No activity log found with the specified id");
            }
            _userActivityService.DeleteActivity(activityLog);

            return new NullJsonResult();
        }

        public IActionResult ClearAll()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            _userActivityService.ClearAllActivities();
            return RedirectToAction("ListLogs");
        }

        #endregion

        #region Acticity Stats

        public IActionResult ListStats()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            var activityLogSearchModel = new ActivityLogSearchModel();
            activityLogSearchModel.ActivityLogType.Add(new SelectListItem
            {
                Value = "",
                Text = "All"
            });


            foreach (var at in _userActivityService.GetAllActivityTypes())
            {
                activityLogSearchModel.ActivityLogType.Add(new SelectListItem
                {
                    Value = at.Id.ToString(),
                    Text = at.Name
                });
            }
            return View(activityLogSearchModel);
        }

        [HttpPost]
        public IActionResult ListStats(DataSourceRequest command, ActivityLogSearchModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActivityLog))
                return AccessDeniedView();

            DateTime? startDateValue = (model.CreatedOnFrom == null) ? null
                : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnFrom.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.CreatedOnTo == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.CreatedOnTo.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var activityLog = _userActivityService.GetStatsActivities(startDateValue, endDateValue, model.ActivityLogTypeId, command.Page - 1, command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = activityLog.Select(x =>
                {
                    var activityLogType = _userActivityService.GetActivityTypeById(x.ActivityLogTypeId);
                    string _name = "-empty-";
                    if(activityLogType!=null)
                    {
                        IList<string> systemKeywordsCategory = new List<string>();
                        systemKeywordsCategory.Add("PublicStore.ViewCategory");
                        systemKeywordsCategory.Add("EditCategory");
                        systemKeywordsCategory.Add("AddNewCategory");

                        if (systemKeywordsCategory.Contains(activityLogType.SystemKeyword))
                        {
                            var category = _categoryService.GetCategoryById(x.EntityKeyId);
                            if (category != null)
                                _name = category.Name;
                        }

                        IList<string> systemKeywordsManufacturer = new List<string>();
                        systemKeywordsManufacturer.Add("PublicStore.ViewManufacturer");
                        systemKeywordsManufacturer.Add("EditManufacturer");
                        systemKeywordsManufacturer.Add("AddNewManufacturer");

                        if (systemKeywordsManufacturer.Contains(activityLogType.SystemKeyword))
                        {
                            var manufacturer = _manufacturerService.GetManufacturerById(x.EntityKeyId);
                            if (manufacturer != null)
                                _name = manufacturer.Name;
                        }

                        IList<string> systemKeywordsProduct = new List<string>();
                        systemKeywordsProduct.Add("PublicStore.ViewProduct");
                        systemKeywordsProduct.Add("EditProduct");
                        systemKeywordsProduct.Add("AddNewProduct");

                        if (systemKeywordsProduct.Contains(activityLogType.SystemKeyword))
                        {
                            var product = _productService.GetProductById(x.EntityKeyId);
                            if (product != null)
                                _name = product.Name;
                        }
                        IList<string> systemKeywordsUrl = new List<string>();
                        systemKeywordsUrl.Add("PublicStore.Url");
                        if (systemKeywordsUrl.Contains(activityLogType.SystemKeyword))
                        {
                            _name = x.EntityKeyId;
                        }
                    }

                    var m = x.ToModel();
                    m.ActivityLogTypeName = activityLogType!=null ? activityLogType.Name : "-empty-";
                    m.Name = _name;
                    return m;

                }),
                Total = activityLog.TotalCount
            };
            return Json(gridModel);
        }


        #endregion
    
    }
}
