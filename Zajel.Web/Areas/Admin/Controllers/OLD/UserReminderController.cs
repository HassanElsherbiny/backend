﻿using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Core;
using Zajel.Services.Catalog;
using Zajel.Services.Users;
using Zajel.Services.Localization;
using Zajel.Services.Logging;
using Zajel.Services.Messages;
using Zajel.Services.Security;
using Zajel.Services.Stores;
using Zajel.Services.Vendors;
using Zajel.Framework.Controllers;
using Zajel.Framework.Kendoui;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.Filters;
using Zajel.Framework.Extensions;
using Zajel.Core.Domain.Users;
using Zajel.Framework.Mvc;
using Zajel.Core.Domain.Catalog;
using Zajel.Services.Helpers;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class UserReminderController : BaseAdminController
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly IUserAttributeService _userAttributeService;
        private readonly IUserTagService _userTagService;
        private readonly ILocalizationService _localizationService;
        private readonly IUserActivityService _userActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly IUserReminderService _userReminderService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        #region Constructors

        public UserReminderController(IUserService userService,
            IUserAttributeService userAttributeService,
            IUserTagService userTagService,
            ILocalizationService localizationService,
            IUserActivityService userActivityService,
            IPermissionService permissionService,
            IProductService productService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IWorkContext workContext,
            IUserReminderService userReminderService,
            IMessageTemplateService messageTemplateService,
            IEmailAccountService emailAccountService,
            IDateTimeHelper dateTimeHelper)
        {
            this._userService = userService;
            this._userAttributeService = userAttributeService;
            this._userTagService = userTagService;
            this._localizationService = localizationService;
            this._userActivityService = userActivityService;
            this._permissionService = permissionService;
            this._productService = productService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._workContext = workContext;
            this._userReminderService = userReminderService;
            this._messageTemplateService = messageTemplateService;
            this._emailAccountService = emailAccountService;
            this._dateTimeHelper = dateTimeHelper;
        }

        #endregion

        #region Utilities

        protected virtual void PrepareModel(UserReminderModel.ReminderLevelModel model, UserReminder userReminder)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var emailAccounts = _emailAccountService.GetAllEmailAccounts();
            foreach (var item in emailAccounts)
            {
                model.EmailAccounts.Add(new SelectListItem
                {
                    Text = item.Email,
                    Value = item.Id.ToString()
                });
            }
            var messageTokenProvider = Zajel.Core.Infrastructure.EngineContext.Current.Resolve<IMessageTokenProvider>();
            model.AllowedTokens = FormatTokens(messageTokenProvider.GetListOfUserReminderAllowedTokens(userReminder.ReminderRule));


        }
        public class SerializeUserReminderHistoryModel
        {
            public string Id { get; set; }
            public string Email { get; set; }
            public DateTime SendDate { get; set; }
            public int Level { get; set; }
            public bool OrderId { get; set; }
        }
        protected virtual SerializeUserReminderHistoryModel PrepareHistoryModelForList(SerializeUserReminderHistory history)
        {
            return new SerializeUserReminderHistoryModel
            {
                Id = history.Id,
                Email = _userService.GetUserById(history.UserId).Email,
                SendDate = _dateTimeHelper.ConvertToUserTime(history.SendDate, DateTimeKind.Utc),
                Level = history.Level,
                OrderId = !String.IsNullOrEmpty(history.OrderId)
            };
        }


        private string FormatTokens(string[] tokens)
        {
            return string.Join(", ", tokens);
        }

        #endregion


        #region User reminders

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var useractions = _userReminderService.GetUserReminders();
            var gridModel = new DataSourceResult
            {
                Data = useractions.Select(x => new { Id = x.Id, Name = x.Name, Active = x.Active, Rule = x.ReminderRule.ToString() }),
                Total = useractions.Count()
            };
            return Json(gridModel);
        }

        public IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var model = new UserReminderModel();
            model.StartDateTimeUtc = DateTime.UtcNow;
            model.EndDateTimeUtc = DateTime.UtcNow.AddMonths(1);
            model.LastUpdateDate = DateTime.UtcNow.AddDays(-7);
            model.Active = true;
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Create(UserReminderModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var userreminder = model.ToEntity();
                _userReminderService.InsertUserReminder(userreminder);

                //activity log
                _userActivityService.InsertActivity("AddNewUserReminder", userreminder.Id, _localizationService.GetResource("ActivityLog.AddNewUserReminder"), userreminder.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminder.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = userreminder.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            return View(model);
        }

        public IActionResult Edit(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(id);
            if (userReminder == null)
                return RedirectToAction("List");
            var model = userReminder.ToModel();
            model.ConditionCount = userReminder.Conditions.Count();
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Edit(UserReminderModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userreminder = _userReminderService.GetUserReminderById(model.Id);
            if (userreminder == null)
                return RedirectToAction("List");
            try
            {
                if (ModelState.IsValid)
                {
                    if (userreminder.Conditions.Count() > 0)
                        model.ReminderRuleId = userreminder.ReminderRuleId;
                    if (model.ReminderRuleId == 0)
                        model.ReminderRuleId = userreminder.ReminderRuleId;

                    userreminder = model.ToEntity(userreminder);
                    _userReminderService.UpdateUserReminder(userreminder);
                    _userActivityService.InsertActivity("EditUserReminder", userreminder.Id, _localizationService.GetResource("ActivityLog.EditUserReminder"), userreminder.Name);

                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminder.Updated"));
                    return continueEditing ? RedirectToAction("Edit", new { id = userreminder.Id }) : RedirectToAction("List");
                }
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = userreminder.Id });
            }
        }

        [HttpPost]
        public IActionResult Run(string Id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var model = _userReminderService.GetUserReminderById(Id);
            if (model == null)
                return RedirectToAction("List");

            if (model.ReminderRule == UserReminderRuleEnum.AbandonedCart)
                _userReminderService.Task_AbandonedCart(model.Id);

            if (model.ReminderRule == UserReminderRuleEnum.Birthday)
                _userReminderService.Task_Birthday(model.Id);

            if (model.ReminderRule == UserReminderRuleEnum.LastActivity)
                _userReminderService.Task_LastActivity(model.Id);

            if (model.ReminderRule == UserReminderRuleEnum.LastPurchase)
                _userReminderService.Task_LastPurchase(model.Id);

            if (model.ReminderRule == UserReminderRuleEnum.RegisteredUser)
                _userReminderService.Task_RegisteredUser(model.Id);

            SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminder.Run"));
            return RedirectToAction("Edit", new { id = Id });
        }

        [HttpPost]
        public IActionResult Delete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(id);
            if (userReminder == null)
                return RedirectToAction("List");
            try
            {
                _userActivityService.InsertActivity("DeleteUserReminder", userReminder.Id, _localizationService.GetResource("ActivityLog.DeleteUserReminder"), userReminder.Name);
                _userReminderService.DeleteUserReminder(userReminder);
                SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminder.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = userReminder.Id });
            }
        }

        [HttpPost]
        public IActionResult History(DataSourceRequest command, string userReminderId)
        {
            //we use own own binder for searchUserRoleIds property 
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var history = _userReminderService.GetAllUserReminderHistory(userReminderId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = history.Select(PrepareHistoryModelForList),
                Total = history.TotalCount
            };

            return Json(gridModel);
        }

        #endregion

        #region Condition

        [HttpPost]
        public IActionResult Conditions(string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var gridModel = new DataSourceResult
            {
                Data = userReminder.Conditions.Select(x => new
                { Id = x.Id, Name = x.Name, Condition = x.Condition.ToString() }),
                Total = userReminder.Conditions.Count()
            };
            return Json(gridModel);
        }

        public IActionResult AddCondition(string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);

            var model = new UserReminderModel.ConditionModel();
            model.UserReminderId = userReminderId;
            foreach (UserReminderConditionTypeEnum item in Enum.GetValues(typeof(UserReminderConditionTypeEnum)))
            {
                if (userReminder.ReminderRule == UserReminderRuleEnum.AbandonedCart || userReminder.ReminderRule == UserReminderRuleEnum.CompletedOrder
                    || userReminder.ReminderRule == UserReminderRuleEnum.UnpaidOrder)
                    model.ConditionType.Add(new SelectListItem()
                    {
                        Value = ((int)item).ToString(),
                        Text = item.ToString()
                    });
                else
                {
                    if (item != UserReminderConditionTypeEnum.Product &&
                        item != UserReminderConditionTypeEnum.Manufacturer &&
                        item != UserReminderConditionTypeEnum.Category)
                    {
                        model.ConditionType.Add(new SelectListItem()
                        {
                            Value = ((int)item).ToString(),
                            Text = item.ToString()
                        });

                    }
                }
            }
            return View(model);

        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult AddCondition(UserReminderModel.ConditionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
                if (userReminder == null)
                {
                    return RedirectToAction("List");
                }

                var condition = new UserReminder.ReminderCondition()
                {
                    Name = model.Name,
                    ConditionTypeId = model.ConditionTypeId,
                    ConditionId = model.ConditionId,
                };
                userReminder.Conditions.Add(condition);
                _userReminderService.UpdateUserReminder(userReminder);

                _userActivityService.InsertActivity("AddNewUserReminderCondition", userReminder.Id, _localizationService.GetResource("ActivityLog.AddNewUserReminder"), userReminder.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminder.Condition.Added"));

                return continueEditing ? RedirectToAction("EditCondition", new { userReminderId = userReminder.Id, cid = condition.Id }) : RedirectToAction("Edit", new { id = userReminder.Id });
            }

            return View(model);
        }


        public IActionResult EditCondition(string userReminderId, string cid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            if (userReminder == null)
                return RedirectToAction("List");

            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == cid);
            if (condition == null)
                return RedirectToAction("List");

            var model = condition.ToModel();
            model.UserReminderId = userReminder.Id;
            foreach (UserReminderConditionTypeEnum item in Enum.GetValues(typeof(UserReminderConditionTypeEnum)))
            {
                model.ConditionType.Add(new SelectListItem()
                {
                    Value = ((int)item).ToString(),
                    Text = item.ToString()
                });
            }
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult EditCondition(string userReminderId, string cid, UserReminderModel.ConditionModel model, bool continueEditing)
        {
            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            if (userReminder == null)
                return RedirectToAction("List");

            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == cid);
            if (condition == null)
                return RedirectToAction("List");
            try
            {
                if (ModelState.IsValid)
                {
                    condition = model.ToEntity(condition);
                    _userReminderService.UpdateUserReminder(userReminder);

                    _userActivityService.InsertActivity("EditUserReminderCondition", userReminder.Id, _localizationService.GetResource("ActivityLog.EditUserReminderCondition"), userReminder.Name);

                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminderCondition.Updated"));
                    return continueEditing ? RedirectToAction("EditCondition", new { userReminderId = userReminder.Id, cid = condition.Id }) : RedirectToAction("Edit", new { id = userReminder.Id });
                }
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = userReminder.Id });
            }
        }

        [HttpPost]
        public IActionResult ConditionDelete(string Id, string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == Id);
            userReminder.Conditions.Remove(condition);
            _userReminderService.UpdateUserReminder(userReminder);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult ConditionDeletePosition(string id, string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.Product)
            {
                condition.Products.Remove(id);
                _userReminderService.UpdateUserReminder(userReminder);
            }
            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.Category)
            {
                condition.Categories.Remove(id);
                _userReminderService.UpdateUserReminder(userReminder);
            }
            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.Manufacturer)
            {
                condition.Manufacturers.Remove(id);
                _userReminderService.UpdateUserReminder(userReminder);
            }

            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.UserRole)
            {
                condition.UserRoles.Remove(id);
                _userReminderService.UpdateUserReminder(userReminder);
            }
            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.UserTag)
            {
                condition.UserTags.Remove(id);
                _userReminderService.UpdateUserReminder(userReminder);
            }

            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.CustomUserAttribute)
            {
                condition.CustomUserAttributes.Remove(condition.CustomUserAttributes.FirstOrDefault(x => x.Id == id));
                _userReminderService.UpdateUserReminder(userReminder);
            }
            if (condition.ConditionTypeId == (int)UserReminderConditionTypeEnum.UserRegisterField)
            {
                condition.UserRegistration.Remove(condition.UserRegistration.FirstOrDefault(x => x.Id == id));
                _userReminderService.UpdateUserReminder(userReminder);
            }


            return new NullJsonResult();
        }

        #region Condition Category

        [HttpPost]
        public IActionResult ConditionCategory(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Categories.Select(z => new { Id = z, CategoryName = _categoryService.GetCategoryById(z) != null ? _categoryService.GetCategoryById(z).Name : "" }) : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        public IActionResult CategoryAddPopup(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var model = new UserReminderModel.ConditionModel.AddCategoryConditionModel();
            model.ConditionId = conditionId;
            model.UserReminderId = userReminderId;
            return View(model);
        }

        [HttpPost]
        public IActionResult CategoryAddPopupList(DataSourceRequest command, UserReminderModel.ConditionModel.AddCategoryConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var categories = _categoryService.GetAllCategories(model.SearchCategoryName,
                pageIndex: command.Page - 1, pageSize: command.PageSize, showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = categories.Select(x =>
                {
                    var categoryModel = x.ToModel();
                    categoryModel.Breadcrumb = x.GetFormattedBreadCrumb(_categoryService);
                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult CategoryAddPopup(string btnId, UserReminderModel.ConditionModel.AddCategoryConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            if (model.SelectedCategoryIds != null)
            {
                foreach (string id in model.SelectedCategoryIds)
                {
                    var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
                    if (userReminder != null)
                    {
                        var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                        if (condition != null)
                        {
                            if (condition.Categories.Where(x => x == id).Count() == 0)
                            {
                                condition.Categories.Add(id);
                                _userReminderService.UpdateUserReminder(userReminder);
                            }
                        }
                    }
                }
            }

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion


        #region Condition Manufacturer
        [HttpPost]
        public IActionResult ConditionManufacturer(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Manufacturers.Select(z => new { Id = z, ManufacturerName = _manufacturerService.GetManufacturerById(z) != null ? _manufacturerService.GetManufacturerById(z).Name : "" }) : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        public IActionResult ManufacturerAddPopup(string userReminderId, string conditionId)
        {
            var model = new UserReminderModel.ConditionModel.AddManufacturerConditionModel();
            model.ConditionId = conditionId;
            model.UserReminderId = userReminderId;
            return View(model);
        }

        [HttpPost]
        public IActionResult ManufacturerAddPopupList(DataSourceRequest command, UserReminderModel.ConditionModel.AddManufacturerConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var manufacturers = _manufacturerService.GetAllManufacturers(model.SearchManufacturerName, "",
                command.Page - 1, command.PageSize, true);
            var gridModel = new DataSourceResult
            {
                Data = manufacturers.Select(x => x.ToModel()),
                Total = manufacturers.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult ManufacturerAddPopup(string btnId, UserReminderModel.ConditionModel.AddManufacturerConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            if (model.SelectedManufacturerIds != null)
            {
                foreach (string id in model.SelectedManufacturerIds)
                {
                    var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
                    if (userReminder != null)
                    {
                        var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                        if (condition != null)
                        {
                            if (condition.Manufacturers.Where(x => x == id).Count() == 0)
                            {
                                condition.Manufacturers.Add(id);
                                _userReminderService.UpdateUserReminder(userReminder);
                            }
                        }
                    }

                }
            }

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion

        #region Condition Product

        [HttpPost]
        public IActionResult ConditionProduct(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Products.Select(z => new { Id = z, ProductName = _productService.GetProductById(z) != null ? _productService.GetProductById(z).Name : "" }) : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        public IActionResult ProductAddPopup(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var model = new UserReminderModel.ConditionModel.AddProductToConditionModel();
            model.ConditionId = conditionId;
            model.UserReminderId = userReminderId;
            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });

            return View(model);
        }

        [HttpPost]
        public IActionResult ProductAddPopupList(DataSourceRequest command, UserActionConditionModel.AddProductToConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var searchCategoryIds = new List<string>();
            if (!String.IsNullOrEmpty(model.SearchCategoryId))
                searchCategoryIds.Add(model.SearchCategoryId);

            var products = _productService.SearchProducts(
                categoryIds: searchCategoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
                );
            var gridModel = new DataSourceResult();
            gridModel.Data = products.Select(x => x.ToModel());
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult ProductAddPopup(string btnId, UserReminderModel.ConditionModel.AddProductToConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            if (model.SelectedProductIds != null)
            {
                foreach (string id in model.SelectedProductIds)
                {
                    var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
                    if (userReminder != null)
                    {
                        var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                        if (condition != null)
                        {
                            if (condition.Products.Where(x => x == id).Count() == 0)
                            {
                                condition.Products.Add(id);
                                _userReminderService.UpdateUserReminder(userReminder);
                            }
                        }
                    }
                }
            }
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion

        #region User Tags

        [HttpPost]
        public IActionResult ConditionUserTag(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserTags.Select(z => new { Id = z, UserTag = _userTagService.GetUserTagById(z) != null ? _userTagService.GetUserTagById(z).Name : "" }) : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }



        [HttpPost]
        public IActionResult ConditionUserTagInsert(UserReminderModel.ConditionModel.AddUserTagConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.UserTags.Where(x => x == model.UserTagId).Count() == 0)
                    {
                        condition.UserTags.Add(model.UserTagId);
                        _userReminderService.UpdateUserReminder(userReminder);
                    }
                }
            }
            return new NullJsonResult();
        }


        [HttpGet]
        public IActionResult UserTags()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var userTag = _userTagService.GetAllUserTags().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userTag);
        }
        #endregion

        #region Condition User role

        [HttpPost]
        public IActionResult ConditionUserRole(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserRoles.Select(z => new { Id = z, UserRole = _userService.GetUserRoleById(z) != null ? _userService.GetUserRoleById(z).Name : "" }) : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUserRoleInsert(UserReminderModel.ConditionModel.AddUserRoleConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.UserRoles.Where(x => x == model.UserRoleId).Count() == 0)
                    {
                        condition.UserRoles.Add(model.UserRoleId);
                        _userReminderService.UpdateUserReminder(userReminder);
                    }
                }
            }
            return new NullJsonResult();
        }


        [HttpGet]
        public IActionResult UserRoles()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var userRole = _userService.GetAllUserRoles().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userRole);
        }

        #endregion

        #region Condition User Register


        [HttpPost]
        public IActionResult ConditionUserRegister(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserRegistration.Select(z => new
                {
                    Id = z.Id,
                    UserRegisterName = z.RegisterField,
                    UserRegisterValue = z.RegisterValue
                })
                    : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUserRegisterInsert(UserReminderModel.ConditionModel.AddUserRegisterConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _cr = new UserReminder.ReminderCondition.UserRegister()
                    {
                        RegisterField = model.UserRegisterName,
                        RegisterValue = model.UserRegisterValue,
                    };
                    condition.UserRegistration.Add(_cr);
                    _userReminderService.UpdateUserReminder(userReminder);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionUserRegisterUpdate(UserReminderModel.ConditionModel.AddUserRegisterConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var cr = condition.UserRegistration.FirstOrDefault(x => x.Id == model.Id);
                    cr.RegisterField = model.UserRegisterName;
                    cr.RegisterValue = model.UserRegisterValue;
                    _userReminderService.UpdateUserReminder(userReminder);
                }
            }
            return new NullJsonResult();
        }

        [HttpGet]
        public IActionResult UserRegisterFields()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var list = new List<Tuple<string, string>>();
            list.Add(Tuple.Create("Gender", "Gender"));
            list.Add(Tuple.Create("Company", "Company"));
            list.Add(Tuple.Create("CountryId", "CountryId"));
            list.Add(Tuple.Create("City", "City"));
            list.Add(Tuple.Create("StateProvinceId", "StateProvinceId"));
            list.Add(Tuple.Create("StreetAddress", "StreetAddress"));
            list.Add(Tuple.Create("ZipPostalCode", "ZipPostalCode"));
            list.Add(Tuple.Create("Phone", "Phone"));
            list.Add(Tuple.Create("Fax", "Fax"));

            var user = list.Select(x => new { Id = x.Item1, Name = x.Item2 });
            return Json(user);
        }
        #endregion

        #region Condition Custom User Attribute

        private string UserAttribute(string registerField)
        {
            string _field = registerField;
            var _rf = registerField.Split(':');
            if (_rf.Count() > 1)
            {
                var ca = _userAttributeService.GetUserAttributeById(_rf.FirstOrDefault());
                if (ca != null)
                {
                    _field = ca.Name;
                    if (ca.UserAttributeValues.FirstOrDefault(x => x.Id == _rf.LastOrDefault()) != null)
                    {
                        _field = ca.Name + "->" + ca.UserAttributeValues.FirstOrDefault(x => x.Id == _rf.LastOrDefault()).Name;
                    }
                }

            }

            return _field;
        }

        [HttpPost]
        public IActionResult ConditionCustomUserAttribute(string userReminderId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.CustomUserAttributes.Select(z => new
                {
                    Id = z.Id,
                    UserAttributeId = UserAttribute(z.RegisterField),
                    UserAttributeName = z.RegisterField,
                    UserAttributeValue = z.RegisterValue
                })
                    : null,
                Total = userReminder.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionCustomUserAttributeInsert(UserReminderModel.ConditionModel.AddCustomUserAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _cr = new UserReminder.ReminderCondition.UserRegister()
                    {
                        RegisterField = model.UserAttributeName,
                        RegisterValue = model.UserAttributeValue,
                    };
                    condition.CustomUserAttributes.Add(_cr);
                    _userReminderService.UpdateUserReminder(userReminder);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionCustomUserAttributeUpdate(UserReminderModel.ConditionModel.AddCustomUserAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder != null)
            {
                var condition = userReminder.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var cr = condition.CustomUserAttributes.FirstOrDefault(x => x.Id == model.Id);
                    cr.RegisterField = model.UserAttributeName;
                    cr.RegisterValue = model.UserAttributeValue;
                    _userReminderService.UpdateUserReminder(userReminder);
                }
            }
            return new NullJsonResult();
        }

        [HttpGet]
        public IActionResult CustomUserAttributeFields()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var list = new List<Tuple<string, string>>();
            foreach (var item in _userAttributeService.GetAllUserAttributes())
            {
                if (item.AttributeControlType == AttributeControlType.Checkboxes ||
                    item.AttributeControlType == AttributeControlType.DropdownList ||
                    item.AttributeControlType == AttributeControlType.RadioList)
                {
                    foreach (var value in item.UserAttributeValues)
                    {
                        list.Add(Tuple.Create(string.Format("{0}:{1}", item.Id, value.Id), item.Name + "->" + value.Name));
                    }
                }
            }
            var user = list.Select(x => new { Id = x.Item1, Name = x.Item2 });
            return Json(user);
        }
        #endregion

        #endregion


        #region Levels

        [HttpPost]
        public IActionResult Levels(string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var gridModel = new DataSourceResult
            {
                Data = userReminder.Levels.Select(x => new
                { Id = x.Id, Name = x.Name, Level = x.Level }).OrderBy(x => x.Level),
                Total = userReminder.Levels.Count()
            };
            return Json(gridModel);
        }

        public IActionResult AddLevel(string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var model = new UserReminderModel.ReminderLevelModel();
            model.UserReminderId = userReminderId;
            PrepareModel(model, userReminder);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult AddLevel(UserReminderModel.ReminderLevelModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();
            var userReminder = _userReminderService.GetUserReminderById(model.UserReminderId);
            if (userReminder == null)
            {
                return RedirectToAction("List");
            }
            if (userReminder.Levels.Where(x => x.Level == model.Level).Count() > 0)
            {
                ModelState.AddModelError("Error-LevelExists", _localizationService.GetResource("Admin.Users.UserReminderLevel.Exists"));
            }

            if (ModelState.IsValid)
            {

                var level = new UserReminder.ReminderLevel()
                {
                    Name = model.Name,
                    Level = model.Level,
                    BccEmailAddresses = model.BccEmailAddresses,
                    Body = model.Body,
                    EmailAccountId = model.EmailAccountId,
                    Subject = model.Subject,
                    Day = model.Day,
                    Hour = model.Hour,
                    Minutes = model.Minutes,
                };

                userReminder.Levels.Add(level);
                _userReminderService.UpdateUserReminder(userReminder);
                _userActivityService.InsertActivity("AddNewUserReminderLevel", userReminder.Id, _localizationService.GetResource("ActivityLog.AddNewUserReminderLevel"), userReminder.Name);
                SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminderLevel.Added"));
                return continueEditing ? RedirectToAction("EditLevel", new { userReminderId = userReminder.Id, cid = level.Id }) : RedirectToAction("Edit", new { id = userReminder.Id });
            }
            PrepareModel(model, userReminder);
            return View(model);
        }

        public IActionResult EditLevel(string userReminderId, string cid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            if (userReminder == null)
            {
                return RedirectToAction("List");
            }

            var level = userReminder.Levels.FirstOrDefault(x => x.Id == cid);
            if (level == null)
                return RedirectToAction("List");


            var model = level.ToModel();
            model.UserReminderId = userReminderId;
            PrepareModel(model, userReminder);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult EditLevel(string userReminderId, string cid, UserReminderModel.ReminderLevelModel model, bool continueEditing)
        {
            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            if (userReminder == null)
                return RedirectToAction("List");

            var level = userReminder.Levels.FirstOrDefault(x => x.Id == cid);
            if (level == null)
                return RedirectToAction("List");

            if (level.Level != model.Level)
            {
                if (userReminder.Levels.Where(x => x.Level == model.Level).Count() > 0)
                {
                    ModelState.AddModelError("Error-LevelExists", _localizationService.GetResource("Admin.Users.UserReminderLevel.Exists"));
                }
            }
            try
            {
                if (ModelState.IsValid)
                {
                    level.Level = model.Level;
                    level.Name = model.Name;
                    level.Subject = model.Subject;
                    level.BccEmailAddresses = model.BccEmailAddresses;
                    level.Body = model.Body;
                    level.EmailAccountId = model.EmailAccountId;
                    level.Day = model.Day;
                    level.Hour = model.Hour;
                    level.Minutes = model.Minutes;
                    _userReminderService.UpdateUserReminder(userReminder);

                    _userActivityService.InsertActivity("EditUserReminderCondition", userReminder.Id, _localizationService.GetResource("ActivityLog.EditUserReminderLevel"), userReminder.Name);

                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserReminderLevel.Updated"));
                    return continueEditing ? RedirectToAction("EditLevel", new { userReminderId = userReminder.Id, cid = level.Id }) : RedirectToAction("Edit", new { id = userReminder.Id });
                }

                PrepareModel(model, userReminder);
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = userReminder.Id });
            }
        }

        [HttpPost]
        public IActionResult DeleteLevel(string Id, string userReminderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReminders))
                return AccessDeniedView();

            var userReminder = _userReminderService.GetUserReminderById(userReminderId);
            var level = userReminder.Levels.FirstOrDefault(x => x.Id == Id);
            userReminder.Levels.Remove(level);
            _userReminderService.UpdateUserReminder(userReminder);

            return new NullJsonResult();
        }


        #endregion

    }
}
