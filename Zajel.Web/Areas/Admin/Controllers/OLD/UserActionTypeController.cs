﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Services.Localization;
using Zajel.Services.Security;
using Zajel.Services.Users;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class UserActionTypeController : BaseAdminController
    {
        #region Fields

        private readonly IPermissionService _permissionService;
        private readonly IUserActionService _userActionService;
        private readonly ILocalizationService _localizationService;
        #endregion Fields

        #region Constructors

        public UserActionTypeController(            
            IPermissionService permissionService,
            ILocalizationService localizationService,
            IUserActionService userActionService
            )
        {
            this._userActionService = userActionService;
            this._permissionService = permissionService;
            this._localizationService = localizationService;
        }

        #endregion

        #region Action types

        public IActionResult ListTypes()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var model = _userActionService.GetUserActionType()
                .Select(x => x.ToModel())
                .ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult SaveTypes(IFormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            string formKey = "checkbox_action_types";
            var checkedActionTypes = form[formKey].ToString() != null ? form[formKey].ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x).ToList() : new List<string>();

            var activityTypes = _userActionService.GetUserActionType();
            foreach (var actionType in activityTypes)
            {
                actionType.Enabled = checkedActionTypes.Contains(actionType.Id);
                _userActionService.UpdateUserActionType(actionType);
            }
            SuccessNotification(_localizationService.GetResource("Admin.Users.ActionType.Updated"));
            return RedirectToAction("ListTypes");
        }

        #endregion

    
    }
}
