﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Web.Areas.Admin.Extensions;
using Zajel.Web.Areas.Admin.Models.Users;
using Zajel.Core;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Users;
using Zajel.Services.Catalog;
using Zajel.Services.Users;
using Zajel.Services.Localization;
using Zajel.Services.Logging;
using Zajel.Services.Security;
using Zajel.Services.Stores;
using Zajel.Services.Vendors;
using Zajel.Framework.Controllers;
using Zajel.Framework.Kendoui;
using Zajel.Framework.Mvc;
using Zajel.Services.Messages;
using Zajel.Services.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Zajel.Framework.Mvc.Filters;
using Zajel.Framework.Extensions;

namespace Zajel.Web.Areas.Admin.Controllers
{
    public partial class UserActionController : BaseAdminController
    {
        #region Fields

        private readonly IUserService _userService;
        private readonly IUserAttributeService _userAttributeService;
        private readonly IUserTagService _userTagService;
        private readonly ILocalizationService _localizationService;
        private readonly IUserActivityService _userActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IProductService _productService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IWorkContext _workContext;
        private readonly IUserActionService _userActionService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly IBannerService _bannerService;
        private readonly IInteractiveFormService _interactiveFormService;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IDateTimeHelper _dateTimeHelper;
        #endregion

        #region Constructors

        public UserActionController(IUserService userService,
            IUserAttributeService userAttributeService,
            IUserTagService userTagService,
            ILocalizationService localizationService,
            IUserActivityService userActivityService,
            IPermissionService permissionService,
            IProductService productService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IStoreService storeService,
            IVendorService vendorService,
            IWorkContext workContext,
            IUserActionService userActionService,
            IProductAttributeService productAttributeService,
            ISpecificationAttributeService specificationAttributeService,
            IBannerService bannerService,
            IInteractiveFormService interactiveFormService,
            IMessageTemplateService messageTemplateService,
            IDateTimeHelper dateTimeHelper)
        {
            this._userService = userService;
            this._userAttributeService = userAttributeService;
            this._userTagService = userTagService;
            this._localizationService = localizationService;
            this._userActivityService = userActivityService;
            this._permissionService = permissionService;
            this._productService = productService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._workContext = workContext;
            this._userActionService = userActionService;
            this._productAttributeService = productAttributeService;
            this._specificationAttributeService = specificationAttributeService;
            this._bannerService = bannerService;
            this._interactiveFormService = interactiveFormService;
            this._messageTemplateService = messageTemplateService;
            this._dateTimeHelper = dateTimeHelper;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual void PrepareReactObjectModel(UserActionModel model)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            var banners = _bannerService.GetAllBanners();
            foreach (var item in banners)
            {
                model.Banners.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });

            }
            var message = _messageTemplateService.GetAllMessageTemplates("");
            foreach (var item in message)
            {
                model.MessageTemplates.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }
            var userRole = _userService.GetAllUserRoles();
            foreach (var item in userRole)
            {
                model.UserRoles.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }

            var userTag = _userTagService.GetAllUserTags();
            foreach (var item in userTag)
            {
                model.UserTags.Add(new SelectListItem
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }

            foreach (var item in _userActionService.GetUserActionType())
            {
                model.ActionType.Add(new SelectListItem()
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });
            }

            foreach (var item in _interactiveFormService.GetAllForms())
            {
                model.InteractiveForms.Add(new SelectListItem()
                {
                    Text = item.Name,
                    Value = item.Id.ToString()
                });

            }


        }

        public class SerializeUserActionHistory
        {
            public string Email { get; set; }
            public DateTime CreateDateUtc { get; set; }

        }
        protected virtual SerializeUserActionHistory PrepareHistoryModelForList(UserActionHistory history)
        {
            var user = _userService.GetUserById(history.UserId);
            return new SerializeUserActionHistory
            {
                Email = user != null ? String.IsNullOrEmpty(user.Email) ? "(unknown)" : user.Email : "(unknown)",
                CreateDateUtc = _dateTimeHelper.ConvertToUserTime(history.CreateDateUtc, DateTimeKind.Utc),
            };
        }

        protected void CheckValidateModel(UserActionModel model)
        {
            if ((model.ReactionType == UserReactionTypeEnum.Banner) && String.IsNullOrEmpty(model.BannerId))
                ModelState.AddModelError("error", "Banner is required");
            if ((model.ReactionType == UserReactionTypeEnum.InteractiveForm) && String.IsNullOrEmpty(model.InteractiveFormId))
                ModelState.AddModelError("error", "Interactive form is required");
            if ((model.ReactionType == UserReactionTypeEnum.Email) && String.IsNullOrEmpty(model.MessageTemplateId))
                ModelState.AddModelError("error", "Email is required");
            if ((model.ReactionType == UserReactionTypeEnum.AssignToUserRole) && String.IsNullOrEmpty(model.UserRoleId))
                ModelState.AddModelError("error", "User role is required");
            if ((model.ReactionType == UserReactionTypeEnum.AssignToUserTag) && String.IsNullOrEmpty(model.UserTagId))
                ModelState.AddModelError("error", "Tag is required");
        }

        #endregion


        #region User Actions

        public IActionResult Index()
        {
            return RedirectToAction("List");
        }

        public IActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public IActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var useractions = _userActionService.GetUserActions();
            var gridModel = new DataSourceResult
            {
                Data = useractions.Select(x => new { Id = x.Id, Name = x.Name, Active = x.Active, ActionType = _userActionService.GetUserActionTypeById(x.ActionTypeId).Name }),
                Total = useractions.Count()
            };
            return Json(gridModel);
        }

        public IActionResult Create()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var model = new UserActionModel();
            model.Active = true;
            model.StartDateTimeUtc = DateTime.UtcNow;
            model.EndDateTimeUtc = DateTime.UtcNow.AddMonths(1);
            model.ReactionTypeId = (int)UserReactionTypeEnum.Banner;
            PrepareReactObjectModel(model);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Create(UserActionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            CheckValidateModel(model);
            if (ModelState.IsValid)
            {
                var useraction = model.ToEntity();
                _userActionService.InsertUserAction(useraction);
                _userActivityService.InsertActivity("AddNewUserAction", useraction.Id, _localizationService.GetResource("ActivityLog.AddNewUserAction"), useraction.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserAction.Added"));
                return continueEditing ? RedirectToAction("Edit", new { id = useraction.Id }) : RedirectToAction("List");
            }
            PrepareReactObjectModel(model);
            return View(model);
        }

        public IActionResult Edit(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(id);
            if (userAction == null)
                return RedirectToAction("List");

            var model = userAction.ToModel();
            model.ConditionCount = userAction.Conditions.Count();
            PrepareReactObjectModel(model);


            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult Edit(UserActionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            CheckValidateModel(model);

            var useraction = _userActionService.GetUserActionById(model.Id);
            if (useraction == null)
                return RedirectToAction("List");
            try
            {
                if (ModelState.IsValid)
                {
                    if (useraction.Conditions.Count() > 0)
                        model.ActionTypeId = useraction.ActionTypeId;
                    if (String.IsNullOrEmpty(model.ActionTypeId))
                        model.ActionTypeId = useraction.ActionTypeId;

                    useraction = model.ToEntity(useraction);


                    _userActionService.UpdateUserAction(useraction);

                    _userActivityService.InsertActivity("EditUserAction", useraction.Id, _localizationService.GetResource("ActivityLog.EditUserAction"), useraction.Name);

                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserAction.Updated"));
                    return continueEditing ? RedirectToAction("Edit", new { id = useraction.Id }) : RedirectToAction("List");
                }
                PrepareReactObjectModel(model);
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = useraction.Id });
            }
        }

        [HttpPost]
        public IActionResult History(DataSourceRequest command, string userActionId)
        {
            //we use own own binder for searchUserRoleIds property 
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var history = _userActionService.GetAllUserActionHistory(userActionId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = history.Select(PrepareHistoryModelForList),
                Total = history.TotalCount
            };

            return Json(gridModel);
        }


        [HttpPost]
        public IActionResult Delete(string id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(id);
            if (userAction == null)
                return RedirectToAction("List");

            try
            {
                //activity log
                _userActivityService.InsertActivity("DeleteUserAction", userAction.Id, _localizationService.GetResource("ActivityLog.DeleteUserAction"), userAction.Name);

                _userActionService.DeleteUserAction(userAction);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserAction.Deleted"));
                return RedirectToAction("List");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc.Message);
                return RedirectToAction("Edit", new { id = userAction.Id });
            }
        }

        [HttpPost]
        public IActionResult Conditions(string userActionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var conditions = _userActionService.GetUserActionById(userActionId);
            var gridModel = new DataSourceResult
            {
                Data = conditions.Conditions.Select(x => new { Id = x.Id, Name = x.Name, Condition = x.UserActionConditionType.ToString() }),
                Total = conditions.Conditions.Count()
            };
            return Json(gridModel);
        }

        #endregion

        #region Conditions

        public IActionResult AddCondition(string userActionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(userActionId);
            var actionType = _userActionService.GetUserActionTypeById(userAction.ActionTypeId);

            var model = new UserActionConditionModel();
            model.UserActionId = userActionId;

            foreach (var item in actionType.ConditionType)
            {
                model.UserActionConditionType.Add(new SelectListItem()
                {
                    Value = item.ToString(),
                    Text = ((UserActionConditionTypeEnum)item).ToString()
                });
            }


            return View(model);

        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult AddCondition(UserActionConditionModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var userAction = _userActionService.GetUserActionById(model.UserActionId);
                if (userAction == null)
                {
                    return RedirectToAction("List");
                }

                var condition = new UserAction.ActionCondition()
                {
                    Name = model.Name,
                    UserActionConditionTypeId = model.UserActionConditionTypeId,
                    ConditionId = model.ConditionId,
                };
                userAction.Conditions.Add(condition);
                _userActionService.UpdateUserAction(userAction);

                _userActivityService.InsertActivity("AddNewUserActionCondition", userAction.Id, _localizationService.GetResource("ActivityLog.AddNewUserAction"), userAction.Name);

                SuccessNotification(_localizationService.GetResource("Admin.Users.UserActionCondition.Added"));

                return continueEditing ? RedirectToAction("EditCondition", new { userActionId = userAction.Id, cid = condition.Id }) : RedirectToAction("Edit", new { id = userAction.Id });
            }

            return View(model);
        }

        public IActionResult EditCondition(string userActionId, string cid)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(userActionId);
            if (userAction == null)
                return RedirectToAction("List");
            var actionType = _userActionService.GetUserActionTypeById(userAction.ActionTypeId);
            var condition = userAction.Conditions.FirstOrDefault(x => x.Id == cid);
            if (condition == null)
                return RedirectToAction("List");

            var model = condition.ToModel();
            model.UserActionId = userActionId;

            foreach (var item in actionType.ConditionType)
            {
                model.UserActionConditionType.Add(new SelectListItem()
                {
                    Value = item.ToString(),
                    Text = ((UserActionConditionTypeEnum)item).ToString()
                });
            }

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public IActionResult EditCondition(string userActionId, string cid, UserActionConditionModel model, bool continueEditing)
        {
            var userAction = _userActionService.GetUserActionById(userActionId);
            if (userAction == null)
                return RedirectToAction("List");

            var condition = userAction.Conditions.FirstOrDefault(x => x.Id == cid);
            if (condition == null)
                return RedirectToAction("List");
            try
            {
                if (ModelState.IsValid)
                {
                    condition = model.ToEntity(condition);
                    _userActionService.UpdateUserAction(userAction);
                    //activity log
                    _userActivityService.InsertActivity("EditUserActionCondition", userAction.Id, _localizationService.GetResource("ActivityLog.EditUserActionCondition"), userAction.Name);
                    SuccessNotification(_localizationService.GetResource("Admin.Users.UserActionCondition.Updated"));
                    return continueEditing ? RedirectToAction("EditCondition", new { userActionId = userAction.Id, cid = condition.Id }) : RedirectToAction("Edit", new { id = userAction.Id });
                }
                return View(model);
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("Edit", new { id = userAction.Id });
            }
        }

        [HttpPost]
        public IActionResult ConditionDelete(string Id, string userActionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(userActionId);
            var condition = userAction.Conditions.FirstOrDefault(x => x.Id == Id);
            userAction.Conditions.Remove(condition);
            _userActionService.UpdateUserAction(userAction);

            return new NullJsonResult();
        }

        [HttpPost]
        public IActionResult ConditionDeletePosition(string id, string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.Product)
            {
                condition.Products.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.Category)
            {
                condition.Categories.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.Manufacturer)
            {
                condition.Manufacturers.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.Vendor)
            {
                condition.Vendors.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.ProductAttribute)
            {
                condition.ProductAttribute.Remove(condition.ProductAttribute.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.ProductSpecification)
            {
                condition.ProductSpecifications.Remove(condition.ProductSpecifications.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.UserRole)
            {
                condition.UserRoles.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.UserTag)
            {
                condition.UserTags.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }

            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.CustomUserAttribute)
            {
                condition.CustomUserAttributes.Remove(condition.CustomUserAttributes.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.UserRegisterField)
            {
                condition.UserRegistration.Remove(condition.UserRegistration.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.UrlCurrent)
            {
                condition.UrlCurrent.Remove(condition.UrlCurrent.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.UrlReferrer)
            {
                condition.UrlReferrer.Remove(condition.UrlReferrer.FirstOrDefault(x => x.Id == id));
                _userActionService.UpdateUserAction(userActions);
            }
            if (condition.UserActionConditionTypeId == (int)UserActionConditionTypeEnum.Store)
            {
                condition.Stores.Remove(id);
                _userActionService.UpdateUserAction(userActions);
            }


            return new NullJsonResult();
        }
        #endregion

        #region Condition Product

        [HttpPost]
        public IActionResult ConditionProduct(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Products.Select(z => new { Id = z, ProductName = _productService.GetProductById(z) != null ? _productService.GetProductById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }


        public IActionResult ProductAddPopup(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var model = new UserActionConditionModel.AddProductToConditionModel();
            model.UserActionConditionId = conditionId;
            model.UserActionId = userActionId;
            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = " " });

            return View(model);
        }

        [HttpPost]
        public IActionResult ProductAddPopupList(DataSourceRequest command, UserActionConditionModel.AddProductToConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var searchCategoryIds = new List<string>();
            if (!String.IsNullOrEmpty(model.SearchCategoryId))
                searchCategoryIds.Add(model.SearchCategoryId);

            var products = _productService.SearchProducts(
                categoryIds: searchCategoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
                );
            var gridModel = new DataSourceResult();
            gridModel.Data = products.Select(x => x.ToModel());
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult ProductAddPopup(string btnId, UserActionConditionModel.AddProductToConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (model.SelectedProductIds != null)
            {
                foreach (string id in model.SelectedProductIds)
                {
                    var userAction = _userActionService.GetUserActionById(model.UserActionId);
                    if (userAction != null)
                    {
                        var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.UserActionConditionId);
                        if (condition != null)
                        {
                            if (condition.Products.Where(x => x == id).Count() == 0)
                            {
                                condition.Products.Add(id);
                                _userActionService.UpdateUserAction(userAction);
                            }
                        }
                    }
                }
            }
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }
        #endregion

        #region Condition Category

        [HttpPost]
        public IActionResult ConditionCategory(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Categories.Select(z => new { Id = z, CategoryName = _categoryService.GetCategoryById(z) != null ? _categoryService.GetCategoryById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        public IActionResult CategoryAddPopup(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var model = new UserActionConditionModel.AddCategoryConditionModel();
            model.UserActionConditionId = conditionId;
            model.UserActionId = userActionId;
            return View(model);
        }

        [HttpPost]
        public IActionResult CategoryAddPopupList(DataSourceRequest command, UserActionConditionModel.AddCategoryConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var categories = _categoryService.GetAllCategories(model.SearchCategoryName,
                pageIndex: command.Page - 1, pageSize: command.PageSize, showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = categories.Select(x =>
                {
                    var categoryModel = x.ToModel();
                    categoryModel.Breadcrumb = x.GetFormattedBreadCrumb(_categoryService);
                    return categoryModel;
                }),
                Total = categories.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult CategoryAddPopup(string btnId, UserActionConditionModel.AddCategoryConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (model.SelectedCategoryIds != null)
            {
                foreach (string id in model.SelectedCategoryIds)
                {
                    var userAction = _userActionService.GetUserActionById(model.UserActionId);
                    if (userAction != null)
                    {
                        var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.UserActionConditionId);
                        if (condition != null)
                        {
                            if (condition.Categories.Where(x => x == id).Count() == 0)
                            {
                                condition.Categories.Add(id);
                                _userActionService.UpdateUserAction(userAction);
                            }
                        }
                    }
                }
            }

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion

        #region Condition Manufacturer
        [HttpPost]
        public IActionResult ConditionManufacturer(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Manufacturers.Select(z => new { Id = z, ManufacturerName = _manufacturerService.GetManufacturerById(z) != null ? _manufacturerService.GetManufacturerById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        public IActionResult ManufacturerAddPopup(string userActionId, string conditionId)
        {
            var model = new UserActionConditionModel.AddManufacturerConditionModel();
            model.UserActionConditionId = conditionId;
            model.UserActionId = userActionId;
            return View(model);

        }

        [HttpPost]
        public IActionResult ManufacturerAddPopupList(DataSourceRequest command, UserActionConditionModel.AddManufacturerConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var manufacturers = _manufacturerService.GetAllManufacturers(model.SearchManufacturerName, "",
                command.Page - 1, command.PageSize, true);
            var gridModel = new DataSourceResult
            {
                Data = manufacturers.Select(x => x.ToModel()),
                Total = manufacturers.TotalCount
            };

            return Json(gridModel);
        }

        [HttpPost]
        [FormValueRequired("save")]
        public IActionResult ManufacturerAddPopup(string btnId, UserActionConditionModel.AddManufacturerConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (model.SelectedManufacturerIds != null)
            {
                foreach (string id in model.SelectedManufacturerIds)
                {
                    var userAction = _userActionService.GetUserActionById(model.UserActionId);
                    if (userAction != null)
                    {
                        var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.UserActionConditionId);
                        if (condition != null)
                        {
                            if (condition.Manufacturers.Where(x => x == id).Count() == 0)
                            {
                                condition.Manufacturers.Add(id);
                                _userActionService.UpdateUserAction(userAction);
                            }
                        }
                    }

                }
            }

            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            return View(model);
        }


        #endregion

        #region Condition Vendor

        [HttpPost]
        public IActionResult ConditionVendor(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Vendors.Select(z => new { Id = z, VendorName = _vendorService.GetVendorById(z).Name }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionVendorInsert(UserActionConditionModel.AddVendorConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.Vendors.Where(x => x == model.VendorId).Count() == 0)
                    {
                        condition.Vendors.Add(model.VendorId);
                        _userActionService.UpdateUserAction(userAction);
                    }
                }
            }
            return new NullJsonResult();
        }


        [HttpGet]
        public IActionResult Vendors()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var userVendors = _vendorService.GetAllVendors().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userVendors);
        }
        #endregion

        #region Condition User role

        [HttpPost]
        public IActionResult ConditionUserRole(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserRoles.Select(z => new { Id = z, UserRole = _userService.GetUserRoleById(z) != null ? _userService.GetUserRoleById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUserRoleInsert(UserActionConditionModel.AddUserRoleConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.UserRoles.Where(x => x == model.UserRoleId).Count() == 0)
                    {
                        condition.UserRoles.Add(model.UserRoleId);
                        _userActionService.UpdateUserAction(userAction);
                    }
                }
            }
            return new NullJsonResult();
        }


        [HttpGet]
        public IActionResult UserRoles()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var userRole = _userService.GetAllUserRoles().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userRole);
        }

        #endregion

        #region Stores

        [HttpGet]
        public IActionResult Stores()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var stores = _storeService.GetAllStores().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(stores);
        }

        [HttpPost]
        public IActionResult ConditionStore(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.Stores.Select(z => new { Id = z, Store = _storeService.GetStoreById(z) != null ? _storeService.GetStoreById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionStoreInsert(UserActionConditionModel.AddStoreConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.Stores.Where(x => x == model.StoreId).Count() == 0)
                    {
                        condition.Stores.Add(model.StoreId);
                        _userActionService.UpdateUserAction(userAction);
                    }
                }
            }
            return new NullJsonResult();
        }


        #endregion

        #region User Tags

        [HttpPost]
        public IActionResult ConditionUserTag(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserTags.Select(z => new { Id = z, UserTag = _userTagService.GetUserTagById(z) != null ? _userTagService.GetUserTagById(z).Name : "" }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }



        [HttpPost]
        public IActionResult ConditionUserTagInsert(UserActionConditionModel.AddUserTagConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.UserTags.Where(x => x == model.UserTagId).Count() == 0)
                    {
                        condition.UserTags.Add(model.UserTagId);
                        _userActionService.UpdateUserAction(userAction);
                    }
                }
            }
            return new NullJsonResult();
        }


        [HttpGet]
        public IActionResult UserTags()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var userTag = _userTagService.GetAllUserTags().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userTag);
        }
        #endregion


        #region Condition Product Attributes

        [HttpPost]
        public IActionResult ConditionProductAttribute(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.ProductAttribute.Select(z => new { Id = z.Id, ProductAttributeId = z.ProductAttributeId, ProductAttributeName = _productAttributeService.GetProductAttributeById(z.ProductAttributeId).Name, Name = z.Name }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionProductAttributeInsert(UserActionConditionModel.AddProductAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _pv = new UserAction.ActionCondition.ProductAttributeValue()
                    {
                        ProductAttributeId = model.ProductAttributeId,
                        Name = model.Name
                    };
                    condition.ProductAttribute.Add(_pv);
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionProductAttributeUpdate(UserActionConditionModel.AddProductAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var pva = condition.ProductAttribute.FirstOrDefault(x => x.Id == model.Id);
                    pva.ProductAttributeId = model.ProductAttributeId;
                    pva.Name = model.Name;
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }



        [HttpGet]
        public IActionResult ProductAttributes()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var userAttr = _productAttributeService.GetAllProductAttributes().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userAttr);
        }
        #endregion

        #region Condition Product Specification

        [HttpPost]
        public IActionResult ConditionProductSpecification(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.ProductSpecifications
                        .Select(z => new
                        {
                            Id = z.Id,
                            SpecificationId = z.ProductSpecyficationId,
                            SpecificationName = _specificationAttributeService.GetSpecificationAttributeById(z.ProductSpecyficationId).Name,
                            SpecificationValueName = !String.IsNullOrEmpty(z.ProductSpecyficationValueId) ? _specificationAttributeService.GetSpecificationAttributeById(z.ProductSpecyficationId).SpecificationAttributeOptions.FirstOrDefault(x => x.Id == z.ProductSpecyficationValueId).Name : "Undefined"
                        }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }


        [HttpPost]
        public IActionResult ConditionProductSpecificationInsert(UserActionConditionModel.AddProductSpecificationConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    if (condition.ProductSpecifications.Where(x => x.ProductSpecyficationId == model.SpecificationId && x.ProductSpecyficationValueId == model.SpecificationValueId).Count() == 0)
                    {
                        var _ps = new UserAction.ActionCondition.ProductSpecification()
                        {
                            ProductSpecyficationId = model.SpecificationId,
                            ProductSpecyficationValueId = model.SpecificationValueId
                        };
                        condition.ProductSpecifications.Add(_ps);
                        _userActionService.UpdateUserAction(userAction);
                    }
                }
            }
            return new NullJsonResult();
        }

        [HttpGet]
        public IActionResult ProductSpecification()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var userAttr = _specificationAttributeService.GetSpecificationAttributes().Select(x => new { Id = x.Id, Name = x.Name });
            return Json(userAttr);
        }

        [HttpGet]
        public IActionResult ProductSpecificationValue(string specificationId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            if (String.IsNullOrEmpty(specificationId))
                return new NullJsonResult();

            var userSpec = _specificationAttributeService.GetSpecificationAttributeById(specificationId).SpecificationAttributeOptions.Select(x => new { Id = x.Id, Name = x.Name });

            return Json(userSpec);
        }

        #endregion

        #region Condition User Register

        [HttpPost]
        public IActionResult ConditionUserRegister(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UserRegistration.Select(z => new
                {
                    Id = z.Id,
                    UserRegisterName = z.RegisterField,
                    UserRegisterValue = z.RegisterValue
                })
                    : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUserRegisterInsert(UserActionConditionModel.AddUserRegisterConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _cr = new UserAction.ActionCondition.UserRegister()
                    {
                        RegisterField = model.UserRegisterName,
                        RegisterValue = model.UserRegisterValue,
                    };
                    condition.UserRegistration.Add(_cr);
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionUserRegisterUpdate(UserActionConditionModel.AddUserRegisterConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var cr = condition.UserRegistration.FirstOrDefault(x => x.Id == model.Id);
                    cr.RegisterField = model.UserRegisterName;
                    cr.RegisterValue = model.UserRegisterValue;
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }

        [HttpGet]
        public IActionResult UserRegisterFields()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var list = new List<Tuple<string, string>>();
            list.Add(Tuple.Create("Gender", "Gender"));
            list.Add(Tuple.Create("Company", "Company"));
            list.Add(Tuple.Create("CountryId", "CountryId"));
            list.Add(Tuple.Create("City", "City"));
            list.Add(Tuple.Create("StateProvinceId", "StateProvinceId"));
            list.Add(Tuple.Create("StreetAddress", "StreetAddress"));
            list.Add(Tuple.Create("ZipPostalCode", "ZipPostalCode"));
            list.Add(Tuple.Create("Phone", "Phone"));
            list.Add(Tuple.Create("Fax", "Fax"));

            var user = list.Select(x => new { Id = x.Item1, Name = x.Item2 });
            return Json(user);
        }
        #endregion

        #region Condition Custom User Attribute

        private string UserAttribute(string registerField)
        {
            string _field = registerField;
            var _rf = registerField.Split(':');
            if (_rf.Count() > 1)
            {
                var ca = _userAttributeService.GetUserAttributeById(_rf.FirstOrDefault());
                if (ca != null)
                {
                    _field = ca.Name;
                    if (ca.UserAttributeValues.FirstOrDefault(x => x.Id == _rf.LastOrDefault()) != null)
                    {
                        _field = ca.Name + "->" + ca.UserAttributeValues.FirstOrDefault(x => x.Id == _rf.LastOrDefault()).Name;
                    }
                }

            }

            return _field;
        }

        [HttpPost]
        public IActionResult ConditionCustomUserAttribute(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.CustomUserAttributes.Select(z => new
                {
                    Id = z.Id,
                    UserAttributeId = UserAttribute(z.RegisterField),
                    UserAttributeName = z.RegisterField,
                    UserAttributeValue = z.RegisterValue
                })
                    : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionCustomUserAttributeInsert(UserActionConditionModel.AddCustomUserAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _cr = new UserAction.ActionCondition.UserRegister()
                    {
                        RegisterField = model.UserAttributeName,
                        RegisterValue = model.UserAttributeValue,
                    };
                    condition.CustomUserAttributes.Add(_cr);
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionCustomUserAttributeUpdate(UserActionConditionModel.AddCustomUserAttributeConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var cr = condition.CustomUserAttributes.FirstOrDefault(x => x.Id == model.Id);
                    cr.RegisterField = model.UserAttributeName;
                    cr.RegisterValue = model.UserAttributeValue;
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }

        [HttpGet]
        public IActionResult CustomUserAttributeFields()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();
            var list = new List<Tuple<string, string>>();
            foreach (var item in _userAttributeService.GetAllUserAttributes())
            {
                if (item.AttributeControlType == AttributeControlType.Checkboxes ||
                    item.AttributeControlType == AttributeControlType.DropdownList ||
                    item.AttributeControlType == AttributeControlType.RadioList)
                {
                    foreach (var value in item.UserAttributeValues)
                    {
                        list.Add(Tuple.Create(string.Format("{0}:{1}", item.Id, value.Id), item.Name + "->" + value.Name));
                    }
                }
            }
            var user = list.Select(x => new { Id = x.Item1, Name = x.Item2 });
            return Json(user);
        }
        #endregion


        #region Url Referrer

        [HttpPost]
        public IActionResult ConditionUrlReferrer(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UrlReferrer.Select(z => new { Id = z.Id, Name = z.Name }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUrlReferrerInsert(UserActionConditionModel.AddUrlConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _url = new UserAction.ActionCondition.Url()
                    {
                        Name = model.Name
                    };
                    condition.UrlReferrer.Add(_url);
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionUrlReferrerUpdate(UserActionConditionModel.AddUrlConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _url = condition.UrlReferrer.FirstOrDefault(x => x.Id == model.Id);
                    _url.Name = model.Name;
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }

        #endregion

        #region Url Current

        [HttpPost]
        public IActionResult ConditionUrlCurrent(string userActionId, string conditionId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userActions = _userActionService.GetUserActionById(userActionId);
            var condition = userActions.Conditions.FirstOrDefault(x => x.Id == conditionId);

            var gridModel = new DataSourceResult
            {
                Data = condition != null ? condition.UrlCurrent.Select(z => new { Id = z.Id, Name = z.Name }) : null,
                Total = userActions.Conditions.Where(x => x.Id == conditionId).Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public IActionResult ConditionUrlCurrentInsert(UserActionConditionModel.AddUrlConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _url = new UserAction.ActionCondition.Url()
                    {
                        Name = model.Name
                    };
                    condition.UrlCurrent.Add(_url);
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }
        [HttpPost]
        public IActionResult ConditionUrlCurrentUpdate(UserActionConditionModel.AddUrlConditionModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageActions))
                return AccessDeniedView();

            var userAction = _userActionService.GetUserActionById(model.UserActionId);
            if (userAction != null)
            {
                var condition = userAction.Conditions.FirstOrDefault(x => x.Id == model.ConditionId);
                if (condition != null)
                {
                    var _url = condition.UrlCurrent.FirstOrDefault(x => x.Id == model.Id);
                    _url.Name = model.Name;
                    _userActionService.UpdateUserAction(userAction);
                }
            }
            return new NullJsonResult();
        }

        #endregion


    }
}
