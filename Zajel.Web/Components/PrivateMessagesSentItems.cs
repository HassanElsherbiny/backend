﻿using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Web.Models.PrivateMessages;
using Zajel.Web.Models.Common;
using Zajel.Core.Domain.Forums;
using Zajel.Services.Forums;
using Zajel.Core;
using System.Collections.Generic;
using Zajel.Services.Users;
using Zajel.Core.Domain.Users;
using Zajel.Services.Helpers;

namespace Zajel.Web.ViewComponents
{
    public class PrivateMessagesSentItemsViewComponent : ViewComponent
    {
        private readonly ForumSettings _forumSettings;
        private readonly IForumService _forumService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IUserService _userService;
        private readonly UserSettings _userSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        public PrivateMessagesSentItemsViewComponent(ForumSettings forumSettings, IForumService forumService,
            IWorkContext workContext, IStoreContext storeContext, IUserService userService,
            UserSettings userSettings, IDateTimeHelper dateTimeHelper)
        {
            this._forumSettings = forumSettings;
            this._forumService = forumService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._userService = userService;
            this._userSettings = userSettings;
            this._dateTimeHelper = dateTimeHelper;
        }

        public IViewComponentResult Invoke(int pageNumber, string tab)
        {
            if (pageNumber > 0)
            {
                pageNumber -= 1;
            }

            var pageSize = _forumSettings.PrivateMessagesPageSize;

            var list = _forumService.GetAllPrivateMessages(_storeContext.CurrentStore.Id,
                _workContext.CurrentUser.Id, "", null, false, null, string.Empty, pageNumber, pageSize);

            var sentItems = new List<PrivateMessageModel>();

            foreach (var pm in list)
            {
                var fromUser = _userService.GetUserById(pm.FromUserId);
                var toUser = _userService.GetUserById(pm.ToUserId);

                sentItems.Add(new PrivateMessageModel
                {
                    Id = pm.Id,
                    FromUserId = fromUser.Id,
                    UserFromName = fromUser.FormatUserName(),
                    AllowViewingFromProfile = _userSettings.AllowViewingProfiles && fromUser != null && !fromUser.IsGuest(),
                    ToUserId = toUser.Id,
                    UserToName = toUser.FormatUserName(),
                    AllowViewingToProfile = _userSettings.AllowViewingProfiles && toUser != null && !toUser.IsGuest(),
                    Subject = pm.Subject,
                    Message = pm.Text,
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(pm.CreatedOnUtc, DateTimeKind.Utc),
                    IsRead = pm.IsRead,
                });
            }

            var pagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "PrivateMessagesPaged",
                UseRouteLinks = true,
                RouteValues = new PrivateMessageRouteValues { pageNumber = pageNumber, tab = tab }
            };

            var model = new PrivateMessageListModel
            {
                Messages = sentItems,
                PagerModel = pagerModel
            };

            return View(model);

        }
    }
}