﻿using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Core.Domain;
using Zajel.Core.Domain.Users;
using Zajel.Core;
using Zajel.Services.Common;

namespace Zajel.Web.ViewComponents
{
    public class EuCookieLawViewComponent : ViewComponent
    {
        private readonly StoreInformationSettings _storeInformationSettings;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        public EuCookieLawViewComponent(StoreInformationSettings storeInformationSettings,
            IWorkContext workContext, IStoreContext storeContext)
        {
            this._storeInformationSettings = storeInformationSettings;
            this._workContext = workContext;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            if (!_storeInformationSettings.DisplayEuCookieLawWarning)
                //disabled
                return Content("");

            var user = _workContext.CurrentUser;
            //ignore search engines because some pages could be indexed with the EU cookie as description
            if (user.IsSearchEngineAccount())
                return Content("");

            if (user.GetAttribute<bool>(SystemUserAttributeNames.EuCookieLawAccepted, _storeContext.CurrentStore.Id))
                //already accepted
                return Content("");

            //ignore notification?
            //right now it's used during logout so popup window is not displayed twice
            if (TempData["Zajel.IgnoreEuCookieLawWarning"] != null && Convert.ToBoolean(TempData["Zajel.IgnoreEuCookieLawWarning"]))
                return Content("");

            return View();

        }
    }
}