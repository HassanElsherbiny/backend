﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Web.Services;

namespace Zajel.Web.ViewComponents
{
    public class LogoViewComponent : ViewComponent
    {
        private readonly ICommonWebService _commonWebService;

        public LogoViewComponent(ICommonWebService commonWebService)
        {
            this._commonWebService = commonWebService;
        }

        public IViewComponentResult Invoke()
        {
            var model = _commonWebService.PrepareLogo();
            return View(model);
        }
    }
}