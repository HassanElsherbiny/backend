﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Web.Services;
using Zajel.Core.Domain;

namespace Zajel.Web.ViewComponents
{
    public class StoreThemeSelectorViewComponent : ViewComponent
    {
        private readonly ICommonWebService _commonWebService;
        private readonly StoreInformationSettings _storeInformationSettings;
        public StoreThemeSelectorViewComponent(ICommonWebService commonWebService,
            StoreInformationSettings storeInformationSettings)
        {
            this._commonWebService = commonWebService;
            this._storeInformationSettings = storeInformationSettings;
        }

        public IViewComponentResult Invoke()
        {
            if (!_storeInformationSettings.AllowUserToSelectTheme)
                return Content("");
            var model = _commonWebService.PrepareStoreThemeSelector();
            return View(model);


        }
    }
}