﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Web.Services;
using Zajel.Core.Infrastructure;
using Zajel.Core;
using Zajel.Services.Localization;
using Zajel.Core.Domain.Forums;
using Zajel.Services.Common;
using Zajel.Core.Domain.Users;

namespace Zajel.Web.ViewComponents
{
    public class HeaderLinksViewComponent : ViewComponent
    {
        private readonly ICommonWebService _commonWebService;
        private readonly IWorkContext _workContext;
        private readonly ILocalizationService _localizationService;
        private readonly IStoreContext _storeContext;
        private readonly ForumSettings _forumSettings;

        public HeaderLinksViewComponent(ICommonWebService commonWebService, IWorkContext workContext,
            ILocalizationService localizationService, ForumSettings forumSettings,
            IStoreContext storeContext)
        {
            this._commonWebService = commonWebService;
            this._workContext = workContext;
            this._localizationService = localizationService;
            this._forumSettings = forumSettings;
            this._storeContext = storeContext;
        }

        public IViewComponentResult Invoke()
        {
            var user = _workContext.CurrentUser;
            var model = _commonWebService.PrepareHeaderLinks(user);
            if (_forumSettings.AllowPrivateMessages)
            {
                var unreadMessageCount = _commonWebService.GetUnreadPrivateMessages();
                var unreadMessage = string.Empty;
                var alertMessage = string.Empty;
                if (unreadMessageCount > 0)
                {
                    unreadMessage = string.Format(_localizationService.GetResource("PrivateMessages.TotalUnread"), unreadMessageCount);

                    //notifications here
                    if (_forumSettings.ShowAlertForPM &&
                        !user.GetAttribute<bool>(SystemUserAttributeNames.NotifiedAboutNewPrivateMessages, _storeContext.CurrentStore.Id))
                    {
                        EngineContext.Current.Resolve<IGenericAttributeService>().SaveAttribute(user, SystemUserAttributeNames.NotifiedAboutNewPrivateMessages, true, _storeContext.CurrentStore.Id);
                        alertMessage = string.Format(_localizationService.GetResource("PrivateMessages.YouHaveUnreadPM"), unreadMessageCount);
                    }
                }
                model.UnreadPrivateMessages = unreadMessage;
                model.AlertMessage = alertMessage;
            }
            return View(model);
        }
    }
}