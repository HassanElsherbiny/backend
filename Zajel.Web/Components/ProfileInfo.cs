﻿using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Web.Models.Profile;
using Zajel.Services.Forums;
using Zajel.Services.Users;
using Zajel.Services.Logging;
using Zajel.Services.Localization;
using Zajel.Core;
using Zajel.Services.Helpers;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Users;
using Zajel.Services.Common;
using Zajel.Services.Media;
using Zajel.Core.Domain.Media;
using Zajel.Services.Directory;

namespace Zajel.Web.ViewComponents
{
    public class ProfileInfoViewComponent : ViewComponent
    {
        private readonly IForumService _forumService;
        private readonly IUserService _userService;
        private readonly IUserActivityService _userActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ForumSettings _forumSettings;
        private readonly UserSettings _userSettings;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly ICountryService _countryService;

        public ProfileInfoViewComponent(IForumService forumService,
            IUserService userService, IUserActivityService userActivityService,
            ILocalizationService localizationService, IWorkContext workContext,
            IStoreContext storeContext, IDateTimeHelper dateTimeHelper,
            ForumSettings forumSettings, UserSettings userSettings,
            IPictureService pictureService, MediaSettings mediaSettings,
            ICountryService countryService)
        {
            this._forumService = forumService;
            this._userService = userService;
            this._userActivityService = userActivityService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._dateTimeHelper = dateTimeHelper;
            this._forumSettings = forumSettings;
            this._userSettings = userSettings;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
            this._countryService = countryService;
        }

        public IViewComponentResult Invoke(string userProfileId)
        {
            var user = _userService.GetUserById(userProfileId);
            if (user == null)
            {
                return Content("");
            }

            //avatar
            var avatarUrl = "";
            if (_userSettings.AllowUsersToUploadAvatars)
            {
                avatarUrl = _pictureService.GetPictureUrl(
                 user.GetAttribute<string>(SystemUserAttributeNames.AvatarPictureId),
                 _mediaSettings.AvatarPictureSize,
                 _userSettings.DefaultAvatarEnabled,
                 defaultPictureType: PictureType.Avatar);
            }

            //location
            bool locationEnabled = false;
            string location = string.Empty;
            if (_userSettings.ShowUsersLocation)
            {
                locationEnabled = true;

                var countryId = user.GetAttribute<string>(SystemUserAttributeNames.CountryId);
                var country = _countryService.GetCountryById(countryId);
                if (country != null)
                {
                    location = country.GetLocalized(x => x.Name);
                }
                else
                {
                    locationEnabled = false;
                }
            }

            //private message
            bool pmEnabled = _forumSettings.AllowPrivateMessages && !user.IsGuest();

            //total forum posts
            bool totalPostsEnabled = false;
            int totalPosts = 0;
            if (_forumSettings.ForumsEnabled && _forumSettings.ShowUsersPostCount)
            {
                totalPostsEnabled = true;
                totalPosts = user.GetAttribute<int>(SystemUserAttributeNames.ForumPostCount);
            }

            //registration date
            bool joinDateEnabled = false;
            string joinDate = string.Empty;

            if (_userSettings.ShowUsersJoinDate)
            {
                joinDateEnabled = true;
                joinDate = _dateTimeHelper.ConvertToUserTime(user.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
            }

            //birth date
            bool dateOfBirthEnabled = false;
            string dateOfBirth = string.Empty;
            if (_userSettings.DateOfBirthEnabled)
            {
                var dob = user.GetAttribute<DateTime?>(SystemUserAttributeNames.DateOfBirth);
                if (dob.HasValue)
                {
                    dateOfBirthEnabled = true;
                    dateOfBirth = dob.Value.ToString("D");
                }
            }

            var model = new ProfileInfoModel
            {
                UserProfileId = user.Id,
                AvatarUrl = avatarUrl,
                LocationEnabled = locationEnabled,
                Location = location,
                PMEnabled = pmEnabled,
                TotalPostsEnabled = totalPostsEnabled,
                TotalPosts = totalPosts.ToString(),
                JoinDateEnabled = joinDateEnabled,
                JoinDate = joinDate,
                DateOfBirthEnabled = dateOfBirthEnabled,
                DateOfBirth = dateOfBirth,
            };

            return View(model);
        }
    }
}