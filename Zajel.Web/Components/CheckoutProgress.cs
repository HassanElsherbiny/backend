﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Web.Models.Checkout;

namespace Zajel.Web.ViewComponents
{
    public class CheckoutProgressViewComponent : ViewComponent
    {
        public CheckoutProgressViewComponent()
        {
        }

        public IViewComponentResult Invoke(CheckoutProgressStep step)
        {
            var model = new CheckoutProgressModel { CheckoutProgressStep = step };
            return View(model);

        }
    }
}