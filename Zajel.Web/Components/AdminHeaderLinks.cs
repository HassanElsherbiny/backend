﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Web.Services;
using Zajel.Core;

namespace Zajel.Web.ViewComponents
{
    public class AdminHeaderLinksViewComponent : ViewComponent
    {
        private readonly ICommonWebService _commonWebService;
        private readonly IWorkContext _workContext;
        public AdminHeaderLinksViewComponent(ICommonWebService commonWebService,
            IWorkContext workContext)
        {
            this._commonWebService = commonWebService;
            this._workContext = workContext;
        }

        public IViewComponentResult Invoke()
        {
            var model = _commonWebService.PrepareAdminHeaderLinks(_workContext.CurrentUser);
            return View(model);
        }
    }
}