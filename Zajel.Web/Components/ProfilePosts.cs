﻿using Microsoft.AspNetCore.Mvc;
using System;
using Zajel.Web.Models.Profile;
using Zajel.Services.Forums;
using Zajel.Services.Users;
using Zajel.Services.Logging;
using Zajel.Services.Localization;
using Zajel.Core;
using Zajel.Services.Helpers;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Users;
using Zajel.Services.Media;
using Zajel.Core.Domain.Media;
using Zajel.Services.Directory;
using System.Collections.Generic;
using Zajel.Services.Seo;
using Zajel.Web.Models.Common;

namespace Zajel.Web.ViewComponents
{
    public class ProfilePostsViewComponent : ViewComponent
    {
        private readonly IForumService _forumService;
        private readonly IUserService _userService;
        private readonly IUserActivityService _userActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ForumSettings _forumSettings;
        private readonly UserSettings _userSettings;
        private readonly IPictureService _pictureService;
        private readonly MediaSettings _mediaSettings;
        private readonly ICountryService _countryService;

        public ProfilePostsViewComponent(IForumService forumService,
            IUserService userService, IUserActivityService userActivityService,
            ILocalizationService localizationService, IWorkContext workContext,
            IStoreContext storeContext, IDateTimeHelper dateTimeHelper,
            ForumSettings forumSettings, UserSettings userSettings,
            IPictureService pictureService, MediaSettings mediaSettings,
            ICountryService countryService)
        {
            this._forumService = forumService;
            this._userService = userService;
            this._userActivityService = userActivityService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._dateTimeHelper = dateTimeHelper;
            this._forumSettings = forumSettings;
            this._userSettings = userSettings;
            this._pictureService = pictureService;
            this._mediaSettings = mediaSettings;
            this._countryService = countryService;
        }

        public IViewComponentResult Invoke(string userProfileId, int pageNumber)
        {
            var user = _userService.GetUserById(userProfileId);
            if (user == null)
            {
                return Content("");
            }

            if (pageNumber > 0)
            {
                pageNumber -= 1;
            }

            var pageSize = _forumSettings.LatestUserPostsPageSize;

            var list = _forumService.GetAllPosts("", user.Id, string.Empty, false, pageNumber, pageSize);

            var latestPosts = new List<PostsModel>();

            foreach (var forumPost in list)
            {
                var posted = string.Empty;
                if (_forumSettings.RelativeDateTimeFormattingEnabled)
                {
                    posted = forumPost.CreatedOnUtc.ToString("f");
                }
                else
                {
                    posted = _dateTimeHelper.ConvertToUserTime(forumPost.CreatedOnUtc, DateTimeKind.Utc).ToString("f");
                }
                var forumtopic = _forumService.GetTopicById(forumPost.TopicId);
                latestPosts.Add(new PostsModel
                {
                    ForumTopicId = forumPost.TopicId,
                    ForumTopicTitle = forumtopic.Subject,
                    ForumTopicSlug = forumtopic.GetSeName(),
                    ForumPostText = forumPost.FormatPostText(),
                    Posted = posted
                });
            }

            var pagerModel = new PagerModel
            {
                PageSize = list.PageSize,
                TotalRecords = list.TotalCount,
                PageIndex = list.PageIndex,
                ShowTotalSummary = false,
                RouteActionName = "UserProfilePaged",
                UseRouteLinks = true,
                RouteValues = new RouteValues { pageNumber = pageNumber, id = userProfileId }
            };

            var model = new ProfilePostsModel
            {
                PagerModel = pagerModel,
                Posts = latestPosts,
            };

            return View(model);
        }
    }
}