﻿using Zajel.Web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Zajel.Web.Components
{
    public class UserNavigationViewComponent : ViewComponent
    {
        private readonly IUserWebService _userWebService;

        public UserNavigationViewComponent(IUserWebService userWebService)
        {
            this._userWebService = userWebService;
        }

        public IViewComponentResult Invoke(int selectedTabId = 0)
        {
            var model = _userWebService.PrepareNavigation(selectedTabId);
            return View(model);
        }
    }
}
