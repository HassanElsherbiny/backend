﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Blogs
{
    public partial class BlogPostYearModel : BaseZajelModel
    {
        public BlogPostYearModel()
        {
            Months = new List<BlogPostMonthModel>();
        }
        public int Year { get; set; }
        public IList<BlogPostMonthModel> Months { get; set; }
    }
    public partial class BlogPostMonthModel : BaseZajelModel
    {
        public int Month { get; set; }

        public int BlogPostCount { get; set; }
    }
}