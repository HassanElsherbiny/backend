﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Media;

namespace Zajel.Web.Models.Blogs
{
    public partial class BlogPostListModel : BaseZajelModel
    {
        public BlogPostListModel()
        {
            PagingFilteringContext = new BlogPagingFilteringModel();
            BlogPosts = new List<BlogPostModel>();
            PictureModel = new PictureModel();
        }
        public PictureModel PictureModel { get; set; }
        public string WorkingLanguageId { get; set; }
        public BlogPagingFilteringModel PagingFilteringContext { get; set; }
        public IList<BlogPostModel> BlogPosts { get; set; }
    }
}