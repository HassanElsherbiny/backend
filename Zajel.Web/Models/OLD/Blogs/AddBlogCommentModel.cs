﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Blogs
{
    public partial class AddBlogCommentModel : BaseZajelEntityModel
    {
        [ZajelResourceDisplayName("Blog.Comments.CommentText")]
        public string CommentText { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}