﻿using System;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Blogs
{
    public partial class BlogCommentModel : BaseZajelEntityModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserAvatarUrl { get; set; }

        public string CommentText { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool AllowViewingProfiles { get; set; }
    }
}