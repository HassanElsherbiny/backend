﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Blogs
{
    public partial class BlogPostTagModel : BaseZajelModel
    {
        public string Name { get; set; }

        public int BlogPostCount { get; set; }
    }
}