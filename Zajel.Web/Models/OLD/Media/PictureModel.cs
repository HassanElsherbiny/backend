﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Media
{
    public partial class PictureModel : BaseZajelModel
    {
        public string ImageUrl { get; set; }

        public string FullSizeImageUrl { get; set; }

        public string Title { get; set; }

        public string AlternateText { get; set; }
    }
}