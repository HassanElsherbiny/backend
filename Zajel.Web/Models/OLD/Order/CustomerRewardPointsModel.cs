﻿using System;
using System.Collections.Generic;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Order
{
    public partial class UserRewardPointsModel : BaseZajelModel
    {
        public UserRewardPointsModel()
        {
            RewardPoints = new List<RewardPointsHistoryModel>();
        }

        public IList<RewardPointsHistoryModel> RewardPoints { get; set; }
        public int RewardPointsBalance { get; set; }
        public string RewardPointsAmount { get; set; }
        public int MinimumRewardPointsBalance { get; set; }
        public string MinimumRewardPointsAmount { get; set; }

        #region Nested classes

        public partial class RewardPointsHistoryModel : BaseZajelEntityModel
        {
            [ZajelResourceDisplayName("RewardPoints.Fields.Points")]
            public int Points { get; set; }

            [ZajelResourceDisplayName("RewardPoints.Fields.PointsBalance")]
            public int PointsBalance { get; set; }

            [ZajelResourceDisplayName("RewardPoints.Fields.Message")]
            public string Message { get; set; }

            [ZajelResourceDisplayName("RewardPoints.Fields.Date")]
            public DateTime CreatedOn { get; set; }
        }

        #endregion
    }
}