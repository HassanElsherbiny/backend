﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Order
{
    public partial class SubmitReturnRequestModel : BaseZajelModel
    {
        public SubmitReturnRequestModel()
        {
            Items = new List<OrderItemModel>();
            AvailableReturnReasons = new List<ReturnRequestReasonModel>();
            AvailableReturnActions = new List<ReturnRequestActionModel>();
        }

        public string OrderId { get; set; }
        public int OrderNumber { get; set; }

        public IList<OrderItemModel> Items { get; set; }

        [ZajelResourceDisplayName("ReturnRequests.ReturnReason")]
        public string ReturnRequestReasonId { get; set; }
        public IList<ReturnRequestReasonModel> AvailableReturnReasons { get; set; }

        [ZajelResourceDisplayName("ReturnRequests.ReturnAction")]
        public string ReturnRequestActionId { get; set; }
        public IList<ReturnRequestActionModel> AvailableReturnActions { get; set; }

        [ZajelResourceDisplayName("ReturnRequests.Comments")]
        public string Comments { get; set; }

        public string Result { get; set; }

        #region Nested classes

        public partial class OrderItemModel : BaseZajelEntityModel
        {
            public string ProductId { get; set; }

            public string ProductName { get; set; }

            public string ProductSeName { get; set; }

            public string AttributeInfo { get; set; }

            public string UnitPrice { get; set; }

            public int Quantity { get; set; }
        }

        public partial class ReturnRequestReasonModel : BaseZajelEntityModel
        {
            public string Name { get; set; }
        }
        public partial class ReturnRequestActionModel : BaseZajelEntityModel
        {
            public string Name { get; set; }
        }

        #endregion
    }

}