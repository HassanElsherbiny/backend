﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Routing;

namespace Zajel.Web.Models.Cms
{
    public partial class RenderWidgetModel : BaseZajelModel
    {
        public string WidgetViewComponentName { get; set; }
        public object WidgetViewComponentArguments { get; set; }
    }
}