﻿using System.Collections.Generic;
using Zajel.Web.Models.Common;

namespace Zajel.Web.Models.Profile
{
    public partial class ProfilePostsModel
    {
        public IList<PostsModel> Posts { get; set; }
        public PagerModel PagerModel { get; set; }
    }
}