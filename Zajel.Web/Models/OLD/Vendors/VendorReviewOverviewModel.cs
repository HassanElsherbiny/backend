﻿using System.Collections.Generic;
using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Web.Validators.Vendors;

namespace Zajel.Web.Models.Vendors
{
    public partial class VendorReviewOverviewModel : BaseZajelModel
    {
        public string VendorId { get; set; }

        public int RatingSum { get; set; }

        public int TotalReviews { get; set; }

        public bool AllowUserReviews { get; set; }
    }

    [Validator(typeof(VendorReviewsValidator))]
    public partial class VendorReviewsModel : BaseZajelModel
    {
        public VendorReviewsModel()
        {
            Items = new List<VendorReviewModel>();
            AddVendorReview = new AddVendorReviewModel();
        }
        public string VendorId { get; set; }

        public string VendorName { get; set; }

        public string VendorSeName { get; set; }

        public IList<VendorReviewModel> Items { get; set; }
        public AddVendorReviewModel AddVendorReview { get; set; }
    }

    public partial class VendorReviewModel : BaseZajelEntityModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public bool AllowViewingProfiles { get; set; }

        public string Title { get; set; }

        public string ReviewText { get; set; }

        public int Rating { get; set; }

        public VendorReviewHelpfulnessModel Helpfulness { get; set; }

        public string WrittenOnStr { get; set; }
    }

    public partial class VendorReviewHelpfulnessModel : BaseZajelModel
    {
        public string VendorReviewId { get; set; }
        public string VendorId { get; set; }

        public int HelpfulYesTotal { get; set; }

        public int HelpfulNoTotal { get; set; }
    }

    public partial class AddVendorReviewModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Reviews.Fields.Title")]
        public string Title { get; set; }

        [ZajelResourceDisplayName("Reviews.Fields.ReviewText")]
        public string ReviewText { get; set; }

        [ZajelResourceDisplayName("Reviews.Fields.Rating")]
        public int Rating { get; set; }

        public bool DisplayCaptcha { get; set; }

        public bool CanCurrentUserLeaveReview { get; set; }
        public bool SuccessfullyAdded { get; set; }
        public string Result { get; set; }
    }
}