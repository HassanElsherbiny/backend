﻿using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;
using Zajel.Web.Validators.Vendors;

namespace Zajel.Web.Models.Vendors
{
    [Validator(typeof(VendorInfoValidator))]
    public partial class VendorInfoModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Account.VendorInfo.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Account.VendorInfo.Email")]
        public string Email { get; set; }

        [ZajelResourceDisplayName("Account.VendorInfo.Description")]
        public string Description { get; set; }

        [ZajelResourceDisplayName("Account.VendorInfo.Picture")]
        public string PictureUrl { get; set; }
    }
}