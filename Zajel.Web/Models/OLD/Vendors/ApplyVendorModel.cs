﻿using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.Vendors;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Vendors
{
    [Validator(typeof(ApplyVendorValidator))]
    public partial class ApplyVendorModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("Vendors.ApplyAccount.Name")]
        public string Name { get; set; }

        [ZajelResourceDisplayName("Vendors.ApplyAccount.Email")]
        public string Email { get; set; }

        [ZajelResourceDisplayName("Vendors.ApplyAccount.Description")]
        public string Description { get; set; }

        public bool DisplayCaptcha { get; set; }

        public bool TermsOfServiceEnabled { get; set; }
        public bool TermsOfServicePopup { get; set; }

        public bool DisableFormInput { get; set; }
        public string Result { get; set; }
    }
}