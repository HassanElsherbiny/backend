﻿using Zajel.Core.Domain.Forums;

namespace Zajel.Web.Models.Boards
{
    public partial class ForumTopicRowModel
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string SeName { get; set; }
        public string LastPostId { get; set; }

        public int NumPosts { get; set; }
        public int Views { get; set; }
        public int NumReplies { get; set; }
        public ForumTopicType ForumTopicType { get; set; }

        public string UserId { get; set; }
        public bool AllowViewingProfiles { get; set; }
        public string UserName { get; set; }
        public bool IsUserGuest { get; set; }

        //posts
        public int TotalPostPages { get; set; }
        public int Votes { get; set; }
    }
}