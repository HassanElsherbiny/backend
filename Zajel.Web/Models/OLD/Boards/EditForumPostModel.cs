﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Core.Domain.Forums;
using Zajel.Web.Validators.Boards;

namespace Zajel.Web.Models.Boards
{
    [Validator(typeof(EditForumPostValidator))]
    public partial class EditForumPostModel
    {
        public string Id { get; set; }
        public string ForumTopicId { get; set; }

        public bool IsEdit { get; set; }

        public string Text { get; set; }
        public EditorType ForumEditor { get; set; }

        public string ForumName { get; set; }
        public string ForumTopicSubject { get; set; }
        public string ForumTopicSeName { get; set; }

        public bool IsUserAllowedToSubscribe { get; set; }
        public bool Subscribed { get; set; }
    }
}