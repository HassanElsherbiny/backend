﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Zajel.Web.Models.Boards
{
    public partial class TopicMoveModel : BaseZajelEntityModel
    {
        public TopicMoveModel()
        {
            ForumList = new List<SelectListItem>();
        }

        public string ForumSelected { get; set; }
        public string TopicSeName { get; set; }

        public IEnumerable<SelectListItem> ForumList { get; set; }
    }
}