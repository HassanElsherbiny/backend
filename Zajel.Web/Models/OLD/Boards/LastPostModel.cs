﻿
namespace Zajel.Web.Models.Boards
{
    public partial class LastPostModel
    {
        public string Id { get; set; }
        public string ForumTopicId { get; set; }
        public string ForumTopicSeName { get; set; }
        public string ForumTopicSubject { get; set; }
        
        public string UserId { get; set; }
        public bool AllowViewingProfiles { get; set; }
        public string UserName { get; set; }
        public bool IsUserGuest { get; set; }

        public string PostCreatedOnStr { get; set; }
        
        public bool ShowTopic { get; set; }
    }
}