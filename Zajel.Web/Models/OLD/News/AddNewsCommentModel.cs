﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.News
{
    public partial class AddNewsCommentModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("News.Comments.CommentTitle")]
        public string CommentTitle { get; set; }

        [ZajelResourceDisplayName("News.Comments.CommentText")]
        public string CommentText { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}