﻿using System;
using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.News
{
    public partial class HomePageNewsItemsModel : BaseZajelModel, ICloneable
    {
        public HomePageNewsItemsModel()
        {
            NewsItems = new List<NewsItemModel>();
        }

        public string WorkingLanguageId { get; set; }
        public IList<NewsItemModel> NewsItems { get; set; }

        public object Clone()
        {
            //we use a shallow copy (deep clone is not required here)
            return this.MemberwiseClone();
        }
    }
}