﻿using System;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.News
{
    public partial class NewsCommentModel : BaseZajelEntityModel
    {
        public string UserId { get; set; }

        public string UserName { get; set; }

        public string UserAvatarUrl { get; set; }

        public string CommentTitle { get; set; }

        public string CommentText { get; set; }

        public DateTime CreatedOn { get; set; }

        public bool AllowViewingProfiles { get; set; }
    }
}