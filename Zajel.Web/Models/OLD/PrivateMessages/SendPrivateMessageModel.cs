﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.PrivateMessages;

namespace Zajel.Web.Models.PrivateMessages
{
    [Validator(typeof(SendPrivateMessageValidator))]
    public partial class SendPrivateMessageModel : BaseZajelEntityModel
    {
        public string ToUserId { get; set; }
        public string UserToName { get; set; }
        public bool AllowViewingToProfile { get; set; }

        public string ReplyToMessageId { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }
    }
}