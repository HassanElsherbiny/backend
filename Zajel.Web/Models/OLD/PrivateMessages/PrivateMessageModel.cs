﻿using System;
using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.PrivateMessages;

namespace Zajel.Web.Models.PrivateMessages
{
    [Validator(typeof(SendPrivateMessageValidator))]
    public partial class PrivateMessageModel : BaseZajelEntityModel
    {
        public string FromUserId { get; set; }
        public string UserFromName { get; set; }
        public bool AllowViewingFromProfile { get; set; }

        public string ToUserId { get; set; }
        public string UserToName { get; set; }
        public bool AllowViewingToProfile { get; set; }

        public string Subject { get; set; }

        public string Message { get; set; }
        
        public DateTime CreatedOn { get; set; }

        public bool IsRead { get; set; }
    }
}