﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class CurrencySelectorModel : BaseZajelModel
    {
        public CurrencySelectorModel()
        {
            AvailableCurrencies = new List<CurrencyModel>();
        }

        public IList<CurrencyModel> AvailableCurrencies { get; set; }

        public string CurrentCurrencyId { get; set; }
    }
}