﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class LanguageModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public string FlagImageFileName { get; set; }

    }
}