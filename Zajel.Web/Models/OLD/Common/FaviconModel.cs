﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class FaviconModel : BaseZajelModel
    {
        public string FaviconUrl { get; set; }
    }
}