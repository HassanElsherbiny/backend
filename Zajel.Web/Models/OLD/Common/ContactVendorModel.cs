﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.Common;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Common
{
    [Validator(typeof(ContactVendorValidator))]
    public partial class ContactVendorModel : BaseZajelModel
    {
        public string VendorId { get; set; }
        public string VendorName { get; set; }

        [ZajelResourceDisplayName("ContactVendor.Email")]
        public string Email { get; set; }

        [ZajelResourceDisplayName("ContactVendor.Subject")]
        public string Subject { get; set; }
        public bool SubjectEnabled { get; set; }

        [ZajelResourceDisplayName("ContactVendor.Enquiry")]
        public string Enquiry { get; set; }

        [ZajelResourceDisplayName("ContactVendor.FullName")]
        public string FullName { get; set; }

        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}