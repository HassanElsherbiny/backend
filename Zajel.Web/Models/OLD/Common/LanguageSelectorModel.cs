﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class LanguageSelectorModel : BaseZajelModel
    {
        public LanguageSelectorModel()
        {
            AvailableLanguages = new List<LanguageModel>();
        }

        public IList<LanguageModel> AvailableLanguages { get; set; }

        public string CurrentLanguageId { get; set; }

        public bool UseImages { get; set; }
    }
}