﻿using Zajel.Core.Domain.Tax;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class TaxTypeSelectorModel : BaseZajelModel
    {
        public TaxDisplayType CurrentTaxType { get; set; }
    }
}