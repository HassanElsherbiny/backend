﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Catalog;
using Zajel.Web.Models.Topics;

namespace Zajel.Web.Models.Common
{
    public partial class SitemapModel : BaseZajelModel
    {
        public SitemapModel()
        {
            Products = new List<ProductOverviewModel>();
            Categories = new List<CategoryModel>();
            Manufacturers = new List<ManufacturerModel>();
            Topics = new List<TopicModel>();
        }
        public IList<ProductOverviewModel> Products { get; set; }
        public IList<CategoryModel> Categories { get; set; }
        public IList<ManufacturerModel> Manufacturers { get; set; }
        public IList<TopicModel> Topics { get; set; }

        public bool NewsEnabled { get; set; }
        public bool BlogEnabled { get; set; }
        public bool ForumEnabled { get; set; }
    }
}