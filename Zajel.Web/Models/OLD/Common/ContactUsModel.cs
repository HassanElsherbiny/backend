﻿using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.Common;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Common
{
    [Validator(typeof(ContactUsValidator))]
    public partial class ContactUsModel : BaseZajelModel
    {
        [ZajelResourceDisplayName("ContactUs.Email")]
        public string Email { get; set; }

        [ZajelResourceDisplayName("ContactUs.Subject")]
        public string Subject { get; set; }
        public bool SubjectEnabled { get; set; }

        [ZajelResourceDisplayName("ContactUs.Enquiry")]
        public string Enquiry { get; set; }

        [ZajelResourceDisplayName("ContactUs.FullName")]
        public string FullName { get; set; }

        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}