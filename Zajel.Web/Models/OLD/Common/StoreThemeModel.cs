﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class StoreThemeModel : BaseZajelModel
    {
        public string Name { get; set; }
        public string Title { get; set; }
    }
}