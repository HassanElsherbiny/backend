﻿using System.Collections.Generic;
using Zajel.Core.Domain.Catalog;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class AddressAttributeModel : BaseZajelEntityModel
    {
        public AddressAttributeModel()
        {
            Values = new List<AddressAttributeValueModel>();
        }

        public string Name { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// Default value for textboxes
        /// </summary>
        public string DefaultValue { get; set; }

        public AttributeControlType AttributeControlType { get; set; }

        public IList<AddressAttributeValueModel> Values { get; set; }
    }

    public partial class AddressAttributeValueModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public bool IsPreSelected { get; set; }
    }
}