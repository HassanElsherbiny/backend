﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class CurrencyModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public string CurrencySymbol { get; set; }
        public string CurrencyCode { get; set; }
    }
}