﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Common
{
    public partial class LogoModel : BaseZajelModel
    {
        public string StoreName { get; set; }

        public string LogoPath { get; set; }
    }
}