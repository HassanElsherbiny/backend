﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Catalog
{
    public partial class ProductSpecificationModel : BaseZajelModel
    {
        public string SpecificationAttributeId { get; set; }

        public string SpecificationAttributeName { get; set; }

        //this value is already HTML encoded
        public string ValueRaw { get; set; }
        public string ColorSquaresRgb { get; set; }
    }
}