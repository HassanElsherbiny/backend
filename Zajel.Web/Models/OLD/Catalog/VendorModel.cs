﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Media;
using Zajel.Web.Models.Vendors;

namespace Zajel.Web.Models.Catalog
{
    public partial class VendorModel : BaseZajelEntityModel
    {
        public VendorModel()
        {
            Products = new List<ProductOverviewModel>();
            PagingFilteringContext = new CatalogPagingFilteringModel();
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string MetaKeywords { get; set; }
        public string MetaDescription { get; set; }
        public string MetaTitle { get; set; }
        public string SeName { get; set; }
        public bool AllowUsersToContactVendors { get; set; }
        public PictureModel PictureModel { get; set; }
        public CatalogPagingFilteringModel PagingFilteringContext { get; set; }
        public VendorReviewOverviewModel VendorReviewOverview { get; set; }
        public IList<ProductOverviewModel> Products { get; set; }
    }
}