﻿using FluentValidation.Attributes;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.Catalog
{
    [Validator(typeof(ProductAskQuestionValidator))]
    public partial class ProductAskQuestionModel: BaseZajelEntityModel
    {
        public string ProductId { get; set; }

        public string ProductName { get; set; }

        public string ProductSeName { get; set; }

        [ZajelResourceDisplayName("Products.AskQuestion.Email")]
        public string Email { get; set; }

        [ZajelResourceDisplayName("Products.AskQuestion.FullName")]
        public string FullName { get; set; }

        [ZajelResourceDisplayName("Products.AskQuestion.Phone")]
        public string Phone { get; set; }

        [ZajelResourceDisplayName("Products.AskQuestion.Message")]
        public string Message { get; set; }

        public bool SuccessfullySent { get; set; }
        public string Result { get; set; }

        public bool DisplayCaptcha { get; set; }

    }
}