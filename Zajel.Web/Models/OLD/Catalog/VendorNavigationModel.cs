﻿using System.Collections.Generic;
using Zajel.Framework.Mvc;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Catalog
{
    public partial class VendorNavigationModel : BaseZajelModel
    {
        public VendorNavigationModel()
        {
            this.Vendors = new List<VendorBriefInfoModel>();
        }

        public IList<VendorBriefInfoModel> Vendors { get; set; }

        public int TotalVendors { get; set; }
    }

    public partial class VendorBriefInfoModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public string SeName { get; set; }
    }
}