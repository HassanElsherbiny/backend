﻿using System.Collections.Generic;
using Zajel.Framework.Mvc;
using Zajel.Web.Models.Common;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Catalog
{
    public partial class UserBackInStockSubscriptionsModel
    {
        public UserBackInStockSubscriptionsModel()
        {
            this.Subscriptions = new List<BackInStockSubscriptionModel>();
        }

        public IList<BackInStockSubscriptionModel> Subscriptions { get; set; }
        public PagerModel PagerModel { get; set; }

        #region Nested classes

        public partial class BackInStockSubscriptionModel : BaseZajelEntityModel
        {
            public string ProductId { get; set; }
            public string ProductName { get; set; }
            public string SeName { get; set; }
        }

        #endregion
    }
}