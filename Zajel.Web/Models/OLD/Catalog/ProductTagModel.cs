﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Catalog
{
    public partial class ProductTagModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public string SeName { get; set; }

        public int ProductCount { get; set; }
    }
}