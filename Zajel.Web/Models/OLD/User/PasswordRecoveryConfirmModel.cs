﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.User;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.User
{
    [Validator(typeof(PasswordRecoveryConfirmValidator))]
    public partial class PasswordRecoveryConfirmModel : BaseZajelModel
    {
        [NoTrim]
        [DataType(DataType.Password)]
        [ZajelResourceDisplayName("Account.PasswordRecovery.NewPassword")]
        public string NewPassword { get; set; }

        [NoTrim]
        [DataType(DataType.Password)]
        [ZajelResourceDisplayName("Account.PasswordRecovery.ConfirmNewPassword")]
        public string ConfirmNewPassword { get; set; }

        public bool DisablePasswordChanging { get; set; }
        public string Result { get; set; }
    }
}