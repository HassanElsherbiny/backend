﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.User
{
    public partial class ExternalAuthenticationMethodModel : BaseZajelModel
    {
        public string ViewComponentName { get; set; }
    }
}