﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.User
{
    public partial class UserAvatarModel : BaseZajelModel
    {
        public string AvatarUrl { get; set; }
    }
}