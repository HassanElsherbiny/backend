﻿using System.Collections.Generic;
using Zajel.Core.Domain.Catalog;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.User
{
    public partial class UserAttributeModel : BaseZajelEntityModel
    {
        public UserAttributeModel()
        {
            Values = new List<UserAttributeValueModel>();
        }

        public string Name { get; set; }

        public bool IsRequired { get; set; }

        /// <summary>
        /// Default value for textboxes
        /// </summary>
        public string DefaultValue { get; set; }

        public AttributeControlType AttributeControlType { get; set; }

        public IList<UserAttributeValueModel> Values { get; set; }

    }

    public partial class UserAttributeValueModel : BaseZajelEntityModel
    {
        public string Name { get; set; }

        public bool IsPreSelected { get; set; }
    }
}