﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.User
{
    public partial class AccountActivationModel : BaseZajelModel
    {
        public string Result { get; set; }
    }
}