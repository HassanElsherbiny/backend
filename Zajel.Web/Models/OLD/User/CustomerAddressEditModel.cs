﻿using FluentValidation.Attributes;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Common;
using Zajel.Web.Validators.User;

namespace Zajel.Web.Models.User
{
    [Validator(typeof(UserAddressEditValidator))]
    public partial class UserAddressEditModel : BaseZajelModel
    {
        public UserAddressEditModel()
        {
            this.Address = new AddressModel();
        }
        public AddressModel Address { get; set; }
    }
}