﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.User
{
    public partial class RegisterResultModel : BaseZajelModel
    {
        public string Result { get; set; }
    }
}