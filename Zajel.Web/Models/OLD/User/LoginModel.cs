﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Attributes;
using Zajel.Framework;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Validators.User;
using Zajel.Framework.Mvc.ModelBinding;

namespace Zajel.Web.Models.User
{
    [Validator(typeof(LoginValidator))]
    public partial class LoginModel : BaseZajelModel
    {
        public bool CheckoutAsGuest { get; set; }

        [ZajelResourceDisplayName("Account.Login.Fields.Email")]
        public string Email { get; set; }

        public bool UsernamesEnabled { get; set; }
        [ZajelResourceDisplayName("Account.Login.Fields.UserName")]
        public string Username { get; set; }

        [DataType(DataType.Password)]
        [NoTrim]
        [ZajelResourceDisplayName("Account.Login.Fields.Password")]
        public string Password { get; set; }

        [ZajelResourceDisplayName("Account.Login.Fields.RememberMe")]
        public bool RememberMe { get; set; }

        public bool DisplayCaptcha { get; set; }
    }
}