﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Common;

namespace Zajel.Web.Models.User
{
    public partial class UserAddressListModel : BaseZajelModel
    {
        public UserAddressListModel()
        {
            Addresses = new List<AddressModel>();
        }

        public IList<AddressModel> Addresses { get; set; }
    }
}