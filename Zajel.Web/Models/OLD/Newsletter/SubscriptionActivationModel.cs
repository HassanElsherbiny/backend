﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Newsletter
{
    public partial class SubscriptionActivationModel : BaseZajelModel
    {
        public string Result { get; set; }
    }
}