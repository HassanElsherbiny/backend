﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Newsletter
{
    public partial class NewsletterBoxModel : BaseZajelModel
    {
        public string NewsletterEmail { get; set; }
        public bool AllowToUnsubscribe { get; set; }
    }
}