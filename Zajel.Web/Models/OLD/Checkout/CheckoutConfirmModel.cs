﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Checkout
{
    public partial class CheckoutConfirmModel : BaseZajelModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();
        }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public string MinOrderTotalWarning { get; set; }

        public IList<string> Warnings { get; set; }
    }
}