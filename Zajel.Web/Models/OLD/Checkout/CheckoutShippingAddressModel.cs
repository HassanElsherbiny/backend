﻿using System.Collections.Generic;
using Zajel.Framework.Mvc.Models;
using Zajel.Web.Models.Common;
using FluentValidation.Attributes;
using Zajel.Web.Validators.User;

namespace Zajel.Web.Models.Checkout
{
    [Validator(typeof(CheckoutShippingAddressValidator))]
    public partial class CheckoutShippingAddressModel : BaseZajelModel
    {
        public CheckoutShippingAddressModel()
        {
            ExistingAddresses = new List<AddressModel>();
            NewAddress = new AddressModel();
            Warnings = new List<string>();
            PickupPoints = new List<CheckoutPickupPointModel>();
        }

        public IList<AddressModel> ExistingAddresses { get; set; }
        public IList<string> Warnings { get; set; }
        public AddressModel NewAddress { get; set; }

        public bool NewAddressPreselected { get; set; }
        public IList<CheckoutPickupPointModel> PickupPoints { get; set; }
        public bool AllowPickUpInStore { get; set; }

        public bool PickUpInStore { get; set; }
        public bool PickUpInStoreOnly { get; set; }
    }
}