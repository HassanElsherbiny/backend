﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Checkout
{
    public partial class CheckoutCompletedModel : BaseZajelModel
    {
        public string OrderId { get; set; }
        public int OrderNumber { get; set; }
        public bool OnePageCheckoutEnabled { get; set; }
    }
}