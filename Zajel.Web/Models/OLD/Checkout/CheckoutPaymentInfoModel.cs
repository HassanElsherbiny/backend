﻿using Zajel.Framework.Mvc.Models;
using Microsoft.AspNetCore.Routing;

namespace Zajel.Web.Models.Checkout
{
    public partial class CheckoutPaymentInfoModel : BaseZajelModel
    {
        public string PaymentViewComponentName { get; set; }

        /// <summary>
        /// Used on one-page checkout page
        /// </summary>
        public bool DisplayOrderTotals { get; set; }
    }
}