﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Checkout
{
    public partial class CheckoutProgressModel : BaseZajelModel
    {
        public CheckoutProgressStep CheckoutProgressStep { get; set; }
    }

    public enum CheckoutProgressStep
    {
        Cart,
        Address,
        Shipping,
        Payment,
        Confirm,
        Complete
    }
}