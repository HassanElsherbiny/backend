﻿using Zajel.Core.Domain.Common;
using Zajel.Framework.Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Zajel.Web.Models.Checkout
{
    public partial class CheckoutPickupPointModel : BaseZajelModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public Address Address { get; set; }

        public string PickupFee { get; set; }

    }
}