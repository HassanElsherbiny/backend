﻿using Zajel.Framework.Mvc.Models;

namespace Zajel.Web.Models.Checkout
{
    public partial class OnePageCheckoutModel : BaseZajelModel
    {
        public bool ShippingRequired { get; set; }
        public bool DisableBillingAddressCheckoutStep { get; set; }
        public CheckoutBillingAddressModel BillingAddress { get; set; }

    }
}