﻿using System;
using Microsoft.AspNetCore.Mvc;
using Zajel.Core;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Forums;
using Zajel.Services.Users;
using Zajel.Services.Forums;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Logging;
using Zajel.Framework.Controllers;
using Zajel.Framework.Security;
using Zajel.Web.Models.PrivateMessages;
using Zajel.Framework.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace Zajel.Web.Controllers
{
    [HttpsRequirement(SslRequirement.Yes)]
    public partial class PrivateMessagesController : BasePublicController
    {
        #region Fields

        private readonly IForumService _forumService;
        private readonly IUserService _userService;
        private readonly IUserActivityService _userActivityService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ForumSettings _forumSettings;
        private readonly UserSettings _userSettings;

        #endregion

        #region Constructors

        public PrivateMessagesController(IForumService forumService,
            IUserService userService, IUserActivityService userActivityService,
            ILocalizationService localizationService, IWorkContext workContext, 
            IStoreContext storeContext, IDateTimeHelper dateTimeHelper,
            ForumSettings forumSettings, UserSettings userSettings)
        {
            this._forumService = forumService;
            this._userService = userService;
            this._userActivityService = userActivityService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._dateTimeHelper = dateTimeHelper;
            this._forumSettings = forumSettings;
            this._userSettings = userSettings;
        }

        #endregion

        #region Methods

        public virtual IActionResult Index(int? pageNumber, string tab)
        {
            if (!_forumSettings.AllowPrivateMessages)
            {
                return RedirectToRoute("HomePage");
            }

            if (_workContext.CurrentUser.IsGuest())
            {
                return new UnauthorizedResult();
            }

            int inboxPage = 0;
            int sentItemsPage = 0;
            bool sentItemsTabSelected = false;

            switch (tab)
            {
                case "inbox":
                    if (pageNumber.HasValue)
                    {
                        inboxPage = pageNumber.Value;
                    }
                    break;
                case "sent":
                    if (pageNumber.HasValue)
                    {
                        sentItemsPage = pageNumber.Value;
                    }
                    sentItemsTabSelected = true;
                    break;
                default:
                    break;
            }

            var model = new PrivateMessageIndexModel
            {
                InboxPage = inboxPage,
                SentItemsPage = sentItemsPage,
                SentItemsTabSelected = sentItemsTabSelected
            };

            return View(model);
        }

        [HttpPost, FormValueRequired("delete-inbox"), ActionName("InboxUpdate")]
        [PublicAntiForgery]
        public virtual IActionResult DeleteInboxPM(IFormCollection formCollection)
        {
            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("pm", StringComparison.OrdinalIgnoreCase))
                {
                    var id = key.Replace("pm", "").Trim();
                    var pm = _forumService.GetPrivateMessageById(id);
                    if (pm != null)
                    {
                        if (pm.ToUserId == _workContext.CurrentUser.Id)
                        {
                            pm.IsDeletedByRecipient = true;
                            _forumService.UpdatePrivateMessage(pm);
                        }
                    }
                }
            }
            return RedirectToRoute("PrivateMessages");
        }

        [HttpPost, FormValueRequired("mark-unread"), ActionName("InboxUpdate")]
        [PublicAntiForgery]
        public virtual IActionResult MarkUnread(IFormCollection formCollection)
        {
            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("pm", StringComparison.OrdinalIgnoreCase))
                {
                    var id = key.Replace("pm", "").Trim();
                    var pm = _forumService.GetPrivateMessageById(id);
                    if (pm != null)
                    {
                        if (pm.ToUserId == _workContext.CurrentUser.Id)
                        {
                            pm.IsRead = false;
                            _forumService.UpdatePrivateMessage(pm);
                        }
                    }
                }
            }
            return RedirectToRoute("PrivateMessages");
        }

        //updates sent items (deletes PrivateMessages)
        [HttpPost, FormValueRequired("delete-sent"), ActionName("SentUpdate")]
        [PublicAntiForgery]
        public virtual IActionResult DeleteSentPM(IFormCollection formCollection)
        {
            foreach (var key in formCollection.Keys)
            {
                var value = formCollection[key];

                if (value.Equals("on") && key.StartsWith("si", StringComparison.OrdinalIgnoreCase))
                {
                    var id = key.Replace("si", "").Trim();
                    PrivateMessage pm = _forumService.GetPrivateMessageById(id);
                    if (pm != null)
                    {
                        if (pm.FromUserId == _workContext.CurrentUser.Id)
                        {
                            pm.IsDeletedByAuthor = true;
                            _forumService.UpdatePrivateMessage(pm);
                        }
                    }
                }

            }
            return RedirectToRoute("PrivateMessages", new {tab = "sent"});
        }

        public virtual IActionResult SendPM(string toUserId, string replyToMessageId)
        {
            if (!_forumSettings.AllowPrivateMessages)
            {
                return RedirectToRoute("HomePage");
            }

            if (_workContext.CurrentUser.IsGuest())
            {
                return new UnauthorizedResult();
            }

            var userTo = _userService.GetUserById(toUserId);

            if (userTo == null || userTo.IsGuest())
            {
                return RedirectToRoute("PrivateMessages");
            }

            var model = new SendPrivateMessageModel();
            model.ToUserId = userTo.Id;
            model.UserToName = userTo.FormatUserName();
            model.AllowViewingToProfile = _userSettings.AllowViewingProfiles && !userTo.IsGuest();

            if (!String.IsNullOrEmpty(replyToMessageId))
            {
                var replyToPM = _forumService.GetPrivateMessageById(replyToMessageId);
                if (replyToPM == null)
                {
                    return RedirectToRoute("PrivateMessages");
                }

                if (replyToPM.ToUserId == _workContext.CurrentUser.Id || replyToPM.FromUserId == _workContext.CurrentUser.Id)
                {
                    model.ReplyToMessageId = replyToPM.Id;
                    model.Subject = string.Format("Re: {0}", replyToPM.Subject);
                }
                else
                {
                    return RedirectToRoute("PrivateMessages");
                }
            }
            return View(model);
        }

        [HttpPost]
        [PublicAntiForgery]
        public virtual IActionResult SendPM(SendPrivateMessageModel model)
        {
            if (!_forumSettings.AllowPrivateMessages)
            {
                return RedirectToRoute("HomePage");
            }

            if (_workContext.CurrentUser.IsGuest())
            {
                return new UnauthorizedResult();
            }

            User toUser = null;
            var replyToPM = _forumService.GetPrivateMessageById(model.ReplyToMessageId);
            if (replyToPM != null)
            {
                if (replyToPM.ToUserId == _workContext.CurrentUser.Id || replyToPM.FromUserId == _workContext.CurrentUser.Id)
                {
                    toUser = replyToPM.FromUserId == _workContext.CurrentUser.Id
                                ? _userService.GetUserById(replyToPM.ToUserId)
                                : _userService.GetUserById(replyToPM.FromUserId);

                }
                else
                {
                    return RedirectToRoute("PrivateMessages");
                }
            }
            else
            {
                toUser = _userService.GetUserById(model.ToUserId);
            }

            if (toUser == null || toUser.IsGuest())
            {
                return RedirectToRoute("PrivateMessages");
            }
            model.ToUserId = toUser.Id;
            model.UserToName = toUser.FormatUserName();
            model.AllowViewingToProfile = _userSettings.AllowViewingProfiles && !toUser.IsGuest();

            if (ModelState.IsValid)
            {
                try
                {
                    string subject = model.Subject;
                    if (_forumSettings.PMSubjectMaxLength > 0 && subject.Length > _forumSettings.PMSubjectMaxLength)
                    {
                        subject = subject.Substring(0, _forumSettings.PMSubjectMaxLength);
                    }

                    var text = model.Message;
                    if (_forumSettings.PMTextMaxLength > 0 && text.Length > _forumSettings.PMTextMaxLength)
                    {
                        text = text.Substring(0, _forumSettings.PMTextMaxLength);
                    }

                    var nowUtc = DateTime.UtcNow;

                    var privateMessage = new PrivateMessage
                    {
                        StoreId = _storeContext.CurrentStore.Id,
                        ToUserId = toUser.Id,
                        FromUserId = _workContext.CurrentUser.Id,
                        Subject = subject,
                        Text = text,
                        IsDeletedByAuthor = false,
                        IsDeletedByRecipient = false,
                        IsRead = false,
                        CreatedOnUtc = nowUtc
                    };

                    _forumService.InsertPrivateMessage(privateMessage);

                    //activity log
                    _userActivityService.InsertActivity("PublicStore.SendPM", "", _localizationService.GetResource("ActivityLog.PublicStore.SendPM"), toUser.Email);

                    return RedirectToRoute("PrivateMessages", new { tab = "sent" });
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.Message);
                }
            }

            return View(model);
        }

        public virtual IActionResult ViewPM(string privateMessageId)
        {
            if (!_forumSettings.AllowPrivateMessages)
            {
                return RedirectToRoute("HomePage");
            }

            if (_workContext.CurrentUser.IsGuest())
            {
                return new UnauthorizedResult();
            }

            var pm = _forumService.GetPrivateMessageById(privateMessageId);
            if (pm != null)
            {
                if (pm.ToUserId != _workContext.CurrentUser.Id && pm.FromUserId != _workContext.CurrentUser.Id)
                {
                    return RedirectToRoute("PrivateMessages");
                }

                if (!pm.IsRead && pm.ToUserId == _workContext.CurrentUser.Id)
                {
                    pm.IsRead = true;
                    _forumService.UpdatePrivateMessage(pm);
                }
            }
            else
            {
                return RedirectToRoute("PrivateMessages");
            }

            var fromUser = _userService.GetUserById(pm.FromUserId);
            var toUser = _userService.GetUserById(pm.ToUserId);


            var model = new PrivateMessageModel
            {
                Id = pm.Id,
                FromUserId = fromUser.Id,
                UserFromName = fromUser.FormatUserName(),
                AllowViewingFromProfile = _userSettings.AllowViewingProfiles && fromUser != null && !fromUser.IsGuest(),
                ToUserId = toUser.Id,
                UserToName = toUser.FormatUserName(),
                AllowViewingToProfile = _userSettings.AllowViewingProfiles && toUser != null && !toUser.IsGuest(),
                Subject = pm.Subject,
                Message = pm.FormatPrivateMessageText(),
                CreatedOn = _dateTimeHelper.ConvertToUserTime(pm.CreatedOnUtc, DateTimeKind.Utc),
                IsRead = pm.IsRead,
            };

            return View(model);
        }

        public virtual IActionResult DeletePM(string privateMessageId)
        {
            if (!_forumSettings.AllowPrivateMessages)
            {
                return RedirectToRoute("HomePage");
            }

            if (_workContext.CurrentUser.IsGuest())
            {
                return new UnauthorizedResult();
            }

            var pm = _forumService.GetPrivateMessageById(privateMessageId);
            if (pm != null)
            {
                if (pm.FromUserId == _workContext.CurrentUser.Id)
                {
                    pm.IsDeletedByAuthor = true;
                    _forumService.UpdatePrivateMessage(pm);
                }

                if (pm.ToUserId == _workContext.CurrentUser.Id)
                {
                    pm.IsDeletedByRecipient = true;
                    _forumService.UpdatePrivateMessage(pm);
                }
            }
            return RedirectToRoute("PrivateMessages");
        }

        #endregion
    }
}
