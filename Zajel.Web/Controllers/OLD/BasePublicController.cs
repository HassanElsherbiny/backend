﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Controllers;
using Zajel.Framework.Mvc.Filters;
using Zajel.Framework.Security;

namespace Zajel.Web.Controllers
{
    [HttpsRequirement(SslRequirement.NoMatter)]
    [WwwRequirement]
    [CheckAccessPublicStore]
    [CheckAccessClosedStore]
    [CheckLanguageSeoCode]
    [CheckAffiliate]
    public abstract partial class BasePublicController : BaseController
    {
        protected virtual IActionResult InvokeHttp404()
        {
            Response.StatusCode = 404;
            return new EmptyResult();
        }
    }
}
