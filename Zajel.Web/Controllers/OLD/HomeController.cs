﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Framework.Security;
using Zajel.Framework.Mvc.Filters;

namespace Zajel.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        [HttpsRequirement(SslRequirement.No)]
        public virtual IActionResult Index()
        {
            return View();
        }
    }
}
