﻿using Microsoft.AspNetCore.Mvc;
using Zajel.Services.Authentication.External;
using Zajel.Web.Services;

namespace Zajel.Web.Controllers
{
    public partial class ExternalAuthenticationController : BasePublicController
    {
        #region Fields

        private readonly IExternalAuthenticationWebService _externalAuthenticationWebService;

        #endregion

		#region Constructors

        public ExternalAuthenticationController(IExternalAuthenticationWebService externalAuthenticationWebService)
        {
            this._externalAuthenticationWebService = externalAuthenticationWebService;
        }

        #endregion

        #region Methods

        public RedirectResult RemoveParameterAssociation(string returnUrl)
        {
            //prevent open redirection attack
            if (!Url.IsLocalUrl(returnUrl))
                returnUrl = Url.RouteUrl("HomePage");

            ExternalAuthorizerHelper.RemoveParameters();
            return Redirect(returnUrl);
        }

        #endregion
    }
}
