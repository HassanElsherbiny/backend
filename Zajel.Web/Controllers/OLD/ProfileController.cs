﻿using System;
using Microsoft.AspNetCore.Mvc;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Media;
using Zajel.Services.Users;
using Zajel.Services.Directory;
using Zajel.Services.Forums;
using Zajel.Services.Helpers;
using Zajel.Services.Localization;
using Zajel.Services.Media;
using Zajel.Framework.Security;
using Zajel.Web.Models.Profile;
using Zajel.Services.Security;
using Zajel.Framework.Mvc.Filters;

namespace Zajel.Web.Controllers
{
    [HttpsRequirement(SslRequirement.No)]
    public partial class ProfileController : BasePublicController
    {
        private readonly IForumService _forumService;
        private readonly ILocalizationService _localizationService;
        private readonly IPictureService _pictureService;
        private readonly ICountryService _countryService;
        private readonly IUserService _userService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPermissionService _permissionService;
        private readonly ForumSettings _forumSettings;
        private readonly UserSettings _userSettings;
        private readonly MediaSettings _mediaSettings;

        public ProfileController(IForumService forumService,
            ILocalizationService localizationService,
            IPictureService pictureService,
            ICountryService countryService,
            IUserService userService,
            IDateTimeHelper dateTimeHelper,
            IPermissionService permissionService,
            ForumSettings forumSettings,
            UserSettings userSettings,
            MediaSettings mediaSettings)
        {
            this._forumService = forumService;
            this._localizationService = localizationService;
            this._pictureService = pictureService;
            this._countryService = countryService;
            this._userService = userService;
            this._permissionService = permissionService;
            this._dateTimeHelper = dateTimeHelper;
            this._forumSettings = forumSettings;
            this._userSettings = userSettings;
            this._mediaSettings = mediaSettings;
        }

        public virtual IActionResult Index(string id, int? pageNumber)
        {
            if (!_userSettings.AllowViewingProfiles)
            {
                return RedirectToRoute("HomePage");
            }

            var userId ="";
            if (!String.IsNullOrEmpty(id))
            {
                userId = id;
            }

            var user = _userService.GetUserById(userId);
            if (user == null || user.IsGuest())
            {
                return RedirectToRoute("HomePage");
            }

            bool pagingPosts = false;
            int postsPage = 0;

            if (pageNumber.HasValue)
            {
                postsPage = pageNumber.Value;
                pagingPosts = true;
            }

            var name = user.FormatUserName();
            var title = string.Format(_localizationService.GetResource("Profile.ProfileOf"), name);

            var model = new ProfileIndexModel
            {
                ProfileTitle = title,
                PostsPage = postsPage,
                PagingPosts = pagingPosts,
                UserProfileId = user.Id,
                ForumsEnabled = _forumSettings.ForumsEnabled
            };

            //display "edit" (manage) link
            if (_permissionService.Authorize(StandardPermissionProvider.AccessAdminPanel) && _permissionService.Authorize(StandardPermissionProvider.ManageUsers))
                DisplayEditLink(Url.Action("Edit", "User", new { id = user.Id, area = "Admin" }));

            return View(model);
        }
    }
}
