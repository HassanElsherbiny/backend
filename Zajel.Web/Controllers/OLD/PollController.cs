﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Zajel.Core;
using Zajel.Core.Domain.Users;
using Zajel.Services.Localization;
using Zajel.Services.Polls;
using Zajel.Web.Services;

namespace Zajel.Web.Controllers
{
    public partial class PollController : BasePublicController
    {
        #region Fields

        private readonly IPollWebService _pollWebService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IPollService _pollService;
        #endregion

        #region Constructors

        public PollController(IPollWebService pollWebService,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IPollService pollService)
        {
            this._pollWebService = pollWebService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._pollService = pollService;
        }

        #endregion

        #region Methods

        [HttpPost]
        public virtual IActionResult Vote(string pollAnswerId, string pollId)
        {
            var poll = _pollService.GetPollById(pollId); 
            if (!poll.Published)
                return Json(new
                {
                    error = "Poll is not available",
                });

            var pollAnswer = poll.PollAnswers.FirstOrDefault(x=>x.Id == pollAnswerId);
            if (pollAnswer == null)
                return Json(new
                {
                    error = "No poll answer found with the specified id",
                });


            if (_workContext.CurrentUser.IsGuest() && !poll.AllowGuestsToVote)
                return Json(new
                {
                    error = _localizationService.GetResource("Polls.OnlyRegisteredUsersVote"),
                });

            bool alreadyVoted = _pollService.AlreadyVoted(poll.Id, _workContext.CurrentUser.Id);
            if (!alreadyVoted)
            {
                //vote
                _pollWebService.PollVoting(poll, pollAnswer);
            }

            return Json(new
            {
                html = this.RenderPartialViewToString("_Poll", _pollWebService.PreparePoll(poll, true)),
            });
        }
        
        #endregion

    }
}
