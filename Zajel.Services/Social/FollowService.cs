﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain;
using Zajel.Core.Domain.Localization;

using Zajel.Services.Localization;
using Zajel.Services.Messages;

namespace Zajel.Services.Social
{
    public class FollowService : IFollowService
    {
        #region Constants

        private const string FOLLOW_BY_ID_KEY = "Zajel.product.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string FOLLOW_PATTERN_KEY = "Zajel.Follow.";

        #endregion


        #region Fields
        private readonly IRepository<UserFollow> _FollowRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly LocalizationSettings _localizationSettings;
        #endregion

        #region Ctor
        public FollowService(
            IRepository<UserFollow> _FollowRepository,
        ILanguageService _languageService,
        IWorkflowMessageService _workflowMessageService,
        IDataProvider _dataProvider,
        ICacheManager _cacheManager,
        IWorkContext _workContext,
        IStoreContext _storeContext,
        LocalizationSettings _localizationSettings
            )
        {
            this._FollowRepository = _FollowRepository;
            this._languageService = _languageService;
            this._workflowMessageService = _workflowMessageService;
            this._dataProvider = _dataProvider;
            this._cacheManager = _cacheManager;
            this._workContext = _workContext;
            this._storeContext = _storeContext;
            this._localizationSettings = _localizationSettings;
        }
        #endregion

        #region Shared Methods

        public virtual IQueryable<UserFollow> Get(Expression<Func<UserFollow, bool>> where = null, Expression<Func<UserFollow, object>> orderBy = null, bool ascending = true, int page = 1, int count = int.MaxValue)
        {
            if (where == null)
                where = (x => true);

            var resultSet = _FollowRepository.Table.Where(@where);

            if (orderBy != null)
                //order
                resultSet = ascending ? resultSet.OrderBy(orderBy) : resultSet.OrderByDescending(orderBy);
            else
                resultSet = resultSet.OrderBy(x => x.Id);

            //pagination
            resultSet = resultSet.Skip((page - 1) * count).Take(count);
            return resultSet;

        }

        #endregion

        #region Methods

        public UserFollow GetCustomerFollow<T>(int customerId, int entityId)
        {
            return
                _FollowRepository.Table.Where(
                    x =>
                        x.FollowingEntityId == entityId && x.UserId == customerId &&
                        x.FollowingEntityName == typeof(T).Name).FirstOrDefault();
        }

        public IQueryable<UserFollow> GetFollowing<T>(int customerId, int page = 1, int count = 15)
        {
            return
                Get(x => x.UserId == customerId && x.FollowingEntityName == typeof(T).Name, null, true, page, count);
        }

        public IQueryable<UserFollow> GetFollowing(int customerId, int page = 1, int count = 15)
        {
            return
               Get(x => x.UserId == customerId, null, true, page, count);
        }

        public void Insert<T>(int customerId, int entityId)
        {
            //insert only if required
            if (!_FollowRepository.Table.Any(
                    x =>
                        x.FollowingEntityId == entityId && x.UserId == customerId &&
                        x.FollowingEntityName == typeof(T).Name))
            {
                var customerFollow = new UserFollow()
                {
                    UserId = customerId,
                    FollowingEntityId = entityId,
                    FollowingEntityName = typeof(T).Name,
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                };
                _FollowRepository.Insert(customerFollow);
            }

        }

        public void Delete<T>(int customerId, int entityId)
        {
            var customerFollow = GetCustomerFollow<T>(customerId, entityId);
            if (customerFollow != null)
                _FollowRepository.Delete(customerFollow);
        }

        public int GetFollowerCount<T>(int entityId)
        {
            return GetFollowers<T>(entityId).Count();
        }

        public IQueryable<UserFollow> GetFollowers<T>(int entityId, int page = 1, int count = 15)
        {
            return
               Get(x => x.FollowingEntityId == entityId && x.FollowingEntityName == typeof(T).Name, null, true, page, count);
        }
        #endregion
    }
}