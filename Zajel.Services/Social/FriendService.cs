﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain;
using Zajel.Core.Domain.Localization;

using Zajel.Services.Localization;
using Zajel.Services.Messages;

namespace Zajel.Services.Social
{
    public class FriendService : IFriendService
    {
        #region Constants

        private const string Friend_BY_ID_KEY = "Zajel.product.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string Friend_PATTERN_KEY = "Zajel.Friend.";

        #endregion


        #region Fields
        private readonly IRepository<Friend> _FriendRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly LocalizationSettings _localizationSettings;
        #endregion

        #region Ctor
        public FriendService(
            IRepository<Friend> _FriendRepository,
        ILanguageService _languageService,
        IWorkflowMessageService _workflowMessageService,
        IDataProvider _dataProvider,
        ICacheManager _cacheManager,
        IWorkContext _workContext,
        IStoreContext _storeContext,
        LocalizationSettings _localizationSettings
            )
        {
            this._FriendRepository = _FriendRepository;
            this._languageService = _languageService;
            this._workflowMessageService = _workflowMessageService;
            this._dataProvider = _dataProvider;
            this._cacheManager = _cacheManager;
            this._workContext = _workContext;
            this._storeContext = _storeContext;
            this._localizationSettings = _localizationSettings;
        }
        #endregion

        #region Shared Methods

        public virtual IQueryable<Friend> Get(Expression<Func<Friend, bool>> where = null, Expression<Func<Friend, object>> orderBy = null, bool ascending = true, int page = 1, int count = int.MaxValue)
        {
            if (where == null)
                where = (x => true);

            var resultSet = _FriendRepository.Table.Where(@where);

            if (orderBy != null)
                //order
                resultSet = ascending ? resultSet.OrderBy(orderBy) : resultSet.OrderByDescending(orderBy);
            else
                resultSet = resultSet.OrderBy(x => x.Id);

            //pagination
            resultSet = resultSet.Skip((page - 1) * count).Take(count);
            return resultSet;

        }
       
        #endregion

        #region Methods

        public Friend GetUserFriendship(int User1Id, int User2Id)
        {
            return
                Get(x =>
                    (x.FromUserId == User1Id && x.ToUserId == User2Id) ||
                    (x.ToUserId == User1Id && x.FromUserId == User2Id)).FirstOrDefault();
        }


        public Friend GetUserFriend(int fromUserId, int toUserId)
        {
            return
                Get(x => (x.FromUserId == fromUserId && x.ToUserId == toUserId)).FirstOrDefault();
        }

        public FriendStatus GetFriendStatus(int currentUserId, int friendId)
        {
            if (currentUserId == friendId)
                return FriendStatus.Self;

            var friend = GetUserFriendship(currentUserId, friendId);
            if (friend == null)
                return FriendStatus.None;

            if (friend.Confirmed)
                return FriendStatus.Friends;

            return friend.FromUserId == currentUserId ? FriendStatus.FriendRequestSent : FriendStatus.NeedsConfirmed;
        }


        public IList<Friend> GetFriendRequests(int UserId)
        {
            return Get(x => !x.Confirmed && x.ToUserId == UserId).ToList();
        }

        public IQueryable<Friend> GetFriends(int UserId, int page = 1, int count = 15, bool random = false)
        {
            var friends =
                Get(x => (x.FromUserId == UserId || x.ToUserId == UserId) && x.Confirmed, null, true,
                    page, count);

            if (random)
                friends = friends.OrderBy(x => Guid.NewGuid());

            return friends;
        }

        public IList<Friend> GetAllUserFriends(int UserId)
        {
            return Get(x => x.FromUserId == UserId || x.ToUserId == UserId).ToList();
        }

        public int GetTotalFriendCount(int UserId)
        {
            return _FriendRepository.Table.Count(x => x.Confirmed && (x.FromUserId == UserId || x.ToUserId == UserId));
        }
        #endregion
    }

    public enum FriendStatus
    {
        FriendRequestSent = 1,
        NeedsConfirmed = 2,
        Friends = 3,
        Self = 4,
        None = 0
    }
}