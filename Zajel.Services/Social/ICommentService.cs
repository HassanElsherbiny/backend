﻿using System.Collections.Generic;
using Zajel.Core.Domain;

namespace Zajel.Services.Social
{
    public interface ICommentService
    {
        int GetCommentsCount(int entityId, string entityName);

        IList<Comment> GetEntityComments(int entityId, string entityName, int page = 1, int count = 5);
    }
}