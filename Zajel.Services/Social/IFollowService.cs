﻿using System.Linq;
using Zajel.Core.Domain;


namespace Zajel.Services.Social
{
    public interface IFollowService
    {
        UserFollow GetCustomerFollow<T>(int customerId, int entityId);

        IQueryable<UserFollow> GetFollowing<T>(int customerId, int page = 1, int count = 15);

        IQueryable<UserFollow> GetFollowing(int customerId, int page = 1, int count = 15);

        void Insert<T>(int customerId, int entityId);

        void Delete<T>(int customerId, int entityId);

        int GetFollowerCount<T>(int entityId);

        IQueryable<UserFollow> GetFollowers<T>(int entityId, int page = 1, int count = 15);
    }
}