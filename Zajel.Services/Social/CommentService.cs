﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain;
using Zajel.Core.Domain.Localization;

using Zajel.Services.Localization;
using Zajel.Services.Messages;

namespace Zajel.Services.Social
{
    public class CommentService : ICommentService
    {
        #region Constants

        private const string Comment_BY_ID_KEY = "Zajel.product.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string Comment_PATTERN_KEY = "Zajel.Comment.";

        #endregion


        #region Fields
        private readonly IRepository<Comment> _CommentRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly LocalizationSettings _localizationSettings;
        #endregion

        #region Ctor
        public CommentService(
            IRepository<Comment> _CommentRepository,
        ILanguageService _languageService,
        IWorkflowMessageService _workflowMessageService,
        IDataProvider _dataProvider,
        ICacheManager _cacheManager,
        IWorkContext _workContext,
        IStoreContext _storeContext,
        LocalizationSettings _localizationSettings
            )
        {
            this._CommentRepository = _CommentRepository;
            this._languageService = _languageService;
            this._workflowMessageService = _workflowMessageService;
            this._dataProvider = _dataProvider;
            this._cacheManager = _cacheManager;
            this._workContext = _workContext;
            this._storeContext = _storeContext;
            this._localizationSettings = _localizationSettings;
        }
        #endregion

        #region Shared Methods

        public virtual IQueryable<Comment> Get(Expression<Func<Comment, bool>> where = null, Expression<Func<Comment, object>> orderBy = null, bool ascending = true, int page = 1, int count = int.MaxValue)
        {
            if (where == null)
                where = (x => true);

            var resultSet = _CommentRepository.Table.Where(@where);

            if (orderBy != null)
                //order
                resultSet = ascending ? resultSet.OrderBy(orderBy) : resultSet.OrderByDescending(orderBy);
            else
                resultSet = resultSet.OrderBy(x => x.Id);

            //pagination
            resultSet = resultSet.Skip((page - 1) * count).Take(count);
            return resultSet;

        }

        #endregion

        #region Methods
        public int GetCommentsCount(int entityId, string entityName)
        {
            return
                _CommentRepository.Table.Count(x => x.EntityId == entityId && x.EntityName == entityName);
        }

        public IList<Comment> GetEntityComments(int entityId, string entityName, int page = 1, int count = 5)
        {
            return
                Get(x => x.EntityId == entityId && x.EntityName == entityName)
                    .OrderBy(x => x.DateCreated)
                    .Skip(count * (page - 1))
                    .Take(count)
                    .ToList();
        }
        #endregion
    }

   
}