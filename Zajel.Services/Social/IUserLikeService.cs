﻿using System.Collections.Generic;
using Zajel.Core.Domain;

namespace Zajel.Services.Social
{
    public interface IUserLikeService 
    {
        UserLike GetUserLike<T>(int UserId, int entityId);

        IList<UserLike> GetUserLikes<T>(int UserId);

        void Insert<T>(int UserId, int entityId);

        void Delete<T>(int UserId, int entityId);

        int GetLikeCount<T>(int entityId);

        IList<UserLike> GetEntityLikes<T>(int entityId);
    }
}