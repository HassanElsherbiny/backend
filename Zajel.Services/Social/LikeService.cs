﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain;
using Zajel.Core.Domain.Localization;

using Zajel.Services.Localization;
using Zajel.Services.Messages;

namespace Zajel.Services.Social
{
    public class UserLikeService : IUserLikeService
    {
        #region Constants

        private const string UserLike_BY_ID_KEY = "Zajel.product.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string UserLike_PATTERN_KEY = "Zajel.UserLike.";

        #endregion


        #region Fields
        private readonly IRepository<UserLike> _UserLikeRepository;
        private readonly ILanguageService _languageService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IDataProvider _dataProvider;
        private readonly ICacheManager _cacheManager;
        private readonly IWorkContext _workContext;
        private readonly IStoreContext _storeContext;
        private readonly LocalizationSettings _localizationSettings;
        #endregion

        #region Ctor
        public UserLikeService(
            IRepository<UserLike> _UserLikeRepository,
        ILanguageService _languageService,
        IWorkflowMessageService _workflowMessageService,
        IDataProvider _dataProvider,
        ICacheManager _cacheManager,
        IWorkContext _workContext,
        IStoreContext _storeContext,
        LocalizationSettings _localizationSettings
            )
        {
            this._UserLikeRepository = _UserLikeRepository;
            this._languageService = _languageService;
            this._workflowMessageService = _workflowMessageService;
            this._dataProvider = _dataProvider;
            this._cacheManager = _cacheManager;
            this._workContext = _workContext;
            this._storeContext = _storeContext;
            this._localizationSettings = _localizationSettings;
        }
        #endregion

        #region Shared Methods

        public virtual IQueryable<UserLike> Get(Expression<Func<UserLike, bool>> where = null, Expression<Func<UserLike, object>> orderBy = null, bool ascending = true, int page = 1, int count = int.MaxValue)
        {
            if (where == null)
                where = (x => true);

            var resultSet = _UserLikeRepository.Table.Where(@where);

            if (orderBy != null)
                //order
                resultSet = ascending ? resultSet.OrderBy(orderBy) : resultSet.OrderByDescending(orderBy);
            else
                resultSet = resultSet.OrderBy(x => x.Id);

            //pagination
            resultSet = resultSet.Skip((page - 1) * count).Take(count);
            return resultSet;

        }

        #endregion

        #region Methods
        public UserLike GetUserLike<T>(int UserId, int entityId)
        {
            return
                Get(
                    x => x.EntityId == entityId && x.UserId == UserId && x.EntityName == typeof(T).Name)
                    .FirstOrDefault();
        }

        public IList<UserLike> GetUserLikes<T>(int UserId)
        {
            return Get(x => x.UserId == UserId && x.EntityName == typeof(T).Name).ToList();
        }


        public void Insert<T>(int UserId, int entityId)
        {
            //insert only if required
            if (
              _UserLikeRepository.Table.Any(
                    x =>
                        x.EntityId == entityId && x.UserId == UserId &&
                        x.EntityName == typeof(T).Name))
            {
                var UserLike = new UserLike()
                {
                    UserId = UserId,
                    EntityId = entityId,
                    EntityName = typeof(T).Name,
                    DateCreated = DateTime.UtcNow,
                    DateUpdated = DateTime.UtcNow
                };
                _UserLikeRepository.Insert(UserLike);
            }

        }

        public void Delete<T>(int UserId, int entityId)
        {
            var UserLike = GetUserLike<T>(UserId, entityId);
            if (UserLike != null)
                _UserLikeRepository.Delete(UserLike);
        }

        public int GetLikeCount<T>(int entityId)
        {
            return GetEntityLikes<T>(entityId).Count;
        }

        public IList<UserLike> GetEntityLikes<T>(int entityId)
        {
            return
                Get(x => x.EntityId == entityId && x.EntityName == typeof(T).Name).ToList();
        }

       
 
        #endregion
    }

    
}