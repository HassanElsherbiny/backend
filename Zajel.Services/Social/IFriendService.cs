﻿using System.Collections.Generic;
using System.Linq;
using Zajel.Core.Domain;

namespace Zajel.Services.Social
{
    public interface IFriendService
    {
        Friend GetUserFriendship(int User1Id, int User2Id);

        Friend GetUserFriend(int fromUserId, int toUserId);

        FriendStatus GetFriendStatus(int currentUserId, int friendId);

        IList<Friend> GetFriendRequests(int UserId);

        IQueryable<Friend> GetFriends(int UserId, int page = 1, int count = 15, bool random = false);

        IList<Friend> GetAllUserFriends(int UserId);

        int GetTotalFriendCount(int UserId);
    }
}