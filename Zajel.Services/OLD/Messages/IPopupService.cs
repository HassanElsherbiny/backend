﻿using System.Collections.Generic;
using Zajel.Core.Domain.Messages;

namespace Zajel.Services.Messages
{
    public partial interface IPopupService
    {
        /// <summary>
        /// Inserts a popup
        /// </summary>
        /// <param name="Popup">Popup</param>        
        void InsertPopupActive(PopupActive popup);
        /// <summary>
        /// Gets active banner for user
        /// </summary>
        /// <returns>BannerActive</returns>
        PopupActive GetActivePopupByUserId(string userId);

        /// <summary>
        /// Move popup to archive
        /// </summary>
        void MovepopupToArchive(string id, string userId);

    }
}
