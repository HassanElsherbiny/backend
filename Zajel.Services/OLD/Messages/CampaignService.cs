﻿using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Core;
using Zajel.Core.Data;
using Zajel.Core.Domain.Messages;
using Zajel.Services.Users;
using Zajel.Services.Events;
using Zajel.Core.Domain.Users;
using Zajel.Services.Logging;
using Zajel.Services.Localization;

namespace Zajel.Services.Messages
{
    public partial class CampaignService : ICampaignService
    {
        private readonly IRepository<Campaign> _campaignRepository;
        private readonly IRepository<CampaignHistory> _campaignHistoryRepository;
        private readonly IRepository<NewsLetterSubscription> _newsLetterSubscriptionRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IEmailSender _emailSender;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly ITokenizer _tokenizer;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IUserService _userService;
        private readonly IStoreContext _storeContext;
        private readonly IEventPublisher _eventPublisher;
        private readonly IUserActivityService _userActivityService;
        private readonly ILocalizationService _localizationService;

        public CampaignService(IRepository<Campaign> campaignRepository,
            IRepository<CampaignHistory> campaignHistoryRepository,
            IRepository<NewsLetterSubscription> newsLetterSubscriptionRepository,
            IRepository<User> userRepository,
            IEmailSender emailSender, IMessageTokenProvider messageTokenProvider,
            ITokenizer tokenizer, IQueuedEmailService queuedEmailService,
            IUserService userService, IStoreContext storeContext,
            IEventPublisher eventPublisher,
            IUserActivityService userActivityService,
            ILocalizationService localizationService)
        {
            this._campaignRepository = campaignRepository;
            this._campaignHistoryRepository = campaignHistoryRepository;
            this._newsLetterSubscriptionRepository = newsLetterSubscriptionRepository;
            this._userRepository = userRepository;
            this._emailSender = emailSender;
            this._messageTokenProvider = messageTokenProvider;
            this._tokenizer = tokenizer;
            this._queuedEmailService = queuedEmailService;
            this._storeContext = storeContext;
            this._userService = userService;
            this._eventPublisher = eventPublisher;
            this._userActivityService = userActivityService;
            this._localizationService = localizationService;
        }

        /// <summary>
        /// Inserts a campaign
        /// </summary>
        /// <param name="campaign">Campaign</param>        
        public virtual void InsertCampaign(Campaign campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            _campaignRepository.Insert(campaign);

            //event notification
            _eventPublisher.EntityInserted(campaign);
        }

        /// <summary>
        /// Inserts a campaign history
        /// </summary>
        /// <param name="campaign">Campaign</param>        
        public virtual void InsertCampaignHistory(CampaignHistory campaignhistory)
        {
            if (campaignhistory == null)
                throw new ArgumentNullException("campaignhistory");

            _campaignHistoryRepository.Insert(campaignhistory);

        }

        /// <summary>
        /// Updates a campaign
        /// </summary>
        /// <param name="campaign">Campaign</param>
        public virtual void UpdateCampaign(Campaign campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            _campaignRepository.Update(campaign);

            //event notification
            _eventPublisher.EntityUpdated(campaign);
        }

        /// <summary>
        /// Deleted a queued email
        /// </summary>
        /// <param name="campaign">Campaign</param>
        public virtual void DeleteCampaign(Campaign campaign)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            _campaignRepository.Delete(campaign);

            //event notification
            _eventPublisher.EntityDeleted(campaign);
        }

        /// <summary>
        /// Gets a campaign by identifier
        /// </summary>
        /// <param name="campaignId">Campaign identifier</param>
        /// <returns>Campaign</returns>
        public virtual Campaign GetCampaignById(string campaignId)
        {
            return _campaignRepository.GetById(campaignId);

        }

        /// <summary>
        /// Gets all campaigns
        /// </summary>
        /// <returns>Campaigns</returns>
        public virtual IList<Campaign> GetAllCampaigns()
        {

            var query = from c in _campaignRepository.Table
                        orderby c.CreatedOnUtc
                        select c;
            var campaigns = query.ToList();

            return campaigns;
        }
        public virtual IPagedList<CampaignHistory> GetCampaignHistory(Campaign campaign, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            var query = from c in _campaignHistoryRepository.Table
                        where c.CampaignId == campaign.Id
                        orderby c.CreatedDateUtc descending
                        select c;
            var list = new PagedList<CampaignHistory>(query, pageIndex, pageSize);
            return list;

        }
        public virtual IPagedList<NewsLetterSubscription> UserSubscriptions(Campaign campaign, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            var model = new PagedList<NewsLetterSubscription>();
            if (campaign.UserCreatedDateFrom.HasValue || campaign.UserCreatedDateTo.HasValue ||
                campaign.UserHasShoppingCartCondition != CampaignCondition.All || campaign.UserHasShoppingCartCondition != CampaignCondition.All ||
                campaign.UserLastActivityDateFrom.HasValue || campaign.UserLastActivityDateTo.HasValue ||
                campaign.UserLastPurchaseDateFrom.HasValue || campaign.UserLastPurchaseDateTo.HasValue ||
                campaign.UserTags.Count > 0 || campaign.UserRoles.Count > 0 || campaign.NewsletterCategories.Count > 0 )
            {

                var query = from o in _newsLetterSubscriptionRepository.Table
                            where o.Active && o.UserId!="" && (o.StoreId == campaign.StoreId || String.IsNullOrEmpty(campaign.StoreId))
                            join c in _userRepository.Table on o.UserId equals c.Id into joined
                            from users in joined
                            select new CampaignUserHelp() {
                                UserEmail = users.Email,
                                Email = o.Email,
                                UserId = users.Id,
                                CreatedOnUtc = users.CreatedOnUtc,
                                UserTags = users.UserTags,
                                UserRoles = users.UserRoles,
                                NewsletterCategories = o.Categories,
                                HasShoppingCartItems = users.HasShoppingCartItems,
                                IsHasOrders = users.IsHasOrders,
                                LastActivityDateUtc = users.LastActivityDateUtc,
                                LastPurchaseDateUtc = users.LastPurchaseDateUtc,
                                NewsLetterSubscriptionGuid = o.NewsLetterSubscriptionGuid
                            };

                //create date
                if (campaign.UserCreatedDateFrom.HasValue)
                    query = query.Where(x => x.CreatedOnUtc >= campaign.UserCreatedDateFrom.Value);
                if (campaign.UserCreatedDateTo.HasValue)
                    query = query.Where(x => x.CreatedOnUtc <= campaign.UserCreatedDateTo.Value);

                //last activity
                if (campaign.UserLastActivityDateFrom.HasValue)
                    query = query.Where(x => x.LastActivityDateUtc >= campaign.UserLastActivityDateFrom.Value);
                if (campaign.UserLastActivityDateTo.HasValue)
                    query = query.Where(x => x.LastActivityDateUtc <= campaign.UserLastActivityDateTo.Value);

                //last purchase
                if (campaign.UserLastPurchaseDateFrom.HasValue)
                    query = query.Where(x => x.LastPurchaseDateUtc >= campaign.UserLastPurchaseDateFrom.Value);
                if (campaign.UserLastPurchaseDateTo.HasValue)
                    query = query.Where(x => x.LastPurchaseDateUtc <= campaign.UserLastPurchaseDateTo.Value);

                //user has shopping carts
                if(campaign.UserHasShoppingCartCondition == CampaignCondition.True)
                    query = query.Where(x => x.HasShoppingCartItems);
                if (campaign.UserHasShoppingCartCondition == CampaignCondition.False)
                    query = query.Where(x => !x.HasShoppingCartItems);

                //user has order
                if (campaign.UserHasOrdersCondition == CampaignCondition.True)
                    query = query.Where(x => x.IsHasOrders);
                if (campaign.UserHasOrdersCondition == CampaignCondition.False)
                    query = query.Where(x => !x.IsHasOrders);

                //tags
                if(campaign.UserTags.Count > 0)
                {
                    foreach (var item in campaign.UserTags)
                    {
                        query = query.Where(x => x.UserTags.Contains(item));
                    }
                }
                //roles
                if (campaign.UserRoles.Count > 0)
                {
                    foreach (var item in campaign.UserRoles)
                    {
                        query = query.Where(x => x.UserRoles.Any(z=>z.Id == item));
                    }
                }
                //categories news
                if (campaign.NewsletterCategories.Count > 0)
                {
                    foreach (var item in campaign.NewsletterCategories)
                    {
                        query = query.Where(x => x.NewsletterCategories.Contains(item));
                    }
                }

                model = new PagedList<NewsLetterSubscription>(query.Select(x => new NewsLetterSubscription() { UserId = x.UserId, Email = x.Email, NewsLetterSubscriptionGuid = x.NewsLetterSubscriptionGuid }), pageIndex, pageSize);
            }
            else
            {
                var query = from o in _newsLetterSubscriptionRepository.Table
                            where o.Active && (o.StoreId == campaign.StoreId || String.IsNullOrEmpty(campaign.StoreId))
                            select o;
                model = new PagedList<NewsLetterSubscription>(query, pageIndex, pageSize);
            }

            return model;
        }

        private class CampaignUserHelp
        {
            public CampaignUserHelp()
            {
                //UserTags = new List<string>();
                UserRoles = new List<UserRole>();
            }
            public string UserId { get; set; }
            public string UserEmail { get; set; }
            public string Email { get; set; }
            public DateTime CreatedOnUtc { get; set; }
            public DateTime LastActivityDateUtc { get; set; }
            public DateTime? LastPurchaseDateUtc { get; set; }
            public bool HasShoppingCartItems { get; set; }
            public bool IsHasOrders { get; set; }
            public ICollection<string> UserTags { get; set; }
            public ICollection<string> NewsletterCategories { get; set; }
            public ICollection<UserRole> UserRoles { get; set; }
            public Guid NewsLetterSubscriptionGuid { get; set; }
        }

        /// <summary>
        /// Sends a campaign to specified emails
        /// </summary>
        /// <param name="campaign">Campaign</param>
        /// <param name="emailAccount">Email account</param>
        /// <param name="subscriptions">Subscriptions</param>
        /// <returns>Total emails sent</returns>
        public virtual int SendCampaign(Campaign campaign, EmailAccount emailAccount,
            IEnumerable<NewsLetterSubscription> subscriptions)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            if (emailAccount == null)
                throw new ArgumentNullException("emailAccount");

            int totalEmailsSent = 0;

            foreach (var subscription in subscriptions)
            {
                User user = null;

                if(!String.IsNullOrEmpty(subscription.UserId))
                {
                    user = _userService.GetUserById(subscription.UserId);
                }

                if(user == null)
                {
                    user = _userService.GetUserByEmail(subscription.Email);
                }

                //ignore deleted or inactive users when sending newsletter campaigns
                if (user != null && (!user.Active || user.Deleted))
                    continue;

                var tokens = new List<Token>();
                _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
                _messageTokenProvider.AddNewsLetterSubscriptionTokens(tokens, subscription);
                if (user != null)
                {
                    _messageTokenProvider.AddUserTokens(tokens, user);
                    _messageTokenProvider.AddShoppingCartTokens(tokens, user);
                    _messageTokenProvider.AddRecommendedProductsTokens(tokens, user);
                    _messageTokenProvider.AddRecentlyViewedProductsTokens(tokens, user);
                }

                string subject = _tokenizer.Replace(campaign.Subject, tokens, false);
                string body = _tokenizer.Replace(campaign.Body, tokens, true);

                var email = new QueuedEmail
                {
                    Priority = QueuedEmailPriority.Low,
                    From = emailAccount.Email,
                    FromName = emailAccount.DisplayName,
                    To = subscription.Email,
                    Subject = subject,
                    Body = body,
                    CreatedOnUtc = DateTime.UtcNow,
                    EmailAccountId = emailAccount.Id
                };
                _queuedEmailService.InsertQueuedEmail(email);
                InsertCampaignHistory(new CampaignHistory() { CampaignId = campaign.Id, UserId = subscription.UserId, Email = subscription.Email, CreatedDateUtc = DateTime.UtcNow, StoreId = campaign.StoreId });

                //activity log
                if(user!=null)
                    _userActivityService.InsertActivity("UserReminder.SendCampaign", campaign.Id, _localizationService.GetResource("ActivityLog.SendCampaign"), user, campaign.Name);
                else
                    _userActivityService.InsertActivity("UserReminder.SendCampaign", campaign.Id, _localizationService.GetResource("ActivityLog.SendCampaign"), campaign.Name + " - " + subscription.Email);

                totalEmailsSent++;
            }
            return totalEmailsSent;
        }

        /// <summary>
        /// Sends a campaign to specified email
        /// </summary>
        /// <param name="campaign">Campaign</param>
        /// <param name="emailAccount">Email account</param>
        /// <param name="email">Email</param>
        public virtual void SendCampaign(Campaign campaign, EmailAccount emailAccount, string email)
        {
            if (campaign == null)
                throw new ArgumentNullException("campaign");

            if (emailAccount == null)
                throw new ArgumentNullException("emailAccount");

            var tokens = new List<Token>();
            _messageTokenProvider.AddStoreTokens(tokens, _storeContext.CurrentStore, emailAccount);
            var user = _userService.GetUserByEmail(email);
            if (user != null)
            {
                _messageTokenProvider.AddUserTokens(tokens, user);
                _messageTokenProvider.AddShoppingCartTokens(tokens, user);
                _messageTokenProvider.AddRecommendedProductsTokens(tokens, user);
                _messageTokenProvider.AddRecentlyViewedProductsTokens(tokens, user);
            }

            string subject = _tokenizer.Replace(campaign.Subject, tokens, false);
            string body = _tokenizer.Replace(campaign.Body, tokens, true);

            _emailSender.SendEmail(emailAccount, subject, body, emailAccount.Email, emailAccount.DisplayName, email, null);
        }
    }
}
