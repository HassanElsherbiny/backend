﻿using System.Collections.Generic;
using Zajel.Core.Domain.Blogs;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Messages;
using Zajel.Core.Domain.News;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Shipping;
using Zajel.Core.Domain.Stores;
using Zajel.Core.Domain.Vendors;

namespace Zajel.Services.Messages
{
    public partial interface IMessageTokenProvider
    {
        void AddStoreTokens(IList<Token> tokens, Store store, EmailAccount emailAccount);
        void AddOrderTokens(IList<Token> tokens, Order order, string languageId, string vendorId = "");
        void AddOrderRefundedTokens(IList<Token> tokens, Order order, decimal refundedAmount);
        void AddShipmentTokens(IList<Token> tokens, Shipment shipment, string languageId);
        void AddOrderNoteTokens(IList<Token> tokens, OrderNote orderNote);
        void AddRecurringPaymentTokens(IList<Token> tokens, RecurringPayment recurringPayment);
        void AddReturnRequestTokens(IList<Token> tokens, ReturnRequest returnRequest, OrderItem orderItem);
        void AddGiftCardTokens(IList<Token> tokens, GiftCard giftCard);
        void AddUserTokens(IList<Token> tokens, User user);
        void AddShoppingCartTokens(IList<Token> tokens, User user);
        void AddRecommendedProductsTokens(IList<Token> tokens, User user);
        void AddRecentlyViewedProductsTokens(IList<Token> tokens, User user);
        void AddVendorTokens(IList<Token> tokens, Vendor vendor);
        void AddNewsLetterSubscriptionTokens(IList<Token> tokens, NewsLetterSubscription subscription);
        void AddProductReviewTokens(IList<Token> tokens, ProductReview productReview);
        void AddVendorReviewTokens(IList<Token> tokens, VendorReview VendorReview);
        void AddBlogCommentTokens(IList<Token> tokens, BlogComment blogComment);
        void AddNewsCommentTokens(IList<Token> tokens, NewsComment newsComment);
        void AddProductTokens(IList<Token> tokens, Product product, string languageId);
        void AddAttributeCombinationTokens(IList<Token> tokens, ProductAttributeCombination combination, string languageId);
        void AddForumTokens(IList<Token> tokens, Forum forum);
        void AddForumTopicTokens(IList<Token> tokens, ForumTopic forumTopic,
            int? friendlyForumTopicPageIndex = null, string appendedPostIdentifierAnchor = "");
        void AddForumPostTokens(IList<Token> tokens, ForumPost forumPost);
        void AddPrivateMessageTokens(IList<Token> tokens, PrivateMessage privateMessage);
        void AddBackInStockTokens(IList<Token> tokens, BackInStockSubscription subscription);
        string[] GetListOfCampaignAllowedTokens();
        string[] GetListOfAllowedTokens();
        string[] GetListOfUserReminderAllowedTokens(UserReminderRuleEnum rule);
    }
}
