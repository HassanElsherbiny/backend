using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Zajel.Core;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain.Blogs;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Common;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Shipping;
using Zajel.Services.Common;
using Zajel.Services.Events;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson;

namespace Zajel.Services.Users
{
    /// <summary>
    /// User service
    /// </summary>
    public partial class UserService : IUserService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : show hidden records?
        /// </remarks>
        private const string CUSTOMERROLES_ALL_KEY = "Zajel.userrole.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : system name
        /// </remarks>
        private const string CUSTOMERROLES_BY_SYSTEMNAME_KEY = "Zajel.userrole.systemname-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string CUSTOMERROLES_PATTERN_KEY = "Zajel.userrole.";
        private const string CUSTOMERROLESPRODUCTS_PATTERN_KEY = "Zajel.product.cr";

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : user role Id?
        /// </remarks>
        private const string CUSTOMERROLESPRODUCTS_ROLE_KEY = "Zajel.userroleproducts.role-{0}";

        #endregion

        #region Fields

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<UserRole> _userRoleRepository;
        private readonly IRepository<UserRoleProduct> _userRoleProductRepository;
        private readonly IRepository<UserProductPrice> _userProductPriceRepository;
        private readonly IRepository<UserHistoryPassword> _userHistoryPasswordProductRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<ForumPost> _forumPostRepository;
        private readonly IRepository<ForumTopic> _forumTopicRepository;
        private readonly IRepository<BlogComment> _blogCommentRepository;
        private readonly IRepository<ProductReview> _productReviewRepository;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly IDataProvider _dataProvider;
        private readonly ICacheManager _cacheManager;
        private readonly IEventPublisher _eventPublisher;
        private readonly UserSettings _userSettings;
        private readonly CommonSettings _commonSettings;

        #endregion

        #region Ctor

        public UserService(ICacheManager cacheManager,
            IRepository<User> userRepository,
            IRepository<UserRole> userRoleRepository,
            IRepository<UserProductPrice> userProductPriceRepository,
            IRepository<UserHistoryPassword> userHistoryPasswordProductRepository,
            IRepository<UserRoleProduct> userRoleProductRepository,
            IRepository<Order> orderRepository,
            IRepository<ForumPost> forumPostRepository,
            IRepository<ForumTopic> forumTopicRepository,
            IRepository<BlogComment> blogCommentRepository,
            IRepository<ProductReview> productReviewRepository,
            IGenericAttributeService genericAttributeService,
            IDataProvider dataProvider,
            IEventPublisher eventPublisher, 
            UserSettings userSettings,
            CommonSettings commonSettings)
        {
            this._cacheManager = cacheManager;
            this._userRepository = userRepository;
            this._userRoleRepository = userRoleRepository;
            this._userProductPriceRepository = userProductPriceRepository;
            this._userHistoryPasswordProductRepository = userHistoryPasswordProductRepository;
            this._userRoleProductRepository = userRoleProductRepository;
            this._orderRepository = orderRepository;
            this._forumPostRepository = forumPostRepository;
            this._forumTopicRepository = forumTopicRepository;
            this._blogCommentRepository = blogCommentRepository;
            this._productReviewRepository = productReviewRepository;
            this._genericAttributeService = genericAttributeService;
            this._dataProvider = dataProvider;
            this._eventPublisher = eventPublisher;
            this._userSettings = userSettings;
            this._commonSettings = commonSettings;
        }

        #endregion

        #region Methods

        #region Users
        
        /// <summary>
        /// Gets all users
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="userRoleIds">A list of user role identifiers to filter by (at least one match); pass null or empty list in order to load all users; </param>
        /// <param name="email">Email; null to load all users</param>
        /// <param name="username">Username; null to load all users</param>
        /// <param name="firstName">First name; null to load all users</param>
        /// <param name="lastName">Last name; null to load all users</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all users</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all users</param>
        /// <param name="company">Company; null to load all users</param>
        /// <param name="phone">Phone; null to load all users</param>
        /// <param name="zipPostalCode">Phone; null to load all users</param>
        /// <param name="loadOnlyWithShoppingCart">Value indicating whether to load users only with shopping cart</param>
        /// <param name="sct">Value indicating what shopping cart type to filter; userd when 'loadOnlyWithShoppingCart' param is 'true'</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Users</returns>
        public virtual IPagedList<User> GetAllUsers(DateTime? createdFromUtc = null,
            DateTime? createdToUtc = null, string affiliateId = "", string vendorId = "",
            string[] userRoleIds = null, string[] userTagIds = null, string email = null, string username = null,
            string firstName = null, string lastName = null,
            string company = null, string phone = null, string zipPostalCode = null,
            bool loadOnlyWithShoppingCart = false, ShoppingCartType? sct = null,
            int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = _userRepository.Table;

            if (createdFromUtc.HasValue)
                query = query.Where(c => createdFromUtc.Value <= c.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(c => createdToUtc.Value >= c.CreatedOnUtc);
            if (!String.IsNullOrEmpty(affiliateId))
                query = query.Where(c => affiliateId == c.AffiliateId);
            if (!String.IsNullOrEmpty(vendorId))
                query = query.Where(c => vendorId == c.VendorId);
            query = query.Where(c => !c.Deleted);
            if (userRoleIds != null && userRoleIds.Length > 0)
                query = query.Where(c => c.UserRoles.Any(x => userRoleIds.Contains(x.Id)));
            if (userTagIds != null && userTagIds.Length > 0)
            {
                foreach (var item in userTagIds)
                {
                    query = query.Where(c => c.UserTags.Contains(item));
                }
            }
            if (!String.IsNullOrWhiteSpace(email))
                query = query.Where(c => c.Email!=null && c.Email.ToLower().Contains(email.ToLower()));
            if (!String.IsNullOrWhiteSpace(username))
                query = query.Where(c => c.Username!=null && c.Username.ToLower().Contains(username.ToLower()));
            
            if (!String.IsNullOrWhiteSpace(firstName))
            {
                query = query.Where(x => x.GenericAttributes.Any(y => y.Key == SystemUserAttributeNames.FirstName && y.Value!=null && y.Value.ToLower().Contains(firstName.ToLower())));
            }
            
            if (!String.IsNullOrWhiteSpace(lastName))
            {
                query = query.Where(x => x.GenericAttributes.Any(y => y.Key == SystemUserAttributeNames.LastName && y.Value != null && y.Value.ToLower().Contains(lastName.ToLower())));
            }

            //search by company
            if (!String.IsNullOrWhiteSpace(company))
            {
                query = query.Where(x => x.GenericAttributes.Any(y => y.Key == SystemUserAttributeNames.Company && y.Value != null && y.Value.ToLower().Contains(company.ToLower())));
            }
            //search by phone
            if (!String.IsNullOrWhiteSpace(phone))
            {
                query = query.Where(x => x.GenericAttributes.Any(y => y.Key == SystemUserAttributeNames.Phone && y.Value != null && y.Value.ToLower().Contains(phone.ToLower())));
            }
            //search by zip
            if (!String.IsNullOrWhiteSpace(zipPostalCode))
            {
                query = query.Where(x => x.GenericAttributes.Any(y => y.Key == SystemUserAttributeNames.ZipPostalCode && y.Value != null && y.Value.ToLower().Contains(zipPostalCode.ToLower())));
            }

            if (loadOnlyWithShoppingCart)
            {
                int? sctId = null;
                if (sct.HasValue)
                    sctId = (int)sct.Value;

                query = sct.HasValue ?
                    query.Where(c => c.ShoppingCartItems.Any(x => x.ShoppingCartTypeId == sctId.Value)) :
                    query.Where(c => c.ShoppingCartItems.Count() > 0);
            }
            
            query = query.OrderByDescending(c => c.CreatedOnUtc);

            var users = new PagedList<User>(query, pageIndex, pageSize);
            return users;
        }

        /// <summary>
        /// Gets all users by user format (including deleted ones)
        /// </summary>
        /// <param name="passwordFormat">Password format</param>
        /// <returns>Users</returns>
        public virtual IList<User> GetAllUsersByPasswordFormat(PasswordFormat passwordFormat)
        {
            var passwordFormatId = (int)passwordFormat;

            var query = _userRepository.Table;
            query = query.Where(c => c.PasswordFormatId == passwordFormatId);
            query = query.OrderByDescending(c => c.CreatedOnUtc);
            var users = query.ToList();
            return users;
        }

        /// <summary>
        /// Gets online users
        /// </summary>
        /// <param name="lastActivityFromUtc">User last activity date (from)</param>
        /// <param name="userRoleIds">A list of user role identifiers to filter by (at least one match); pass null or empty list in order to load all users; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Users</returns>
        public virtual IPagedList<User> GetOnlineUsers(DateTime lastActivityFromUtc,
            string[] userRoleIds, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _userRepository.Table;
            query = query.Where(c => lastActivityFromUtc <= c.LastActivityDateUtc);
            query = query.Where(c => !c.Deleted);
            if (userRoleIds != null && userRoleIds.Length > 0)
                query = query.Where(c => c.UserRoles.Select(cr => cr.Id).Intersect(userRoleIds).Any());
            
            query = query.OrderByDescending(c => c.LastActivityDateUtc);
            var users = new PagedList<User>(query, pageIndex, pageSize);
            return users;
        }


        public virtual int GetCountOnlineShoppingCart(DateTime lastActivityFromUtc)
        {
            var query = _userRepository.Table;
            query = query.Where(c => lastActivityFromUtc <= c.LastUpdateCartDateUtc);
            return query.Count();
        }


        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="user">User</param>
        public virtual void DeleteUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (user.IsSystemAccount)
                throw new ZajelException(string.Format("System user account ({0}) could not be deleted", user.SystemName));

            user.Deleted = true;

            if (_userSettings.SuffixDeletedUsers)
            {
                if (!String.IsNullOrEmpty(user.Email))
                    user.Email += "-DELETED";
                if (!String.IsNullOrEmpty(user.Username))
                    user.Username += "-DELETED";
            }

            UpdateUser(user);
        }

        /// <summary>
        /// Gets a user
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <returns>A user</returns>
        public virtual User GetUserById(string userId)
        {            
            return _userRepository.GetById(userId);
        }

        /// <summary>
        /// Get users by identifiers
        /// </summary>
        /// <param name="userIds">User identifiers</param>
        /// <returns>Users</returns>
        public virtual IList<User> GetUsersByIds(string[] userIds)
        {
            if (userIds == null || userIds.Length == 0)
                return new List<User>();

            var query = from c in _userRepository.Table
                        where userIds.Contains(c.Id)
                        select c;
            var users = query.ToList();
            //sort by passed identifiers
            var sortedUsers = new List<User>();
            foreach (string id in userIds)
            {
                var user = users.Find(x => x.Id == id);
                if (user != null)
                    sortedUsers.Add(user);
            }
            return sortedUsers;
        }

        /// <summary>
        /// Gets a user by GUID
        /// </summary>
        /// <param name="userGuid">User GUID</param>
        /// <returns>A user</returns>
        public virtual User GetUserByGuid(Guid userGuid)
        {
            if (userGuid == Guid.Empty)
                return null;

            var query = from c in _userRepository.Table
                        where c.UserGuid == userGuid
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>User</returns>
        public virtual User GetUserByEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return null;
            var query = from c in _userRepository.Table
                        where c.Email == email.ToLower()
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        /// <summary>
        /// Get user by system name
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>User</returns>
        public virtual User GetUserBySystemName(string systemName)
        {
            if (string.IsNullOrWhiteSpace(systemName))
                return null;

            var query = from c in _userRepository.Table
                        where c.SystemName == systemName
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        public virtual User GetUserByUsername(string username)
        {
            if (string.IsNullOrWhiteSpace(username))
                return null;

            var query = from c in _userRepository.Table
                        where c.Username == username.ToLower()
                        select c;
            var user = query.FirstOrDefault();
            return user;
        }
        
        /// <summary>
        /// Insert a guest user
        /// </summary>
        /// <returns>User</returns>
        public virtual User InsertGuestUser()
        {
            var user = new User
            {
                UserGuid = Guid.NewGuid(),
                Active = true,
                CreatedOnUtc = DateTime.UtcNow,
                LastActivityDateUtc = DateTime.UtcNow,
            };

            //add to 'Guests' role
            var guestRole = GetUserRoleBySystemName(SystemUserRoleNames.Guests);
            if (guestRole == null)
                throw new ZajelException("'Guests' role could not be loaded");
            user.UserRoles.Add(guestRole);

            _userRepository.Insert(user);

            return user;
        }
        
        /// <summary>
        /// Insert a user
        /// </summary>
        /// <param name="user">User</param>
        public virtual void InsertUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            if (!string.IsNullOrEmpty(user.Email))
                user.Email = user.Email.ToLower();

            if (!string.IsNullOrEmpty(user.Username))
                user.Username = user.Username.ToLower();

            _userRepository.Insert(user);

            //event notification
            _eventPublisher.EntityInserted(user);
        }

        /// <summary>
        /// Insert a user history password
        /// </summary>
        /// <param name="user">User</param>
        public virtual void InsertUserPassword(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var chp = new UserHistoryPassword();
            chp.Password = user.Password;
            chp.PasswordFormatId = user.PasswordFormatId;
            chp.PasswordSalt = user.PasswordSalt;
            chp.UserId = user.Id;
            chp.CreatedOnUtc = DateTime.UtcNow;

            _userHistoryPasswordProductRepository.Insert(chp);

            //event notification
            _eventPublisher.EntityInserted(chp);
        }

        /// <summary>
        /// Gets user passwords
        /// </summary>
        /// <param name="userId">User identifier; pass null to load all records</param>
        /// <param name="passwordsToReturn">Number of returning passwords; pass null to load all records</param>
        /// <returns>List of user passwords</returns>
        public virtual IList<UserHistoryPassword> GetPasswords(string userId, int passwordsToReturn)
        {
            var query = from c in _userHistoryPasswordProductRepository.Table
                        where c.UserId == userId
                        select c;
            query = query.OrderByDescending(password => password.CreatedOnUtc).Take(passwordsToReturn);
            return query.ToList();
        }


        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.Email, string.IsNullOrEmpty(user.Email) ? "" : user.Email.ToLower())
                .Set(x => x.PasswordFormatId, user.PasswordFormatId)
                .Set(x => x.PasswordSalt, user.PasswordSalt)
                .Set(x => x.Active, user.Active)
                .Set(x => x.Password, user.Password)
                .Set(x => x.PasswordChangeDateUtc, user.PasswordChangeDateUtc)
                .Set(x => x.Username, string.IsNullOrEmpty(user.Username) ? "" : user.Username.ToLower())
                .Set(x => x.Deleted, user.Deleted);

            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

            //event notification
            _eventPublisher.EntityUpdated(user);
        }
        /// <summary>
        /// Updates the user - last activity date
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUserLastActivityDate(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.LastActivityDateUtc, user.LastActivityDateUtc);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        /// <summary>
        /// Updates the user - last activity date
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUserLastLoginDate(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.LastLoginDateUtc, user.LastLoginDateUtc)
                .Set(x => x.FailedLoginAttempts, user.FailedLoginAttempts)
                .Set(x => x.CannotLoginUntilDateUtc, user.CannotLoginUntilDateUtc);

            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        /// Updates the user - last activity date
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUserVendor(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.VendorId, user.VendorId);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

            //event notification
            _eventPublisher.EntityUpdated(user);
        }

        /// <summary>
        /// Updates the user - last activity date
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUserLastIpAddress(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.LastIpAddress, user.LastIpAddress);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        /// <summary>
        /// Updates the user - password
        /// </summary>
        /// <param name="user">User</param>
        public virtual void UpdateUserPassword(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.Password, user.Password);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }

        public virtual void UpdateUserinAdminPanel(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.Active, user.Active)
                .Set(x => x.AdminComment, user.AdminComment)
                .Set(x => x.AffiliateId, user.AffiliateId)
                .Set(x => x.IsSystemAccount, user.IsSystemAccount)
                .Set(x => x.Active, user.Active)
                .Set(x => x.Email, string.IsNullOrEmpty(user.Email) ? "" : user.Email.ToLower())
                .Set(x => x.IsTaxExempt, user.IsTaxExempt)
                .Set(x => x.Password, user.Password)
                .Set(x => x.SystemName, user.SystemName)
                .Set(x => x.Username, string.IsNullOrEmpty(user.Username) ? "" : user.Username.ToLower())
                .Set(x => x.UserRoles, user.UserRoles)
                .Set(x => x.Addresses, user.Addresses)
                .Set(x => x.FreeShipping, user.FreeShipping)
                .Set(x => x.VendorId, user.VendorId);

            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
            //event notification
            _eventPublisher.EntityUpdated(user);

        }

        public virtual void UpdateFreeShipping(string userId, bool freeShipping)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.FreeShipping, freeShipping);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }

        public virtual void UpdateAffiliate(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.AffiliateId, user.AffiliateId);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }
        public virtual void UpdateActive(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.Active, user.Active);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateNewsItem(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.IsNewsItem, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        
        public virtual void UpdateHasForumTopic(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasForumTopic, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasForumPost(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasForumPost, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasOrders(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasOrders, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasBlogComments(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasBlogComments, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasProductReview(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasProductReview, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasProductReviewH(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasProductReviewH, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasPoolVoting(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasPoolVoting, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }

        public virtual void UpdateUserLastPurchaseDate(string userId, DateTime date)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.LastPurchaseDateUtc, date);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateUserLastUpdateCartDate(string userId, DateTime? date)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.LastUpdateCartDateUtc, date);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }
        public virtual void UpdateUserLastUpdateWishList(string userId, DateTime date)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.LastUpdateWishListDateUtc, date);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        public virtual void UpdateHasVendorReview(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasVendorReview, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        public virtual void UpdateHasVendorReviewH(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.IsHasVendorReviewH, true);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        public virtual void ResetCheckoutData(User user, string storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true)
        {
            if (user == null)
                throw new ArgumentNullException();
            
            //clear entered coupon codes
            if (clearCouponCodes)
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(user, SystemUserAttributeNames.DiscountCouponCode, null);
                _genericAttributeService.SaveAttribute<ShippingOption>(user, SystemUserAttributeNames.GiftCardCouponCodes, null);
            }

            //clear checkout attributes
            if (clearCheckoutAttributes)
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(user, SystemUserAttributeNames.CheckoutAttributes, null, storeId);
            }

            //clear reward points flag
            if (clearRewardPoints)
            {
                _genericAttributeService.SaveAttribute(user, SystemUserAttributeNames.UseRewardPointsDuringCheckout, false, storeId);
            }

            //clear selected shipping method
            if (clearShippingMethod)
            {
                _genericAttributeService.SaveAttribute<ShippingOption>(user, SystemUserAttributeNames.SelectedShippingOption, null, storeId);
                _genericAttributeService.SaveAttribute<ShippingOption>(user, SystemUserAttributeNames.OfferedShippingOptions, null, storeId);
                _genericAttributeService.SaveAttribute(user, SystemUserAttributeNames.SelectedPickupPoint, "", storeId);
                _genericAttributeService.SaveAttribute(user, SystemUserAttributeNames.ShippingOptionAttributeDescription, "", storeId);
                _genericAttributeService.SaveAttribute(user, SystemUserAttributeNames.ShippingOptionAttributeXml, "", storeId);
            }

            //clear selected payment method
            if (clearPaymentMethod)
            {
                _genericAttributeService.SaveAttribute<string>(user, SystemUserAttributeNames.SelectedPaymentMethod, null, storeId);
            }
            
        }

        public virtual void UpdateUserReminderHistory(string userId, string orderId)
        {
            var builder = Builders<UserReminderHistory>.Filter;
            var filter = builder.Eq(x => x.UserId, userId);
            filter = filter & builder.Eq(x => x.Status, (int)UserReminderHistoryStatusEnum.Started);

            var update = Builders<UserReminderHistory>.Update
                .Set(x => x.EndDate, DateTime.UtcNow)
                .Set(x => x.Status, (int)UserReminderHistoryStatusEnum.CompletedOrdered)
                .Set(x => x.OrderId, orderId);

            var userReminderRepository = Zajel.Core.Infrastructure.EngineContext.Current.Resolve<IRepository<UserReminderHistory>>();
            userReminderRepository.Collection.UpdateManyAsync(filter, update);

        }


        /// <summary>
        /// Delete guest user records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete users only without shopping cart</param>
        /// <returns>Number of deleted users</returns>
        public virtual int DeleteGuestUsers(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart)
        {

            var guestRole = GetUserRoleBySystemName(SystemUserRoleNames.Guests);
            if (guestRole == null)
                throw new ZajelException("'Guests' role could not be loaded");

            var query = _userRepository.Table;

            if (createdFromUtc.HasValue)
                query = query.Where(c => createdFromUtc.Value <= c.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(c => createdToUtc.Value >= c.CreatedOnUtc);
            query = query.Where(c => c.UserRoles.Any(cr => cr.Id == guestRole.Id));
            if (onlyWithoutShoppingCart)
                query = query.Where(c => !c.ShoppingCartItems.Any());

            //no orders     
            query = query.Where(c => !c.IsHasOrders);
            //no blog comments
            query = query.Where(c => !c.IsHasBlogComments);

            //no news comments
            query = query.Where(c => !c.IsNewsItem);

            //no product reviews
            query = query.Where(c => !c.IsHasProductReview);

            //no product reviews helpfulness
            query = query.Where(c => !c.IsHasProductReviewH);

            //no poll voting
            query = query.Where(c => !c.IsHasPoolVoting);

            //no forum posts 
            query = query.Where(c => !c.IsHasForumPost);

            //no forum topics
            query = query.Where(c => !c.IsHasForumTopic);

            //don't delete system accounts
            query = query.Where(c => !c.IsSystemAccount);

            var users = query.ToList();

            int totalRecordsDeleted = 0;
            foreach (var c in users)
            {
                try
                {
                    //delete from database
                    _userRepository.Delete(c);
                    totalRecordsDeleted++;
                }
                catch (Exception exc)
                {
                    Debug.WriteLine(exc);
                }
            }
            return totalRecordsDeleted;

        }

        #endregion
        
        #region User roles

        /// <summary>
        /// Delete a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        public virtual void DeleteUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            if (userRole.IsSystemRole)
                throw new ZajelException("System role could not be deleted");

            _userRoleRepository.Delete(userRole);

            var builder = Builders<User>.Update;
            var updatefilter = builder.PullFilter(x => x.UserRoles, y => y.Id == userRole.Id);
            var result = _userRepository.Collection.UpdateManyAsync(new BsonDocument(), updatefilter).Result;

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(userRole);
        }

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="userRoleId">User role identifier</param>
        /// <returns>User role</returns>
        public virtual UserRole GetUserRoleById(string userRoleId)
        {
            return _userRoleRepository.GetById(userRoleId);
        }

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="systemName">User role system name</param>
        /// <returns>User role</returns>
        public virtual UserRole GetUserRoleBySystemName(string systemName)
        {
            if (String.IsNullOrWhiteSpace(systemName))
                return null;

            string key = string.Format(CUSTOMERROLES_BY_SYSTEMNAME_KEY, systemName);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userRoleRepository.Table
                            where cr.SystemName == systemName
                            select cr;
                var userRole = query.FirstOrDefault();
                return userRole;
            });
        }

        /// <summary>
        /// Gets all user roles
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>User roles</returns>
        public virtual IList<UserRole> GetAllUserRoles(bool showHidden = false)
        {
            string key = string.Format(CUSTOMERROLES_ALL_KEY, showHidden);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userRoleRepository.Table
                            where (showHidden || cr.Active)
                            orderby cr.Name
                            select cr;
                var userRoles = query.ToList();
                return userRoles;
            });
        }
        
        /// <summary>
        /// Inserts a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        public virtual void InsertUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _userRoleRepository.Insert(userRole);

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(userRole);
        }

        /// <summary>
        /// Updates the user role
        /// </summary>
        /// <param name="userRole">User role</param>
        public virtual void UpdateUserRole(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("userRole");

            _userRoleRepository.Update(userRole);

            var builder = Builders<User>.Filter;
            var filter = builder.ElemMatch(x => x.UserRoles, y => y.Id == userRole.Id);
            var update = Builders<User>.Update
                .Set(x => x.UserRoles.ElementAt(-1), userRole);

            var result = _userRepository.Collection.UpdateManyAsync(filter, update).Result;

            _cacheManager.RemoveByPattern(CUSTOMERROLES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(userRole);
        }

        #endregion

        #region User role in user

        public virtual void DeleteUserRoleInUser(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("pwi");

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.Pull(p => p.UserRoles, userRole);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userRole.UserId), update);

        }

        public virtual void InsertUserRoleInUser(UserRole userRole)
        {
            if (userRole == null)
                throw new ArgumentNullException("productWarehouse");

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.AddToSet(p => p.UserRoles, userRole);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userRole.UserId), update);

        }

        #endregion

        #region User Role Products

        /// <summary>
        /// Delete a user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        public virtual void DeleteUserRoleProduct(UserRoleProduct userRoleProduct)
        {
            if (userRoleProduct == null)
                throw new ArgumentNullException("userRole");

            _userRoleProductRepository.Delete(userRoleProduct);
            
            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERROLESPRODUCTS_ROLE_KEY, userRoleProduct.UserRoleId));
            _cacheManager.RemoveByPattern(CUSTOMERROLESPRODUCTS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(userRoleProduct);
        }


        /// <summary>
        /// Inserts a user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        public virtual void InsertUserRoleProduct(UserRoleProduct userRoleProduct)
        {
            if (userRoleProduct == null)
                throw new ArgumentNullException("userRoleProduct");

            _userRoleProductRepository.Insert(userRoleProduct);

            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERROLESPRODUCTS_ROLE_KEY, userRoleProduct.UserRoleId));
            _cacheManager.RemoveByPattern(CUSTOMERROLESPRODUCTS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(userRoleProduct);
        }

        /// <summary>
        /// Updates the user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        public virtual void UpdateUserRoleProduct(UserRoleProduct userRoleProduct)
        {
            if (userRoleProduct == null)
                throw new ArgumentNullException("userRoleProduct");

            var builder = Builders<UserRoleProduct>.Filter;
            var filter = builder.Eq(x => x.Id, userRoleProduct.Id);
            var update = Builders<UserRoleProduct>.Update
                .Set(x => x.DisplayOrder, userRoleProduct.DisplayOrder);
            var result = _userRoleProductRepository.Collection.UpdateOneAsync(filter, update).Result;

            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERROLESPRODUCTS_ROLE_KEY, userRoleProduct.UserRoleId));
            _cacheManager.RemoveByPattern(CUSTOMERROLESPRODUCTS_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(userRoleProduct);
        }


        /// <summary>
        /// Gets user roles products for user role
        /// </summary>
        /// <param name="userRoleId">User role id</param>
        /// <returns>User role products</returns>
        public virtual IList<UserRoleProduct> GetUserRoleProducts(string userRoleId)
        {
            string key = string.Format(CUSTOMERROLESPRODUCTS_ROLE_KEY, userRoleId);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userRoleProductRepository.Table
                            where (cr.UserRoleId == userRoleId)
                            orderby cr.DisplayOrder
                            select cr;
                var userRoles = query.ToList();
                return userRoles;
            });
        }

        /// <summary>
        /// Gets user roles products for user role
        /// </summary>
        /// <param name="userRoleId">User role id</param>
        /// <param name="productId">Product id</param>
        /// <returns>User role product</returns>
        public virtual UserRoleProduct GetUserRoleProduct(string userRoleId, string productId)
        {
            var query = from cr in _userRoleProductRepository.Table
                        where cr.UserRoleId == userRoleId && cr.ProductId == productId
                        orderby cr.DisplayOrder
                        select cr;
            var userRoles = query.ToList();
            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets user roles product
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns>User role product</returns>
        public virtual UserRoleProduct GetUserRoleProductById(string id)
        {
            var query = from cr in _userRoleProductRepository.Table
                        where cr.Id == id
                        orderby cr.DisplayOrder
                        select cr;
            var userRoles = query.ToList();
            return query.FirstOrDefault();
        }


        #endregion

        #region User Address

        public virtual void DeleteAddress(Address address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.Pull(p => p.Addresses, address);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", address.UserId), update);

        }

        public virtual void InsertAddress(Address address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            if (address.StateProvinceId == "0")
                address.StateProvinceId = "";

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.AddToSet(p => p.Addresses, address);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", address.UserId), update);

            //event notification
            _eventPublisher.EntityInserted(address);
        }

        public virtual void UpdateAddress(Address address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, address.UserId);
            filter = filter & builder.ElemMatch(x => x.Addresses, y => y.Id == address.Id);
            var update = Builders<User>.Update
                .Set(x => x.Addresses.ElementAt(-1).Address1, address.Address1)
                .Set(x => x.Addresses.ElementAt(-1).Address2, address.Address2)
                .Set(x => x.Addresses.ElementAt(-1).City, address.City)
                .Set(x => x.Addresses.ElementAt(-1).Company, address.Company)
                .Set(x => x.Addresses.ElementAt(-1).CountryId, address.CountryId)
                .Set(x => x.Addresses.ElementAt(-1).CustomAttributes, address.CustomAttributes)
                .Set(x => x.Addresses.ElementAt(-1).Email, address.Email)
                .Set(x => x.Addresses.ElementAt(-1).FaxNumber, address.FaxNumber)
                .Set(x => x.Addresses.ElementAt(-1).FirstName, address.FirstName)
                .Set(x => x.Addresses.ElementAt(-1).LastName, address.LastName)
                .Set(x => x.Addresses.ElementAt(-1).PhoneNumber, address.PhoneNumber)
                .Set(x => x.Addresses.ElementAt(-1).StateProvinceId, address.StateProvinceId)
                .Set(x => x.Addresses.ElementAt(-1).ZipPostalCode, address.ZipPostalCode);

            var result = _userRepository.Collection.UpdateManyAsync(filter, update).Result;
            //event notification
            _eventPublisher.EntityUpdated(address);
        }


        public virtual void UpdateBillingAddress(Address address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, address.UserId);
            var update = Builders<User>.Update
                .Set(x => x.BillingAddress, address);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }
        public virtual void UpdateShippingAddress(Address address)
        {
            if (address == null)
                throw new ArgumentNullException("address");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, address.UserId);
            var update = Builders<User>.Update
                .Set(x => x.ShippingAddress, address);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        public virtual void RemoveShippingAddress(string userId)
        {
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, userId);
            var update = Builders<User>.Update
                .Set(x => x.ShippingAddress, null);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;

        }

        #endregion

        #region User Shopping Cart Item

        public virtual void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem)
        {
            if (shoppingCartItem == null)
                throw new ArgumentNullException("shoppingCartItem");

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.Pull(p => p.ShoppingCartItems, shoppingCartItem);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", shoppingCartItem.UserId), update);

            //event notification
            _eventPublisher.EntityDeleted(shoppingCartItem);

            if (shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                UpdateUserLastUpdateCartDate(shoppingCartItem.UserId, DateTime.UtcNow);
            else
                UpdateUserLastUpdateWishList(shoppingCartItem.UserId, DateTime.UtcNow);

        }

        public virtual void ClearShoppingCartItem(string userId, string storeId, ShoppingCartType shoppingCartType)
        {

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.PullFilter(p => p.ShoppingCartItems, p=>p.StoreId == storeId && p.ShoppingCartTypeId == (int)shoppingCartType);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userId), update);

            if (shoppingCartType == ShoppingCartType.ShoppingCart)
                UpdateUserLastUpdateCartDate(userId, DateTime.UtcNow);
            else
                UpdateUserLastUpdateWishList(userId, DateTime.UtcNow);

        }


        public virtual void InsertShoppingCartItem(ShoppingCartItem shoppingCartItem)
        {
            if (shoppingCartItem == null)
                throw new ArgumentNullException("shoppingCartItem");

            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.AddToSet(p => p.ShoppingCartItems, shoppingCartItem);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", shoppingCartItem.UserId), update);

            //event notification
            _eventPublisher.EntityInserted(shoppingCartItem);

            if (shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                UpdateUserLastUpdateCartDate(shoppingCartItem.UserId, DateTime.UtcNow);
            else
                UpdateUserLastUpdateWishList(shoppingCartItem.UserId, DateTime.UtcNow);
        }

        public virtual void UpdateShoppingCartItem(ShoppingCartItem shoppingCartItem)
        {
            if (shoppingCartItem == null)
                throw new ArgumentNullException("shoppingCartItem");

            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, shoppingCartItem.UserId);
            filter = filter & builder.ElemMatch(x => x.ShoppingCartItems, y => y.Id == shoppingCartItem.Id);
            var update = Builders<User>.Update
                .Set(x => x.ShoppingCartItems.ElementAt(-1).Quantity, shoppingCartItem.Quantity)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).AdditionalShippingChargeProduct, shoppingCartItem.AdditionalShippingChargeProduct)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).IsFreeShipping, shoppingCartItem.IsFreeShipping)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).IsGiftCard, shoppingCartItem.IsGiftCard)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).IsRecurring, shoppingCartItem.IsRecurring)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).IsShipEnabled, shoppingCartItem.IsShipEnabled)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).IsTaxExempt, shoppingCartItem.IsTaxExempt)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).RentalStartDateUtc, shoppingCartItem.RentalStartDateUtc)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).RentalEndDateUtc, shoppingCartItem.RentalEndDateUtc)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).AttributesXml, shoppingCartItem.AttributesXml)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).UserEnteredPrice, shoppingCartItem.UserEnteredPrice)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).UpdatedOnUtc, shoppingCartItem.UpdatedOnUtc)
                .Set(x => x.ShoppingCartItems.ElementAt(-1).ShoppingCartTypeId, shoppingCartItem.ShoppingCartTypeId);

            var result = _userRepository.Collection.UpdateManyAsync(filter, update).Result;
            //event notification
            _eventPublisher.EntityUpdated(shoppingCartItem);

            if (shoppingCartItem.ShoppingCartType == ShoppingCartType.ShoppingCart)
                UpdateUserLastUpdateCartDate(shoppingCartItem.UserId, DateTime.UtcNow);
            else
                UpdateUserLastUpdateWishList(shoppingCartItem.UserId, DateTime.UtcNow);

        }

        public virtual void UpdateHasShoppingCartItems(User user)
        {
            if (user == null)
                throw new ArgumentNullException("user");
            var builder = Builders<User>.Filter;
            var filter = builder.Eq(x => x.Id, user.Id);
            var update = Builders<User>.Update
                .Set(x => x.HasShoppingCartItems, user.HasShoppingCartItems);
            var result = _userRepository.Collection.UpdateOneAsync(filter, update).Result;
        }
        #endregion

        #region User Product Price

        /// <summary>
        /// Gets a user product price
        /// </summary>
        /// <param name="Id">Identifier</param>
        /// <returns>User product price</returns>
        public virtual UserProductPrice GetUserProductPriceById(string id)
        {
            if (id == null)
                throw new ArgumentNullException("Id");

            return _userProductPriceRepository.GetById(id);
        }

        /// <summary>
        /// Gets a price
        /// </summary>
        /// <param name="userId">User Identifier</param>
        /// <param name="productId">Product Identifier</param>
        /// <returns>User product price</returns>
        public virtual decimal? GetPriceByUserProduct(string userId, string productId)
        {
            var builder = Builders<UserProductPrice>.Filter;
            var filter = builder.Eq(x => x.UserId, userId);
            filter = filter & builder.Eq(x => x.ProductId, productId);
            var productprice = _userProductPriceRepository.Collection.Find(filter).FirstOrDefault();
            if (productprice == null)
                return null;
            else
                return productprice.Price;
        }

        /// <summary>
        /// Inserts a user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        public virtual void InsertUserProductPrice(UserProductPrice userProductPrice)
        {
            if (userProductPrice == null)
                throw new ArgumentNullException("userProductPrice");

            _userProductPriceRepository.Insert(userProductPrice);

            //event notification
            _eventPublisher.EntityInserted(userProductPrice);
        }

        /// <summary>
        /// Updates the user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        public virtual void UpdateUserProductPrice(UserProductPrice userProductPrice)
        {
            if (userProductPrice == null)
                throw new ArgumentNullException("userProductPrice");

            _userProductPriceRepository.Update(userProductPrice);

            //event notification
            _eventPublisher.EntityUpdated(userProductPrice);
        }

        /// <summary>
        /// Delete a user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        public virtual void DeleteUserProductPrice(UserProductPrice userProductPrice)
        {
            if (userProductPrice == null)
                throw new ArgumentNullException("userProductPrice");

            _userProductPriceRepository.Delete(userProductPrice);

            //event notification
            _eventPublisher.EntityDeleted(userProductPrice);
        }

        public virtual IPagedList<UserProductPrice> GetProductsByUser(string userId, int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = from pp in _userProductPriceRepository.Table
                        where pp.UserId == userId
                        select pp;
            return new PagedList<UserProductPrice>(query, pageIndex, pageSize);
        }

        #endregion


        #endregion
    }
}