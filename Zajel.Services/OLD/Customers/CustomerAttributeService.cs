using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Core.Caching;
using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Services.Events;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Zajel.Services.Users
{
    /// <summary>
    /// User attribute service
    /// </summary>
    public partial class UserAttributeService : IUserAttributeService
    {
        #region Constants

        /// <summary>
        /// Key for caching
        /// </summary>
        private const string CUSTOMERATTRIBUTES_ALL_KEY = "Zajel.userattribute.all";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : user attribute ID
        /// </remarks>
        private const string CUSTOMERATTRIBUTES_BY_ID_KEY = "Zajel.userattribute.id-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : user attribute ID
        /// </remarks>
        private const string CUSTOMERATTRIBUTEVALUES_ALL_KEY = "Zajel.userattributevalue.all-{0}";
        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : user attribute value ID
        /// </remarks>
        private const string CUSTOMERATTRIBUTEVALUES_BY_ID_KEY = "Zajel.userattributevalue.id-{0}";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string CUSTOMERATTRIBUTES_PATTERN_KEY = "Zajel.userattribute.";
        /// <summary>
        /// Key pattern to clear cache
        /// </summary>
        private const string CUSTOMERATTRIBUTEVALUES_PATTERN_KEY = "Zajel.userattributevalue.";
        #endregion
        
        #region Fields

        private readonly IRepository<UserAttribute> _userAttributeRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;
        
        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="cacheManager">Cache manager</param>
        /// <param name="userAttributeRepository">User attribute repository</param>
        /// <param name="userAttributeValueRepository">User attribute value repository</param>
        /// <param name="eventPublisher">Event published</param>
        public UserAttributeService(ICacheManager cacheManager,
            IRepository<UserAttribute> userAttributeRepository,            
            IEventPublisher eventPublisher)
        {
            this._cacheManager = cacheManager;
            this._userAttributeRepository = userAttributeRepository;
            this._eventPublisher = eventPublisher;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Deletes a user attribute
        /// </summary>
        /// <param name="userAttribute">User attribute</param>
        public virtual void DeleteUserAttribute(UserAttribute userAttribute)
        {
            if (userAttribute == null)
                throw new ArgumentNullException("userAttribute");

            _userAttributeRepository.Delete(userAttribute);

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(userAttribute);
        }

        /// <summary>
        /// Gets all user attributes
        /// </summary>
        /// <returns>User attributes</returns>
        public virtual IList<UserAttribute> GetAllUserAttributes()
        {
            string key = CUSTOMERATTRIBUTES_ALL_KEY;
            return _cacheManager.Get(key, () =>
            {
                var query = from ca in _userAttributeRepository.Table
                            orderby ca.DisplayOrder
                            select ca;
                return query.ToList();
            });
        }

        /// <summary>
        /// Gets a user attribute 
        /// </summary>
        /// <param name="userAttributeId">User attribute identifier</param>
        /// <returns>User attribute</returns>
        public virtual UserAttribute GetUserAttributeById(string userAttributeId)
        {
            string key = string.Format(CUSTOMERATTRIBUTES_BY_ID_KEY, userAttributeId);
            return _cacheManager.Get(key, () => _userAttributeRepository.GetById(userAttributeId));
        }

        /// <summary>
        /// Inserts a user attribute
        /// </summary>
        /// <param name="userAttribute">User attribute</param>
        public virtual void InsertUserAttribute(UserAttribute userAttribute)
        {
            if (userAttribute == null)
                throw new ArgumentNullException("userAttribute");

            _userAttributeRepository.Insert(userAttribute);

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(userAttribute);
        }

        /// <summary>
        /// Updates the user attribute
        /// </summary>
        /// <param name="userAttribute">User attribute</param>
        public virtual void UpdateUserAttribute(UserAttribute userAttribute)
        {
            if (userAttribute == null)
                throw new ArgumentNullException("userAttribute");

            _userAttributeRepository.Update(userAttribute);

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(userAttribute);
        }

        /// <summary>
        /// Deletes a user attribute value
        /// </summary>
        /// <param name="userAttributeValue">User attribute value</param>
        public virtual void DeleteUserAttributeValue(UserAttributeValue userAttributeValue)
        {
            if (userAttributeValue == null)
                throw new ArgumentNullException("userAttributeValue");

            var updatebuilder = Builders<UserAttribute>.Update;
            var update = updatebuilder.Pull(p => p.UserAttributeValues, userAttributeValue);
            _userAttributeRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userAttributeValue.UserAttributeId), update);

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityDeleted(userAttributeValue);
        }

        /// <summary>
        /// Inserts a user attribute value
        /// </summary>
        /// <param name="userAttributeValue">User attribute value</param>
        public virtual void InsertUserAttributeValue(UserAttributeValue userAttributeValue)
        {
            if (userAttributeValue == null)
                throw new ArgumentNullException("userAttributeValue");

            var updatebuilder = Builders<UserAttribute>.Update;
            var update = updatebuilder.AddToSet(p => p.UserAttributeValues, userAttributeValue);
            _userAttributeRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userAttributeValue.UserAttributeId), update);

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityInserted(userAttributeValue);
        }

        /// <summary>
        /// Updates the user attribute value
        /// </summary>
        /// <param name="userAttributeValue">User attribute value</param>
        public virtual void UpdateUserAttributeValue(UserAttributeValue userAttributeValue)
        {
            if (userAttributeValue == null)
                throw new ArgumentNullException("userAttributeValue");

            var builder = Builders<UserAttribute>.Filter;
            var filter = builder.Eq(x => x.Id, userAttributeValue.UserAttributeId);
            filter = filter & builder.ElemMatch(x => x.UserAttributeValues, y => y.Id == userAttributeValue.Id);
            var update = Builders<UserAttribute>.Update
                .Set(x => x.UserAttributeValues.ElementAt(-1).DisplayOrder, userAttributeValue.DisplayOrder)
                .Set(x => x.UserAttributeValues.ElementAt(-1).IsPreSelected, userAttributeValue.IsPreSelected)
                .Set(x => x.UserAttributeValues.ElementAt(-1).Locales, userAttributeValue.Locales)
                .Set(x => x.UserAttributeValues.ElementAt(-1).Name, userAttributeValue.Name);

            var result = _userAttributeRepository.Collection.UpdateManyAsync(filter, update).Result;

            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTES_PATTERN_KEY);
            _cacheManager.RemoveByPattern(CUSTOMERATTRIBUTEVALUES_PATTERN_KEY);

            //event notification
            _eventPublisher.EntityUpdated(userAttributeValue);
        }
        
        #endregion
    }
}
