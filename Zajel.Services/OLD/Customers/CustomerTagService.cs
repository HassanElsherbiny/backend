using System;
using System.Collections.Generic;
using System.Linq;
using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Common;
using Zajel.Services.Events;
using MongoDB.Driver;
using MongoDB.Bson;
using Zajel.Core;
using Zajel.Core.Caching;

namespace Zajel.Services.Users
{
    /// <summary>
    /// User tag service
    /// </summary>
    public partial class UserTagService : IUserTagService
    {
        #region Fields

        private readonly IRepository<UserTag> _userTagRepository;
        private readonly IRepository<UserTagProduct> _userTagProductRepository;
        private readonly IRepository<User> _userRepository;
        private readonly CommonSettings _commonSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        /// <summary>
        /// Key for caching
        /// </summary>
        /// <remarks>
        /// {0} : user tag Id?
        /// </remarks>
        private const string CUSTOMERTAGPRODUCTS_ROLE_KEY = "Zajel.usertagproducts.tag-{0}";

        private const string PRODUCTS_CUSTOMER_TAG = "Zajel.product.ct";

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        public UserTagService(IRepository<UserTag> userTagRepository,
            IRepository<UserTagProduct> userTagProductRepository,
            IRepository<User> userRepository,
            CommonSettings commonSettings,
            IEventPublisher eventPublisher,
            ICacheManager cacheManager
            )
        {
            this._userTagRepository = userTagRepository;
            this._userTagProductRepository = userTagProductRepository;
            this._commonSettings = commonSettings;
            this._eventPublisher = eventPublisher;
            this._userRepository = userRepository;
            this._cacheManager = cacheManager;
        }

        #endregion

        /// <summary>
        /// Gets all user for tag id
        /// </summary>
        /// <returns>Users</returns>
        public virtual IPagedList<User> GetUsersByTag(string userTagId = "", int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = from c in _userRepository.Table
                        where c.UserTags.Contains(userTagId)
                        select c;
            var users = new PagedList<User>(query, pageIndex, pageSize);
            return users;
        }

        /// <summary>
        /// Delete a user tag
        /// </summary>
        /// <param name="userTag">User tag</param>
        public virtual void DeleteUserTag(UserTag userTag)
        {
            if (userTag == null)
                throw new ArgumentNullException("productTag");

            var builder = Builders<User>.Update;
            var updatefilter = builder.Pull(x => x.UserTags, userTag.Id);
            var result = _userRepository.Collection.UpdateManyAsync(new BsonDocument(), updatefilter).Result;

            _userTagRepository.Delete(userTag);

            //event notification
            _eventPublisher.EntityDeleted(userTag);
        }

        /// <summary>
        /// Gets all user tags
        /// </summary>
        /// <returns>User tags</returns>
        public virtual IList<UserTag> GetAllUserTags()
        {
            var query = _userTagRepository.Table;
            return query.ToList();
        }

        /// <summary>
        /// Gets user tag
        /// </summary>
        /// <param name="userTagId">User tag identifier</param>
        /// <returns>User tag</returns>
        public virtual UserTag GetUserTagById(string userTagId)
        {
            return _userTagRepository.GetById(userTagId);
        }

        /// <summary>
        /// Gets user tag by name
        /// </summary>
        /// <param name="name">User tag name</param>
        /// <returns>User tag</returns>
        public virtual UserTag GetUserTagByName(string name)
        {
            var query = from pt in _userTagRepository.Table
                        where pt.Name == name
                        select pt;

            return query.FirstOrDefault(); 
        }

        /// <summary>
        /// Gets user tags search by name
        /// </summary>
        /// <param name="name">User tags name</param>
        /// <returns>User tags</returns>
        public virtual IList<UserTag> GetUserTagsByName(string name)
        {
            var query = from pt in _userTagRepository.Table
                        where pt.Name.ToLower().Contains(name.ToLower())
                        select pt;
            return query.ToList();
        }

        /// <summary>
        /// Inserts a user tag
        /// </summary>
        /// <param name="userTag">User tag</param>
        public virtual void InsertUserTag(UserTag userTag)
        {
            if (userTag == null)
                throw new ArgumentNullException("userTag");

            _userTagRepository.Insert(userTag);

            //event notification
            _eventPublisher.EntityInserted(userTag);
        }

        /// <summary>
        /// Insert tag to a user
        /// </summary>
        public virtual void InsertTagToUser(string userTagId, string userId)
        {
            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.AddToSet(p => p.UserTags, userTagId);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userId), update);
        }

        /// <summary>
        /// Delete tag from a user
        /// </summary>
        public virtual void DeleteTagFromUser(string userTagId, string userId)
        {
            var updatebuilder = Builders<User>.Update;
            var update = updatebuilder.Pull(p => p.UserTags, userTagId);
            _userRepository.Collection.UpdateOneAsync(new BsonDocument("_id", userId), update);
        }

        /// <summary>
        /// Updates the user tag
        /// </summary>
        /// <param name="userTag">User tag</param>
        public virtual void UpdateUserTag(UserTag userTag)
        {
            if (userTag == null)
                throw new ArgumentNullException("userTag");

            _userTagRepository.Update(userTag);

            //event notification
            _eventPublisher.EntityUpdated(userTag);
        }

        /// <summary>
        /// Get number of users
        /// </summary>
        /// <param name="userTagId">User tag identifier</param>
        /// <returns>Number of users</returns>
        public virtual int GetUserCount(string userTagId)
        {
            var query = _userRepository.Table.Where(x => x.UserTags.Contains(userTagId)).GroupBy(p => p, (k, s) => new { Count = s.Count() }).ToList();
            if (query.Count > 0)
                return query.FirstOrDefault().Count;
            return 0;
        }

        #region User tag product


        /// <summary>
        /// Gets user tag products for user tag
        /// </summary>
        /// <param name="userTagId">User tag id</param>
        /// <returns>User tag products</returns>
        public virtual IList<UserTagProduct> GetUserTagProducts(string userTagId)
        {
            string key = string.Format(CUSTOMERTAGPRODUCTS_ROLE_KEY, userTagId);
            return _cacheManager.Get(key, () =>
            {
                var query = from cr in _userTagProductRepository.Table
                            where (cr.UserTagId == userTagId)
                            orderby cr.DisplayOrder
                            select cr;
                var userRoles = query.ToList();
                return userRoles;
            });
        }

        /// <summary>
        /// Gets user tag products for user tag
        /// </summary>
        /// <param name="userTagId">User tag id</param>
        /// <param name="productId">Product id</param>
        /// <returns>User tag product</returns>
        public virtual UserTagProduct GetUserTagProduct(string userTagId, string productId)
        {
            var query = from cr in _userTagProductRepository.Table
                        where cr.UserTagId == userTagId && cr.ProductId == productId
                        orderby cr.DisplayOrder
                        select cr;
            var userRoles = query.ToList();
            return query.FirstOrDefault();
        }

        /// <summary>
        /// Gets user tag product
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns>User tag product</returns>
        public virtual UserTagProduct GetUserTagProductById(string id)
        {
            return _userTagProductRepository.GetById(id);
        }

        /// <summary>
        /// Inserts a user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        public virtual void InsertUserTagProduct(UserTagProduct userTagProduct)
        {
            if (userTagProduct == null)
                throw new ArgumentNullException("userTagProduct");

            _userTagProductRepository.Insert(userTagProduct);

            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERTAGPRODUCTS_ROLE_KEY, userTagProduct.UserTagId));
            _cacheManager.RemoveByPattern(PRODUCTS_CUSTOMER_TAG);

            //event notification
            _eventPublisher.EntityInserted(userTagProduct);
        }

        /// <summary>
        /// Updates the user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        public virtual void UpdateUserTagProduct(UserTagProduct userTagProduct)
        {
            if (userTagProduct == null)
                throw new ArgumentNullException("userTagProduct");

            _userTagProductRepository.Update(userTagProduct);

            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERTAGPRODUCTS_ROLE_KEY, userTagProduct.UserTagId));
            _cacheManager.RemoveByPattern(PRODUCTS_CUSTOMER_TAG);

            //event notification
            _eventPublisher.EntityUpdated(userTagProduct);
        }

        /// <summary>
        /// Delete a user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        public virtual void DeleteUserTagProduct(UserTagProduct userTagProduct)
        {
            if (userTagProduct == null)
                throw new ArgumentNullException("userTagProduct");

            _userTagProductRepository.Delete(userTagProduct);

            //clear cache
            _cacheManager.RemoveByPattern(string.Format(CUSTOMERTAGPRODUCTS_ROLE_KEY, userTagProduct.UserTagId));
            _cacheManager.RemoveByPattern(PRODUCTS_CUSTOMER_TAG);
            //event notification
            _eventPublisher.EntityDeleted(userTagProduct);
        }

        #endregion

    }
}
