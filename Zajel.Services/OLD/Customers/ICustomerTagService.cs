using System.Collections.Generic;
using Zajel.Core.Domain.Users;
using Zajel.Core;

namespace Zajel.Services.Users
{
    /// <summary>
    /// Product tag service interface
    /// </summary>
    public partial interface IUserTagService
    {
        /// <summary>
        /// Delete a user tag
        /// </summary>
        /// <param name="productTag">User tag</param>
        void DeleteUserTag(UserTag userTag);

        /// <summary>
        /// Gets all user tags
        /// </summary>
        /// <returns>User tags</returns>
        IList<UserTag> GetAllUserTags();

        /// <summary>
        /// Gets all user for tag id
        /// </summary>
        /// <returns>Users</returns>
        IPagedList<User> GetUsersByTag(string userTagId = "", int pageIndex = 0, int pageSize = 2147483647);
        /// <summary>
        /// Gets user tag
        /// </summary>
        /// <param name="userTagId">User tag identifier</param>
        /// <returns>Product tag</returns>
        UserTag GetUserTagById(string userTagId);

        /// <summary>
        /// Gets user tag by name
        /// </summary>
        /// <param name="name">User tag name</param>
        /// <returns>User tag</returns>
        UserTag GetUserTagByName(string name);

        /// <summary>
        /// Gets user tags search by name
        /// </summary>
        /// <param name="name">User tags name</param>
        /// <returns>User tags</returns>
        IList<UserTag> GetUserTagsByName(string name);

        /// <summary>
        /// Inserts a user tag
        /// </summary>
        /// <param name="userTag">User tag</param>
        void InsertUserTag(UserTag userTag);

        /// <summary>
        /// Insert tag to a user
        /// </summary>
        void InsertTagToUser(string userTagId, string userId);

        /// <summary>
        /// Delete tag from a user
        /// </summary>
        void DeleteTagFromUser(string userTagId, string userId);

        /// <summary>
        /// Updates the user tag
        /// </summary>
        /// <param name="productTag">Product tag</param>
        void UpdateUserTag(UserTag userTag);

        /// <summary>
        /// Get number of users
        /// </summary>
        /// <param name="userTagId">User tag identifier</param>
        /// <returns>Number of products</returns>
        int GetUserCount(string userTagId);

        /// <summary>
        /// Gets user tag products for user tag
        /// </summary>
        /// <param name="userTagId">User tag id</param>
        /// <returns>User tag products</returns>
        IList<UserTagProduct> GetUserTagProducts(string userTagId);

        /// <summary>
        /// Gets user tag products for user tag
        /// </summary>
        /// <param name="userTagId">User tag id</param>
        /// <param name="productId">Product id</param>
        /// <returns>User tag product</returns>
        UserTagProduct GetUserTagProduct(string userTagId, string productId);

        /// <summary>
        /// Gets user tag product
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns>User tag product</returns>
        UserTagProduct GetUserTagProductById(string id);

        /// <summary>
        /// Inserts a user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        void InsertUserTagProduct(UserTagProduct userTagProduct);

        /// <summary>
        /// Updates the user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        void UpdateUserTagProduct(UserTagProduct userTagProduct);

        /// <summary>
        /// Delete a user tag product
        /// </summary>
        /// <param name="userTagProduct">User tag product</param>
        void DeleteUserTagProduct(UserTagProduct userTagProduct);

    }
}
