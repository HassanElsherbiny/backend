﻿
using Zajel.Core;
using Zajel.Core.Domain.Users;
using System.Collections.Generic;

namespace Zajel.Services.Users
{
    public partial interface IUserActionService
    {


        /// <summary>
        /// Gets user action
        /// </summary>
        /// <param name="id">User action identifier</param>
        /// <returns>User Action</returns>
        UserAction GetUserActionById(string id);


        /// <summary>
        /// Gets all user actions
        /// </summary>
        /// <returns>User actions</returns>
        IList<UserAction> GetUserActions();

        /// <summary>
        /// Inserts a user action
        /// </summary>
        /// <param name="UserAction">User action</param>
        void InsertUserAction(UserAction userAction);

        /// <summary>
        /// Delete a user action
        /// </summary>
        /// <param name="userAction">User action</param>
        void DeleteUserAction(UserAction userAction);

        /// <summary>
        /// Updates the user action
        /// </summary>
        /// <param name="userTag">User tag</param>
        void UpdateUserAction(UserAction userAction);

        //UserActionConditionType GetUserActionConditionTypeById(string id);
        IList<UserActionType> GetUserActionType();
        UserActionType GetUserActionTypeById(string id);

        IPagedList<UserActionHistory> GetAllUserActionHistory(string userActionId, int pageIndex = 0, int pageSize = 2147483647);

        void UpdateUserActionType(UserActionType userActionType);

    }
}
