using System;
using System.Collections.Generic;
using Zajel.Core;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Common;

namespace Zajel.Services.Users
{
    /// <summary>
    /// User service interface
    /// </summary>
    public partial interface IUserService
    {
        #region Users

        /// <summary>
        /// Gets all users
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="affiliateId">Affiliate identifier</param>
        /// <param name="vendorId">Vendor identifier</param>
        /// <param name="userRoleIds">A list of user role identifiers to filter by (at least one match); pass null or empty list in order to load all users; </param>
        /// <param name="email">Email; null to load all users</param>
        /// <param name="username">Username; null to load all users</param>
        /// <param name="firstName">First name; null to load all users</param>
        /// <param name="lastName">Last name; null to load all users</param>
        /// <param name="dayOfBirth">Day of birth; 0 to load all users</param>
        /// <param name="monthOfBirth">Month of birth; 0 to load all users</param>
        /// <param name="company">Company; null to load all users</param>
        /// <param name="phone">Phone; null to load all users</param>
        /// <param name="zipPostalCode">Phone; null to load all users</param>
        /// <param name="loadOnlyWithShoppingCart">Value indicating whether to load users only with shopping cart</param>
        /// <param name="sct">Value indicating what shopping cart type to filter; userd when 'loadOnlyWithShoppingCart' param is 'true'</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Users</returns>
        IPagedList<User> GetAllUsers(DateTime? createdFromUtc = null,
            DateTime? createdToUtc = null, string affiliateId = "", string vendorId = "",
            string[] userRoleIds = null, string[] userTagIds = null, string email = null, string username = null,
            string firstName = null, string lastName = null,
            string company = null, string phone = null, string zipPostalCode = null,
            bool loadOnlyWithShoppingCart = false, ShoppingCartType? sct = null,
            int pageIndex = 0, int pageSize = int.MaxValue); //Int32.MaxValue
        
        /// <summary>
        /// Gets all users by user format (including deleted ones)
        /// </summary>
        /// <param name="passwordFormat">Password format</param>
        /// <returns>Users</returns>
        IList<User> GetAllUsersByPasswordFormat(PasswordFormat passwordFormat);

        /// <summary>
        /// Gets online users
        /// </summary>
        /// <param name="lastActivityFromUtc">User last activity date (from)</param>
        /// <param name="userRoleIds">A list of user role identifiers to filter by (at least one match); pass null or empty list in order to load all users; </param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Users</returns>
        IPagedList<User> GetOnlineUsers(DateTime lastActivityFromUtc,
            string[] userRoleIds, int pageIndex = 0, int pageSize = int.MaxValue);

        int GetCountOnlineShoppingCart(DateTime lastActivityFromUtc);

        /// <summary>
        /// Delete a user
        /// </summary>
        /// <param name="user">User</param>
        void DeleteUser(User user);

        /// <summary>
        /// Gets a user
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <returns>A user</returns>
        User GetUserById(string userId);

        /// <summary>
        /// Get users by identifiers
        /// </summary>
        /// <param name="userIds">User identifiers</param>
        /// <returns>Users</returns>
        IList<User> GetUsersByIds(string[] userIds);

        /// <summary>
        /// Gets a user by GUID
        /// </summary>
        /// <param name="userGuid">User GUID</param>
        /// <returns>A user</returns>
        User GetUserByGuid(Guid userGuid);

        /// <summary>
        /// Get user by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>User</returns>
        User GetUserByEmail(string email);
        
        /// <summary>
        /// Get user by system role
        /// </summary>
        /// <param name="systemName">System name</param>
        /// <returns>User</returns>
        User GetUserBySystemName(string systemName);

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username">Username</param>
        /// <returns>User</returns>
        User GetUserByUsername(string username);

        /// <summary>
        /// Insert a guest user
        /// </summary>
        /// <returns>User</returns>
        User InsertGuestUser();

        /// <summary>
        /// Insert a user
        /// </summary>
        /// <param name="user">User</param>
        void InsertUser(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUser(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastActivityDate(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserVendor(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserPassword(User user);


        /// <summary>
        /// Update free shipping
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="freeShipping"></param>
        void UpdateFreeShipping(string userId, bool freeShipping);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateAffiliate(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateActive(User user);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateNewsItem(User user);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasForumTopic(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasForumPost(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasOrders(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasBlogComments(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasProductReview(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasProductReviewH(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasPoolVoting(string userId);
        

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastLoginDate(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastIpAddress(User user);

        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastPurchaseDate(string userId, DateTime date);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastUpdateCartDate(string userId, DateTime? date);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserLastUpdateWishList(string userId, DateTime date);
        /// <summary>
        /// Updates the user in admin panel
        /// </summary>
        /// <param name="user">User</param>
        void UpdateUserinAdminPanel(User user);

        /// <summary>
        /// Reset data required for checkout
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="clearCouponCodes">A value indicating whether to clear coupon code</param>
        /// <param name="clearCheckoutAttributes">A value indicating whether to clear selected checkout attributes</param>
        /// <param name="clearRewardPoints">A value indicating whether to clear "Use reward points" flag</param>
        /// <param name="clearShippingMethod">A value indicating whether to clear selected shipping method</param>
        /// <param name="clearPaymentMethod">A value indicating whether to clear selected payment method</param>
        void ResetCheckoutData(User user, string storeId,
            bool clearCouponCodes = false, bool clearCheckoutAttributes = false,
            bool clearRewardPoints = true, bool clearShippingMethod = true,
            bool clearPaymentMethod = true);

        /// <summary>
        /// Update User Reminder History
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderId"></param>
        void UpdateUserReminderHistory(string userId, string orderId);


        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasVendorReview(string userId);
        /// <summary>
        /// Updates the user
        /// </summary>
        /// <param name="user">User</param>
        void UpdateHasVendorReviewH(string userId);

        /// <summary>
        /// Delete guest user records
        /// </summary>
        /// <param name="createdFromUtc">Created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Created date to (UTC); null to load all records</param>
        /// <param name="onlyWithoutShoppingCart">A value indicating whether to delete users only without shopping cart</param>
        /// <returns>Number of deleted users</returns>
        int DeleteGuestUsers(DateTime? createdFromUtc, DateTime? createdToUtc, bool onlyWithoutShoppingCart);

        #endregion

        #region Password history

        /// <summary>
        /// Gets user passwords
        /// </summary>
        /// <param name="userId">User identifier; pass null to load all records</param>
        /// <param name="passwordsToReturn">Number of returning passwords; pass null to load all records</param>
        /// <returns>List of user passwords</returns>
        IList<UserHistoryPassword> GetPasswords(string userId, int passwordsToReturn);

        /// <summary>
        /// Insert a user history password
        /// </summary>
        /// <param name="user">User</param>
        void InsertUserPassword(User user);


        #endregion

        #region User roles

        /// <summary>
        /// Inserts a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void InsertUserRole(UserRole userRole);

        /// <summary>
        /// Updates the user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void UpdateUserRole(UserRole userRole);

        /// <summary>
        /// Delete a user role
        /// </summary>
        /// <param name="userRole">User role</param>
        void DeleteUserRole(UserRole userRole);

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="userRoleId">User role identifier</param>
        /// <returns>User role</returns>
        UserRole GetUserRoleById(string userRoleId);

        /// <summary>
        /// Gets a user role
        /// </summary>
        /// <param name="systemName">User role system name</param>
        /// <returns>User role</returns>
        UserRole GetUserRoleBySystemName(string systemName);

        /// <summary>
        /// Gets all user roles
        /// </summary>
        /// <param name="showHidden">A value indicating whether to show hidden records</param>
        /// <returns>User roles</returns>
        IList<UserRole> GetAllUserRoles(bool showHidden = false);

        /// <summary>
        /// Gets user roles products for user role
        /// </summary>
        /// <param name="userRoleId">User role id</param>
        /// <returns>User role products</returns>
        IList<UserRoleProduct> GetUserRoleProducts(string userRoleId);

        /// <summary>
        /// Gets user roles products for user role
        /// </summary>
        /// <param name="userRoleId">User role id</param>
        /// <param name="productId">Product id</param>
        /// <returns>User role product</returns>
        UserRoleProduct GetUserRoleProduct(string userRoleId, string productId);

        /// <summary>
        /// Gets user roles product
        /// </summary>
        /// <param name="Id">id</param>
        /// <returns>User role product</returns>
        UserRoleProduct GetUserRoleProductById(string id);


        /// <summary>
        /// Inserts a user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        void InsertUserRoleProduct(UserRoleProduct userRoleProduct);

        /// <summary>
        /// Updates the user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        void UpdateUserRoleProduct(UserRoleProduct userRoleProduct);

        /// <summary>
        /// Delete a user role product
        /// </summary>
        /// <param name="userRoleProduct">User role product</param>
        void DeleteUserRoleProduct(UserRoleProduct userRoleProduct);


        #endregion

        #region User Role in User

        void InsertUserRoleInUser(UserRole userRole);

        void DeleteUserRoleInUser(UserRole userRole);
        
        #endregion

        #region User address

        void DeleteAddress(Address address);
        void InsertAddress(Address address);
        void UpdateAddress(Address address);
        void UpdateBillingAddress(Address address);
        void UpdateShippingAddress(Address address);
        void RemoveShippingAddress(string userId);

        #endregion

        #region Shopping cart 

        void ClearShoppingCartItem(string userId, string storeId, ShoppingCartType shoppingCartType);
        void DeleteShoppingCartItem(ShoppingCartItem shoppingCartItem);
        void InsertShoppingCartItem(ShoppingCartItem shoppingCartItem);
        void UpdateShoppingCartItem(ShoppingCartItem shoppingCartItem);
        void UpdateHasShoppingCartItems(User user);
        #endregion

        #region User Product Price

        /// <summary>
        /// Gets a user product price
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>User product price</returns>
        UserProductPrice GetUserProductPriceById(string id);

        /// <summary>
        /// Gets a price
        /// </summary>
        /// <param name="userId">User Identifier</param>
        /// <param name="productId">Product Identifier</param>
        /// <returns>User product price</returns>
        decimal? GetPriceByUserProduct(string userId, string productId);

        /// <summary>
        /// Inserts a user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        void InsertUserProductPrice(UserProductPrice userProductPrice);

        /// <summary>
        /// Updates the user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        void UpdateUserProductPrice(UserProductPrice userProductPrice);

        /// <summary>
        /// Delete a user product price
        /// </summary>
        /// <param name="userProductPrice">User product price</param>
        void DeleteUserProductPrice(UserProductPrice userProductPrice);



        /// <summary>
        /// Gets products price for user
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>User products price</returns>
        IPagedList<UserProductPrice> GetProductsByUser(string userId, int pageIndex = 0, int pageSize = int.MaxValue);

        #endregion

    }
}