using System;
using System.Linq;
using Zajel.Core;
using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Payments;
using Zajel.Core.Domain.Shipping;
using Zajel.Services.Helpers;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using MongoDB.Driver;

namespace Zajel.Services.Users
{
    /// <summary>
    /// User report service
    /// </summary>
    public partial class UserReportService : IUserReportService
    {
        #region Fields

        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly IUserService _userService;
        private readonly IDateTimeHelper _dateTimeHelper;

        #endregion

        #region Ctor

        /// <summary>
        /// Ctor
        /// </summary>
        /// <param name="userRepository">User repository</param>
        /// <param name="orderRepository">Order repository</param>
        /// <param name="userService">User service</param>
        /// <param name="dateTimeHelper">Date time helper</param>
        public UserReportService(IRepository<User> userRepository,
            IRepository<Order> orderRepository, IUserService userService,
            IDateTimeHelper dateTimeHelper)
        {
            this._userRepository = userRepository;
            this._orderRepository = orderRepository;
            this._userService = userService;
            this._dateTimeHelper = dateTimeHelper;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Get best users
        /// </summary>
        /// <param name="createdFromUtc">Order created date from (UTC); null to load all records</param>
        /// <param name="createdToUtc">Order created date to (UTC); null to load all records</param>
        /// <param name="os">Order status; null to load all records</param>
        /// <param name="ps">Order payment status; null to load all records</param>
        /// <param name="ss">Order shipment status; null to load all records</param>
        /// <param name="orderBy">1 - order by order total, 2 - order by number of orders</param>
        /// <param name="pageIndex">Page index</param>
        /// <param name="pageSize">Page size</param>
        /// <returns>Report</returns>
        public virtual IPagedList<BestUserReportLine> GetBestUsersReport(DateTime? createdFromUtc,
            DateTime? createdToUtc, OrderStatus? os, PaymentStatus? ps, ShippingStatus? ss, int orderBy,
            int pageIndex = 0, int pageSize = 214748364)
        {
            int? orderStatusId = null;
            if (os.HasValue)
                orderStatusId = (int)os.Value;

            int? paymentStatusId = null;
            if (ps.HasValue)
                paymentStatusId = (int)ps.Value;

            int? shippingStatusId = null;
            if (ss.HasValue)
                shippingStatusId = (int)ss.Value;

            var query = _orderRepository.Table;
            query = query.Where(o => !o.Deleted);
            if (orderStatusId.HasValue)
                query = query.Where(o => o.OrderStatusId == orderStatusId.Value);
            if (paymentStatusId.HasValue)
                query = query.Where(o => o.PaymentStatusId == paymentStatusId.Value);
            if (shippingStatusId.HasValue)
                query = query.Where(o => o.ShippingStatusId == shippingStatusId.Value);
            if (createdFromUtc.HasValue)
                query = query.Where(o => createdFromUtc.Value <= o.CreatedOnUtc);
            if (createdToUtc.HasValue)
                query = query.Where(o => createdToUtc.Value >= o.CreatedOnUtc);

            var query2 = from co in query
                         group co by co.UserId into g
                         select new
                         {
                             UserId = g.Key,
                             OrderTotal = g.Sum(x => x.OrderTotal),
                             OrderCount = g.Count()
                         };
            switch (orderBy)
            {
                case 1:
                    {
                        query2 = query2.OrderByDescending(x => x.OrderTotal);
                    }
                    break;
                case 2:
                    {
                        query2 = query2.OrderByDescending(x => x.OrderCount);
                    }
                    break;
                default:
                    throw new ArgumentException("Wrong orderBy parameter", "orderBy");
            }

            var tmp = new PagedList<dynamic>(query2, pageIndex, pageSize);
            return new PagedList<BestUserReportLine>(tmp.Select(x => new BestUserReportLine
            {
                UserId = x.UserId,
                OrderTotal = x.OrderTotal,
                OrderCount = x.OrderCount
            }),
                tmp.PageIndex, tmp.PageSize, tmp.TotalCount);
        }

        /// <summary>
        /// Gets a report of users registered in the last days
        /// </summary>
        /// <param name="days">Users registered in the last days</param>
        /// <returns>Number of registered users</returns>
        public virtual int GetRegisteredUsersReport(int days)
        {
            DateTime date = _dateTimeHelper.ConvertToUserTime(DateTime.Now).AddDays(-days);

            var registeredUserRole = _userService.GetUserRoleBySystemName(SystemUserRoleNames.Registered);
            if (registeredUserRole == null)
                return 0;

            var query = from c in _userRepository.Table
                        where !c.Deleted &&
                        c.UserRoles.Any(cr => cr.Id == registeredUserRole.Id) &&
                        c.CreatedOnUtc >= date
                        //&& c.CreatedOnUtc <= DateTime.UtcNow
                        select c;
            int count = query.Count();
            return count;
        }


        /// <summary>
        /// Get "user by time" report
        /// </summary>
        /// <param name="startTimeUtc">Start date</param>
        /// <param name="endTimeUtc">End date</param>
        /// <returns>Result</returns>
        public virtual IList<UserByTimeReportLine> GetUserByTimeReport(DateTime? startTimeUtc = null,
            DateTime? endTimeUtc = null)

        {
            List<UserByTimeReportLine> report = new List<UserByTimeReportLine>();
            if (!startTimeUtc.HasValue)
                startTimeUtc = DateTime.MinValue;
            if (!endTimeUtc.HasValue)
                endTimeUtc = DateTime.UtcNow;

            var endTime = new DateTime(endTimeUtc.Value.Year, endTimeUtc.Value.Month, endTimeUtc.Value.Day, 23, 59, 00);

            var builder = Builders<User>.Filter;
            var userRoleRegister = _userService.GetUserRoleBySystemName(SystemUserRoleNames.Registered).Id;
            var filter = builder.Where(o => !o.Deleted);
            filter = filter & builder.Where(o => o.CreatedOnUtc >= startTimeUtc.Value && o.CreatedOnUtc <= endTime);
            filter = filter & builder.Where(o => o.UserRoles.Any(y => y.Id == userRoleRegister));

            var daydiff = (endTimeUtc.Value - startTimeUtc.Value).TotalDays;
            if (daydiff > 32)
            {
                var query = _userRepository.Collection.Aggregate().Match(filter).Group(x =>
                    new { Year = x.CreatedOnUtc.Year, Month = x.CreatedOnUtc.Month },
                    g => new { Okres = g.Key, Count = g.Count() }).SortBy(x => x.Okres).ToList();
                foreach (var item in query)
                {
                    report.Add(new UserByTimeReportLine()
                    {
                        Time = item.Okres.Year.ToString() + "-" + item.Okres.Month.ToString(),
                        Registered = item.Count,
                    });
                }
            }
            else
            {
                var query = _userRepository.Collection.Aggregate().Match(filter).Group(x =>
                    new { Year = x.CreatedOnUtc.Year, Month = x.CreatedOnUtc.Month, Day = x.CreatedOnUtc.Day },
                    g => new { Okres = g.Key, Count = g.Count() }).SortBy(x => x.Okres).ToList();
                foreach (var item in query)
                {
                    report.Add(new UserByTimeReportLine()
                    {
                        Time = item.Okres.Year.ToString() + "-" + item.Okres.Month.ToString() + "-" + item.Okres.Day.ToString(),
                        Registered = item.Count,
                    });
                }
            }



            return report;
        }

        #endregion
    }
}