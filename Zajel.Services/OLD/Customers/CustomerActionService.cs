﻿using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Services.Events;
using System;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Zajel.Core;
using Zajel.Core.Caching;

namespace Zajel.Services.Users
{
    public partial class UserActionService: IUserActionService
    {
        #region Fields
        private const string CUSTOMER_ACTION_TYPE = "Zajel.user.action.type";
        private readonly IRepository<UserAction> _userActionRepository;
        private readonly IRepository<UserActionType> _userActionTypeRepository;
        private readonly IRepository<UserActionHistory> _userActionHistoryRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly ICacheManager _cacheManager;

        #endregion

        #region Ctor

        public UserActionService(IRepository<UserAction> userActionRepository,
            IRepository<UserActionType> userActionTypeRepository,
            IRepository<UserActionHistory> userActionHistoryRepository,
            IEventPublisher eventPublisher,
            ICacheManager cacheManager)
        {
            this._userActionRepository = userActionRepository;
            this._userActionTypeRepository = userActionTypeRepository;
            this._userActionHistoryRepository = userActionHistoryRepository;
            this._eventPublisher = eventPublisher;
            this._cacheManager = cacheManager;
        }

        #endregion


        #region Methods
        
        /// <summary>
        /// Gets user action
        /// </summary>
        /// <param name="id">User action identifier</param>
        /// <returns>User Action</returns>
        public virtual UserAction GetUserActionById(string id)
        {
            return _userActionRepository.GetById(id);
        }


        /// <summary>
        /// Gets all user actions
        /// </summary>
        /// <returns>User actions</returns>
        public virtual IList<UserAction> GetUserActions()
        {
            var query = _userActionRepository.Table;
            return query.ToList();
        }

        /// <summary>
        /// Inserts a user action
        /// </summary>
        /// <param name="UserAction">User action</param>
        public virtual void InsertUserAction(UserAction userAction)
        {
            if (userAction == null)
                throw new ArgumentNullException("userAction");

            _userActionRepository.Insert(userAction);

            //event notification
            _eventPublisher.EntityInserted(userAction);

        }

        /// <summary>
        /// Delete a user action
        /// </summary>
        /// <param name="userAction">User action</param>
        public virtual void DeleteUserAction(UserAction userAction)
        {
            if (userAction == null)
                throw new ArgumentNullException("userAction"); 

            _userActionRepository.Delete(userAction);

            //event notification
            _eventPublisher.EntityDeleted(userAction);

        }

        /// <summary>
        /// Updates the user action
        /// </summary>
        /// <param name="userTag">User tag</param>
        public virtual void UpdateUserAction(UserAction userAction)
        {
            if (userAction == null)
                throw new ArgumentNullException("userAction");

            _userActionRepository.Update(userAction);

            //event notification
            _eventPublisher.EntityUpdated(userAction);
        }

        #endregion

        #region Condition Type

        public virtual IList<UserActionType> GetUserActionType()
        {
            var query = _userActionTypeRepository.Table;
            return query.ToList();
        }

        public virtual IPagedList<UserActionHistory> GetAllUserActionHistory(string userActionId, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = from h in _userActionHistoryRepository.Table
                        where h.UserActionId == userActionId
                        select h;
            var history = new PagedList<UserActionHistory>(query, pageIndex, pageSize);
            return history;
        }

        public virtual UserActionType GetUserActionTypeById(string id)
        {
            return _userActionTypeRepository.GetById(id);
        }

        public virtual void UpdateUserActionType(UserActionType userActionType)
        {
            if (userActionType == null)
                throw new ArgumentNullException("userActionType");

            _userActionTypeRepository.Update(userActionType);

            //clear cache
            _cacheManager.Remove(CUSTOMER_ACTION_TYPE);
            //event notification
            _eventPublisher.EntityUpdated(userActionType);
        }

        #endregion

    }
}
