﻿
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Orders;

namespace Zajel.Services.Users
{
    public partial interface IUserActionEventService
    {
        /// <summary>
        /// Run action add to cart 
        /// </summary>
        void AddToCart(ShoppingCartItem cart, Product product, User user);

        /// <summary>
        /// Run action add new order
        /// </summary>
        void AddOrder(Order order, User user);

        /// <summary>
        /// Viewed
        /// </summary>
        void Viewed(User user, string currentUrl, string previousUrl);

        /// <summary>
        /// Run action url
        /// </summary>
        void Url(User user, string currentUrl, string previousUrl);


        /// <summary>
        /// Run action url
        /// </summary>
        void Registration(User user);
    }
}
