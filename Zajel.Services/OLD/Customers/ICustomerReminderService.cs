﻿
using Zajel.Core;
using Zajel.Core.Domain.Users;
using System.Collections.Generic;

namespace Zajel.Services.Users
{
    public partial interface IUserReminderService
    {


        /// <summary>
        /// Gets user reminder
        /// </summary>
        /// <param name="id">User reminder identifier</param>
        /// <returns>User reminder</returns>
        UserReminder GetUserReminderById(string id);


        /// <summary>
        /// Gets all user reminders
        /// </summary>
        /// <returns>User reminders</returns>
        IList<UserReminder> GetUserReminders();

        /// <summary>
        /// Inserts a user reminder
        /// </summary>
        /// <param name="UserReminder">User reminder</param>
        void InsertUserReminder(UserReminder userReminder);

        /// <summary>
        /// Delete a user reminder
        /// </summary>
        /// <param name="userReminder">User reminder</param>
        void DeleteUserReminder(UserReminder userReminder);

        /// <summary>
        /// Updates the user reminder
        /// </summary>
        /// <param name="UserReminder">User reminder</param>
        void UpdateUserReminder(UserReminder userReminder);

        /// <summary>
        /// Gets user reminders history for reminder
        /// </summary>
        /// <returns>SerializeUserReminderHistory</returns>
        IPagedList<SerializeUserReminderHistory> GetAllUserReminderHistory(string userReminderId, int pageIndex = 0, int pageSize = 2147483647);

        /// <summary>
        /// Run task Abandoned Cart
        /// </summary>
        void Task_AbandonedCart(string id = "");
        void Task_RegisteredUser(string id = "");
        void Task_LastActivity(string id = "");
        void Task_LastPurchase(string id = "");
        void Task_Birthday(string id = "");
        void Task_CompletedOrder(string id = "");
        void Task_UnpaidOrder(string id = "");

    }
}
