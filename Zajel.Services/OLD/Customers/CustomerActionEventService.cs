﻿using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Services.Events;
using System;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Zajel.Core.Domain.Orders;
using Zajel.Services.Catalog;
using Zajel.Core.Domain.Catalog;
using Zajel.Core;
using Zajel.Core.Domain.Messages;
using Zajel.Services.Messages;
using Zajel.Services.Localization;
using Zajel.Core.Domain.Logging;
using System.Threading.Tasks;
using Zajel.Core.Caching;
using Zajel.Services.Helpers;
using Microsoft.AspNetCore.Http;

namespace Zajel.Services.Users
{
    public partial class UserActionEventService : IUserActionEventService
    {
        #region Fields
        private const string CUSTOMER_ACTION_TYPE = "Zajel.user.action.type";

        private readonly IRepository<UserAction> _userActionRepository;
        private readonly IRepository<UserActionHistory> _userActionHistoryRepository;
        private readonly IRepository<UserActionType> _userActionTypeRepository;
        private readonly IRepository<Banner> _bannerRepository;
        private readonly IRepository<InteractiveForm> _interactiveFormRepository;
        private readonly IRepository<PopupActive> _popupActiveRepository;
        private readonly IRepository<ActivityLog> _activityLogRepository;
        private readonly IRepository<ActivityLogType> _activityLogTypeRepository;
        private readonly IEventPublisher _eventPublisher;
        private readonly IProductService _productService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IMessageTemplateService _messageTemplateService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly IWorkContext _workContext;
        private readonly IUserService _userService;
        private readonly IUserAttributeService _userAttributeService;
        private readonly IUserAttributeParser _userAttributeParser;
        private readonly IUserTagService _userTagService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ICacheManager _cacheManager;
        private readonly IPopupService _popupService;
        private readonly IStoreContext _storeContext;

        #endregion

        #region Ctor

        public UserActionEventService(IRepository<UserAction> userActionRepository,
            IRepository<UserActionType> userActionTypeRepository,
            IRepository<UserActionHistory> userActionHistoryRepository,
            IRepository<Banner> bannerRepository,
            IRepository<InteractiveForm> interactiveFormRepository,
            IRepository<PopupActive> popupActiveRepository,
            IRepository<ActivityLog> activityLogRepository,
            IRepository<ActivityLogType> activityLogTypeRepository,
            IEventPublisher eventPublisher,
            IProductService productService,
            IProductAttributeParser productAttributeParser,
            IMessageTemplateService messageTemplateService,
            IWorkflowMessageService workflowMessageService,
            IWorkContext workContext,
            IUserService userService,
            IUserAttributeService userAttributeService,
            IUserAttributeParser userAttributeParser,
            IUserTagService userTagService,
            IHttpContextAccessor httpContextAccessor,
            ICacheManager cacheManager,
            IPopupService popupService,
            IStoreContext storeContext)
        {
            this._userActionRepository = userActionRepository;
            this._userActionTypeRepository = userActionTypeRepository;
            this._userActionHistoryRepository = userActionHistoryRepository;
            this._bannerRepository = bannerRepository;
            this._interactiveFormRepository = interactiveFormRepository;
            this._popupActiveRepository = popupActiveRepository;
            this._activityLogRepository = activityLogRepository;
            this._activityLogTypeRepository = activityLogTypeRepository;
            this._eventPublisher = eventPublisher;
            this._productService = productService;
            this._productAttributeParser = productAttributeParser;
            this._messageTemplateService = messageTemplateService;
            this._workflowMessageService = workflowMessageService;
            this._workContext = workContext;
            this._userService = userService;
            this._userAttributeService = userAttributeService;
            this._userAttributeParser = userAttributeParser;
            this._userTagService = userTagService;
            this._httpContextAccessor = httpContextAccessor;
            this._cacheManager = cacheManager;
            this._popupService = popupService;
            this._storeContext = storeContext;
        }

        #endregion

        #region Utilities
        protected IList<UserActionType> GetAllUserActionType()
        {
            return _cacheManager.Get(CUSTOMER_ACTION_TYPE, () =>
            {
                return _userActionTypeRepository.Table.AsQueryable().ToList();
            });
        }



        #region Action
        protected bool UsedAction(string actionId, string userId)
        {
            var query = from u in _userActionHistoryRepository.Table
                        where u.UserId == userId && u.UserActionId == actionId
                        select u.Id;
            if (query.Count() > 0)
                return true;

            return false;
        }

        protected void SaveActionToUser(string actionId, string userId)
        {
            _userActionHistoryRepository.Insert(new UserActionHistory() { UserId = userId, UserActionId = actionId, CreateDateUtc = DateTime.UtcNow });
        }
        #endregion

        #region Condition
        protected bool Condition(UserAction action, Product product, string attributesXml, User user, string currentUrl, string previousUrl)
        {
            var _cat = GetAllUserActionType();
            if (action.Conditions.Count() == 0)
                return true;

            bool cond = false;
            foreach (var item in action.Conditions)
            {
                #region product
                if (product != null)
                {

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Category)
                    {
                        cond = ConditionCategory(item, product.ProductCategories);
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Manufacturer)
                    {
                        cond = ConditionManufacturer(item, product.ProductManufacturers);
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Product)
                    {
                        cond = ConditionProducts(item, product.Id);
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.ProductAttribute)
                    {
                        if (!String.IsNullOrEmpty(attributesXml))
                        {
                            cond = ConditionProductAttribute(item, product, attributesXml);
                        }
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.ProductSpecification)
                    {
                        cond = ConditionSpecificationAttribute(item, product.ProductSpecificationAttributes);
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Vendor)
                    {
                        cond = ConditionVendors(item, product.VendorId);
                    }

                }
                #endregion

                #region Action type viewed
                if (action.ActionTypeId == _cat.FirstOrDefault(x => x.SystemKeyword == "Viewed").Id)
                {
                    cond = false;
                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Category)
                    {
                        var _actLogType = (from a in _activityLogTypeRepository.Table
                                           where a.SystemKeyword == "PublicStore.ViewCategory"
                                           select a).FirstOrDefault();
                        if (_actLogType != null)
                        {
                            if (_actLogType.Enabled)
                            {
                                var productCategory = (from p in _activityLogRepository.Table
                                                       where p.UserId == user.Id && p.ActivityLogTypeId == _actLogType.Id
                                                       select p.EntityKeyId).Distinct().ToList();
                                cond = ConditionCategory(item, productCategory);
                            }
                        }
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Manufacturer)
                    {
                        cond = false;
                        var _actLogType = (from a in _activityLogTypeRepository.Table
                                           where a.SystemKeyword == "PublicStore.ViewManufacturer"
                                           select a).FirstOrDefault();
                        if (_actLogType != null)
                        {
                            if (_actLogType.Enabled)
                            {
                                var productManufacturer = (from p in _activityLogRepository.Table
                                                           where p.UserId == user.Id && p.ActivityLogTypeId == _actLogType.Id
                                                           select p.EntityKeyId).Distinct().ToList();
                                cond = ConditionManufacturer(item, productManufacturer);
                            }
                        }
                    }

                    if (item.UserActionConditionType == UserActionConditionTypeEnum.Product)
                    {
                        cond = false;
                        var _actLogType = (from a in _activityLogTypeRepository.Table
                                           where a.SystemKeyword == "PublicStore.ViewProduct"
                                           select a).FirstOrDefault();
                        if (_actLogType != null)
                        {
                            if (_actLogType.Enabled)
                            {
                                var products = (from p in _activityLogRepository.Table
                                                where p.UserId == user.Id && p.ActivityLogTypeId == _actLogType.Id
                                                select p.EntityKeyId).Distinct().ToList();
                                cond = ConditionProducts(item, products);
                            }
                        }
                    }
                }
                #endregion

                if (item.UserActionConditionType == UserActionConditionTypeEnum.UserRole)
                {
                    cond = ConditionUserRole(item, user);
                }

                if (item.UserActionConditionType == UserActionConditionTypeEnum.UserTag)
                {
                    cond = ConditionUserTag(item, user);
                }

                if (item.UserActionConditionType == UserActionConditionTypeEnum.UserRegisterField)
                {
                    cond = ConditionUserRegister(item, user);
                }

                if (item.UserActionConditionType == UserActionConditionTypeEnum.CustomUserAttribute)
                {
                    cond = ConditionUserAttribute(item, user);
                }

                if (item.UserActionConditionType == UserActionConditionTypeEnum.UrlCurrent)
                {
                    cond = item.UrlCurrent.Select(x => x.Name).Contains(currentUrl);
                }

                if (item.UserActionConditionType == UserActionConditionTypeEnum.UrlReferrer)
                {
                    cond = item.UrlReferrer.Select(x => x.Name).Contains(previousUrl);
                }

                if(item.UserActionConditionType == UserActionConditionTypeEnum.Store)
                {
                    cond = ConditionStores(item, _storeContext.CurrentStore.Id);
                }

                if (action.Condition == UserActionConditionEnum.OneOfThem && cond)
                    return true;
                if (action.Condition == UserActionConditionEnum.AllOfThem && !cond)
                    return false;
            }

            return cond;
        }

        protected bool ConditionCategory(UserAction.ActionCondition condition, ICollection<ProductCategory> categorties)
        {
            bool cond = true;
            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = categorties.Select(x => x.CategoryId).ContainsAll(condition.Categories);
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                cond = categorties.Select(x => x.CategoryId).ContainsAny(condition.Categories);
            }

            return cond;
        }
        protected bool ConditionCategory(UserAction.ActionCondition condition, ICollection<string> categorties)
        {
            bool cond = true;
            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = categorties.ContainsAll(condition.Categories);
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                cond = categorties.ContainsAny(condition.Categories);
            }

            return cond;
        }

        protected bool ConditionManufacturer(UserAction.ActionCondition condition, ICollection<ProductManufacturer> manufacturers)
        {
            bool cond = true;

            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = manufacturers.Select(x => x.ManufacturerId).ContainsAll(condition.Manufacturers);
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                cond = manufacturers.Select(x => x.ManufacturerId).ContainsAny(condition.Manufacturers);
            }

            return cond;
        }

        protected bool ConditionManufacturer(UserAction.ActionCondition condition, ICollection<string> manufacturers)
        {
            bool cond = true;

            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = manufacturers.ContainsAll(condition.Manufacturers);
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                cond = manufacturers.ContainsAny(condition.Manufacturers);
            }

            return cond;
        }

        protected bool ConditionProducts(UserAction.ActionCondition condition, string productId)
        {
            return condition.Products.Contains(productId);
        }

        protected bool ConditionStores(UserAction.ActionCondition condition, string storeId)
        {
            return condition.Stores.Contains(storeId);
        }


        protected bool ConditionProducts(UserAction.ActionCondition condition, ICollection<string> products)
        {
            bool cond = true;
            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = products.ContainsAll(condition.Products);
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                cond = products.ContainsAny(condition.Products);
            }

            return cond;
        }

        protected bool ConditionProductAttribute(UserAction.ActionCondition condition, Product product, string AttributesXml)
        {
            bool cond = false;
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                var attributes = _productAttributeParser.ParseProductAttributeMappings(product, AttributesXml);
                foreach (var attr in attributes)
                {
                    var attributeValuesStr = _productAttributeParser.ParseValues(AttributesXml, attr.Id);
                    foreach (var attrV in attributeValuesStr)
                    {
                        var attrsv = attr.ProductAttributeValues.Where(x => x.Id == attrV).FirstOrDefault();
                        if (attrsv != null)
                            if (condition.ProductAttribute.Where(x => x.ProductAttributeId == attr.ProductAttributeId && x.Name == attrsv.Name).Count() > 0)
                            {
                                cond = true;
                            }
                    }
                }
            }
            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = true;
                foreach (var itemPA in condition.ProductAttribute)
                {
                    var attributes = _productAttributeParser.ParseProductAttributeMappings(product, AttributesXml);
                    if (attributes.Where(x => x.ProductAttributeId == itemPA.ProductAttributeId).Count() > 0)
                    {
                        cond = false;
                        foreach (var attr in attributes.Where(x => x.ProductAttributeId == itemPA.ProductAttributeId))
                        {
                            var attributeValuesStr = _productAttributeParser.ParseValues(AttributesXml, attr.Id);
                            foreach (var attrV in attributeValuesStr)
                            {
                                var attrsv = attr.ProductAttributeValues.Where(x => x.Id == attrV).FirstOrDefault();
                                if (attrsv != null)
                                {
                                    if (attrsv.Name == itemPA.Name)
                                    {
                                        cond = true;
                                    }
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                        if (!cond)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return cond;
        }

        protected bool ConditionSpecificationAttribute(UserAction.ActionCondition condition, ICollection<ProductSpecificationAttribute> productspecificationattribute)
        {
            bool cond = false;

            if (condition.Condition == UserActionConditionEnum.AllOfThem)
            {
                cond = true;
                foreach (var spec in condition.ProductSpecifications)
                {
                    if (productspecificationattribute.Where(x => x.SpecificationAttributeId == spec.ProductSpecyficationId && x.SpecificationAttributeOptionId == spec.ProductSpecyficationValueId).Count() == 0)
                        cond = false;
                }
            }
            if (condition.Condition == UserActionConditionEnum.OneOfThem)
            {
                foreach (var spec in productspecificationattribute)
                {
                    if (condition.ProductSpecifications.Where(x => x.ProductSpecyficationId == spec.SpecificationAttributeId && x.ProductSpecyficationValueId == spec.SpecificationAttributeOptionId).Count() > 0)
                        cond = true;
                }
            }

            return cond;
        }

        protected bool ConditionVendors(UserAction.ActionCondition condition, string vendorId)
        {
            return condition.Vendors.Contains(vendorId);
        }

        protected bool ConditionUserRole(UserAction.ActionCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var userRoles = user.UserRoles;
                if (condition.Condition == UserActionConditionEnum.AllOfThem)
                {
                    cond = userRoles.Select(x => x.Id).ContainsAll(condition.UserRoles);
                }
                if (condition.Condition == UserActionConditionEnum.OneOfThem)
                {
                    cond = userRoles.Select(x => x.Id).ContainsAny(condition.UserRoles);
                }
            }
            return cond;
        }

        protected bool ConditionUserTag(UserAction.ActionCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var userTags = user.UserTags;
                if (condition.Condition == UserActionConditionEnum.AllOfThem)
                {
                    cond = userTags.Select(x => x).ContainsAll(condition.UserTags);
                }
                if (condition.Condition == UserActionConditionEnum.OneOfThem)
                {
                    cond = userTags.Select(x => x).ContainsAny(condition.UserTags);
                }
            }
            return cond;
        }

        protected bool ConditionUserRegister(UserAction.ActionCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var _genericAttributes = _userService.GetUserById(user.Id).GenericAttributes;
                if (condition.Condition == UserActionConditionEnum.AllOfThem)
                {
                    cond = true;
                    foreach (var item in condition.UserRegistration)
                    {
                        if (_genericAttributes.Where(x => x.Key == item.RegisterField && x.Value.ToLower() == item.RegisterValue.ToLower()).Count() == 0)
                            cond = false;
                    }
                }
                if (condition.Condition == UserActionConditionEnum.OneOfThem)
                {
                    foreach (var item in condition.UserRegistration)
                    {
                        if (_genericAttributes.Where(x => x.Key == item.RegisterField && x.Value.ToLower() == item.RegisterValue.ToLower()).Count() > 0)
                            cond = true;
                    }
                }
            }
            return cond;
        }

        protected bool ConditionUserAttribute(UserAction.ActionCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var _genericAttributes = _userService.GetUserById(user.Id).GenericAttributes;
                if (condition.Condition == UserActionConditionEnum.AllOfThem)
                {
                    var customUserAttributes = _genericAttributes.FirstOrDefault(x => x.Key == "CustomUserAttributes");
                    if (customUserAttributes != null)
                    {
                        if (!String.IsNullOrEmpty(customUserAttributes.Value))
                        {
                            var selectedValues = _userAttributeParser.ParseUserAttributeValues(customUserAttributes.Value);
                            cond = true;
                            foreach (var item in condition.CustomUserAttributes)
                            {
                                var _fields = item.RegisterField.Split(':');
                                if (_fields.Count() > 1)
                                {
                                    if (selectedValues.Where(x => x.UserAttributeId == _fields.FirstOrDefault() && x.Id == _fields.LastOrDefault()).Count() == 0)
                                        cond = false;
                                }
                                else
                                    cond = false;
                            }
                        }
                    }
                }
                if (condition.Condition == UserActionConditionEnum.OneOfThem)
                {
                    var customUserAttributes = _genericAttributes.FirstOrDefault(x => x.Key == "CustomUserAttributes");
                    if (customUserAttributes != null)
                    {
                        if (!String.IsNullOrEmpty(customUserAttributes.Value))
                        {
                            var selectedValues = _userAttributeParser.ParseUserAttributeValues(customUserAttributes.Value);
                            foreach (var item in condition.CustomUserAttributes)
                            {
                                var _fields = item.RegisterField.Split(':');
                                if (_fields.Count() > 1)
                                {
                                    if (selectedValues.Where(x => x.UserAttributeId == _fields.FirstOrDefault() && x.Id == _fields.LastOrDefault()).Count() > 0)
                                        cond = true;
                                }
                            }
                        }
                    }
                }
            }
            return cond;
        }

        #endregion

        #region Reaction
        public void Reaction(UserAction action, User user, ShoppingCartItem cartItem, Order order)
        {
            if (action.ReactionType == UserReactionTypeEnum.Banner)
            {
                var banner = _bannerRepository.GetById(action.BannerId);
                if (banner != null)
                    PrepareBanner(action, banner, user.Id);
            }
            if (action.ReactionType == UserReactionTypeEnum.InteractiveForm)
            {
                var interactiveform = _interactiveFormRepository.GetById(action.InteractiveFormId);
                if (interactiveform != null)
                    PrepareInteractiveForm(action, interactiveform, user.Id);
            }

            var _cat = GetAllUserActionType();

            if (action.ReactionType == UserReactionTypeEnum.Email)
            {
                if (action.ActionTypeId == _cat.FirstOrDefault(x => x.SystemKeyword == "AddToCart").Id)
                {
                    if (cartItem != null)
                        _workflowMessageService.SendUserActionEvent_AddToCart_Notification(action, cartItem,
                            _workContext.WorkingLanguage.Id, user);
                }

                if (action.ActionTypeId == _cat.FirstOrDefault(x => x.SystemKeyword == "AddOrder").Id)
                {
                    if (order != null)
                        _workflowMessageService.SendUserActionEvent_AddToOrder_Notification(action, order, user,
                            _workContext.WorkingLanguage.Id);
                }

                if (action.ActionTypeId != _cat.FirstOrDefault(x => x.SystemKeyword == "AddOrder").Id && action.ActionTypeId != _cat.FirstOrDefault(x => x.SystemKeyword == "AddToCart").Id)
                {
                    _workflowMessageService.SendUserActionEvent_Notification(action,
                        _workContext.WorkingLanguage.Id, user);
                }
            }

            if (action.ReactionType == UserReactionTypeEnum.AssignToUserRole)
            {
                AssignToUserRole(action, user);
            }

            if (action.ReactionType == UserReactionTypeEnum.AssignToUserTag)
            {
                AssignToUserTag(action, user);
            }

            SaveActionToUser(action.Id, user.Id);

        }
        protected void PrepareBanner(UserAction action, Banner banner, string userId)
        {
            var banneractive = new PopupActive()
            {
                Body = banner.Body,
                CreatedOnUtc = DateTime.UtcNow,
                UserId = userId,
                UserActionId = action.Id,
                Name = banner.Name,
                PopupTypeId = (int)PopupType.Banner
            };
            _popupService.InsertPopupActive(banneractive);
        }

        protected void PrepareInteractiveForm(UserAction action, InteractiveForm form, string userId)
        {

            var body = PrepareDataInteractiveForm(form);

            var formactive = new PopupActive()
            {
                Body = body,
                CreatedOnUtc = DateTime.UtcNow,
                UserId = userId,
                UserActionId = action.Id,
                Name = form.Name,
                PopupTypeId = (int)PopupType.InteractiveForm
            };
            _popupService.InsertPopupActive(formactive);
        }

        protected string PrepareDataInteractiveForm(InteractiveForm form)
        {
            var body = form.Body;
            body += "<input type=\"hidden\" name=\"Id\" value=\"" + form.Id + "\">";
            foreach (var item in form.FormAttributes)
            {
                if (item.AttributeControlType == FormControlType.TextBox)
                {
                    string _style = string.Format("{0}", item.Style);
                    string _class = string.Format("{0} {1}", "form-control", item.Class);
                    string _value = item.DefaultValue;
                    var textbox = string.Format("<input type=\"text\"  name=\"{0}\" class=\"{1}\" style=\"{2}\" value=\"{3}\" {4}>", item.SystemName, _class, _style ,_value, item.IsRequired ? "required" : "");
                    body = body.Replace(string.Format("%{0}%", item.SystemName), textbox);
                }
                if (item.AttributeControlType == FormControlType.MultilineTextbox)
                {
                    string _style = string.Format("{0}", item.Style);
                    string _class = string.Format("{0} {1}", "form-control", item.Class);
                    string _value = item.DefaultValue;
                    var textarea = string.Format("<textarea name=\"{0}\" class=\"{1}\" style=\"{2}\" {3}> {4} </textarea>", item.SystemName, _class, _style, item.IsRequired ? "required" : "", _value);
                    body = body.Replace(string.Format("%{0}%", item.SystemName), textarea);
                }
                if (item.AttributeControlType == FormControlType.Checkboxes)
                {
                    var checkbox = "<div class=\"custom-controls-stacked\">";
                    foreach (var itemcheck in item.FormAttributeValues.OrderBy(x => x.DisplayOrder))
                    {
                        string _style = string.Format("{0}", item.Style);
                        string _class = string.Format("{0} {1}", "custom-control-input", item.Class);

                        checkbox += "<label class=\"custom-control custom-checkbox\">";
                        checkbox += string.Format("<input type=\"checkbox\" class=\"{0}\" style=\"{1}\" {2} id=\"{3}\" name=\"{4}\" value=\"{5}\" >", _class, _style, 
                            itemcheck.IsPreSelected ? "checked" : "", itemcheck.Id, item.SystemName, itemcheck.GetLocalized(x => x.Name));
                        checkbox += "<span class=\"custom-control-indicator\"></span>";
                        checkbox += string.Format("<span class=\"custom-control-description\">{0}</span>", itemcheck.GetLocalized(x => x.Name));
                        checkbox += "</label>";
                    }
                    checkbox += "</div>";
                    body = body.Replace(string.Format("%{0}%", item.SystemName), checkbox);
                }

                if (item.AttributeControlType == FormControlType.DropdownList)
                {
                    var dropdown = string.Empty;
                    string _style = string.Format("{0}", item.Style);
                    string _class = string.Format("{0} {1}", "form-control custom-select", item.Class);

                    dropdown = string.Format("<select name=\"{0}\" class=\"{1}\" style=\"{2}\" >", item.SystemName, _class, _style);
                    foreach (var itemdropdown in item.FormAttributeValues.OrderBy(x => x.DisplayOrder))
                    {
                        dropdown += string.Format("<option value=\"{0}\" {1}>{2}</option>", itemdropdown.GetLocalized(x => x.Name), itemdropdown.IsPreSelected ? "selected" : "", itemdropdown.GetLocalized(x => x.Name));
                    }
                    dropdown += "</select>";
                    body = body.Replace(string.Format("%{0}%", item.SystemName), dropdown);
                }
                if (item.AttributeControlType == FormControlType.RadioList)
                {
                    var radio = "<div class=\"custom-controls-stacked\">";
                    foreach (var itemradio in item.FormAttributeValues.OrderBy(x => x.DisplayOrder))
                    {
                        string _style = string.Format("{0}", item.Style);
                        string _class = string.Format("{0} {1}", "custom-control-input", item.Class);

                        radio += "<label class=\"custom-control custom-radio\">";
                        radio += string.Format("<input type=\"radio\" class=\"{0}\" style=\"{1}\" {2} id=\"{3}\" name=\"{4}\" value=\"{5}\">", _class, _style,
                            itemradio.IsPreSelected ? "checked" : "", itemradio.Id, item.SystemName, itemradio.GetLocalized(x => x.Name));
                        radio += "<span class=\"custom-control-indicator\"></span>";
                        radio += string.Format("<span class=\"custom-control-description\">{0}</span>", itemradio.GetLocalized(x => x.Name));
                        radio += "</label>";
                    }
                    radio += "</div>";
                    body = body.Replace(string.Format("%{0}%", item.SystemName), radio);
                }
            }

            return body;
        }

        protected void AssignToUserRole(UserAction action, User user)
        {
            if (user.UserRoles.Where(x => x.Id == action.UserRoleId).Count() == 0)
            {
                var userRole = _userService.GetUserRoleById(action.UserRoleId);
                if (userRole != null)
                {
                    userRole.UserId = user.Id;
                    _userService.InsertUserRoleInUser(userRole);
                }
            }
        }

        protected void AssignToUserTag(UserAction action, User user)
        {
            if (user.UserTags.Where(x => x == action.UserTagId).Count() == 0)
            {
                _userTagService.InsertTagToUser(action.UserTagId, user.Id);
            }
        }

        #endregion

        #endregion

        #region Methods

        public virtual void AddToCart(ShoppingCartItem cart, Product product, User user)
        {
            Task.Run(() =>
            {
                var actionType = GetAllUserActionType().Where(x => x.SystemKeyword == UserActionTypeEnum.AddToCart.ToString()).FirstOrDefault();
                if (actionType.Enabled)
                {
                    var datetimeUtcNow = DateTime.UtcNow;
                    var query = from a in _userActionRepository.Table
                                where a.Active == true && a.ActionTypeId == actionType.Id
                                        && datetimeUtcNow >= a.StartDateTimeUtc && datetimeUtcNow <= a.EndDateTimeUtc
                                select a;

                    foreach (var item in query.ToList())
                    {
                        if (!UsedAction(item.Id, cart.UserId))
                        {
                            if (Condition(item, product, cart.AttributesXml, user, null, null))
                            {
                                Reaction(item, user, cart, null);
                            }
                        }
                    }
                }
            });
        }

        public virtual void AddOrder(Order order, User user)
        {
            Task.Run(() =>
            {
                var actionType = GetAllUserActionType().Where(x => x.SystemKeyword == UserActionTypeEnum.AddOrder.ToString()).FirstOrDefault();
                if (actionType.Enabled)
                {
                    var datetimeUtcNow = DateTime.UtcNow;
                    var query = from a in _userActionRepository.Table
                                where a.Active == true && a.ActionTypeId == actionType.Id
                                        && datetimeUtcNow >= a.StartDateTimeUtc && datetimeUtcNow <= a.EndDateTimeUtc
                                select a;

                    foreach (var item in query.ToList())
                    {
                        Task.Run(() =>
                        {
                            if (!UsedAction(item.Id, order.UserId))
                            {
                                foreach (var orderItem in order.OrderItems)
                                {
                                    var product = _productService.GetProductById(orderItem.ProductId);
                                    if (Condition(item, product, orderItem.AttributesXml, user, null, null))
                                    {
                                        Reaction(item, user, null, order);
                                        break;
                                    }
                                }
                            }
                        });
                    }

                }
            });
        }

        public virtual void Url(User user, string currentUrl, string previousUrl)
        {
            if (!user.IsSystemAccount)
            {
                var actionType = GetAllUserActionType().Where(x => x.SystemKeyword == UserActionTypeEnum.Url.ToString()).FirstOrDefault();
                if (actionType.Enabled)
                {
                    var datetimeUtcNow = DateTime.UtcNow;
                    var query = from a in _userActionRepository.Table
                                where a.Active == true && a.ActionTypeId == actionType.Id
                                        && datetimeUtcNow >= a.StartDateTimeUtc && datetimeUtcNow <= a.EndDateTimeUtc
                                select a;

                    foreach (var item in query.ToList())
                    {
                        if (!UsedAction(item.Id, user.Id))
                        {
                            if (Condition(item, null, null, user, currentUrl, previousUrl))
                            {
                                Reaction(item, user, null, null);
                            }
                        }
                    }
                }
            }
        }

        public virtual void Viewed(User user, string currentUrl, string previousUrl)
        {
            Task.Run(() =>
            {
                if (!user.IsSystemAccount)
                {
                    var actionType = GetAllUserActionType().Where(x => x.SystemKeyword == UserActionTypeEnum.Viewed.ToString()).FirstOrDefault();
                    if (actionType.Enabled)
                    {
                        var datetimeUtcNow = DateTime.UtcNow;
                        var query = from a in _userActionRepository.Table
                                    where a.Active == true && a.ActionTypeId == actionType.Id
                                            && datetimeUtcNow >= a.StartDateTimeUtc && datetimeUtcNow <= a.EndDateTimeUtc
                                    select a;

                        foreach (var item in query.ToList())
                        {
                            if (!UsedAction(item.Id, user.Id))
                            {
                                if (Condition(item, null, null, user, currentUrl, previousUrl))
                                {
                                    Reaction(item, user, null, null);
                                }
                            }
                        }

                    }
                }
            });
        }
        public virtual void Registration(User user)
        {
            Task.Run(() =>
            {
                var actionType = GetAllUserActionType().Where(x => x.SystemKeyword == UserActionTypeEnum.Registration.ToString()).FirstOrDefault();
                if (actionType.Enabled)
                {
                    var datetimeUtcNow = DateTime.UtcNow;
                    var query = from a in _userActionRepository.Table
                                where a.Active == true && a.ActionTypeId == actionType.Id
                                        && datetimeUtcNow >= a.StartDateTimeUtc && datetimeUtcNow <= a.EndDateTimeUtc
                                select a;

                    foreach (var item in query.ToList())
                    {
                        if (!UsedAction(item.Id, user.Id))
                        {
                            if (Condition(item, null, null, user, null, null))
                            {
                                Reaction(item, user, null, null);
                            }
                        }
                    }

                }
            });
        }
        #endregion
    }
}
