﻿using Zajel.Core.Data;
using Zajel.Core.Domain.Users;
using Zajel.Services.Events;
using System;
using System.Linq;
using System.Collections.Generic;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Zajel.Services.Messages;
using Zajel.Core.Domain.Messages;
using Zajel.Core;
using Zajel.Services.Stores;
using Zajel.Services.Catalog;
using Zajel.Services.Logging;
using Zajel.Services.Localization;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Payments;
using Zajel.Services.Helpers;

namespace Zajel.Services.Users
{
    public partial class UserReminderService : IUserReminderService
    {
        #region Fields

        private readonly IRepository<UserReminder> _userReminderRepository;
        private readonly IRepository<UserReminderHistory> _userReminderHistoryRepository;
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Order> _orderRepository;
        private readonly UserSettings _userSettings;
        private readonly IEventPublisher _eventPublisher;
        private readonly ITokenizer _tokenizer;
        private readonly IEmailAccountService _emailAccountService;
        private readonly IQueuedEmailService _queuedEmailService;
        private readonly IMessageTokenProvider _messageTokenProvider;
        private readonly IStoreService _storeService;
        private readonly IUserAttributeParser _userAttributeParser;
        private readonly IProductService _productService;
        private readonly IUserActivityService _userActivityService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public UserReminderService(
            IRepository<UserReminder> userReminderRepository,
            IRepository<UserReminderHistory> userReminderHistoryRepository,
            IRepository<User> userRepository,
            IRepository<Order> orderRepository,
            UserSettings userSettings,
            IEventPublisher eventPublisher,
            ITokenizer tokenizer,
            IEmailAccountService emailAccountService,
            IQueuedEmailService queuedEmailService,
            IMessageTokenProvider messageTokenProvider,
            IStoreService storeService,
            IProductService productService,
            IUserAttributeParser userAttributeParser,
            IUserActivityService userActivityService,
            ILocalizationService localizationService)
        {
            this._userReminderRepository = userReminderRepository;
            this._userReminderHistoryRepository = userReminderHistoryRepository;
            this._userRepository = userRepository;
            this._orderRepository = orderRepository;
            this._userSettings = userSettings;
            this._eventPublisher = eventPublisher;
            this._tokenizer = tokenizer;
            this._emailAccountService = emailAccountService;
            this._messageTokenProvider = messageTokenProvider;
            this._queuedEmailService = queuedEmailService;
            this._storeService = storeService;
            this._userAttributeParser = userAttributeParser;
            this._productService = productService;
            this._userActivityService = userActivityService;
            this._localizationService = localizationService;
        }

        #endregion

        #region Utilities

        protected bool SendEmail(User user, UserReminder userReminder, string reminderlevelId)
        {
            var reminderLevel = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId);
            var emailAccount = _emailAccountService.GetEmailAccountById(reminderLevel.EmailAccountId);
            var store = user.ShoppingCartItems.Count > 0 ? _storeService.GetStoreById(user.ShoppingCartItems.FirstOrDefault().StoreId) : _storeService.GetAllStores().FirstOrDefault();

            //retrieve message template data
            var bcc = reminderLevel.BccEmailAddresses;
            var subject = reminderLevel.Subject;
            var body = reminderLevel.Body;

            var tokens = new List<Token>();

            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddUserTokens(tokens, user);
            _messageTokenProvider.AddShoppingCartTokens(tokens, user);
            _messageTokenProvider.AddRecommendedProductsTokens(tokens, user);
            _messageTokenProvider.AddRecentlyViewedProductsTokens(tokens, user);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);
            //limit name length
            var toName = CommonHelper.EnsureMaximumLength(user.GetFullName(), 300);
            var email = new QueuedEmail
            {
                Priority = QueuedEmailPriority.High,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = user.Email,
                ToName = toName,
                ReplyTo = string.Empty,
                ReplyToName = string.Empty,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                AttachmentFilePath = "",
                AttachmentFileName = "",
                AttachedDownloadId = "",
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id,
            };

            _queuedEmailService.InsertQueuedEmail(email);
            //activity log
            _userActivityService.InsertActivity(string.Format("UserReminder.{0}", userReminder.ReminderRule.ToString()), user.Id, _localizationService.GetResource(string.Format("ActivityLog.{0}", userReminder.ReminderRule.ToString())), user, userReminder.Name);

            return true;
        }
        protected bool SendEmail(User user, Order order, UserReminder userReminder, string reminderlevelId)
        {

            var reminderLevel = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId);
            var emailAccount = _emailAccountService.GetEmailAccountById(reminderLevel.EmailAccountId);
            var store = user.ShoppingCartItems.Count > 0 ? _storeService.GetStoreById(user.ShoppingCartItems.FirstOrDefault().StoreId) : _storeService.GetAllStores().FirstOrDefault();

            //retrieve message template data
            var bcc = reminderLevel.BccEmailAddresses;
            var subject = reminderLevel.Subject;
            var body = reminderLevel.Body;

            var tokens = new List<Token>();

            _messageTokenProvider.AddStoreTokens(tokens, store, emailAccount);
            _messageTokenProvider.AddUserTokens(tokens, user);
            _messageTokenProvider.AddShoppingCartTokens(tokens, user);
            _messageTokenProvider.AddRecommendedProductsTokens(tokens, user);
            _messageTokenProvider.AddOrderTokens(tokens, order, order.UserLanguageId);

            //Replace subject and body tokens 
            var subjectReplaced = _tokenizer.Replace(subject, tokens, false);
            var bodyReplaced = _tokenizer.Replace(body, tokens, true);
            //limit name length
            var toName = CommonHelper.EnsureMaximumLength(user.GetFullName(), 300);
            var email = new QueuedEmail
            {
                Priority = QueuedEmailPriority.High,
                From = emailAccount.Email,
                FromName = emailAccount.DisplayName,
                To = user.Email,
                ToName = toName,
                ReplyTo = string.Empty,
                ReplyToName = string.Empty,
                CC = string.Empty,
                Bcc = bcc,
                Subject = subjectReplaced,
                Body = bodyReplaced,
                AttachmentFilePath = "",
                AttachmentFileName = "",
                AttachedDownloadId = "",
                CreatedOnUtc = DateTime.UtcNow,
                EmailAccountId = emailAccount.Id,
            };

            _queuedEmailService.InsertQueuedEmail(email);
            //activity log
            _userActivityService.InsertActivity(string.Format("UserReminder.{0}", userReminder.ReminderRule.ToString()), user.Id, _localizationService.GetResource(string.Format("ActivityLog.{0}", userReminder.ReminderRule.ToString())), user, userReminder.Name);

            return true;
        }


        #region Conditions
        protected bool CheckConditions(UserReminder userReminder, User user)
        {
            if (userReminder.Conditions.Count == 0)
                return true;


            bool cond = false;
            foreach (var item in userReminder.Conditions)
            {
                if (item.ConditionType == UserReminderConditionTypeEnum.Category)
                {
                    cond = ConditionCategory(item, user.ShoppingCartItems.Where(x => x.ShoppingCartType == Core.Domain.Orders.ShoppingCartType.ShoppingCart).Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.Product)
                {
                    cond = ConditionProducts(item, user.ShoppingCartItems.Where(x => x.ShoppingCartType == Core.Domain.Orders.ShoppingCartType.ShoppingCart).Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.Manufacturer)
                {
                    cond = ConditionManufacturer(item, user.ShoppingCartItems.Where(x => x.ShoppingCartType == Core.Domain.Orders.ShoppingCartType.ShoppingCart).Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserTag)
                {
                    cond = ConditionUserTag(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserRole)
                {
                    cond = ConditionUserRole(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserRegisterField)
                {
                    cond = ConditionUserRegister(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.CustomUserAttribute)
                {
                    cond = ConditionUserAttribute(item, user);
                }
            }

            return cond;
        }
        protected bool CheckConditions(UserReminder userReminder, User user, Order order)
        {
            if (userReminder.Conditions.Count == 0)
                return true;


            bool cond = false;
            foreach (var item in userReminder.Conditions)
            {
                if (item.ConditionType == UserReminderConditionTypeEnum.Category)
                {
                    cond = ConditionCategory(item, order.OrderItems.Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.Product)
                {
                    cond = ConditionProducts(item, order.OrderItems.Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.Manufacturer)
                {
                    cond = ConditionManufacturer(item, order.OrderItems.Select(x => x.ProductId).ToList());
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserTag)
                {
                    cond = ConditionUserTag(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserRole)
                {
                    cond = ConditionUserRole(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.UserRegisterField)
                {
                    cond = ConditionUserRegister(item, user);
                }
                if (item.ConditionType == UserReminderConditionTypeEnum.CustomUserAttribute)
                {
                    cond = ConditionUserAttribute(item, user);
                }
            }

            return cond;
        }
        protected bool ConditionCategory(UserReminder.ReminderCondition condition, ICollection<string> products)
        {
            bool cond = false;
            if (condition.Condition == UserReminderConditionEnum.AllOfThem)
            {
                cond = true;
                foreach (var item in condition.Categories)
                {
                    foreach (var product in products)
                    {
                        var pr = _productService.GetProductById(product);
                        if (pr != null)
                        {
                            if (pr.ProductCategories.Where(x => x.CategoryId == item).Count() == 0)
                                return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            if (condition.Condition == UserReminderConditionEnum.OneOfThem)
            {
                foreach (var item in condition.Categories)
                {
                    foreach (var product in products)
                    {
                        var pr = _productService.GetProductById(product);
                        if (pr != null)
                        {
                            if (pr.ProductCategories.Where(x => x.CategoryId == item).Count() > 0)
                                return true;
                        }
                    }
                }
            }

            return cond;
        }
        protected bool ConditionManufacturer(UserReminder.ReminderCondition condition, ICollection<string> products)
        {
            bool cond = false;
            if (condition.Condition == UserReminderConditionEnum.AllOfThem)
            {
                cond = true;
                foreach (var item in condition.Manufacturers)
                {
                    foreach (var product in products)
                    {
                        var pr = _productService.GetProductById(product);
                        if (pr != null)
                        {
                            if (pr.ProductManufacturers.Where(x => x.ManufacturerId == item).Count() == 0)
                                return false;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            if (condition.Condition == UserReminderConditionEnum.OneOfThem)
            {
                foreach (var item in condition.Manufacturers)
                {
                    foreach (var product in products)
                    {
                        var pr = _productService.GetProductById(product);
                        if (pr != null)
                        {
                            if (pr.ProductManufacturers.Where(x => x.ManufacturerId == item).Count() > 0)
                                return true;
                        }
                    }
                }
            }

            return cond;
        }
        protected bool ConditionProducts(UserReminder.ReminderCondition condition, ICollection<string> products)
        {
            bool cond = true;
            if (condition.Condition == UserReminderConditionEnum.AllOfThem)
            {
                cond = products.ContainsAll(condition.Products);
            }
            if (condition.Condition == UserReminderConditionEnum.OneOfThem)
            {
                cond = products.ContainsAny(condition.Products);
            }

            return cond;
        }
        protected bool ConditionUserRole(UserReminder.ReminderCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var userRoles = user.UserRoles;
                if (condition.Condition == UserReminderConditionEnum.AllOfThem)
                {
                    cond = userRoles.Select(x => x.Id).ContainsAll(condition.UserRoles);
                }
                if (condition.Condition == UserReminderConditionEnum.OneOfThem)
                {
                    cond = userRoles.Select(x => x.Id).ContainsAny(condition.UserRoles);
                }
            }
            return cond;
        }
        protected bool ConditionUserTag(UserReminder.ReminderCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                var userTags = user.UserTags;
                if (condition.Condition == UserReminderConditionEnum.AllOfThem)
                {
                    cond = userTags.Select(x => x).ContainsAll(condition.UserTags);
                }
                if (condition.Condition == UserReminderConditionEnum.OneOfThem)
                {
                    cond = userTags.Select(x => x).ContainsAny(condition.UserTags);
                }
            }
            return cond;
        }
        protected bool ConditionUserRegister(UserReminder.ReminderCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                if (condition.Condition == UserReminderConditionEnum.AllOfThem)
                {
                    cond = true;
                    foreach (var item in condition.UserRegistration)
                    {
                        if (user.GenericAttributes.Where(x => x.Key == item.RegisterField && x.Value == item.RegisterValue).Count() == 0)
                            cond = false;
                    }
                }
                if (condition.Condition == UserReminderConditionEnum.OneOfThem)
                {
                    foreach (var item in condition.UserRegistration)
                    {
                        if (user.GenericAttributes.Where(x => x.Key == item.RegisterField && x.Value == item.RegisterValue).Count() > 0)
                            cond = true;
                    }
                }
            }
            return cond;
        }
        protected bool ConditionUserAttribute(UserReminder.ReminderCondition condition, User user)
        {
            bool cond = false;
            if (user != null)
            {
                if (condition.Condition == UserReminderConditionEnum.AllOfThem)
                {
                    var customUserAttributes = user.GenericAttributes.FirstOrDefault(x => x.Key == "CustomUserAttributes");
                    if (customUserAttributes != null)
                    {
                        if (!String.IsNullOrEmpty(customUserAttributes.Value))
                        {
                            var selectedValues = _userAttributeParser.ParseUserAttributeValues(customUserAttributes.Value);
                            cond = true;
                            foreach (var item in condition.CustomUserAttributes)
                            {
                                var _fields = item.RegisterField.Split(':');
                                if (_fields.Count() > 1)
                                {
                                    if (selectedValues.Where(x => x.UserAttributeId == _fields.FirstOrDefault() && x.Id == _fields.LastOrDefault()).Count() == 0)
                                        cond = false;
                                }
                                else
                                    cond = false;
                            }
                        }
                    }
                }
                if (condition.Condition == UserReminderConditionEnum.OneOfThem)
                {

                    var customUserAttributes = user.GenericAttributes.FirstOrDefault(x => x.Key == "CustomUserAttributes");
                    if (customUserAttributes != null)
                    {
                        if (!String.IsNullOrEmpty(customUserAttributes.Value))
                        {
                            var selectedValues = _userAttributeParser.ParseUserAttributeValues(customUserAttributes.Value);
                            foreach (var item in condition.CustomUserAttributes)
                            {
                                var _fields = item.RegisterField.Split(':');
                                if (_fields.Count() > 1)
                                {
                                    if (selectedValues.Where(x => x.UserAttributeId == _fields.FirstOrDefault() && x.Id == _fields.LastOrDefault()).Count() > 0)
                                        cond = true;
                                }
                            }
                        }
                    }
                }
            }
            return cond;
        }
        #endregion

        #region History

        protected void UpdateHistory(User user, UserReminder userReminder, string reminderlevelId, UserReminderHistory history)
        {
            if (history != null)
            {
                history.Levels.Add(new UserReminderHistory.HistoryLevel()
                {
                    Level = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level,
                    ReminderLevelId = reminderlevelId,
                    SendDate = DateTime.UtcNow,
                });
                if (userReminder.Levels.Max(x => x.Level) ==
                    userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level)
                {
                    history.Status = (int)UserReminderHistoryStatusEnum.CompletedReminder;
                    history.EndDate = DateTime.UtcNow;
                }
                _userReminderHistoryRepository.Update(history);
            }
            else
            {
                history = new UserReminderHistory();
                history.UserId = user.Id;
                history.Status = (int)UserReminderHistoryStatusEnum.Started;
                history.StartDate = DateTime.UtcNow;
                history.UserReminderId = userReminder.Id;
                history.ReminderRuleId = userReminder.ReminderRuleId;
                history.Levels.Add(new UserReminderHistory.HistoryLevel()
                {
                    Level = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level,
                    ReminderLevelId = reminderlevelId,
                    SendDate = DateTime.UtcNow,
                });

                _userReminderHistoryRepository.Insert(history);
            }

        }

        protected void UpdateHistory(Order order, UserReminder userReminder, string reminderlevelId, UserReminderHistory history)
        {
            if (history != null)
            {
                history.Levels.Add(new UserReminderHistory.HistoryLevel()
                {
                    Level = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level,
                    ReminderLevelId = reminderlevelId,
                    SendDate = DateTime.UtcNow,
                });
                if (userReminder.Levels.Max(x => x.Level) ==
                    userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level)
                {
                    history.Status = (int)UserReminderHistoryStatusEnum.CompletedReminder;
                    history.EndDate = DateTime.UtcNow;
                }
                _userReminderHistoryRepository.Update(history);
            }
            else
            {
                history = new UserReminderHistory();
                history.BaseOrderId = order.Id;
                history.UserId = order.UserId;
                history.Status = (int)UserReminderHistoryStatusEnum.Started;
                history.StartDate = DateTime.UtcNow;
                history.UserReminderId = userReminder.Id;
                history.ReminderRuleId = userReminder.ReminderRuleId;
                history.Levels.Add(new UserReminderHistory.HistoryLevel()
                {
                    Level = userReminder.Levels.FirstOrDefault(x => x.Id == reminderlevelId).Level,
                    ReminderLevelId = reminderlevelId,
                    SendDate = DateTime.UtcNow,
                });

                _userReminderHistoryRepository.Insert(history);
            }

        }
        protected void CloseHistoryReminder(UserReminder userReminder, UserReminderHistory history)
        {
            history.Status = (int)UserReminderHistoryStatusEnum.CompletedReminder;
            history.EndDate = DateTime.UtcNow;
            _userReminderHistoryRepository.Update(history);
        }

        #endregion

        #endregion

        #region Methods

        /// <summary>
        /// Gets user reminder
        /// </summary>
        /// <param name="id">User reminder identifier</param>
        /// <returns>User reminder</returns>
        public virtual UserReminder GetUserReminderById(string id)
        {
            return _userReminderRepository.GetById(id);
        }


        /// <summary>
        /// Gets all user reminders
        /// </summary>
        /// <returns>User reminders</returns>
        public virtual IList<UserReminder> GetUserReminders()
        {
            var query = from p in _userReminderRepository.Table
                        orderby p.DisplayOrder
                        select p;
            return query.ToList();
        }

        /// <summary>
        /// Inserts a user reminder
        /// </summary>
        /// <param name="UserReminder">User reminder</param>
        public virtual void InsertUserReminder(UserReminder userReminder)
        {
            if (userReminder == null)
                throw new ArgumentNullException("userReminder");

            _userReminderRepository.Insert(userReminder);

            //event notification
            _eventPublisher.EntityInserted(userReminder);

        }

        /// <summary>
        /// Delete a user reminder
        /// </summary>
        /// <param name="userReminder">User reminder</param>
        public virtual void DeleteUserReminder(UserReminder userReminder)
        {
            if (userReminder == null)
                throw new ArgumentNullException("userReminder");

            _userReminderRepository.Delete(userReminder);

            //event notification
            _eventPublisher.EntityDeleted(userReminder);

        }

        /// <summary>
        /// Updates the user reminder
        /// </summary>
        /// <param name="UserReminder">User reminder</param>
        public virtual void UpdateUserReminder(UserReminder userReminder)
        {
            if (userReminder == null)
                throw new ArgumentNullException("userReminder");

            _userReminderRepository.Update(userReminder);

            //event notification
            _eventPublisher.EntityUpdated(userReminder);
        }



        public IPagedList<SerializeUserReminderHistory> GetAllUserReminderHistory(string userReminderId, int pageIndex = 0, int pageSize = 2147483647)
        {
            var query = from h in _userReminderHistoryRepository.Table
                        from l in h.Levels
                        select new SerializeUserReminderHistory()
                        { UserId = h.UserId, Id = h.Id, UserReminderId = h.UserReminderId, Level = l.Level, SendDate = l.SendDate, OrderId = h.OrderId };

            query = from p in query
                    where p.UserReminderId == userReminderId
                    select p;

            var history = new PagedList<SerializeUserReminderHistory>(query, pageIndex, pageSize);
            return history;
        }

        #endregion

        #region Tasks

        public virtual void Task_AbandonedCart(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.AbandonedCart
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.AbandonedCart
                                    select cr).ToList();
            }

            foreach (var reminder in userReminder)
            {
                var users = from cu in _userRepository.Table
                                where cu.HasShoppingCartItems && cu.LastUpdateCartDateUtc > reminder.LastUpdateDate
                                && (!String.IsNullOrEmpty(cu.Email))
                                select cu;

                foreach (var user in users)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                    where hc.UserId == user.Id && hc.UserReminderId == reminder.Id
                                    select hc).ToList();
                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {

                                    if (DateTime.UtcNow > user.LastUpdateCartDateUtc.Value.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                                    {
                                        if (CheckConditions(reminder, user))
                                        {
                                            var send = SendEmail(user, reminder, level.Id);
                                            if (send)
                                                UpdateHistory(user, reminder, level.Id, null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {

                            if (DateTime.UtcNow > user.LastUpdateCartDateUtc.Value.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                            {
                                if (CheckConditions(reminder, user))
                                {
                                    var send = SendEmail(user, reminder, level.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, level.Id, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void Task_RegisteredUser(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.RegisteredUser
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.RegisteredUser
                                    select cr).ToList();
            }
            foreach (var reminder in userReminder)
            {
                var users = from cu in _userRepository.Table
                                where cu.CreatedOnUtc > reminder.LastUpdateDate
                                && (!String.IsNullOrEmpty(cu.Email))
                                && !cu.IsSystemAccount
                                select cu;

                foreach (var user in users)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                    where hc.UserId == user.Id && hc.UserReminderId == reminder.Id
                                    select hc).ToList();
                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {

                                    if (DateTime.UtcNow > user.CreatedOnUtc.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                                    {
                                        if (CheckConditions(reminder, user))
                                        {
                                            var send = SendEmail(user, reminder, level.Id);
                                            if (send)
                                                UpdateHistory(user, reminder, level.Id, null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {

                            if (DateTime.UtcNow > user.CreatedOnUtc.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                            {
                                if (CheckConditions(reminder, user))
                                {
                                    var send = SendEmail(user, reminder, level.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, level.Id, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void Task_LastActivity(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.LastActivity
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.LastActivity
                                    select cr).ToList();
            }
            foreach (var reminder in userReminder)
            {
                var users = from cu in _userRepository.Table
                                where cu.LastActivityDateUtc < reminder.LastUpdateDate
                                && (!String.IsNullOrEmpty(cu.Email))
                                select cu;

                foreach (var user in users)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                    where hc.UserId == user.Id && hc.UserReminderId == reminder.Id
                                    select hc).ToList();
                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {

                                    if (DateTime.UtcNow > user.LastActivityDateUtc.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                                    {
                                        if (CheckConditions(reminder, user))
                                        {
                                            var send = SendEmail(user, reminder, level.Id);
                                            if (send)
                                                UpdateHistory(user, reminder, level.Id, null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {
                            if (DateTime.UtcNow > user.LastActivityDateUtc.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes))
                            {
                                if (CheckConditions(reminder, user))
                                {
                                    var send = SendEmail(user, reminder, level.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, level.Id, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void Task_LastPurchase(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.LastPurchase
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.LastPurchase
                                    select cr).ToList();
            }
            foreach (var reminder in userReminder)
            {
                var users = from cu in _userRepository.Table
                                where cu.LastPurchaseDateUtc < reminder.LastUpdateDate || cu.LastPurchaseDateUtc == null
                                && (!String.IsNullOrEmpty(cu.Email))
                                && !cu.IsSystemAccount
                                select cu;

                foreach (var user in users)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                    where hc.UserId == user.Id && hc.UserReminderId == reminder.Id
                                    select hc).ToList();
                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {
                                    DateTime lastpurchaseDate = user.LastPurchaseDateUtc.HasValue ? user.LastPurchaseDateUtc.Value.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes) : DateTime.MinValue;
                                    if (DateTime.UtcNow > lastpurchaseDate)
                                    {
                                        if (CheckConditions(reminder, user))
                                        {
                                            var send = SendEmail(user, reminder, level.Id);
                                            if (send)
                                                UpdateHistory(user, reminder, level.Id, null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {
                            DateTime lastpurchaseDate = user.LastPurchaseDateUtc.HasValue ? user.LastPurchaseDateUtc.Value.AddDays(level.Day).AddHours(level.Hour).AddMinutes(level.Minutes) : DateTime.MinValue;
                            if (DateTime.UtcNow > lastpurchaseDate)
                            {
                                if (CheckConditions(reminder, user))
                                {
                                    var send = SendEmail(user, reminder, level.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, level.Id, null);
                                }
                            }
                        }
                    }
                }
            }
        }

        public virtual void Task_Birthday(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.Birthday
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.Birthday
                                    select cr).ToList();
            }

            foreach (var reminder in userReminder)
            {
                int day = 0;
                if (reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null)
                    day = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault().Day;

                string dateDDMM = DateTime.Now.AddDays(-day).ToString("-MM-dd");

                var users = from cu in _userRepository.Table
                                where (!String.IsNullOrEmpty(cu.Email))
                                && cu.GenericAttributes.Any(x => x.Key == "DateOfBirth" && x.Value.Contains(dateDDMM))
                                select cu;

                foreach (var user in users)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                    where hc.UserId == user.Id && hc.UserReminderId == reminder.Id
                                    select hc).ToList();
                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {
                                    if (CheckConditions(reminder, user))
                                    {
                                        var send = SendEmail(user, reminder, level.Id);
                                        if (send)
                                            UpdateHistory(user, reminder, level.Id, null);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {
                            if (CheckConditions(reminder, user))
                            {
                                var send = SendEmail(user, reminder, level.Id);
                                if (send)
                                    UpdateHistory(user, reminder, level.Id, null);
                            }
                        }
                    }
                }

                var activehistory = (from hc in _userReminderHistoryRepository.Table
                                        where hc.UserReminderId == reminder.Id && hc.Status == (int)UserReminderHistoryStatusEnum.Started
                                        select hc).ToList();

                foreach (var activereminderhistory in activehistory)
                {
                    var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                    var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                    var user = _userRepository.Table.FirstOrDefault(x => x.Id == activereminderhistory.UserId);
                    if (reminderLevel != null && user != null)
                    {
                        if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                        {
                            var send = SendEmail(user, reminder, reminderLevel.Id);
                            if (send)
                                UpdateHistory(user, reminder, reminderLevel.Id, activereminderhistory);
                        }
                    }
                    else
                    {
                        CloseHistoryReminder(reminder, activereminderhistory);
                    }
                }
            }

        }

        public virtual void Task_CompletedOrder(string id = "")
        {
            var dateNow = DateTime.UtcNow.Date;
            var datetimeUtcNow = DateTime.UtcNow;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.CompletedOrder
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.CompletedOrder
                                    select cr).ToList();
            }

            foreach (var reminder in userReminder)
            {
                int day = 0;
                if (reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null)
                    day = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault().Day;

                var orders = from or in _orderRepository.Table
                             where or.OrderStatusId == (int)OrderStatus.Complete
                             && or.CreatedOnUtc >= reminder.LastUpdateDate && or.CreatedOnUtc >= dateNow.AddDays(-day)
                             select or;

                foreach (var order in orders)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                   where hc.BaseOrderId == order.Id && hc.UserReminderId == reminder.Id
                                   select hc).ToList();

                    User user = _userRepository.Table.FirstOrDefault(x => x.Id == order.UserId);

                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, order, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(order, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {
                                    if (CheckConditions(reminder, user, order))
                                    {
                                        var send = SendEmail(user, order, reminder, level.Id);
                                        if (send)
                                            UpdateHistory(order, reminder, level.Id, null);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {
                            if (CheckConditions(reminder, user, order))
                            {
                                var send = SendEmail(user, order, reminder, level.Id);
                                if (send)
                                    UpdateHistory(order, reminder, level.Id, null);
                            }
                        }
                    }
                }

                var activehistory = (from hc in _userReminderHistoryRepository.Table
                                     where hc.UserReminderId == reminder.Id && hc.Status == (int)UserReminderHistoryStatusEnum.Started
                                     select hc).ToList();

                foreach (var activereminderhistory in activehistory)
                {
                    var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                    var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                    var order = _orderRepository.Table.FirstOrDefault(x => x.Id == activereminderhistory.BaseOrderId);
                    if (reminderLevel != null && order != null)
                    {
                        var user = _userRepository.Table.FirstOrDefault(x => x.Id == order.UserId);
                        if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                        {
                            var send = SendEmail(user, order, reminder, reminderLevel.Id);
                            if (send)
                                UpdateHistory(order, reminder, reminderLevel.Id, activereminderhistory);
                        }
                    }
                    else
                    {
                        CloseHistoryReminder(reminder, activereminderhistory);
                    }
                }

            }

        }
        public virtual void Task_UnpaidOrder(string id = "")
        {
            var datetimeUtcNow = DateTime.UtcNow;
            var dateNow = DateTime.UtcNow.Date;
            var userReminder = new List<UserReminder>();
            if (String.IsNullOrEmpty(id))
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Active && datetimeUtcNow >= cr.StartDateTimeUtc && datetimeUtcNow <= cr.EndDateTimeUtc
                                    && cr.ReminderRuleId == (int)UserReminderRuleEnum.UnpaidOrder
                                    select cr).ToList();
            }
            else
            {
                userReminder = (from cr in _userReminderRepository.Table
                                    where cr.Id == id && cr.ReminderRuleId == (int)UserReminderRuleEnum.UnpaidOrder
                                    select cr).ToList();
            }

            foreach (var reminder in userReminder)
            {
                int day = 0;
                if (reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null)
                    day = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault().Day;

                var orders = from or in _orderRepository.Table
                             where or.PaymentStatusId == (int)PaymentStatus.Pending
                             && or.CreatedOnUtc >= reminder.LastUpdateDate && or.CreatedOnUtc >= dateNow.AddDays(-day)
                             select or;

                foreach (var order in orders)
                {
                    var history = (from hc in _userReminderHistoryRepository.Table
                                   where hc.BaseOrderId == order.Id && hc.UserReminderId == reminder.Id
                                   select hc).ToList();

                    User user = _userRepository.Table.FirstOrDefault(x => x.Id == order.UserId);

                    if (history.Any())
                    {
                        var activereminderhistory = history.FirstOrDefault(x => x.HistoryStatus == UserReminderHistoryStatusEnum.Started);
                        if (activereminderhistory != null)
                        {
                            var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                            var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                            if (reminderLevel != null)
                            {
                                if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                                {
                                    var send = SendEmail(user, order, reminder, reminderLevel.Id);
                                    if (send)
                                        UpdateHistory(order, reminder, reminderLevel.Id, activereminderhistory);
                                }
                            }
                            else
                            {
                                CloseHistoryReminder(reminder, activereminderhistory);
                            }
                        }
                        else
                        {
                            if (DateTime.UtcNow > history.Max(x => x.EndDate).AddDays(reminder.RenewedDay) && reminder.AllowRenew)
                            {
                                var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                                if (level != null)
                                {
                                    if (CheckConditions(reminder, user, order))
                                    {
                                        var send = SendEmail(user, order, reminder, level.Id);
                                        if (send)
                                            UpdateHistory(order, reminder, level.Id, null);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        var level = reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() != null ? reminder.Levels.OrderBy(x => x.Level).FirstOrDefault() : null;
                        if (level != null)
                        {
                            if (CheckConditions(reminder, user, order))
                            {
                                var send = SendEmail(user, order, reminder, level.Id);
                                if (send)
                                    UpdateHistory(order, reminder, level.Id, null);
                            }
                        }
                    }
                }
                var activehistory = (from hc in _userReminderHistoryRepository.Table
                                     where hc.UserReminderId == reminder.Id && hc.Status == (int)UserReminderHistoryStatusEnum.Started
                                     select hc).ToList();

                foreach (var activereminderhistory in activehistory)
                {
                    var lastLevel = activereminderhistory.Levels.OrderBy(x => x.SendDate).LastOrDefault();
                    var reminderLevel = reminder.Levels.FirstOrDefault(x => x.Level > lastLevel.Level);
                    var order = _orderRepository.Table.FirstOrDefault(x => x.Id == activereminderhistory.BaseOrderId);
                    if (reminderLevel != null && order != null)
                    {
                        if (order.PaymentStatusId == (int)PaymentStatus.Pending)
                        {
                            var user = _userRepository.Table.FirstOrDefault(x => x.Id == order.UserId);
                            if (DateTime.UtcNow > lastLevel.SendDate.AddDays(reminderLevel.Day).AddHours(reminderLevel.Hour).AddMinutes(reminderLevel.Minutes))
                            {
                                var send = SendEmail(user, order, reminder, reminderLevel.Id);
                                if (send)
                                    UpdateHistory(order, reminder, reminderLevel.Id, activereminderhistory);
                            }
                        }
                        else
                            CloseHistoryReminder(reminder, activereminderhistory);

                    }
                    else
                    {
                        CloseHistoryReminder(reminder, activereminderhistory);
                    }
                }
            }
        }

        #endregion
    }

    public class SerializeUserReminderHistory
    {
        public string Id { get; set; }
        public string UserReminderId { get; set; }
        public string UserId { get; set; }
        public DateTime SendDate { get; set; }
        public int Level { get; set; }
        public string OrderId { get; set; }
    }
}
