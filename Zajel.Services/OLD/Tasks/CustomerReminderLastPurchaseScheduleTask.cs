﻿using Zajel.Services.Users;
using Zajel.Core.Domain.Tasks;

namespace Zajel.Services.Tasks
{
    public partial class UserReminderLastPurchaseScheduleTask : ScheduleTask, IScheduleTask
    {
        private readonly IUserReminderService _userReminderService;

        public UserReminderLastPurchaseScheduleTask(IUserReminderService userReminderService)
        {
            this._userReminderService = userReminderService;
        }

        public void Execute()
        {
            _userReminderService.Task_LastPurchase();
        }
    }
}
