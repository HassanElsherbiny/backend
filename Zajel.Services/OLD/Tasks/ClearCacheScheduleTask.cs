﻿using Zajel.Core.Caching;
using Zajel.Core.Domain.Tasks;
using Zajel.Core.Infrastructure;
using Zajel.Services.Logging;

namespace Zajel.Services.Tasks
{
    /// <summary>
    /// Clear cache schedueled task implementation
    /// </summary>
    public partial class ClearCacheScheduleTask : ScheduleTask, IScheduleTask
    {
        /// <summary>
        /// Executes a task
        /// </summary>
        /// 
        public ClearCacheScheduleTask() { }

        public void Execute()
        {
            var cacheManager = EngineContext.Current.Resolve<ICacheManager>();
            cacheManager.Clear();
        }
    }
}
