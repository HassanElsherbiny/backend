﻿using Zajel.Core.Caching;
using Zajel.Core.Domain.Tasks;
using Zajel.Core.Infrastructure;
using Zajel.Services.Logging;
using Zajel.Services.Tasks;

namespace Zajel.Services.Tasks
{
    /// <summary>
    /// Represents a task to clear [Log] table
    /// </summary>
    public partial class ClearLogScheduleTask : ScheduleTask, IScheduleTask
    {
        private readonly ILogger _logger;

        public ClearLogScheduleTask(ILogger logger)
        {
            this._logger = logger;
        }

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            _logger.ClearLog();
        }
    }
}
