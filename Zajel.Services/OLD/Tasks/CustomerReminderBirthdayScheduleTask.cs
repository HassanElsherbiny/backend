﻿using Zajel.Services.Logging;
using Zajel.Services.Users;
using Zajel.Core.Caching;
using Zajel.Core.Infrastructure;
using Zajel.Core.Domain.Tasks;

namespace Zajel.Services.Tasks
{
    public partial class UserReminderBirthdayScheduleTask : ScheduleTask, IScheduleTask
    {
        private readonly IUserReminderService _userReminderService;

        public UserReminderBirthdayScheduleTask(IUserReminderService userReminderService)
        {
            this._userReminderService = userReminderService;
        }

        public void Execute()
        {
            _userReminderService.Task_Birthday();
        }
    }
}
