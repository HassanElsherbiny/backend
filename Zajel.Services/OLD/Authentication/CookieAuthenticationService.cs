﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using Zajel.Core.Domain.Users;
using Zajel.Services.Users;
using Zajel.Services.Authentication;
using Microsoft.AspNetCore.Authentication;
using System.Collections.Generic;

namespace Zajel.Service.Authentication
{
    public partial class CookieAuthenticationService : IZajelAuthenticationService
    {
        #region Fields

        private readonly UserSettings _userSettings;
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        private User _cachedUser;

        #endregion

        #region Ctor

        public CookieAuthenticationService(UserSettings userSettings,
            IUserService userService,
            IHttpContextAccessor httpContextAccessor)
        {
            this._userSettings = userSettings;
            this._userService = userService;
            this._httpContextAccessor = httpContextAccessor;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sign in
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="isPersistent">Whether the authentication session is persisted across multiple requests</param>
        public virtual void SignIn(User user, bool isPersistent)
        {
            var authenticationManager = _httpContextAccessor.HttpContext;
            if (authenticationManager == null)
                return;

            //create claims for username and email of the user
            var claims = new List<Claim>();
            if (!string.IsNullOrEmpty(user.Username))
                claims.Add(new Claim(ClaimTypes.Name, user.Username, ClaimValueTypes.String, ZajelCookieAuthenticationDefaults.ClaimsIssuer));

            if (!string.IsNullOrEmpty(user.Email))
                claims.Add(new Claim(ClaimTypes.Email, user.Email, ClaimValueTypes.Email, ZajelCookieAuthenticationDefaults.ClaimsIssuer));

            //create principal for the current authentication scheme
            var userIdentity = new ClaimsIdentity(claims, ZajelCookieAuthenticationDefaults.AuthenticationScheme);
            var userPrincipal = new ClaimsPrincipal(userIdentity);

            //set value indicating whether session is persisted and the time at which the authentication was issued
            var authenticationProperties = new AuthenticationProperties
            {
                IsPersistent = isPersistent,
                IssuedUtc = DateTime.UtcNow
            };

            //sign in
            var signInTask = authenticationManager.SignInAsync(ZajelCookieAuthenticationDefaults.AuthenticationScheme, userPrincipal, authenticationProperties);
            signInTask.Wait();

            //cache authenticated user
            _cachedUser = user;
        }

        /// <summary>
        /// Sign out
        /// </summary>
        public virtual void SignOut()
        {
            var authenticationManager = _httpContextAccessor.HttpContext;
            if (authenticationManager == null)
                return;

            //reset cached user
            _cachedUser = null;

            //and sign out from the current authentication scheme
            var signOutTask = authenticationManager.SignOutAsync(ZajelCookieAuthenticationDefaults.AuthenticationScheme);
            signOutTask.Wait();
        }

        /// <summary>
        /// Get authenticated user
        /// </summary>
        /// <returns>User</returns>
        public virtual User GetAuthenticatedUser()
        {
            //whether there is a cached user
            if (_cachedUser != null)
                return _cachedUser;

            var authenticationManager = _httpContextAccessor.HttpContext;
            if (authenticationManager == null)
                return null;

            //try to get authenticated user identity
            var authenticateTask = authenticationManager.AuthenticateAsync(ZajelCookieAuthenticationDefaults.AuthenticationScheme);
            var userPrincipal = authenticateTask.Result;
            var userIdentity = userPrincipal?.Principal?.Identities?.FirstOrDefault(identity => identity.IsAuthenticated);
            if (userIdentity == null)
                return null;

            User user = null;
            if (_userSettings.UsernamesEnabled)
            {
                //try to get user by username
                var usernameClaim = userIdentity.FindFirst(claim => claim.Type == ClaimTypes.Name
                    && claim.Issuer.Equals(ZajelCookieAuthenticationDefaults.ClaimsIssuer, StringComparison.OrdinalIgnoreCase));
                if (usernameClaim != null)
                    user = _userService.GetUserByUsername(usernameClaim.Value);
            }
            else
            {
                //try to get user by email
                var emailClaim = userIdentity.FindFirst(claim => claim.Type == ClaimTypes.Email
                    && claim.Issuer.Equals(ZajelCookieAuthenticationDefaults.ClaimsIssuer, StringComparison.OrdinalIgnoreCase));
                if (emailClaim != null)
                    user = _userService.GetUserByEmail(emailClaim.Value);
            }

            //whether the found user is available
            if (user == null || !user.Active || user.Deleted || !user.IsRegistered())
                return null;

            //cache authenticated user
            _cachedUser = user;

            return _cachedUser;
        }

        #endregion
    }
}