﻿using Zajel.Core.Domain.Users;
using System.Collections.Generic;

namespace Zajel.Services.Orders
{
    /// <summary>
    /// RewardPoints service interface
    /// </summary>
    public partial interface IRewardPointsService
    {

        /// <summary>
        /// Gets reward points balance
        /// </summary>
        /// <param name="userId">User identifier</param>
        /// <param name="storeId">Store identifier; pass </param>
        /// <returns>Balance</returns>
        int GetRewardPointsBalance(string userId, string storeId);

        /// <summary>
        /// Add reward points history record
        /// </summary>
        /// <param name="user">User</param>
        /// <param name="points">Number of points to add</param>
        /// <param name="storeId">Store identifier</param>
        /// <param name="message">Message</param>
        /// <param name="usedWithOrder">the order for which points were redeemed as a payment</param>
        /// <param name="usedAmount">Used amount</param>
        RewardPointsHistory AddRewardPointsHistory(string userId, int points, string storeId, string message = "",
           string usedWithOrderId = "", decimal usedAmount = 0M);

        /// <summary>
        /// Load reward point history records
        /// </summary>
        /// <param name="userId">User identifier; 0 to load all records</param>
        /// <param name="showHidden">A value indicating whether to show hidden records (filter by current store if possible)</param>
        /// <returns>Reward point history records</returns>
        IList<RewardPointsHistory> GetRewardPointsHistory(string userId = "", bool showHidden = false);

    }
}
