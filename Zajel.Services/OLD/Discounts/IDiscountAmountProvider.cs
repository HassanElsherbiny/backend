﻿using Zajel.Core.Domain.Discounts;
using Zajel.Core.Plugins;

namespace Zajel.Services.Discounts
{
    public partial interface IDiscountAmountProvider : IPlugin
    {
        decimal DiscountAmount(Discount discount, decimal amount);
    }
}
