﻿using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Stores;

namespace Zajel.Services.Discounts
{
    /// <summary>
    /// Represents a request of discount requirement validation
    /// </summary>
    public partial class DiscountRequirementValidationRequest
    {
        /// <summary>
        /// Gets or sets the appropriate discount requirement ID (identifier)
        /// </summary>
        public string DiscountRequirementId { get; set; }

        /// <summary>
        /// Gets or sets the discount ID (identifier)
        /// </summary>
        public string DiscountId { get; set; }

        /// <summary>
        /// Gets or sets the user
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Gets or sets the store
        /// </summary>
        public Store Store { get; set; }
    }
}
