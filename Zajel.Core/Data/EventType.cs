﻿namespace Zajel.Core.Data
{
    public enum EventType
    {
        Insert,
        Update,
        Delete
    }
}