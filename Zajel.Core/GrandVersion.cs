﻿
namespace Zajel.Core
{
    public static class ZajelVersion
    {
        /// <summary>
        /// Gets or sets the store version
        /// </summary>
        public static string CurrentVersion 
        {
            get
            {
                return "4.00";
            }
        }
    }
}
