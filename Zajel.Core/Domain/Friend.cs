﻿using System;



namespace Zajel.Core.Domain
{
    public class Friend : BaseEntity
    {
        public int FromUserId { get; set; }
        public int ToUserId { get; set; }
        public bool Confirmed { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime? DateConfirmed { get; set; }
        public int NotificationCount { get; set; }
        public DateTime? LastNotificationDate { get; set; }

        public Friend()
        {
            NotificationCount = 0;
        }

        
    }

 


}