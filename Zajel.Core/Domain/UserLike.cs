﻿using System;



namespace Zajel.Core.Domain
{
    public class UserLike : BaseEntity
    {
        public int UserId { get; set; }

        public int EntityId { get; set; }

        public string EntityName { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateUpdated { get; set; }

        
    }
 
}