﻿using System;
using System.Collections.Generic;

namespace Zajel.Core.Domain.Messages
{
    /// <summary>
    /// Represents a campaign
    /// </summary>
    public partial class Campaign : BaseEntity
    {
        private ICollection<string> _userTags;
        private ICollection<string> _userRoles;
        private ICollection<string> _newsletterCategories;

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets the body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets or sets the store identifier  which subscribers it will be sent to; set 0 for all newsletter subscribers
        /// </summary>
        public string StoreId { get; set; }

        public DateTime? UserCreatedDateFrom { get; set; }
        public DateTime? UserCreatedDateTo { get; set; }
        public DateTime? UserLastActivityDateFrom { get; set; }
        public DateTime? UserLastActivityDateTo { get; set; }
        public DateTime? UserLastPurchaseDateFrom { get; set; }
        public DateTime? UserLastPurchaseDateTo { get; set; }

        public int UserHasOrders { get; set; }
        public CampaignCondition UserHasOrdersCondition {
            get { return (CampaignCondition)UserHasOrders; }
            set { this.UserHasOrders = (int)value; }
        }

        public int UserHasShoppingCart { get; set; }
        public CampaignCondition UserHasShoppingCartCondition {
            get { return (CampaignCondition)UserHasShoppingCart; }
            set { this.UserHasShoppingCart = (int)value; }
        }

        /// <summary>
        /// Gets or sets the date and time of instance creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the used email account identifier
        /// </summary>
        public string EmailAccountId { get; set; }
        /// <summary>
        /// Gets or sets the user tags
        /// </summary>
        public virtual ICollection<string> UserTags
        {
            get { return _userTags ?? (_userTags = new List<string>()); }
            protected set { _userTags = value; }
        }
        /// <summary>
        /// Gets or sets the user roles
        /// </summary>
        public virtual ICollection<string> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new List<string>()); }
            protected set { _userRoles = value; }
        }
        /// <summary>
        /// Gets or sets the newsletter categories
        /// </summary>
        public virtual ICollection<string> NewsletterCategories
        {
            get { return _newsletterCategories ?? (_newsletterCategories = new List<string>()); }
            protected set { _newsletterCategories = value; }
        }

    }
}
