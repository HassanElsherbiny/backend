﻿
namespace Zajel.Core.Domain.Common
{
    public partial class ZajelNodeVersion: BaseEntity
    {
        public string DataBaseVersion { get; set; }
    }
}
