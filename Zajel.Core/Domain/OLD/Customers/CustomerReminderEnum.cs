﻿
namespace Zajel.Core.Domain.Users
{
    public enum UserReminderRuleEnum
    {
        AbandonedCart = 1,
        RegisteredUser = 2,
        LastPurchase = 3,
        LastActivity = 4,
        Birthday = 5,
        CompletedOrder = 6,
        UnpaidOrder = 7
    }

    public enum UserReminderConditionTypeEnum
    {
        Product = 1,
        Category = 2,
        Manufacturer = 3,
        UserRole = 4,
        UserTag = 5,
        UserRegisterField = 6,
        CustomUserAttribute = 7,
    }

    public enum UserReminderConditionEnum
    {
        OneOfThem = 0,
        AllOfThem = 1,
    }
    public enum UserReminderHistoryStatusEnum
    {
        Started = 10,
        CompletedReminder = 20,
        CompletedOrdered = 30,
    }


}
