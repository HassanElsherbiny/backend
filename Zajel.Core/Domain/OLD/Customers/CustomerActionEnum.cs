﻿
namespace Zajel.Core.Domain.Users
{
    public enum UserActionTypeEnum
    {
        AddToCart = 1,
        AddOrder = 2,
        Viewed = 3,
        Url = 4,
        Registration = 5,

    }
    public enum UserActionConditionEnum
    {
        OneOfThem = 0,
        AllOfThem = 1,
    }

    public enum UserActionConditionTypeEnum
    {
        Product = 1,
        Category = 2,
        Manufacturer = 3,
        Vendor = 4,
        ProductAttribute = 5,
        ProductSpecification = 6,
        UserRole = 7,
        UserTag = 8,
        UserRegisterField = 9,
        CustomUserAttribute = 10,
        UrlReferrer = 11,
        UrlCurrent = 12,
        Store = 13
    }

    public enum UserReactionTypeEnum
    {
        Banner = 1,
        Email = 2,
        AssignToUserRole = 3,
        AssignToUserTag = 4,
        InteractiveForm = 5,
    }

    
}
