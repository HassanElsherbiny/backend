﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zajel.Core.Domain.Users
{
    public class UserProductPrice: BaseEntity
    {
        public string UserId { get; set; }
        public string ProductId { get; set; }
        public decimal Price { get; set; }
    }
}
