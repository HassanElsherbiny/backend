
namespace Zajel.Core.Domain.Users
{
   
    /// <summary>
    /// Represents a user tag product
    /// </summary>
    public partial class UserTagProduct : BaseEntity
    {

        /// <summary>
        /// Gets or sets the user tag id
        /// </summary>
        public string UserTagId { get; set; }

        /// <summary>
        /// Gets or sets the product Id
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

    }

}