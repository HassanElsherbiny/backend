
namespace Zajel.Core.Domain.Users
{
    public static partial class SystemUserNames
    {
        public static string SearchEngine { get { return "SearchEngine"; } }
        public static string BackgroundTask { get { return "BackgroundTask"; } }
        public static string WebApi { get { return "WebApi"; } }
    }
}