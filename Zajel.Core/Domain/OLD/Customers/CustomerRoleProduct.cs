namespace Zajel.Core.Domain.Users
{
    /// <summary>
    /// Represents a user role products
    /// </summary>
    public partial class UserRoleProduct : BaseEntity
    {

        /// <summary>
        /// Gets or sets the user role id
        /// </summary>
        public string UserRoleId { get; set; }

        /// <summary>
        /// Gets or sets the product Id
        /// </summary>
        public string ProductId { get; set; }

        /// <summary>
        /// Gets or sets the display order
        /// </summary>
        public int DisplayOrder { get; set; }

    }
    

}