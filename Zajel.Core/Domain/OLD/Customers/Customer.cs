using System;
using System.Collections.Generic;
using Zajel.Core.Domain.Common;
using Zajel.Core.Domain.Orders;

namespace Zajel.Core.Domain.Users
{
    /// <summary>
    /// Represents a user
    /// </summary>
    public partial class User : BaseEntity
    {
        private ICollection<UserRole> _userRoles;
        private ICollection<ShoppingCartItem> _shoppingCartItems;
        private ICollection<Address> _addresses;
        private ICollection<string> _userTags;

        /// <summary>
        /// Ctor
        /// </summary>
        public User()
        {
            this.UserGuid = Guid.NewGuid();
            this.PasswordFormat = PasswordFormat.Clear;
        }

        /// <summary>
        /// Gets or sets the user Guid
        /// </summary>
        public Guid UserGuid { get; set; }

        /// <summary>
        /// Gets or sets the username
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Gets or sets the email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Gets or sets the password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public int PasswordFormatId { get; set; }
        /// <summary>
        /// Gets or sets the password format
        /// </summary>
        public PasswordFormat PasswordFormat
        {
            get { return (PasswordFormat)PasswordFormatId; }
            set { this.PasswordFormatId = (int)value; }
        }
        /// <summary>
        /// Gets or sets the password salt
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Gets or sets the admin comment
        /// </summary>
        public string AdminComment { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is tax exempt
        /// </summary>
        public bool IsTaxExempt { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a free shipping to the next a order
        /// </summary>
        public bool FreeShipping { get; set; }

        /// <summary>
        /// Gets or sets the affiliate identifier
        /// </summary>
        public string AffiliateId { get; set; }

        /// <summary>
        /// Gets or sets the vendor identifier with which this user is associated (maganer)
        /// </summary>
        public string VendorId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this user has some products in the shopping cart
        /// <remarks>The same as if we run this.ShoppingCartItems.Count > 0
        /// We use this property for performance optimization:
        /// if this property is set to false, then we do not need to load "ShoppingCartItems" navigation property for each page load
        /// It's used only in a couple of places in the presenation layer
        /// </remarks>
        /// </summary>
        public bool HasShoppingCartItems { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user is active
        /// </summary>
        public bool Active { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has been deleted
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user account is system
        /// </summary>
        public bool IsSystemAccount { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the user add news
        /// </summary>
        public bool IsNewsItem { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether the user has a orders
        /// </summary>
        public bool IsHasOrders { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a blog comments
        /// </summary>
        public bool IsHasBlogComments { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a product review
        /// </summary>
        public bool IsHasProductReview { get; set; }
        
        /// <summary>
        /// Gets or sets a value indicating whether the user has a product review help
        /// </summary>
        public bool IsHasProductReviewH { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a Vendor review
        /// </summary>
        public bool IsHasVendorReview { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a Vendor review help
        /// </summary>
        public bool IsHasVendorReviewH { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a pool voting
        /// </summary>
        public bool IsHasPoolVoting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a forum post
        /// </summary>
        public bool IsHasForumPost { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the user has a forum topic
        /// </summary>
        public bool IsHasForumTopic { get; set; }

        /// <summary>
        /// Gets or sets a value indicating number of failed login attempts (wrong password)
        /// </summary>
        public int FailedLoginAttempts { get; set; }

        /// <summary>
        /// Gets or sets the date and time until which a user cannot login (locked out)
        /// </summary>
        public DateTime? CannotLoginUntilDateUtc { get; set; }
        /// <summary>
        /// Gets or sets the user system name
        /// </summary>
        public string SystemName { get; set; }

        /// <summary>
        /// Gets or sets the last IP address
        /// </summary>
        public string LastIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the date and time of entity creation
        /// </summary>
        public DateTime CreatedOnUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last login
        /// </summary>
        public DateTime? LastLoginDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last activity
        /// </summary>
        public DateTime LastActivityDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last purchase
        /// </summary>
        public DateTime? LastPurchaseDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last update cart
        /// </summary>
        public DateTime? LastUpdateCartDateUtc { get; set; }

        /// <summary>
        /// Gets or sets the date and time of last update wishlist
        /// </summary>
        public DateTime? LastUpdateWishListDateUtc { get; set; }

        /// <summary>
        /// Last date to change password
        /// </summary>
        public DateTime? PasswordChangeDateUtc { get; set; }

        #region Navigation properties

        /// <summary>
        /// Gets or sets the user roles
        /// </summary>
        public virtual ICollection<UserRole> UserRoles
        {
            get { return _userRoles ?? (_userRoles = new List<UserRole>()); }
            protected set { _userRoles = value; }
        }

        /// <summary>
        /// Gets or sets shopping cart items
        /// </summary>
        public virtual ICollection<ShoppingCartItem> ShoppingCartItems
        {
            get { return _shoppingCartItems ?? (_shoppingCartItems = new List<ShoppingCartItem>()); }
            protected set { _shoppingCartItems = value; }            
        }

        /// <summary>
        /// Default billing address
        /// </summary>
        public virtual Address BillingAddress { get; set; }

        /// <summary>
        /// Default shipping address
        /// </summary>
        public virtual Address ShippingAddress { get; set; }

        /// <summary>
        /// Gets or sets user addresses
        /// </summary>
        public virtual ICollection<Address> Addresses
        {
            get { return _addresses ?? (_addresses = new List<Address>()); }
            protected set { _addresses = value; }            
        }

        /// <summary>
        /// Gets or sets the user tags
        /// </summary>
        public virtual ICollection<string> UserTags
        {
            get { return _userTags ?? (_userTags = new List<string>()); }
            protected set { _userTags = value; }
        }
        #endregion
    }
}