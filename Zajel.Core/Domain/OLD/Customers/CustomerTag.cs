
namespace Zajel.Core.Domain.Users
{
    /// <summary>
    /// Represents a product tag
    /// </summary>
    public partial class UserTag : BaseEntity
    {
        /// <summary>
        /// Gets or sets the name
        /// </summary>
        public string Name { get; set; }
    }
}
