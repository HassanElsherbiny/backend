﻿
namespace Zajel.Core.Domain.Users
{
    public partial class UserByTimeReportLine
    {
        public string Time { get; set; }

        public int Registered { get; set; }

    }
}
