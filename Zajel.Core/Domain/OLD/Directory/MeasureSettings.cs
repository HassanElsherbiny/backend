﻿
using Zajel.Core.Configuration;

namespace Zajel.Core.Domain.Directory
{
    public class MeasureSettings : ISettings
    {
        public string BaseDimensionId { get; set; }
        public string BaseWeightId { get; set; }
    }
}