﻿using Zajel.Core.Configuration;
using Zajel.Core.Domain.Catalog;
using Zajel.Core.Domain.Common;
using Zajel.Core.Domain.Users;
using Zajel.Core.Domain.Directory;
using Zajel.Core.Domain.Discounts;
using Zajel.Core.Domain.Forums;
using Zajel.Core.Domain.Logging;
using Zajel.Core.Domain.Media;
using Zajel.Core.Domain.Messages;
using Zajel.Core.Domain.Orders;
using Zajel.Core.Domain.Shipping;
using Zajel.Core.Domain.Vendors;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;

namespace Zajel.Core.Infrastructure.MongoDB
{
    public class MongoDBMapperConfiguration
    {

        /// <summary>
        /// Register MongoDB mappings
        /// </summary>
        /// <param name="config">Config</param>
        public static void RegisterMongoDBMappings(ZajelConfig config)
        {
            BsonSerializer.RegisterSerializer(typeof(decimal), new DecimalSerializer(BsonType.Double));
            BsonSerializer.RegisterSerializer(typeof(decimal?), new NullableSerializer<decimal>(new DecimalSerializer(BsonType.Double)));

            //global set an equivalent of [BsonIgnoreExtraElements] for every Domain Model
            var cp = new ConventionPack();
            cp.Add(new IgnoreExtraElementsConvention(true));
            ConventionRegistry.Register("ApplicationConventions", cp, t => true);

            BsonClassMap.RegisterClassMap<Product>(cm =>
            {
                cm.AutoMap();

                //ignore these Fields, an equivalent of [BsonIgnore]
                cm.UnmapMember(c => c.ProductType);
                cm.UnmapMember(c => c.BackorderMode);
                cm.UnmapMember(c => c.DownloadActivationType);
                cm.UnmapMember(c => c.GiftCardType);
                cm.UnmapMember(c => c.LowStockActivity);
                cm.UnmapMember(c => c.ManageInventoryMethod);
                cm.UnmapMember(c => c.RecurringCyclePeriod);
                cm.UnmapMember(c => c.RentalPricePeriod);
            });

            BsonClassMap.RegisterClassMap<ProductAttributeCombination>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<ProductAttributeMapping>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
                cm.UnmapMember(c => c.AttributeControlType);
            });

            BsonClassMap.RegisterClassMap<ProductAttributeValue>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductAttributeMappingId);
                cm.UnmapMember(c => c.ProductId);
                cm.UnmapMember(c => c.AttributeValueType);
            });

            BsonClassMap.RegisterClassMap<ProductCategory>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<ProductManufacturer>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<ProductPicture>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<ProductSpecificationAttribute>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
                cm.UnmapMember(c => c.AttributeType);
            });

            BsonClassMap.RegisterClassMap<ProductTag>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<ProductWarehouseInventory>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<RelatedProduct>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId1);
            });

            BsonClassMap.RegisterClassMap<TierPrice>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ProductId);
            });

            BsonClassMap.RegisterClassMap<Address>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.UserId);
            });

            BsonClassMap.RegisterClassMap<User>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.PasswordFormat);
            });

            BsonClassMap.RegisterClassMap<UserAction>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.Condition);
                cm.UnmapMember(c => c.ReactionType);
            });

            BsonClassMap.RegisterClassMap<UserAction.ActionCondition>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.UserActionConditionType);
                cm.UnmapMember(c => c.Condition);
            });

            BsonClassMap.RegisterClassMap<UserAttribute>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.AttributeControlType);
            });

            BsonClassMap.RegisterClassMap<UserHistoryPassword>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.PasswordFormat);
            });

            BsonClassMap.RegisterClassMap<UserReminder>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.Condition);
                cm.UnmapMember(c => c.ReminderRule);
            });

            BsonClassMap.RegisterClassMap<UserReminder.ReminderCondition>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ConditionType);
                cm.UnmapMember(c => c.Condition);
            });

            BsonClassMap.RegisterClassMap<UserReminderHistory>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ReminderRule);
                cm.UnmapMember(c => c.HistoryStatus);
            });

            BsonClassMap.RegisterClassMap<UserRole>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.UserId);
            });

            BsonClassMap.RegisterClassMap<Discount>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.DiscountType);
                cm.UnmapMember(c => c.DiscountLimitation);
            });

            BsonClassMap.RegisterClassMap<ForumTopic>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ForumTopicType);
            });

            BsonClassMap.RegisterClassMap<Log>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.LogLevel);
            });

            BsonClassMap.RegisterClassMap<Download>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.DownloadBinary);
            });

            BsonClassMap.RegisterClassMap<Campaign>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.UserHasOrdersCondition);
                cm.UnmapMember(c => c.UserHasShoppingCartCondition);
            });

            BsonClassMap.RegisterClassMap<EmailAccount>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.FriendlyName);
            });

            BsonClassMap.RegisterClassMap<InteractiveForm.FormAttribute>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.AttributeControlType);
            });

            BsonClassMap.RegisterClassMap<MessageTemplate>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.DelayPeriod);
            });

            BsonClassMap.RegisterClassMap<QueuedEmail>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.Priority);
            });

            BsonClassMap.RegisterClassMap<CheckoutAttribute>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.AttributeControlType);
            });

            BsonClassMap.RegisterClassMap<GiftCard>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.GiftCardType);
            });

            BsonClassMap.RegisterClassMap<Order>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.OrderStatus);
                cm.UnmapMember(c => c.PaymentStatus);
                cm.UnmapMember(c => c.ShippingStatus);
                cm.UnmapMember(c => c.UserTaxDisplayType);
                cm.UnmapMember(c => c.TaxRatesDictionary);
            });

            BsonClassMap.RegisterClassMap<ShipmentItem>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.ShipmentId);
            });

            BsonClassMap.RegisterClassMap<VendorNote>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.VendorId);
            });

            BsonClassMap.RegisterClassMap<Currency>(cm =>
            {
                cm.AutoMap();
                cm.UnmapMember(c => c.RoundingType);
            });
        }
    }
}
